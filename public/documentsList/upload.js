let load = 0
let button = document.getElementsByTagName('button')

let upload = () => {
    if (load >= 100) {
        clearInterval(proces)
        p_i.innerHTML = '100%' + ' ' + 'Upload completed'
        button[0].classList.remove('active')
    } else {
        load++
        progress.value = load
        p_i.innerHTML = load + '%' + ' ' + 'Upload '
        button[1].onclick = e => {
            e.preventDefault()
            clearInterval(proces)
            document.querySelector('.pr').style.display = 'none'
            button[1].style.visibility = 'hidden'
            button[0].classList.remove('active')
        }
    }

}


function uploadFile (event) {
    let action = $('#uploadDocument').attr('action')
    //--- my inputs
    event.preventDefault()

    var _token = document.querySelector('meta[name="csrf-token"]').getAttribute('content')
    var document_id = document.getElementById('document_id').value
    // let p_i = document.querySelector('.progress-indicator')
    var file = $('#upload')[0].files[0]
    var form_data = new FormData();

    form_data.append('file', file)
    form_data.append('file_size', file['size'])
    form_data.append('document_id', document_id)
    form_data.append('_token', _token)
    $.ajax({

        xhr: function () {
            var xhr = new window.XMLHttpRequest()
            xhr.upload.addEventListener('progress', function (evt) {
                document.querySelector('.uploadFile').style.display = 'none';
                if (evt.lengthComputable) {
                    var percentComplete = evt.loaded / evt.total;
                    percentComplete = parseInt(percentComplete);
                    $('.progress-bar').css({
                        width: percentComplete * 100 + '%'
                    });
                    $('.progress-bar').text((percentComplete * 100) + '%');

                    // p_i.innerHTML = (percentComplete * 100) + '%' + ' ' + 'Upload ';


                    if (percentComplete === 1) {
                        document.querySelector('.progress-bar').style.backgroundColor='darkcyan';
                    }
                }
            }, true);
            return xhr;
        },

        url: action,
        type: 'POST',
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        dataType: 'text',  // <-- what to expect back from the PHP script, if anything
        cache: false,
        contentType: false,
        processData: false,
        data: form_data,
        beforeSend: function () {
            $('.progress-bar').css({
                width: 1 + '%'
            })
            $('.progress-bar').text((1) + '%')

        },
        success: function (data) {
            //-- console.log(data)
            document.querySelector('.uploadFile').style.display = 'block';
        },
        error: function () {
            console.log('IsErorr')
        }
    })

}

$(document).ready(function () {
    let uploadBtn = document.getElementById('uploadFile')
    uploadBtn.addEventListener('click', uploadFile)
})

/*


        let action = $('#uploadDocument').attr('action');
        //--- my inputs
        var file = $('#upload')[0].files[0];
        // var CSRF_TOKEN = document.querySelector('meta[name="csrf-token"]').getAttribute('content');
        var document_id = document.getElementById('document_id').value;
        console.log(action)
        $.ajax({
            url: action,
            type: 'POST',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {
                'file':file,
                'document_id': document_id
            },
            success: function (data) {
                console.log('success');
            },
            error: function () {
                console.log('IsErorr');
            }
        });

 */

/*
*            xhr.addEventListener("progress", function (evt) {
                if (evt.lengthComputable) {
                    var percentComplete = evt.loaded / evt.total;
                    console.log(percentComplete*100);
                    $('.progress-bar').css({
                        width: percentComplete * 100 + '%'
                    });
                }
            }, true);
*
*
*
*
*
*
*
*
*
*
*
*
*
*  */
