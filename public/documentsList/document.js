$(document).ready(function () {
  $('body').delegate('[data-target="#descriptionModal"]', 'click', function () {
    var content = $(this).attr('content');
    $('#descriptionContent').text(content);
    $('#RemoveModal').on('shown.bs.modal', function () {
      $('#myInput').trigger('focus');
    });
  });
});

$('body').delegate('#downloadFile', 'click', function (e) {
   var downloads_link=$(this).attr('path');
   window.location.href=downloads_link;
});
