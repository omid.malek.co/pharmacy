$(document).ready(function(){
  var elements=["#bottomRange","#topRange"]
  jQuery.each(elements,function(index,value){
    $(value).persianDatepicker({
      cellWidth: 60,
      cellHeight: 50,
      fontSize: 16,
      formatDate: "YYYY-0M-0D"
    });
  })
})