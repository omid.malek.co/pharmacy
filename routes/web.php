<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


//Auth::routes();
//
//Route::get('/', function () {
//    return view('welcome');
//});

use App\Http\Middleware\AccessAdmin;
use App\Http\Middleware\isValideCustomer;

//
Route::prefix('admin')->group(function () {
    Route::resources([
        'posts' => 'Admin\PostController',
        'settings' => 'Admin\AdminController'
    ]);

    Route::get('posts/switch/{post}', 'Admin\PostController@switch')->name('posts.switch');
    Route::get('posts/remove/{post}', 'Admin\PostController@destroy')->name('posts.remove');
//
    Route::get('admin_password', 'Admin\PasswordController@index')->name('admin.password.index');
    Route::post('admin_password', 'Admin\PasswordController@update')->name('passwords.update');
    Route::get('admins_show', 'Admin\AdminController@edit')->name('admin.edit');
    Route::post('admins_update', 'Admin\AdminController@update')->name('admin.update');
    Route::get('logout', 'Admin\AdminController@logout')->name('admin.logout');
    Route::get('/panel', 'SignInController@LoginPage')->name('admin.panel');
    Route::post('/signin', 'SignInController@Signin')->name('admin.signin');
    Route::get('/home', 'Admin\AdminController@index')->name('home');

    // images request routings

    Route::get('imagesInsert/{used}', 'ImagesController@index')->name('imagesInsert');
    Route::get('InsertIndexSlider', 'ImagesController@imageIndexSlider')->name('InsertSliderIndex');
    Route::post('Storeindex', 'ImagesController@storeSliderIndex')->name('StoreSliderIndex');
    Route::post('Storeimage', 'ImagesController@store')->name('StoreImage');

    Route::get('edit_articles_icon/{image}', 'ImagesController@editArticlesIcon')->name('edit.articles.icon');
    Route::put('update_article_images/{image}', 'ImagesController@updateArticlesIcon')->name('update.articles.icon')->middleware(['auth', 'admin']);

    Route::get('RemoveImage/{id}', 'ImagesController@destroy')->name('image.remove');
    Route::resource('images', ImagesController::class)->except(['index', 'store', 'destroy']);
    Route::post('ComboCategorie', 'CategoriesController@GetCategoriesController')->name('ComboCategories');
    Route::post('Storeimage', 'ImagesController@store')->name('StoreImage');

    Route::put('products_update/{product_id}', 'ProductsManagement@update')->name('admin.products.update')->middleware(['auth', 'admin']);
    Route::get('products_edit/{product_id}', 'ProductsManagement@edit')->name('admin.products.edit')->middleware(['auth', 'admin']);
    Route::get('products_management_list', 'ProductsManagement@index')->name('admin.products.index')->middleware(['auth', 'admin']);
    Route::get('products_create', 'ProductsManagement@create')->name('admin.products.create')->middleware(['auth', 'admin']);
    Route::post('products_store', 'ProductsManagement@store')->name('admin.products.store')->middleware(['auth', 'admin']);
    Route::get('products_image_edit/{image}', 'ProductsManagement@editImage')->name('admin.products.image')->middleware(['auth', 'admin']);
    Route::put('products_images/{image}', 'ProductsManagement@updateImage')->name('admin.Productsimage.update')->middleware(['auth', 'admin']);
    Route::get('remove_product/{product_id}', 'ProductsManagement@destroy')->name('admin.image.remove')->middleware(['auth', 'admin']);
    Route::get('options/{product_id}', 'optionManagement@index')->name('admin.options.index')->middleware(['auth', 'admin']);
    Route::get('options/create/{product_id}', 'optionManagement@create')->name('admin.options.create')->middleware(['auth', 'admin']);
    Route::post('options/store', 'optionManagement@store')->name('admin.options.store')->middleware(['auth', 'admin']);
    Route::get('options/edit/{option_id}', 'optionManagement@edit')->name('admin.options.edit')->middleware(['auth', 'admin']);
    Route::put('options/edit/{option}', 'optionManagement@update')->name('admin.options.update')->middleware(['auth', 'admin']);
    /*******************images management operation*******************/

    Route::get('images/{image}', 'ImagesManagement@edit')->name('admin.image.edit')->middleware(['auth', 'admin']);
    Route::put('images/{image}', 'ImagesManagement@update')->name('admin.image.update')->middleware(['auth', 'admin']);


    Route::get('factors_list', 'FactorsManagement@index')->name('factors.management.index');
    Route::get('details_factor/{payment_id}', 'FactorsManagement@show')->name('factors.management.show');
    Route::get('sent_product/{payment}', 'FactorsManagement@sentProduct')->name('sent.product');

    Route::get('destroy_discount/{discount_id}', 'DiscountManagement@destroy')
        ->name('admin.discount.destroy');
    Route::resource('discounts', 'DiscountManagement')->only([
        'index', 'create', 'store'
    ])->middleware(['auth', 'admin']);
    //--
    Route::get('remove_drug/{id}','Admin\drugsController@destroy')->name('remove.drugs');
    Route::resource('drugs', 'Admin\drugsController')->only([
        'index','create','store','edit','update'
    ]);
    //--
    Route::get('drugsExcel', 'ExcelController@drugs')->name('excel.drugs.show');
    //--
    Route::get('personnels_destroy/{user_id}', 'Admin\personnelsController@destroy')
        ->name('personnels.destroy');
    //--
    Route::resource('personnels', 'Admin\personnelsController')->only([
        'index','create','store','edit','update'
    ]);
    //--
    Route::resource('items', 'Admin\itemsController')->only([
        'index','create','store','edit','update'
    ]);
    //--
    Route::get('forgotten_password','SignInController@forgottenPassword')->name('admin.forgotten.password');
    Route::post('reset_password','SignInController@resetPassword')->name('admin.reset.password');
    //--
    Route::get( 'edit_document_file/{document_id}', 'documentsController@editDocumentFile')->name('documents.edit.file');
    Route::post( 'update_document_file', 'documentsController@docsUploader')->name('documents.update.file');
    Route::get( 'download_document/{document_id}', 'documentsController@downloadFile')->name('documents.download.file');
    Route::get( 'show_documents_for_personnel', 'documentsController@showDocs')->name('documents.documents.personnels');
    Route::get('list_documents/{user_id}', 'documentsController@index')->name('documents.index');
    Route::get('removeDocuments/{id}', 'documentsController@destroy')->name('documents.remove');
    Route::resource('documents', 'documentsController')->only([
       'create','store','edit','update'
    ]);
    Route::post( 'destroy_access', 'Admin\accessController@destroy')->name('access.remover');
    Route::resource('access','Admin\accessController')->only([
        'index','edit','store'])->middleware(AccessAdmin::class);
    Route::get('entries_present','Admin\entriesController@present')
        ->name('entries.present')->middleware(IsValideCustomer::class);
    Route::get('leaving_the_workplace','Admin\entriesController@leaving')
        ->name('leaving.workplace')->middleware(IsValideCustomer::class);
    //------------
    Route::resource('entries','Admin\entriesController')->only([
        'index','destroy'])->middleware(IsValideCustomer::class);
    //-----------
    Route::post('managementEntries_search','Admin\AdminEntriesController@searchEntries')
        ->name('managementEntries.search')->middleware(IsValideCustomer::class);
    Route::get('managementEntries_result/{keyword}','Admin\AdminEntriesController@SearchResult')
        ->name('result.search.managementEntries');

    //-----------
    Route::get('managementEntries_confirm/{id}','Admin\AdminEntriesController@confirm')
        ->name('managementEntries.confirm')->middleware(IsValideCustomer::class);
    //-----------
    Route::resource('managementEntries','Admin\AdminEntriesController')->only([
        'index','destroy'])->middleware(IsValideCustomer::class);
});

Route::get( 'remove_document_file', 'systemController@removeFile')->name('remove.documents.file');
Route::get('/', 'HomeController@index')->name('home.index');
Route::get('درباره_ما', 'otherPagesController@about')->name('about.us');
Route::get('تماس_با_ما', 'otherPagesController@contact')->name('contact.us');

Route::get('/logout', 'CustomersController@logOut')->name('customer.logOut');
Route::get('/login', 'CustomersController@loginPage')->name('customer.login');
Route::post('/login/submit', 'CustomersController@validation')->name('customer.validation');

Route::get('/register', 'CustomersController@registerPage')->name('customer.register');
Route::post('/register_store', 'CustomersController@storeCustomer')->name('store.customer');

Route::get('details_products/{product}', 'ProductsController@show')->name('products.show');
Route::get('get_products/{categorie_id?}', 'ProductsController@getProducts')->name('select.products');

Route::post('UpdateProduct', 'FactorsController@update')->name('factors.update');
Route::get('addProductTobasket/{product_id}', 'FactorsController@store')->name('factors.store');
Route::get('removeFromBasket/{factor_id}', 'FactorsController@destroy')->name('factors.destroy');
Route::get('نمایش_سبد_خرید', 'FactorsController@show')->name('factors.show');

Route::get('set_address_reciever/{payment_id}', 'PaymentsController@show')->name('factor.set.address');
Route::post('update_address_reciever', 'PaymentsController@update')->name('factor.update.address');
Route::get('/blog', 'ArticlesController@index')->name('articles.index');
Route::get('/blog/{post_id}', 'ArticlesController@show')->name('articles.show');

Route::get('account_information', 'HomeController@AcountInformation')->name('account.show');
Route::get('مقررات', 'HomeController@regulations')->name('regulations.show');
Route::post('play_discount', 'DiscountController@edit')->name('discount.play');
Route::get('payment/{payment_id}', 'ZarinPalController@ZarinPalPayment')->name('payment.zarinpal');
Route::get('receipt_show/{reference}', 'receiptController@show')->name('receipt.show');

Route::resource('wallets', 'WalletsController')
    ->only(['index', 'store']);


Route::post('gift_apply','giftController@InsertToPayment')->name('gift.to.payments');
Route::resource('gifts','giftController')->only([
    'index', 'store','create','destroy']);



//Route::resource('gifts','')

//Route::get('')


//Auth::routes();

//Route::get('/home', 'HomeController@index')->name('home');
