/* eslint-disable semi */
const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

// mix.js('resources/js/app.js', 'public/js')
//   .sass('resources/sass/app.scss', 'public/css').options({
//     processCssUrls: false
//   });
//
mix.js('resources/js/AdminApp.js', 'public/js')
  .sass('resources/sass/admin.scss', 'public/css').options({
    processCssUrls: false
  }).version();

mix.scripts([
  'node_modules/jquery/dist/jquery.js',
  'resources/js/AdminScripts.js'
], 'public/js/alladmin.js').options({
    processCssUrls: false
});

mix.js('resources/js/app.js', 'public/js')
  .sass('resources/sass/app.scss', 'public/css')
  .copy('node_modules/@fortawesome/fontawesome-free/webfonts', 'public/fonts/vendor/@fortawesome/fontawesome-free')
  .options({
    processCssUrls: false
  });

// mix.js('resources/js/app.js', 'public/js')
//   .sass('resources/sass/login.scss', 'public/css/login.css')
//   .copy('node_modules/@fortawesome/fontawesome-free/webfonts', 'public/fonts/vendor/@fortawesome/fontawesome-free')
//   .options({
//     processCssUrls: false
//   });