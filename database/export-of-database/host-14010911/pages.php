<?php
/**
 * Export to PHP Array plugin for PHPMyAdmin
 * @version 4.9.7
 */

/**
 * Database `drmalekp_db`
 */

/* `drmalekp_db`.`pages` */
$pages = array(
  array('id' => '1','title' => 'کسری دارو','address' => 'drugs','route_name' => 'admin.drugs.index','base_url' => 'null','is_base_page' => '1','permission_id' => '1','permission_title' => 'مدیر سامانه','active' => '1','created_at' => '2022-05-10 22:37:37','updated_at' => '2022-05-10 22:37:46'),
  array('id' => '2','title' => 'تغییر رمز','address' => 'admin_password','route_name' => 'admin.password.index','base_url' => 'null','is_base_page' => '1','permission_id' => '1','permission_title' => 'مدیر سامانه','active' => '1','created_at' => '2022-05-08 00:14:33','updated_at' => '2022-05-08 00:14:33'),
  array('id' => '3','title' => 'اطلاعات مدیریت','address' => 'admins_show','route_name' => 'admin.edit','base_url' => 'null','is_base_page' => '1','permission_id' => '1','permission_title' => 'مدیر سامانه','active' => '1','created_at' => '2022-05-08 00:14:33','updated_at' => '2022-05-08 00:14:33'),
  array('id' => '4','title' => 'محصولات','address' => 'products_management_list','route_name' => 'admin.products.index','base_url' => 'null','is_base_page' => '1','permission_id' => '1','permission_title' => 'مدیر سامانه','active' => '1','created_at' => '2022-05-08 00:14:33','updated_at' => '2022-05-08 00:14:33'),
  array('id' => '5','title' => 'افزودن محصول','address' => 'products_create','route_name' => 'admin.products.create','base_url' => 'null','is_base_page' => '0','permission_id' => '1','permission_title' => 'مدیر سامانه','active' => '1','created_at' => '2022-05-08 00:14:33','updated_at' => '2022-05-08 00:14:33'),
  array('id' => '6','title' => 'فاکتورها','address' => 'factors_list','route_name' => 'factors.management.index','base_url' => 'null','is_base_page' => '1','permission_id' => '1','permission_title' => 'مدیر سامانه','active' => '1','created_at' => '2022-05-08 00:14:33','updated_at' => '2022-05-08 00:14:33'),
  array('id' => '7','title' => 'کسری دارو','address' => 'drugs','route_name' => 'admin.drugs.index','base_url' => 'null','is_base_page' => '1','permission_id' => '2','permission_title' => 'پرسنل','active' => '1','created_at' => '2022-05-10 22:32:38','updated_at' => '2022-05-10 22:32:38'),
  array('id' => '8','title' => 'محصولات','address' => 'products_management_list','route_name' => 'admin.products.index','base_url' => 'null','is_base_page' => '1','permission_id' => '2','permission_title' => 'پرسنل','active' => '1','created_at' => '2022-05-08 00:14:33','updated_at' => '2022-05-08 00:14:33'),
  array('id' => '9','title' => 'افزودن محصول','address' => 'products_create','route_name' => 'admin.products.create','base_url' => 'null','is_base_page' => '0','permission_id' => '2','permission_title' => 'پرسنل','active' => '1','created_at' => '2022-05-08 00:14:33','updated_at' => '2022-05-08 00:14:33'),
  array('id' => '10','title' => 'فاکتورها','address' => 'factors_list','route_name' => 'factors.management.index','base_url' => 'null','is_base_page' => '1','permission_id' => '2','permission_title' => 'پرسنل','active' => '1','created_at' => '2022-05-10 22:37:15','updated_at' => '2022-05-10 22:37:15'),
  array('id' => '11','title' => 'پرسنل','address' => 'items','route_name' => 'items.index','base_url' => 'null','is_base_page' => '1','permission_id' => '1','permission_title' => 'مدیر سامانه','active' => '1','created_at' => '2022-05-08 00:14:33','updated_at' => '2022-05-08 00:14:33'),
  array('id' => '12','title' => 'تغییر رمز','address' => 'admin_password','route_name' => 'admin.password.index','base_url' => 'null','is_base_page' => '1','permission_id' => '2','permission_title' => 'پرسنل','active' => '1','created_at' => '2022-05-08 00:14:33','updated_at' => '2022-05-08 00:14:33'),
  array('id' => '13','title' => 'مستندات','address' => 'list_documents','route_name' => 'documents.index','base_url' => 'null','is_base_page' => '0','permission_id' => '1','permission_title' => 'مدیر سامانه','active' => '1','created_at' => '2022-05-08 00:14:33','updated_at' => '2022-05-08 00:14:33'),
  array('id' => '14','title' => 'افزودن مستندات','address' => 'documents/create','route_name' => 'documents.create','base_url' => 'null','is_base_page' => '0','permission_id' => '1','permission_title' => 'مدیر سامانه','active' => '1','created_at' => '2022-05-08 00:14:33','updated_at' => '2022-05-08 00:14:33'),
  array('id' => '15','title' => 'مستندات','address' => 'show_documents_for_personnel','route_name' => 'documents.documents.personnels','base_url' => 'null','is_base_page' => '1','permission_id' => '2','permission_title' => 'پرسنل','active' => '1','created_at' => '2022-05-08 00:14:33','updated_at' => '2022-05-08 00:14:33'),
  array('id' => '16','title' => 'افزودن مستندات','address' => 'documents/create','route_name' => 'documents.create','base_url' => 'null','is_base_page' => '0','permission_id' => '2','permission_title' => 'پرسنل','active' => '1','created_at' => '2022-05-08 00:14:33','updated_at' => '2022-05-08 00:14:33')
);
