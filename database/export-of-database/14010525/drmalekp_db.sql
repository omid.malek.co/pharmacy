-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 16, 2022 at 09:43 PM
-- Server version: 10.4.16-MariaDB
-- PHP Version: 7.4.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `drmalekp_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(100) NOT NULL COMMENT 'عنوان مجموعه',
  `active` tinyint(1) NOT NULL DEFAULT 1 COMMENT 'وضعیت نمایش و یا عدم نمایش نمایش نظر',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `title`, `active`, `created_at`, `updated_at`) VALUES
(1, 'آرایشی بهداشتی', 1, '2020-01-17 05:16:33', '2020-01-17 05:16:33'),
(2, 'مکمل غذایی', 1, '2020-01-17 05:16:33', '2020-01-17 05:16:33'),
(3, 'مکمل ورزشی', 1, '2020-01-17 05:16:33', '2020-01-17 05:16:33'),
(4, 'مادر و کودک', 1, '2020-01-17 05:16:33', '2020-01-17 05:16:33'),
(5, 'تجهیزات پزشکی', 1, '2020-01-17 05:16:33', '2020-01-17 05:16:33');

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `conditions`
--

CREATE TABLE `conditions` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(80) NOT NULL COMMENT 'عنوان',
  `key_word` varchar(40) NOT NULL COMMENT 'کلمه کلیدی نوع',
  `active` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `conditions`
--

INSERT INTO `conditions` (`id`, `title`, `key_word`, `active`, `created_at`, `updated_at`) VALUES
(1, 'دیده نشده', 'Unseen', 1, '2020-08-09 11:38:33', '2020-08-09 11:38:33'),
(2, 'استفاده نشده', 'notUsed', 1, '2020-09-06 03:45:35', '2020-09-06 03:45:35'),
(3, 'ارسال نشده', 'notSent', 1, '2020-09-06 03:50:19', '2020-09-06 03:50:19'),
(4, 'دیده شده', 'seen', 1, '2020-08-09 11:36:02', '2020-08-09 11:36:02'),
(5, 'استفاده شده', 'used', 1, '2020-09-06 03:45:35', '2020-09-06 03:45:35'),
(6, 'ارسال شده', 'Sent', 1, '2020-09-06 03:50:19', '2020-09-06 03:50:19');

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE `customers` (
  `id` int(10) UNSIGNED NOT NULL,
  `fullname` varchar(150) NOT NULL DEFAULT 'null',
  `email` varchar(250) NOT NULL COMMENT 'ایمیل کاربر',
  `password` varchar(255) NOT NULL COMMENT 'کلمه عبور',
  `address` varchar(600) NOT NULL DEFAULT 'null' COMMENT 'آدرس',
  `postal_code` varchar(40) NOT NULL DEFAULT 'null' COMMENT 'کد پستی',
  `phone` varchar(16) NOT NULL DEFAULT '0' COMMENT 'شماره تماس',
  `token` varchar(100) NOT NULL DEFAULT 'null' COMMENT ' کد توکن',
  `active` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`id`, `fullname`, `email`, `password`, `address`, `postal_code`, `phone`, `token`, `active`, `created_at`, `updated_at`) VALUES
(1, 'امید مالک', 'omid.malek.1990@gmail.com', '25f9e794323b453885f5181f1b624d0b', 'بلوار معلم، نبش معلم 16، پلاک 36', '9189171111', '09365647585', 'R6sKMKFwX482LFLZRjSeUKvGdA1INDftbZ9VtjetVUr1TKL7S3OvOLsdXlmN', 1, '2020-02-15 14:02:42', '2021-03-23 14:45:10'),
(2, 'null', 'test@gmail.com', '25f9e794323b453885f5181f1b624d0b', 'null', 'null', '09354612685', '55JaaHmXAZc8jkZIJbIPgbFE2o3VaHkMQX9Cjr9shUBZGwFRkiDqLEvkRRIM', 1, '2020-09-09 17:32:51', '2020-09-09 17:33:04');

-- --------------------------------------------------------

--
-- Table structure for table `discounts`
--

CREATE TABLE `discounts` (
  `id` int(10) UNSIGNED NOT NULL,
  `text` varchar(30) NOT NULL DEFAULT 'null' COMMENT 'کد تخفیف',
  `percent` double(8,2) NOT NULL DEFAULT 0.00 COMMENT 'میران درصد تخفیف',
  `start_date` date NOT NULL COMMENT 'تاریخ شروع اعمال تخفیف',
  `expire_date` date NOT NULL COMMENT 'تاریخ پایان اعمال تخفیف',
  `number_validation` int(11) NOT NULL DEFAULT 1,
  `active` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `discounts`
--

INSERT INTO `discounts` (`id`, `text`, `percent`, `start_date`, `expire_date`, `number_validation`, `active`, `created_at`, `updated_at`) VALUES
(2, 'takhfif', 10.00, '2020-09-08', '2020-09-12', 1, 1, '2020-07-16 18:51:09', '2020-08-07 15:16:50');

-- --------------------------------------------------------

--
-- Table structure for table `documents`
--

CREATE TABLE `documents` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `src` varchar(100) NOT NULL,
  `icon` varchar(100) NOT NULL DEFAULT 'folder.png',
  `title` varchar(100) NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT 'کلید خارجی',
  `description` varchar(1000) NOT NULL DEFAULT 'null' COMMENT 'نام جدول',
  `keyword` varchar(80) NOT NULL DEFAULT 'null' COMMENT 'نام جدول',
  `active` tinyint(1) NOT NULL DEFAULT 1 COMMENT 'وضعیت نمایش و یا عدم نمایش نمایش نظر',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `documents`
--

INSERT INTO `documents` (`id`, `src`, `icon`, `title`, `user_id`, `description`, `keyword`, `active`, `created_at`, `updated_at`) VALUES
(6, 'documents_14_1659714743.jpg', 'folder.png', 'صفحه اول شناسنامه', 14, 'دارای توضیح', 'documents', 1, '2022-08-05 15:52:23', '2022-08-12 06:25:23'),
(8, 'documents_1_1659905001.jpg', 'folder.png', 'تست', 1, 'تست', 'documents', 1, '2022-08-05 19:30:40', '2022-08-08 04:34:45'),
(9, 'documents_1_1659932392.jpg', 'folder.png', 'کارت', 1, 'فاقد توضیحات', 'List', 1, '2022-08-08 04:19:52', '2022-08-08 04:19:52');

-- --------------------------------------------------------

--
-- Table structure for table `drugs`
--

CREATE TABLE `drugs` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(150) NOT NULL COMMENT 'عنوان کالا',
  `shape` varchar(200) NOT NULL COMMENT 'شکل دارو',
  `count` int(11) NOT NULL COMMENT 'تعداد',
  `scale_id` int(10) UNSIGNED DEFAULT NULL COMMENT 'کد معیار پذیرش',
  `description` text NOT NULL COMMENT 'توضیحات مربوط به دارو',
  `active` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `drugs`
--

INSERT INTO `drugs` (`id`, `name`, `shape`, `count`, `scale_id`, `description`, `active`, `created_at`, `updated_at`) VALUES
(2, 'استامینوفن کدئین', 'قرص', 10, 1, 'بدون توضیحات', 0, '2022-03-17 20:10:57', '2022-03-17 20:10:57'),
(3, 'استامینوفن کدئین', 'قرص', 3, 2, 'بدون توضیحات', 1, '2022-03-17 20:41:25', '2022-03-17 20:41:25');

-- --------------------------------------------------------

--
-- Table structure for table `factors`
--

CREATE TABLE `factors` (
  `id` int(10) UNSIGNED NOT NULL,
  `product_id` int(10) UNSIGNED DEFAULT NULL COMMENT 'کلید خارجی پست',
  `sent_number` int(11) NOT NULL COMMENT 'تعداد کالاهای ارسال شده ',
  `type_id` int(10) UNSIGNED DEFAULT 1 COMMENT 'نوع پرداخت',
  `customer_id` int(10) UNSIGNED DEFAULT NULL COMMENT 'کلید خارجی مشتری ها',
  `payment_id` varchar(40) NOT NULL DEFAULT '0' COMMENT 'مبلغ پرداخت شده',
  `active` tinyint(1) NOT NULL DEFAULT 1 COMMENT 'حذف منطقی شده=0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `factors`
--

INSERT INTO `factors` (`id`, `product_id`, `sent_number`, `type_id`, `customer_id`, `payment_id`, `active`, `created_at`, `updated_at`) VALUES
(1, 4, 1, 1, 1, '1', 1, '2020-05-13 12:19:14', '2020-05-13 12:19:14'),
(7, 6, 1, 1, 1, '1', 1, '2020-08-04 10:01:33', '2020-08-04 10:01:33'),
(10, 2, 1, 1, 1, '13', 1, '2020-09-07 05:53:21', '2020-09-07 05:53:21'),
(11, 1, 1, 1, 1, '14', 1, '2020-09-09 17:21:51', '2020-09-09 17:21:51'),
(12, 11, 1, 1, 1, '15', 1, '2021-02-17 14:04:49', '2021-02-17 14:04:49');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text NOT NULL,
  `queue` text NOT NULL,
  `payload` longtext NOT NULL,
  `exception` longtext NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `gifts`
--

CREATE TABLE `gifts` (
  `id` int(10) UNSIGNED NOT NULL,
  `text` varchar(30) NOT NULL DEFAULT 'null' COMMENT 'کد تخفیف',
  `amount` double NOT NULL DEFAULT 0 COMMENT 'میزان تخفیف',
  `wallet_id` int(10) UNSIGNED DEFAULT 0 COMMENT 'کد کارت هدیه',
  `used_by` int(11) NOT NULL DEFAULT 0 COMMENT 'استفاده شده توسط',
  `created_by` int(11) NOT NULL DEFAULT 0 COMMENT 'ایجاد شده توسط',
  `condition_id` int(10) UNSIGNED DEFAULT 1 COMMENT 'کلید وضعیت',
  `start_date` date NOT NULL COMMENT 'تاریخ شروع اعمال تخفیف',
  `expire_date` date NOT NULL COMMENT 'تاریخ پایان اعمال تخفیف',
  `active` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `gifts`
--

INSERT INTO `gifts` (`id`, `text`, `amount`, `wallet_id`, `used_by`, `created_by`, `condition_id`, `start_date`, `expire_date`, `active`, `created_at`, `updated_at`) VALUES
(3, '81425963', 12100, 1, 1, 1, 2, '2020-09-07', '2020-09-12', 1, '2020-09-06 09:54:09', '2020-09-09 12:07:51'),
(4, '34286195', 3000, 1, 1, 1, 2, '2020-09-07', '2020-09-14', 1, '2020-09-06 16:50:55', '2020-09-09 17:28:08');

-- --------------------------------------------------------

--
-- Table structure for table `images`
--

CREATE TABLE `images` (
  `id` int(10) UNSIGNED NOT NULL,
  `src` varchar(100) NOT NULL,
  `record_id` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT 'کلید خارجی',
  `used_in` varchar(20) NOT NULL DEFAULT 'null' COMMENT 'نام جدول',
  `active` tinyint(1) NOT NULL DEFAULT 1 COMMENT 'وضعیت نمایش و یا عدم نمایش نمایش نظر',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `images`
--

INSERT INTO `images` (`id`, `src`, `record_id`, `used_in`, `active`, `created_at`, `updated_at`) VALUES
(1, 'pharmacy_products_161122131882.jpg', 1, 'products', 0, '2020-01-17 05:17:28', '2021-03-25 09:05:40'),
(2, 'pharmacy_products_161122621021.jpg', 2, 'products', 0, '2020-01-17 05:17:28', '2021-03-23 14:46:53'),
(3, 'pharmacy_products_161122642566.jpg', 3, 'products', 0, '2020-01-17 05:17:28', '2021-03-23 14:49:15'),
(4, 'pharmacy_products_161122656575.jpg', 4, 'products', 0, '2020-01-17 05:17:28', '2021-03-25 09:06:10'),
(5, 'pharmacy_products_161122678249.jpg', 5, 'products', 0, '2020-01-17 05:17:28', '2021-03-25 09:05:34'),
(6, 'dart_3.jpg', 6, 'products', 1, '2020-01-17 05:17:28', '2020-01-17 05:17:28'),
(7, 'noPhoto.jpg', 1, 'categories', 1, '2020-01-17 05:17:56', '2020-01-17 05:17:56'),
(8, 'noPhoto.jpg', 2, 'categories', 1, '2020-01-17 05:17:56', '2020-01-17 05:17:56'),
(9, 'noPhoto.jpg', 3, 'categories', 1, '2020-01-17 05:17:56', '2020-01-17 05:17:56'),
(10, 'noPhoto.jpg', 4, 'categories', 1, '2020-01-17 05:17:56', '2020-01-17 05:17:56'),
(11, 'noPhoto.jpg', 5, 'categories', 1, '2020-01-17 05:17:56', '2020-01-17 05:17:56'),
(12, 'noPhoto.jpg', 6, 'categories', 1, '2020-01-17 05:17:56', '2020-01-17 05:17:56'),
(13, 'pharmacy_slider_1.jpg', 0, 'sliders', 1, '2020-01-17 05:17:56', '2020-01-17 05:17:56'),
(14, 'noPhoto.jpg', 2, 'articles', 1, '2020-06-09 14:44:39', '2020-06-09 14:44:39'),
(15, 'products_159251850134.jpg', 3, 'articles', 1, '2020-06-11 13:55:31', '2020-06-18 22:15:01'),
(16, 'products_159251848752.jpg', 4, 'articles', 1, '2020-06-14 14:48:12', '2020-06-18 22:14:47'),
(17, 'products_159251847372.jpg', 5, 'articles', 1, '2020-06-14 14:55:42', '2020-06-18 22:14:33'),
(18, 'products_159251828144.jpg', 6, 'articles', 1, '2020-06-14 14:56:49', '2020-06-18 22:11:21'),
(19, 'products_159251824434.jpg', 7, 'articles', 1, '2020-06-14 14:57:47', '2020-06-18 22:10:44'),
(20, 'noPhoto.jpg', 8, 'right.slider', 1, '2020-06-09 14:44:39', '2020-06-09 14:44:39'),
(21, 'pharmacy_products_161122115959.jpg', 7, 'products', 0, '2020-09-24 16:33:31', '2021-03-25 09:05:44'),
(22, 'products_160336288763.jpg', 8, 'products', 1, '2020-10-22 10:34:00', '2020-10-22 10:34:47'),
(23, 'pharmacy_products_161122037280.jpg', 9, 'products', 0, '2020-10-22 12:12:29', '2021-03-25 09:06:00'),
(24, 'pharmacy_products_161122095564.jpg', 10, 'products', 0, '2020-10-22 12:26:05', '2021-03-25 09:05:50'),
(25, 'pharmacy_products_161122070863.jpg', 11, 'products', 0, '2020-10-22 12:27:33', '2021-03-25 09:05:54'),
(26, 'products_160345761695.jpg', 12, 'products', 1, '2020-10-23 12:53:26', '2020-10-23 12:53:36'),
(27, 'products_160347352920.jpg', 13, 'products', 1, '2020-10-23 17:18:31', '2020-10-23 17:18:49'),
(28, 'pharmacy_products_161121998810.jpg', 14, 'products', 0, '2020-10-23 17:27:25', '2021-03-25 09:06:05'),
(29, 'pharmacy_slider_2.jpg', 0, 'sliders', 1, '2020-01-17 05:17:56', '2020-01-17 05:17:56'),
(30, 'pharmacy_products_161122746014.jpg', 15, 'products', 0, '2021-01-21 11:10:55', '2021-03-23 14:47:17'),
(31, 'pharmacy_products_161122761892.jpg', 16, 'products', 0, '2021-01-21 11:13:33', '2021-03-23 14:47:03'),
(32, 'pharmacy_products_161122781163.jpg', 17, 'products', 0, '2021-01-21 11:16:42', '2021-03-25 09:04:09'),
(33, 'pharmacy_products_161666345461.jpg', 18, 'products', 1, '2021-01-21 11:19:22', '2021-03-25 09:10:54'),
(34, 'noPhoto.jpg', 19, 'products', 0, '2021-03-03 05:26:04', '2021-03-25 09:05:03'),
(35, 'noPhoto.jpg', 20, 'products', 0, '2021-03-03 05:31:35', '2021-03-25 09:14:37'),
(36, 'noPhoto.jpg', 21, 'products', 0, '2021-03-03 05:34:12', '2021-03-25 09:05:13'),
(37, 'noPhoto.jpg', 22, 'products', 0, '2021-03-03 05:35:45', '2021-03-25 09:12:58'),
(38, 'noPhoto.jpg', 23, 'products', 0, '2021-03-03 05:37:25', '2021-03-25 09:05:29'),
(39, 'noPhoto.jpg', 24, 'products', 0, '2021-03-03 05:42:34', '2021-03-25 09:04:59'),
(40, 'noPhoto.jpg', 25, 'products', 0, '2021-03-03 05:44:44', '2021-03-25 09:14:40'),
(41, 'pharmacy_products_161651165835.jpg', 26, 'products', 1, '2021-03-23 15:00:53', '2021-03-23 15:00:58'),
(42, 'pharmacy_products_161666433720.jpg', 27, 'products', 1, '2021-03-25 09:25:33', '2021-03-25 09:25:37'),
(43, 'pharmacy_products_161666461639.jpg', 28, 'products', 1, '2021-03-25 09:28:51', '2021-03-25 09:30:16'),
(44, 'pharmacy_products_161666471756.jpg', 29, 'products', 1, '2021-03-25 09:31:53', '2021-03-25 09:31:57'),
(45, 'pharmacy_products_161666482057.jpg', 30, 'products', 1, '2021-03-25 09:33:25', '2021-03-25 09:33:40'),
(46, 'pharmacy_products_161666492793.jpg', 31, 'products', 1, '2021-03-25 09:34:57', '2021-03-25 09:35:27'),
(47, 'pharmacy_products_161666505225.jpg', 32, 'products', 1, '2021-03-25 09:36:58', '2021-03-25 09:37:32'),
(48, 'pharmacy_products_161666513652.jpg', 33, 'products', 1, '2021-03-25 09:38:52', '2021-03-25 09:38:56'),
(49, 'pharmacy_products_161666524258.jpg', 34, 'products', 1, '2021-03-25 09:40:39', '2021-03-25 09:40:42'),
(50, 'pharmacy_products_161666538630.jpg', 35, 'products', 1, '2021-03-25 09:42:55', '2021-03-25 09:43:06'),
(51, 'pharmacy_products_161666549611.jpg', 36, 'products', 1, '2021-03-25 09:44:34', '2021-03-25 09:44:56'),
(52, 'pharmacy_products_161666558827.jpg', 37, 'products', 1, '2021-03-25 09:46:25', '2021-03-25 09:46:28'),
(53, 'pharmacy_products_161666573687.jpg', 38, 'products', 1, '2021-03-25 09:48:40', '2021-03-25 09:48:56'),
(54, 'pharmacy_products_161666584239.jpg', 39, 'products', 1, '2021-03-25 09:50:18', '2021-03-25 09:50:42'),
(55, 'pharmacy_products_161709534926.jpg', 40, 'products', 1, '2021-03-30 09:08:08', '2021-03-30 09:09:09'),
(56, 'pharmacy_products_161709556863.jpg', 41, 'products', 1, '2021-03-30 09:12:39', '2021-03-30 09:12:48'),
(57, 'pharmacy_products_161709580760.jpg', 42, 'products', 1, '2021-03-30 09:16:41', '2021-03-30 09:16:47'),
(58, 'pharmacy_products_161709598510.png', 43, 'products', 1, '2021-03-30 09:19:35', '2021-03-30 09:19:45'),
(59, 'pharmacy_products_161709612271.jpg', 44, 'products', 1, '2021-03-30 09:21:56', '2021-03-30 09:22:02'),
(60, 'pharmacy_products_161709623977.jpg', 45, 'products', 1, '2021-03-30 09:23:54', '2021-03-30 09:23:59'),
(61, 'pharmacy_products_161709719562.jpg', 46, 'products', 1, '2021-03-30 09:39:46', '2021-03-30 09:39:55'),
(62, 'pharmacy_products_161709729778.jpg', 47, 'products', 1, '2021-03-30 09:41:14', '2021-03-30 09:41:37'),
(63, 'pharmacy_products_161709739462.jpg', 48, 'products', 1, '2021-03-30 09:43:04', '2021-03-30 09:43:14'),
(64, 'pharmacy_products_161709748046.jpg', 49, 'products', 1, '2021-03-30 09:44:34', '2021-03-30 09:44:40'),
(65, 'pharmacy_products_161709759268.jpg', 50, 'products', 1, '2021-03-30 09:46:23', '2021-03-30 09:46:32'),
(66, 'pharmacy_products_161709765645.jpg', 51, 'products', 1, '2021-03-30 09:47:30', '2021-03-30 09:47:36');

-- --------------------------------------------------------

--
-- Table structure for table `items`
--

CREATE TABLE `items` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `personnel_id` bigint(20) NOT NULL DEFAULT 0 COMMENT 'کلید کاربر در جدول پرسنل',
  `user_id` bigint(20) NOT NULL DEFAULT 0 COMMENT 'کلید کاربری',
  `page_id` bigint(20) NOT NULL DEFAULT 0 COMMENT 'کلید صفحه',
  `page_title` varchar(60) NOT NULL DEFAULT 'بدون عنوان' COMMENT 'عنوان صفحه',
  `route_name` varchar(100) NOT NULL DEFAULT 'null',
  `address` varchar(200) NOT NULL DEFAULT 'null' COMMENT 'آدرس نسبی صفحه',
  `parameters` varchar(250) DEFAULT 'null',
  `base_url` varchar(150) NOT NULL DEFAULT 'null' COMMENT 'آدرس سایت',
  `is_base_page` int(11) NOT NULL DEFAULT 1 COMMENT 'صفحه زیر مجموعه صفحه دیگری نیست؟',
  `child_id` bigint(20) NOT NULL DEFAULT 0 COMMENT 'کد آیتم زیر مجموعه این صفحه',
  `parent_id` bigint(20) NOT NULL DEFAULT 0 COMMENT 'کد آیتم والد این صفحه',
  `permission_id` bigint(20) NOT NULL DEFAULT 1 COMMENT 'کلید سطح دسترسی',
  `permission_title` varchar(40) NOT NULL DEFAULT '1' COMMENT 'عنوان سطح دسترسی',
  `active` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `items`
--

INSERT INTO `items` (`id`, `personnel_id`, `user_id`, `page_id`, `page_title`, `route_name`, `address`, `parameters`, `base_url`, `is_base_page`, `child_id`, `parent_id`, `permission_id`, `permission_title`, `active`, `created_at`, `updated_at`) VALUES
(1, 0, 1, 1, 'کسری دارو', 'drugs.index', 'drugs', 'null', 'null', 1, 0, 0, 1, 'مدیر سامانه', 1, '2022-05-08 20:17:29', '2022-05-08 20:17:29'),
(2, 0, 1, 2, 'تغییر  رمز', 'admin.password.index', 'admin_password', 'null', 'null', 1, 0, 0, 1, 'مدیر سامانه', 1, '2022-05-08 20:17:29', '2022-05-08 20:17:29'),
(3, 0, 1, 3, 'اطلاعات مدیریت', 'admin.edit', 'admins_show', 'null', 'null', 1, 0, 0, 1, 'مدیر سامانه', 1, '2022-05-08 20:17:29', '2022-05-08 20:17:29'),
(4, 0, 1, 4, 'محصولات', 'admin.products.index', 'products_management_list', 'null', 'null', 1, 0, 0, 1, 'مدیر سامانه', 1, '2022-05-08 20:17:29', '2022-05-08 20:17:29'),
(5, 0, 1, 5, 'افزودن محصول', 'admin.products.create', 'products_create', 'null', 'null', 0, 0, 0, 1, 'مدیر سامانه', 1, '2022-05-10 17:55:50', '2022-05-10 17:55:50'),
(6, 0, 1, 6, 'فاکتورها', 'factors.management.index', 'factors_list', 'null', 'null', 1, 0, 0, 1, 'مدیر سامانه', 1, '2022-05-10 17:58:18', '2022-05-10 17:58:18'),
(7, 0, 1, 11, 'پرسنل', 'items.index', 'items', 'null', 'null', 1, 0, 0, 1, 'مدیر سامانه', 1, '2022-05-08 20:17:29', '2022-05-08 20:17:29'),
(28, 0, 13, 7, 'کسری دارو', 'drugs.index', 'drugs', 'null', 'null', 1, 0, 0, 2, 'پرسنل', 1, '2022-06-17 19:44:39', '2022-06-17 19:44:39'),
(29, 0, 13, 8, 'محصولات', 'admin.products.index', 'products_management_list', 'null', 'null', 1, 0, 0, 2, 'پرسنل', 1, '2022-06-17 19:44:39', '2022-06-17 19:44:39'),
(30, 0, 13, 10, 'فاکتورها', 'factors.management.index', 'factors_list', 'null', 'null', 1, 0, 0, 2, 'پرسنل', 1, '2022-06-17 19:44:39', '2022-06-17 19:44:39'),
(31, 0, 13, 12, 'تغییر رمز', 'admin.password.index', 'admin_password', 'null', 'null', 1, 0, 0, 2, 'پرسنل', 1, '2022-06-17 19:44:39', '2022-06-17 19:44:39'),
(32, 0, 1, 13, 'مستندات', 'documents.index', 'list_documents', 'null', 'null', 0, 0, 7, 1, 'مدیر سامانه', 1, '2022-05-10 17:55:50', '2022-05-10 17:55:50'),
(33, 0, 1, 14, 'افزودن مستندات', 'documents.create', 'documents/create', 'null', 'null', 0, 0, 7, 1, 'مدیر سامانه', 1, '2022-05-10 17:55:50', '2022-05-10 17:55:50'),
(36, 0, 14, 7, 'کسری دارو', 'admin.drugs.index', 'drugs', 'null', 'null', 1, 0, 0, 2, 'پرسنل', 1, '2022-07-29 19:29:12', '2022-07-29 19:29:12'),
(37, 0, 14, 8, 'محصولات', 'admin.products.index', 'products_management_list', 'null', 'null', 1, 0, 0, 2, 'پرسنل', 1, '2022-07-29 19:29:12', '2022-07-29 19:29:12'),
(38, 0, 14, 10, 'فاکتورها', 'factors.management.index', 'factors_list', 'null', 'null', 1, 0, 0, 2, 'پرسنل', 1, '2022-07-29 19:29:12', '2022-07-29 19:29:12'),
(39, 0, 14, 12, 'تغییر رمز', 'admin.password.index', 'admin_password', 'null', 'null', 1, 0, 0, 2, 'پرسنل', 1, '2022-07-29 19:29:12', '2022-07-29 19:29:12'),
(40, 0, 14, 15, 'مستندات', 'documents.documents.personnels', 'show_documents_for_personnel', 'null', 'null', 1, 0, 0, 2, 'پرسنل', 1, '2022-07-29 19:29:12', '2022-07-29 19:29:12');

-- --------------------------------------------------------

--
-- Table structure for table `meta_tages`
--

CREATE TABLE `meta_tages` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_09_04_213034_create_payments_table', 1),
(4, '2019_12_29_094125_create_failed_jobs_table', 1),
(5, '2019_12_29_094502_create_posts_table', 1),
(6, '2020_01_01_104208_create_categories_table', 1),
(7, '2020_01_01_104258_create-comments_table', 1),
(8, '2020_01_01_104453_create_images_table', 1),
(9, '2020_01_01_111720_create_customers_table', 1),
(10, '2020_01_01_112025_create_factors_table', 1),
(11, '2020_01_01_122934_create_products_table', 1),
(12, '2020_01_02_090210_create_pages_table', 1),
(13, '2020_01_02_095457_create_meta_tages_table', 1),
(14, '2020_01_02_104053_create_discounts_table', 1),
(15, '2020_01_02_113401_create_relations_table', 1),
(16, '2020_04_18_173337_create_tokens_table', 1),
(17, '2020_07_16_081158_create_wallets_table', 1),
(18, '2020_07_17_222756_create_types_table', 1),
(19, '2020_08_31_001711_create_gifts_table', 1),
(20, '2020_08_31_011723_create_conditions_table', 1),
(21, '2021_01_29_014208_create_options_table', 1),
(22, '2022_03_13_204307_create_drugs_table', 2),
(23, '2022_03_13_210913_create_scales_table', 3),
(24, '2022_03_27_235311_create_pages_table', 4),
(25, '2022_03_27_224242_create_items_table', 5),
(26, '2022_03_27_224045_create_permissions_table', 6),
(29, '2022_03_21_133608_create_personnels_table', 7),
(33, '2022_06_28_000849_create_documents_table', 8);

-- --------------------------------------------------------

--
-- Table structure for table `options`
--

CREATE TABLE `options` (
  `id` int(10) UNSIGNED NOT NULL,
  `categorie_id` int(10) UNSIGNED DEFAULT NULL COMMENT 'کلید دسته بندی کالا',
  `product_id` int(10) UNSIGNED DEFAULT NULL COMMENT 'کلید کالا',
  `title` varchar(80) NOT NULL COMMENT 'عنوان ویژگی',
  `value` varchar(100) NOT NULL COMMENT 'مقدار ویژگی',
  `active` int(11) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `options`
--

INSERT INTO `options` (`id`, `categorie_id`, `product_id`, `title`, `value`, `active`, `created_at`, `updated_at`) VALUES
(1, 5, 18, 'وزن محصول', '500 گرم', 1, '2021-02-02 18:21:26', '2021-02-05 00:03:00'),
(2, 5, 18, 'تعداد', '50', 1, '2021-02-04 23:46:10', '2021-03-25 09:12:00');

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(40) NOT NULL DEFAULT 'بدون عنوان' COMMENT 'عنوان صفحه',
  `address` varchar(200) NOT NULL DEFAULT 'null' COMMENT 'آدرس نسبی صفحه',
  `route_name` varchar(100) NOT NULL DEFAULT 'null',
  `base_url` varchar(150) NOT NULL DEFAULT 'null' COMMENT 'آدرس سایت',
  `is_base_page` int(11) NOT NULL DEFAULT 1 COMMENT 'صفحه زیر مجموعه صفحه دیگری نیست؟',
  `permission_id` bigint(20) NOT NULL DEFAULT 0 COMMENT 'کلید سطح دسترسی',
  `permission_title` varchar(40) NOT NULL DEFAULT 'بدون عنوان' COMMENT 'عنوان سطح دسترسی',
  `active` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`id`, `title`, `address`, `route_name`, `base_url`, `is_base_page`, `permission_id`, `permission_title`, `active`, `created_at`, `updated_at`) VALUES
(1, 'کسری دارو', 'drugs', 'admin.drugs.index', 'null', 1, 1, 'مدیر سامانه', 1, '2022-05-10 18:07:37', '2022-05-10 18:07:46'),
(2, 'تغییر رمز', 'admin_password', 'admin.password.index', 'null', 1, 1, 'مدیر سامانه', 1, '2022-05-07 19:44:33', '2022-05-07 19:44:33'),
(3, 'اطلاعات مدیریت', 'admins_show', 'admin.edit', 'null', 1, 1, 'مدیر سامانه', 1, '2022-05-07 19:44:33', '2022-05-07 19:44:33'),
(4, 'محصولات', 'products_management_list', 'admin.products.index', 'null', 1, 1, 'مدیر سامانه', 1, '2022-05-07 19:44:33', '2022-05-07 19:44:33'),
(5, 'افزودن محصول', 'products_create', 'admin.products.create', 'null', 0, 1, 'مدیر سامانه', 1, '2022-05-07 19:44:33', '2022-05-07 19:44:33'),
(6, 'فاکتورها', 'factors_list', 'factors.management.index', 'null', 1, 1, 'مدیر سامانه', 1, '2022-05-07 19:44:33', '2022-05-07 19:44:33'),
(7, 'کسری دارو', 'drugs', 'admin.drugs.index', 'null', 1, 2, 'پرسنل', 1, '2022-05-10 18:02:38', '2022-05-10 18:02:38'),
(8, 'محصولات', 'products_management_list', 'admin.products.index', 'null', 1, 2, 'پرسنل', 1, '2022-05-07 19:44:33', '2022-05-07 19:44:33'),
(9, 'افزودن محصول', 'products_create', 'admin.products.create', 'null', 0, 2, 'پرسنل', 1, '2022-05-07 19:44:33', '2022-05-07 19:44:33'),
(10, 'فاکتورها', 'factors_list', 'factors.management.index', 'null', 1, 2, 'پرسنل', 1, '2022-05-10 18:07:15', '2022-05-10 18:07:15'),
(11, 'پرسنل', 'items', 'items.index', 'null', 1, 1, 'مدیر سامانه', 1, '2022-05-07 19:44:33', '2022-05-07 19:44:33'),
(12, 'تغییر رمز', 'admin_password', 'admin.password.index', 'null', 1, 2, 'پرسنل', 1, '2022-05-07 19:44:33', '2022-05-07 19:44:33'),
(13, 'مستندات', 'list_documents', 'documents.index', 'null', 0, 1, 'مدیر سامانه', 1, '2022-05-07 19:44:33', '2022-05-07 19:44:33'),
(14, 'افزودن مستندات', 'documents/create', 'documents.create', 'null', 0, 1, 'مدیر سامانه', 1, '2022-05-07 19:44:33', '2022-05-07 19:44:33'),
(15, 'مستندات', 'show_documents_for_personnel', 'documents.documents.personnels', 'null', 1, 2, 'پرسنل', 1, '2022-05-07 19:44:33', '2022-05-07 19:44:33'),
(16, 'افزودن مستندات', 'documents/create', 'documents.create', 'null', 0, 2, 'پرسنل', 1, '2022-05-07 19:44:33', '2022-05-07 19:44:33');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(250) NOT NULL,
  `token` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `payments`
--

CREATE TABLE `payments` (
  `id` int(10) UNSIGNED NOT NULL,
  `payment_amount` varchar(40) NOT NULL DEFAULT '0' COMMENT 'مبلغ قابل پرداخت',
  `total_sum` varchar(40) NOT NULL DEFAULT '0' COMMENT 'مبلغ نهایی',
  `payment_message` varchar(200) NOT NULL DEFAULT 'null' COMMENT 'پیام ارسال شده از طرف درگاه',
  `payment_message_code` int(11) NOT NULL DEFAULT 0 COMMENT 'کد ارسال شده از طرف درگاه',
  `customer_id` int(10) UNSIGNED DEFAULT NULL COMMENT 'کلید ایندکس مشتری',
  `discount_id` int(10) UNSIGNED DEFAULT 0 COMMENT 'کلید کلید مربوط به تخفیفات',
  `gift_id` int(10) UNSIGNED DEFAULT 0 COMMENT 'کلید کلید مربوط به تخفیفات',
  `gift_amount` varchar(40) NOT NULL DEFAULT '0' COMMENT 'کلید کلید مربوط به تخفیفات',
  `discount_value` varchar(40) NOT NULL DEFAULT '0' COMMENT 'مقدار مبلغ تخفیف',
  `sent_address` varchar(600) NOT NULL DEFAULT 'null' COMMENT 'آدرس گیرنده کالا',
  `sent_phone` varchar(600) NOT NULL DEFAULT 'null' COMMENT 'شماره تماس گیرنده کالا',
  `receiver_fullname` varchar(150) NOT NULL DEFAULT 'null' COMMENT 'نام گیرنده کالا',
  `postal_code_sent` varchar(40) NOT NULL DEFAULT 'null' COMMENT 'کد پستی',
  `condition_id` int(10) UNSIGNED DEFAULT 1 COMMENT 'کلید وضعیت',
  `type_id` int(10) UNSIGNED DEFAULT 1,
  `Authority` varchar(100) NOT NULL DEFAULT 'null',
  `reference_code` varchar(80) NOT NULL DEFAULT 'null' COMMENT 'کد پیگیری',
  `payment_status` int(11) NOT NULL DEFAULT 0 COMMENT 'وضعیت پرداخت فاکتور ثبت شده',
  `active` tinyint(1) NOT NULL DEFAULT 1 COMMENT 'نمایش یا عدم نمایش رکورد',
  `RefID` varchar(45) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `payments`
--

INSERT INTO `payments` (`id`, `payment_amount`, `total_sum`, `payment_message`, `payment_message_code`, `customer_id`, `discount_id`, `gift_id`, `gift_amount`, `discount_value`, `sent_address`, `sent_phone`, `receiver_fullname`, `postal_code_sent`, `condition_id`, `type_id`, `Authority`, `reference_code`, `payment_status`, `active`, `RefID`, `created_at`, `updated_at`) VALUES
(1, '228000', '228000', 'null', 0, 1, 2, 0, '0', '21280.000000000004', 'null', 'null', 'null', 'null', 6, 1, 'null', 'null', 3, 1, '0', '2020-05-13 12:19:14', '2020-09-11 03:28:35'),
(7, '2000', '0', 'عمليات با موفقيت انجام گرديده است.', 100, 1, 0, 0, '0', '0', 'null', 'null', 'null', 'null', 1, 2, 'A00000000000000000000000000214740100', '37852490', 3, 1, '21474010001', '2020-09-03 22:19:23', '2020-09-03 22:22:07'),
(8, '2000', '0', 'عمليات با موفقيت انجام گرديده است.', 100, 1, 0, 0, '0', '0', 'null', 'null', 'null', 'null', 1, 2, 'A00000000000000000000000000214740431', '73029685', 3, 1, '21474043101', '2020-09-03 22:26:04', '2020-09-03 22:27:07'),
(9, '2000', '0', 'عمليات با موفقيت انجام گرديده است.', 100, 1, 0, 0, '0', '0', 'null', 'null', 'null', 'null', 1, 2, 'A00000000000000000000000000214742627', '82309764', 3, 1, '0', '2020-09-03 23:18:32', '2020-09-03 23:22:34'),
(10, '2000', '0', 'عمليات با موفقيت انجام گرديده است.', 100, 1, 0, 0, '0', '0', 'null', 'null', 'null', 'null', 1, 2, 'A00000000000000000000000000214742814', '92765183', 3, 1, '0', '2020-09-03 23:24:02', '2020-09-03 23:25:38'),
(11, '1000', '0', 'عمليات با موفقيت انجام گرديده است.', 100, 1, 0, 0, '0', '0', 'null', 'null', 'null', 'null', 1, 2, 'A00000000000000000000000000214742899', '76208543', 3, 1, '0', '2020-09-03 23:27:01', '2020-09-03 23:29:45'),
(12, '1000', '0', 'عمليات با موفقيت انجام گرديده است.', 100, 1, 0, 0, '0', '0', 'null', 'null', 'null', 'null', 1, 2, 'A00000000000000000000000000214743022', '82576130', 3, 1, '21474302201', '2020-09-03 23:30:21', '2020-09-03 23:32:26'),
(13, '76000', '0', 'عمليات با موفقيت انجام گرديده است.', 100, 1, 0, 3, '64400', '0', 'بلوار معلم، نبش معلم 16، پلاک 36', '09365647585', 'امید مالک', '9189171111', 3, 1, 'null', '60523189', 3, 1, '0', '2020-09-07 03:20:58', '2020-09-09 12:08:07'),
(14, '1000', '0', 'عمليات با موفقيت انجام گرديده است.', 100, 1, 0, 4, '1000', '0', 'بلوار معلم، نبش معلم 16، پلاک 36', '09365647585', 'امید مالک', '9189171111', 3, 1, 'null', '94306582', 3, 1, '0', '2020-09-09 17:21:51', '2020-09-09 17:29:02'),
(15, '0', '0', 'null', 0, 1, 0, 0, '0', '0', 'null', 'null', 'null', 'null', 1, 1, 'null', 'null', 1, 1, '0', '2021-02-17 14:04:49', '2021-02-17 14:04:49');

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(40) NOT NULL DEFAULT 'بدون عنوان' COMMENT 'عنوان سطح دسترسی',
  `keyword` varchar(40) NOT NULL DEFAULT 'null' COMMENT 'شماره تماس',
  `active` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `title`, `keyword`, `active`, `created_at`, `updated_at`) VALUES
(1, 'مدیر سامانه', 'Admin', 1, '2022-05-07 19:08:44', '2022-05-07 19:08:44'),
(2, 'پرسنل', 'Personnel', 1, '2022-05-07 19:08:44', '2022-05-07 19:08:44');

-- --------------------------------------------------------

--
-- Table structure for table `personnels`
--

CREATE TABLE `personnels` (
  `id` int(10) UNSIGNED NOT NULL,
  `fullname` varchar(255) NOT NULL,
  `email` varchar(250) NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `phone` varchar(16) NOT NULL DEFAULT 'null' COMMENT 'شماره تماس',
  `address` varchar(200) NOT NULL DEFAULT 'null' COMMENT 'آدرس',
  `number_cart` varchar(20) NOT NULL DEFAULT 'null' COMMENT 'آدرس',
  `number_account` varchar(20) NOT NULL DEFAULT 'null' COMMENT 'آدرس',
  `facebook_address` varchar(300) NOT NULL DEFAULT 'null' COMMENT 'آدرس فیس بوک',
  `instagram_address` varchar(300) NOT NULL DEFAULT 'null' COMMENT 'آدرس اینستاگرام',
  `telegram_address` varchar(300) NOT NULL DEFAULT 'null' COMMENT 'آدرس تلگرام',
  `permission_id` int(10) UNSIGNED DEFAULT 1 COMMENT 'کلید سطح دسترسی',
  `active` tinyint(1) NOT NULL DEFAULT 1,
  `remember_token` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) NOT NULL,
  `body` text DEFAULT NULL,
  `published` tinyint(1) NOT NULL DEFAULT 1,
  `used_in` varchar(20) NOT NULL DEFAULT 'articles',
  `visited` int(11) NOT NULL DEFAULT 1 COMMENT 'تعداد دفعات بازدید شده',
  `created_by` bigint(20) NOT NULL,
  `updated_by` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `title`, `body`, `published`, `used_in`, `visited`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(3, 'اولین مقاله دانستنی ها', '<p>متن اولین مقاله دانستنی</p>', 1, 'articles', 21, 0, 1, '2020-06-11 13:55:31', '2022-02-16 16:12:31'),
(4, 'دومین پست دانستنی ها', '<p><img style=\"display: block; margin-left: auto; margin-right: auto;\" src=\"/photos/1/bg-01.jpg\" alt=\"\" width=\"836\" height=\"409\" /></p>\r\n<p style=\"text-align: center;\">متن دومین مقاله </p>', 1, 'articles', 17, 0, 1, '2020-06-14 14:48:12', '2022-02-16 16:12:32'),
(5, 'سومین پست', '<p style=\"text-align: center;\"><img style=\"display: block; margin-left: auto; margin-right: auto;\" src=\"/photos/1/bg-01.jpg\" alt=\"\" width=\"899\" height=\"440\" /></p>\r\n<p style=\"text-align: center;\">متن سومین پست </p>', 1, 'articles', 26, 0, 1, '2020-06-14 14:55:42', '2022-03-07 17:23:06'),
(6, 'چهارمین پست دارت مارکت', '<p>متن چهارمین پست مارکت</p>', 1, 'articles', 18, 0, 1, '2020-06-14 14:56:49', '2022-03-10 20:23:49'),
(7, 'پنجمین پست دانستنی', '<p>متن پنجمین پست دانستنی</p>', 1, 'articles', 23, 0, 1, '2020-06-14 14:57:47', '2022-03-07 17:23:07'),
(8, 'داروخانه دکتر مالک\r\n', '<h5 class=\"text-right\">این متن صرفا جهت تست سامانه است\r\n</h5>', 1, 'right.slider', 2, 0, 1, '2020-07-04 20:54:56', '2020-09-30 11:43:19');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(150) NOT NULL COMMENT 'عنوان کالا',
  `description` text NOT NULL COMMENT 'توضیحات مربوط به کالا',
  `sales` int(11) NOT NULL DEFAULT 0 COMMENT 'تعداد کالاهای فروخته شده',
  `amount` varchar(40) NOT NULL COMMENT 'قیمت کالا',
  `active` tinyint(1) NOT NULL DEFAULT 1,
  `number` tinyint(1) NOT NULL DEFAULT 1 COMMENT 'تعداد کالاهای موجود',
  `categorie_id` int(10) UNSIGNED DEFAULT NULL COMMENT 'کلید دسته بندی کالا',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `title`, `description`, `sales`, `amount`, `active`, `number`, `categorie_id`, `created_at`, `updated_at`) VALUES
(1, 'بیوتین جالینوس 5 میلی', '<h4 style=\"text-align: right;\">بیوتین جالینوس 5 میلی گرمی</h4>', 2, '36000', 0, 10, 2, '2020-01-17 05:17:28', '2021-03-25 09:05:40'),
(2, 'پودر آب پنیر', '<h4 style=\"text-align: right;\">پودر آب پنیر</h4>', 3, '27000', 0, 13, 3, '2020-01-17 05:17:28', '2021-03-23 14:46:53'),
(3, 'متاپیور زیرو کرب', '<div class=\"title-fa\">\r\n<h3>متاپیور زیرو کرب</h3>\r\n<span id=\"LblSlaveName\" title=\"19832\">( شکلات بلژیکی )</span></div>', 4, '76000', 0, 15, 3, '2020-01-17 05:17:28', '2021-03-23 14:49:15'),
(4, 'پروتئین وی', '<div class=\"title-fa\" style=\"text-align: right;\">\r\n<h4>پروتئین وی 2270 گرمی</h4>\r\n<span id=\"LblSlaveName\" title=\"19476\"></span></div>', 5, '918000', 0, 15, 3, '2020-01-17 05:17:28', '2021-03-25 09:06:10'),
(5, 'آیروسول', '<p style=\"text-align: right;\">در این قسمت توضیحات مربوط به کالای مورد نظر ارائه می گردد.</p>', 6, '41500', 0, 16, 3, '2020-01-17 05:17:28', '2021-03-25 09:05:34'),
(7, 'ویتامین ث', '<h4 style=\"text-align: right;\">ویتافلش ویتامین ث</h4>', 0, '32700', 0, 2, 2, '2020-09-24 16:33:31', '2021-03-25 09:05:44'),
(9, 'کرم اکسیدان', '<div class=\"title-fa\">\r\n<h4 style=\"text-align: right;\">کرم اکسیدان 12% نمره 3 حاوی روغن بادام</h4>\r\n<div class=\"each-row\" style=\"text-align: right;\">نوع محصول\r\n<div style=\"text-align: right;\">اکسیدان</div>\r\n</div>\r\n<div class=\"each-row\" style=\"text-align: right;\">سایز\r\n<div style=\"text-align: right;\">150 میلی لیتر</div>\r\n</div>\r\n<div class=\"each-row\" style=\"text-align: right;\">محل مصرف\r\n<div>موی سر</div>\r\n</div>\r\n<span id=\"LblSlaveName\" title=\"19902\"></span></div>', 0, '9800', 0, 2, 1, '2020-10-22 12:12:29', '2021-03-25 09:06:00'),
(10, 'ویتامین ای 400', '<h4 style=\"text-align: right;\">ویتامین ای 400 آلفا 30 عددی</h4>', 0, '29500', 0, 2, 2, '2020-10-22 12:26:05', '2021-03-25 09:05:50'),
(11, 'برس مو آی استایل', '<div class=\"title-layer\">\r\n<div class=\"title-fa\">\r\n<h4 style=\"text-align: right;\">برس مو آی استایل لیفت اند کرل</h4>\r\n<span id=\"LblSlaveName\" title=\"19883\"></span>\r\n<h6 class=\"each-row\" style=\"text-align: right;\">نوع محصول</h6>\r\n<div style=\"text-align: right;\">شانه</div>\r\n<h6 class=\"each-row\" style=\"text-align: right;\">محل مصرف</h6>\r\n<div style=\"text-align: right;\">موی سر</div>\r\n</div>\r\n</div>', 0, '46300', 0, 2, 1, '2020-10-22 12:27:32', '2021-03-25 09:05:54'),
(14, 'بوگیر و خوشبو کننده هوای', '<h4 style=\"text-align: right;\">بوگیر و خوشبو کننده هوای توالت با رایحه شیمی سوکی</h4>\r\n<h6 style=\"text-align: right;\">نوع محصول: مایع</h6>\r\n<h6 style=\"text-align: right;\">سایز: 400 میلی لیتر</h6>', 0, '499000', 0, 2, 1, '2020-10-23 17:27:25', '2021-03-25 09:06:05'),
(15, 'غذای کودک برنجین', '<h4 style=\"text-align: right;\">غذای کودک برنجین با شیر و مخلوط میوه</h4>\r\n<p style=\"text-align: right;\">300 گرم</p>', 0, '1000', 0, 2, 4, '2021-01-21 11:10:54', '2021-03-23 14:47:17'),
(16, 'غذای کودک گندمین', '<div class=\"title-layer\">\r\n<div class=\"title-fa\" style=\"text-align: right;\">\r\n<h4>غذای کودک گندمین با شیر و مخلوط میوه</h4>\r\n<span id=\"LblSlaveName\" title=\"19053\"></span></div>\r\n</div>', 0, '12500', 0, 2, 4, '2021-01-21 11:13:33', '2021-03-23 14:47:03'),
(17, 'فشارسنج مچی', '<h4 style=\"text-align: right;\">فشارسنج مچی مدل BP W1 Basic</h4>', 0, '994000', 0, 5, 5, '2021-01-21 11:16:42', '2021-03-25 09:04:09'),
(18, 'ماسک بهداشتی', '<div class=\"title-fa\" style=\"text-align: right;\">\r\n<h4 style=\"text-align: right;\">ماسک بهداشتی 3 بعدی</h4>\r\n</div>\r\n<div class=\"title-fa\" style=\"text-align: right;\">\r\n<h5 style=\"text-align: right;\">سفید</h5>\r\n</div>', 0, '50000', 1, 2, 5, '2021-01-21 11:19:22', '2021-03-25 09:06:00'),
(19, 'فوليکا ماسک مو خشک', '<p>بدون توضیحات</p>', 0, '29000', 0, 1, 1, '2021-03-03 05:26:04', '2021-03-25 09:05:03'),
(20, 'الارو کرم ضد آفتاب SPF50 انواع پوست', '<p>بدون توضیحات</p>', 0, '113400', 0, 1, 1, '2021-03-03 05:31:35', '2021-03-25 09:14:37'),
(21, 'آتوپيا آردن کرم', '<p><img src=\"C:\\Users\\Dr Malek Client4\\Desktop\\New folder\\12753\" alt=\"\" />بدون توضیحات</p>', 0, '122500', 0, 1, 1, '2021-03-03 05:34:12', '2021-03-25 09:05:13'),
(22, 'کليير شامپو مردانه', '<p>بدون توضیحات</p>', 0, '28000', 0, 1, 1, '2021-03-03 05:35:45', '2021-03-25 09:12:58'),
(23, 'موستلا کرم ضد ترک', '<p>بدون توضیحات</p>', 0, '380000', 0, 1, 1, '2021-03-03 05:37:25', '2021-03-25 09:05:29'),
(24, 'کامان شامپو ضد ريزش', '<p>بدون توضیحات</p>', 0, '87300', 0, 1, 1, '2021-03-03 05:42:34', '2021-03-25 09:04:59'),
(25, 'کامان کرم دست و صورت مخصوص پوست چرب', '<p>کامان کرم دست و صورت مخصوص پوست چرب</p>', 0, '32500', 0, 2, 1, '2021-03-03 05:44:44', '2021-03-25 09:14:40'),
(26, 'قطره سیدرال گوچه', '<p style=\"text-align: right;\">قطره آهن&nbsp;<em>سیدرال گوچه</em>&nbsp;برای تامین آهن مورد نیاز بدن به ویژه کودکان استفاده می شود.</p>', 0, '98100', 1, 2, 2, '2021-03-23 15:00:53', '2021-03-30 09:03:00'),
(27, 'سريتا شامپو آنتي پسو', '<p>سريتا شامپو آنتي پسو</p>', 0, '39500', 1, 2, 1, '2021-03-25 09:25:33', '2021-03-25 09:25:33'),
(28, 'سريتا شامپو کافيين دار', '<p>سريتا شامپو کافيين دار</p>', 0, '44500', 1, 2, 1, '2021-03-25 09:28:51', '2021-03-30 09:02:00'),
(29, 'سريتا شامپو پرومين', '<p>سريتا شامپو پرومين</p>', 0, '49500', 1, 2, 1, '2021-03-25 09:31:53', '2021-03-30 09:00:00'),
(30, 'سريتا شامپو ضدريزش چرب', '<p>سريتا شامپو ضدريزش چرب</p>', 0, '35800', 1, 2, 1, '2021-03-25 09:33:25', '2021-03-30 09:02:00'),
(31, 'سريتا شامپو ضدريزش خشک', '<p>سريتا شامپو ضدريزش خشک</p>', 0, '34800', 1, 2, 1, '2021-03-25 09:34:57', '2021-03-25 09:34:57'),
(32, 'سریتا شامپو کاشت مو مناسب انواع مو', '<p>سریتا شامپو کاشت مو مناسب انواع مو</p>', 0, '141000', 1, 2, 1, '2021-03-25 09:36:58', '2021-03-30 09:01:00'),
(33, 'سريتا شامپو محافظ رنگ مو', '<p>سريتا شامپو محافظ رنگ مو</p>', 0, '28000', 1, 2, 1, '2021-03-25 09:38:52', '2021-03-30 09:02:00'),
(34, 'سريتا شامپو موهاي آسيب ديده', '<p>سريتا شامپو موهاي آسيب ديده</p>', 0, '64200', 1, 2, 1, '2021-03-25 09:40:39', '2021-03-30 09:03:00'),
(35, 'سریتا شامپو مینوتا ضد ریزش موهای خشک', '<p>&nbsp;سریتا شامپو مینوتا ضد ریزش موهای خشک</p>', 0, '86000', 1, 2, 1, '2021-03-25 09:42:55', '2021-03-30 09:01:00'),
(36, 'سریتا شامپو ضد شوره 2 در 1 مناسب انواع مو', '<p>سریتا شامپو ضد شوره 2 در 1 مناسب انواع مو</p>', 0, '34800', 1, 2, 1, '2021-03-25 09:44:34', '2021-03-30 09:02:00'),
(37, 'سريتا شامپو ضد شوره خشک', '<p>سريتا شامپو ضد شوره خشک</p>', 0, '48500', 1, 2, 1, '2021-03-25 09:46:25', '2021-03-30 09:02:00'),
(38, 'سريتا شامپو تقويت جوانه گندم', '<p>سريتا شامپو تقويت جوانه گندم</p>', 0, '39500', 1, 2, 1, '2021-03-25 09:48:40', '2021-03-30 09:01:00'),
(39, 'سريتا شامپو ضد شوره چرب', '<p>سريتا شامپو ضد شوره چرب</p>', 0, '48500', 1, 2, 1, '2021-03-25 09:50:18', '2021-03-30 09:00:00'),
(40, 'پريم شامپو ضد شوره ملايم D1', '<p>پريم شامپو ضد شوره ملايم D1</p>', 0, '39000', 1, 2, 1, '2021-03-30 09:08:08', '2021-03-30 09:08:08'),
(41, 'پريم شامپو ملايم روزانه +N', '<p>پريم شامپو ملايم روزانه N+</p>', 0, '29000', 1, 2, 1, '2021-03-30 09:12:39', '2021-03-30 09:12:39'),
(42, 'پريم شامپو اوره 5%', '<p>پريم شامپو اوره 5%</p>', 0, '49000', 1, 2, 1, '2021-03-30 09:16:41', '2021-03-30 09:16:41'),
(43, 'پريم شامپو ضد شوره چرب', '<p>پريم شامپو ضد شوره چرب</p>', 0, '79000', 1, 2, 1, '2021-03-30 09:19:35', '2021-03-30 09:19:35'),
(44, 'ويرگو شامپو ماينوکسيديل', '<p>ويرگو شامپو ماينوکسيديل</p>', 0, '42000', 1, 2, 1, '2021-03-30 09:21:56', '2021-03-30 09:21:56'),
(45, 'ويرگو شامپو ضد شوره منتا', '<p>ويرگو شامپو ضد شوره منتا</p>', 0, '35000', 1, 2, 1, '2021-03-30 09:23:54', '2021-03-30 09:23:54'),
(46, 'درماسيف شامپو ضد شوره چرب', '<p>درماسيف شامپو ضد شوره چرب</p>', 0, '14500', 1, 2, 1, '2021-03-30 09:39:46', '2021-03-30 09:39:46'),
(47, 'درماسيف شامپو ضد شوره خشک', '<p>درماسيف شامپو ضد شوره خشک</p>', 0, '14500', 1, 2, 1, '2021-03-30 09:41:14', '2021-03-30 09:41:14'),
(48, 'فوليکا شامپو خيلي ملايم', '<p>فوليکا شامپو خيلي ملايم</p>', 0, '45000', 1, 2, 1, '2021-03-30 09:43:04', '2021-03-30 09:43:04'),
(49, 'فوليکا شامپو مخصوص موهاي شکننده', '<p>فوليکا شامپو مخصوص موهاي شکننده</p>', 0, '22400', 1, 2, 1, '2021-03-30 09:44:34', '2021-03-30 09:44:34'),
(50, 'سيوند شامپو کافيين', '<p>سيوند شامپو کافيين</p>', 0, '45300', 1, 2, 1, '2021-03-30 09:46:23', '2021-03-30 09:46:23'),
(51, 'سيوند شامپو مخصوص موهاي چرب', '<p>سيوند شامپو مخصوص موهاي چرب</p>', 0, '33000', 1, 2, 1, '2021-03-30 09:47:30', '2021-03-30 09:47:30');

-- --------------------------------------------------------

--
-- Table structure for table `scales`
--

CREATE TABLE `scales` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(100) NOT NULL COMMENT 'عنوان دسته بندی',
  `active` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `scales`
--

INSERT INTO `scales` (`id`, `title`, `active`, `created_at`, `updated_at`) VALUES
(1, 'بسته', 1, '2022-03-13 18:02:49', '2022-03-13 18:02:49'),
(2, 'عدد', 1, '2022-03-13 18:03:12', '2022-03-13 18:03:12'),
(3, 'ورق', 1, '2022-03-13 18:03:33', '2022-03-13 18:03:33');

-- --------------------------------------------------------

--
-- Table structure for table `tokens`
--

CREATE TABLE `tokens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `types`
--

CREATE TABLE `types` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(80) NOT NULL COMMENT 'عنوان',
  `key_word` varchar(40) NOT NULL COMMENT 'کلمه کلیدی نوع',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `types`
--

INSERT INTO `types` (`id`, `title`, `key_word`, `created_at`, `updated_at`) VALUES
(1, 'خرید کالا', 'productPurchase', '2020-07-19 10:46:46', '2020-07-19 10:46:46'),
(2, 'شارژ کیف پول', 'walletCharge', '2020-07-19 10:46:46', '2020-07-19 10:46:46');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `fullname` varchar(255) NOT NULL,
  `username` varchar(100) NOT NULL DEFAULT 'null' COMMENT 'نام کاربری کاربر',
  `email` varchar(150) NOT NULL DEFAULT 'omid.malek.1990@gmail.com',
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `phone` varchar(16) NOT NULL DEFAULT 'null' COMMENT 'شماره تماس',
  `address` varchar(200) NOT NULL DEFAULT 'null' COMMENT 'آدرس',
  `number_cart` varchar(20) NOT NULL DEFAULT 'null' COMMENT 'آدرس',
  `number_account` varchar(20) NOT NULL DEFAULT 'null' COMMENT 'آدرس',
  `facebook_address` varchar(300) NOT NULL DEFAULT 'null' COMMENT 'آدرس فیس بوک',
  `instagram_address` varchar(300) NOT NULL DEFAULT 'null' COMMENT 'آدرس اینستاگرام',
  `telegram_address` varchar(300) NOT NULL DEFAULT 'null' COMMENT 'آدرس تلگرام',
  `permission_id` tinyint(3) UNSIGNED NOT NULL DEFAULT 0 COMMENT 'کد دسترسی',
  `active` tinyint(1) NOT NULL DEFAULT 1,
  `remember_token` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `fullname`, `username`, `email`, `email_verified_at`, `password`, `phone`, `address`, `number_cart`, `number_account`, `facebook_address`, `instagram_address`, `telegram_address`, `permission_id`, `active`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'امید مالک', 'admin', 'omid.malek.1990@gmail.com', NULL, '25f9e794323b453885f5181f1b624d0b', '09365647585', 'طبس خیابان نواب صفوی', '6104337927742401', '6104337927', 'null', 'null', 'null', 1, 1, NULL, '2020-01-17 05:15:35', '2022-05-20 19:29:43'),
(14, 'امید مالک', '09365647585', 'email@sample.com', NULL, '25f9e794323b453885f5181f1b624d0b', '09365647585', 'بدون توضیح', 'null', 'null', 'null', 'null', 'null', 2, 1, NULL, '2022-07-29 19:29:12', '2022-07-29 19:29:12');

-- --------------------------------------------------------

--
-- Table structure for table `wallets`
--

CREATE TABLE `wallets` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(80) NOT NULL COMMENT 'موجودی کیف پول',
  `amount` double NOT NULL DEFAULT 0 COMMENT 'موجودی کیف پول',
  `type_id` double NOT NULL DEFAULT 2 COMMENT 'کد نوع پرداخت',
  `customer_id` int(10) UNSIGNED DEFAULT NULL COMMENT 'کلید داخلی کاربر',
  `active` tinyint(1) NOT NULL DEFAULT 1 COMMENT 'متن پیام پیام فعال=1 و پیام غیر فعال=0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `wallets`
--

INSERT INTO `wallets` (`id`, `title`, `amount`, `type_id`, `customer_id`, `active`, `created_at`, `updated_at`) VALUES
(1, 'کیف پول مجازی', 20000, 2, 1, 1, '2020-09-03 01:12:03', '2020-09-06 16:50:55'),
(2, 'کیف پول مجازی', 0, 2, 2, 1, '2020-09-09 17:33:13', '2020-09-09 17:33:13');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `conditions`
--
ALTER TABLE `conditions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `customers_email_unique` (`email`);

--
-- Indexes for table `discounts`
--
ALTER TABLE `discounts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `documents`
--
ALTER TABLE `documents`
  ADD PRIMARY KEY (`id`),
  ADD KEY `documents_user_id_index` (`user_id`);

--
-- Indexes for table `drugs`
--
ALTER TABLE `drugs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `drugs_scale_id_index` (`scale_id`);

--
-- Indexes for table `factors`
--
ALTER TABLE `factors`
  ADD PRIMARY KEY (`id`),
  ADD KEY `factors_product_id_index` (`product_id`),
  ADD KEY `factors_type_id_index` (`type_id`),
  ADD KEY `factors_customer_id_index` (`customer_id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gifts`
--
ALTER TABLE `gifts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `gifts_wallet_id_index` (`wallet_id`),
  ADD KEY `gifts_condition_id_index` (`condition_id`);

--
-- Indexes for table `images`
--
ALTER TABLE `images`
  ADD PRIMARY KEY (`id`),
  ADD KEY `images_record_id_index` (`record_id`);

--
-- Indexes for table `items`
--
ALTER TABLE `items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `meta_tages`
--
ALTER TABLE `meta_tages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `options`
--
ALTER TABLE `options`
  ADD PRIMARY KEY (`id`),
  ADD KEY `options_categorie_id_index` (`categorie_id`),
  ADD KEY `options_product_id_index` (`product_id`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `payments`
--
ALTER TABLE `payments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `payments_customer_id_index` (`customer_id`),
  ADD KEY `payments_discount_id_index` (`discount_id`),
  ADD KEY `payments_gift_id_index` (`gift_id`),
  ADD KEY `payments_condition_id_index` (`condition_id`),
  ADD KEY `payments_type_id_index` (`type_id`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `personnels`
--
ALTER TABLE `personnels`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personnels_email_unique` (`email`),
  ADD KEY `personnels_permission_id_index` (`permission_id`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `products_categorie_id_index` (`categorie_id`);

--
-- Indexes for table `scales`
--
ALTER TABLE `scales`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tokens`
--
ALTER TABLE `tokens`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `types`
--
ALTER TABLE `types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `permission_id` (`permission_id`);

--
-- Indexes for table `wallets`
--
ALTER TABLE `wallets`
  ADD PRIMARY KEY (`id`),
  ADD KEY `wallets_customer_id_index` (`customer_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `conditions`
--
ALTER TABLE `conditions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `customers`
--
ALTER TABLE `customers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `discounts`
--
ALTER TABLE `discounts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `documents`
--
ALTER TABLE `documents`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `drugs`
--
ALTER TABLE `drugs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `factors`
--
ALTER TABLE `factors`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `gifts`
--
ALTER TABLE `gifts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `images`
--
ALTER TABLE `images`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=67;

--
-- AUTO_INCREMENT for table `items`
--
ALTER TABLE `items`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT for table `meta_tages`
--
ALTER TABLE `meta_tages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT for table `options`
--
ALTER TABLE `options`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `payments`
--
ALTER TABLE `payments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `personnels`
--
ALTER TABLE `personnels`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=52;

--
-- AUTO_INCREMENT for table `scales`
--
ALTER TABLE `scales`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tokens`
--
ALTER TABLE `tokens`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `types`
--
ALTER TABLE `types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `wallets`
--
ALTER TABLE `wallets`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
