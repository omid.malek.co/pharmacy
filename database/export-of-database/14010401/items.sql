-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 22, 2022 at 08:18 PM
-- Server version: 10.4.16-MariaDB
-- PHP Version: 7.4.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `drmalekp_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `items`
--

CREATE TABLE `items` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `personnel_id` bigint(20) NOT NULL DEFAULT 0 COMMENT 'کلید کاربر در جدول پرسنل',
  `user_id` bigint(20) NOT NULL DEFAULT 0 COMMENT 'کلید کاربری',
  `page_id` bigint(20) NOT NULL DEFAULT 0 COMMENT 'کلید صفحه',
  `page_title` varchar(60) NOT NULL DEFAULT 'بدون عنوان' COMMENT 'عنوان صفحه',
  `route_name` varchar(100) NOT NULL DEFAULT 'null',
  `address` varchar(200) NOT NULL DEFAULT 'null' COMMENT 'آدرس نسبی صفحه',
  `base_url` varchar(150) NOT NULL DEFAULT 'null' COMMENT 'آدرس سایت',
  `is_base_page` int(11) NOT NULL DEFAULT 1 COMMENT 'صفحه زیر مجموعه صفحه دیگری نیست؟',
  `child_id` bigint(20) NOT NULL DEFAULT 0 COMMENT 'کد آیتم زیر مجموعه این صفحه',
  `parent_id` bigint(20) NOT NULL DEFAULT 0 COMMENT 'کد آیتم والد این صفحه',
  `permission_id` bigint(20) NOT NULL DEFAULT 1 COMMENT 'کلید سطح دسترسی',
  `permission_title` varchar(40) NOT NULL DEFAULT '1' COMMENT 'عنوان سطح دسترسی',
  `active` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `items`
--

INSERT INTO `items` (`id`, `personnel_id`, `user_id`, `page_id`, `page_title`, `route_name`, `address`, `base_url`, `is_base_page`, `child_id`, `parent_id`, `permission_id`, `permission_title`, `active`, `created_at`, `updated_at`) VALUES
(1, 0, 1, 1, 'کسری دارو', 'drugs.index', 'admin/drugs', 'null', 1, 0, 0, 1, 'مدیر سامانه', 1, '2022-05-08 20:17:29', '2022-05-08 20:17:29'),
(2, 0, 1, 2, 'تغییر  رمز', 'admin.password.index', 'admin/admin_password', 'null', 1, 0, 0, 1, 'مدیر سامانه', 1, '2022-05-08 20:17:29', '2022-05-08 20:17:29'),
(3, 0, 1, 3, 'اطلاعات مدیریت', 'admin.edit', 'admin/admins_show', 'null', 1, 0, 0, 1, 'مدیر سامانه', 1, '2022-05-08 20:17:29', '2022-05-08 20:17:29'),
(4, 0, 1, 4, 'محصولات', 'admin.products.index', 'admin/products_management_list', 'null', 1, 0, 0, 1, 'مدیر سامانه', 1, '2022-05-08 20:17:29', '2022-05-08 20:17:29'),
(5, 0, 1, 5, 'افزودن محصول', 'admin.products.create', 'admin/admin/products_create', 'null', 0, 0, 0, 1, 'مدیر سامانه', 1, '2022-05-10 17:55:50', '2022-05-10 17:55:50'),
(6, 0, 1, 6, 'فاکتورها', 'factors.management.index', 'admin/factors_list', 'null', 1, 0, 0, 1, 'مدیر سامانه', 1, '2022-05-10 17:58:18', '2022-05-10 17:58:18'),
(7, 0, 1, 11, 'پرسنل', 'items.index', 'admin/items_personnel', 'null', 1, 0, 0, 1, 'مدیر سامانه', 1, '2022-05-08 20:17:29', '2022-05-08 20:17:29'),
(28, 0, 13, 7, 'کسری دارو', 'admin.drugs.index', 'admin/drugs', 'null', 1, 0, 0, 2, 'پرسنل', 1, '2022-06-17 19:44:39', '2022-06-17 19:44:39'),
(29, 0, 13, 8, 'محصولات', 'admin.products.index', 'admin/products_management_list', 'null', 1, 0, 0, 2, 'پرسنل', 1, '2022-06-17 19:44:39', '2022-06-17 19:44:39'),
(30, 0, 13, 10, 'فاکتورها', 'factors.management.index', 'admin/factors_list', 'null', 1, 0, 0, 2, 'پرسنل', 1, '2022-06-17 19:44:39', '2022-06-17 19:44:39'),
(31, 0, 13, 12, 'تغییر رمز', 'admin.password.index', 'admin/admin_password', 'null', 1, 0, 0, 2, 'پرسنل', 1, '2022-06-17 19:44:39', '2022-06-17 19:44:39');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `items`
--
ALTER TABLE `items`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `items`
--
ALTER TABLE `items`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
