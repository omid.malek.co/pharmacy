-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 14, 2020 at 03:56 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dart`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(100) NOT NULL COMMENT 'عنوان مجموعه',
  `active` tinyint(1) NOT NULL DEFAULT 1 COMMENT 'وضعیت نمایش و یا عدم نمایش نمایش نظر',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `title`, `active`, `created_at`, `updated_at`) VALUES
(1, 'تخت دارت', 1, '2020-01-17 05:16:33', '2020-01-17 05:16:33'),
(2, 'دارت', 1, '2020-01-17 05:16:33', '2020-01-17 05:16:33'),
(3, 'فلایت', 1, '2020-01-17 05:16:33', '2020-01-17 05:16:33'),
(4, 'شات', 1, '2020-01-17 05:16:33', '2020-01-17 05:16:33'),
(5, 'تیپ', 1, '2020-01-17 05:16:33', '2020-01-17 05:16:33'),
(6, 'لوازم جانبی', 1, '2020-01-17 05:16:33', '2020-01-17 05:16:33');

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `conditions`
--

CREATE TABLE `conditions` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(80) NOT NULL COMMENT 'عنوان',
  `key_word` varchar(40) NOT NULL COMMENT 'کلمه کلیدی نوع',
  `active` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `conditions`
--

INSERT INTO `conditions` (`id`, `title`, `key_word`, `active`, `created_at`, `updated_at`) VALUES
(1, 'دیده نشده', 'Unseen', 1, '2020-08-09 11:38:33', '2020-08-09 11:38:33'),
(2, 'استفاده نشده', 'notUsed', 1, '2020-09-06 03:45:35', '2020-09-06 03:45:35'),
(3, 'ارسال نشده', 'notSent', 1, '2020-09-06 03:50:19', '2020-09-06 03:50:19'),
(4, 'دیده شده', 'seen', 1, '2020-08-09 11:36:02', '2020-08-09 11:36:02'),
(5, 'استفاده شده', 'used', 1, '2020-09-06 03:45:35', '2020-09-06 03:45:35'),
(6, 'ارسال شده', 'Sent', 1, '2020-09-06 03:50:19', '2020-09-06 03:50:19');

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE `customers` (
  `id` int(10) UNSIGNED NOT NULL,
  `fullname` varchar(150) NOT NULL DEFAULT 'null',
  `email` varchar(250) NOT NULL COMMENT 'ایمیل کاربر',
  `password` varchar(255) NOT NULL COMMENT 'کلمه عبور',
  `address` varchar(600) NOT NULL DEFAULT 'null' COMMENT 'آدرس',
  `postal_code` varchar(40) NOT NULL DEFAULT 'null' COMMENT 'کد پستی',
  `phone` varchar(16) NOT NULL DEFAULT '0' COMMENT 'شماره تماس',
  `token` varchar(100) NOT NULL DEFAULT 'null' COMMENT ' کد توکن',
  `active` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`id`, `fullname`, `email`, `password`, `address`, `postal_code`, `phone`, `token`, `active`, `created_at`, `updated_at`) VALUES
(1, 'امیر مداح', 'omid.malek.1990@gmail.com', '25f9e794323b453885f5181f1b624d0b', 'بلوار معلم، نبش معلم 16، پلاک 36', '9189171111', '09365647585', 'tdWG4s6bAp6zsC1tQ6SbzTrz74J5meVeJMDrheuKxhln4wh19BP2tFNYTD9S', 1, '2020-02-15 14:02:42', '2020-09-10 05:51:32'),
(2, 'null', 'test@gmail.com', '25f9e794323b453885f5181f1b624d0b', 'null', 'null', '09354612685', '55JaaHmXAZc8jkZIJbIPgbFE2o3VaHkMQX9Cjr9shUBZGwFRkiDqLEvkRRIM', 1, '2020-09-09 17:32:51', '2020-09-09 17:33:04');

-- --------------------------------------------------------

--
-- Table structure for table `discounts`
--

CREATE TABLE `discounts` (
  `id` int(10) UNSIGNED NOT NULL,
  `text` varchar(30) NOT NULL DEFAULT 'null' COMMENT 'کد تخفیف',
  `percent` double(8,2) NOT NULL DEFAULT 0.00 COMMENT 'میران درصد تخفیف',
  `start_date` date NOT NULL COMMENT 'تاریخ شروع اعمال تخفیف',
  `expire_date` date NOT NULL COMMENT 'تاریخ پایان اعمال تخفیف',
  `number_validation` int(11) NOT NULL DEFAULT 1,
  `active` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `discounts`
--

INSERT INTO `discounts` (`id`, `text`, `percent`, `start_date`, `expire_date`, `number_validation`, `active`, `created_at`, `updated_at`) VALUES
(2, 'takhfif', 10.00, '2020-09-08', '2020-09-12', 1, 1, '2020-07-16 18:51:09', '2020-08-07 15:16:50');

-- --------------------------------------------------------

--
-- Table structure for table `factors`
--

CREATE TABLE `factors` (
  `id` int(10) UNSIGNED NOT NULL,
  `product_id` int(10) UNSIGNED DEFAULT NULL COMMENT 'کلید خارجی پست',
  `sent_number` int(11) NOT NULL COMMENT 'تعداد کالاهای ارسال شده ',
  `type_id` int(10) UNSIGNED DEFAULT 1 COMMENT 'نوع پرداخت',
  `customer_id` int(10) UNSIGNED DEFAULT NULL COMMENT 'کلید خارجی مشتری ها',
  `payment_id` varchar(40) NOT NULL DEFAULT '0' COMMENT 'مبلغ پرداخت شده',
  `active` tinyint(1) NOT NULL DEFAULT 1 COMMENT 'حذف منطقی شده=0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `factors`
--

INSERT INTO `factors` (`id`, `product_id`, `sent_number`, `type_id`, `customer_id`, `payment_id`, `active`, `created_at`, `updated_at`) VALUES
(1, 4, 1, 1, 1, '1', 1, '2020-05-13 12:19:14', '2020-05-13 12:19:14'),
(7, 6, 1, 1, 1, '1', 1, '2020-08-04 10:01:33', '2020-08-04 10:01:33'),
(10, 2, 1, 1, 1, '13', 1, '2020-09-07 05:53:21', '2020-09-07 05:53:21'),
(11, 1, 1, 1, 1, '14', 1, '2020-09-09 17:21:51', '2020-09-09 17:21:51');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text NOT NULL,
  `queue` text NOT NULL,
  `payload` longtext NOT NULL,
  `exception` longtext NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `gifts`
--

CREATE TABLE `gifts` (
  `id` int(10) UNSIGNED NOT NULL,
  `text` varchar(30) NOT NULL DEFAULT 'null' COMMENT 'کد تخفیف',
  `amount` double NOT NULL DEFAULT 0 COMMENT 'میزان تخفیف',
  `wallet_id` int(10) UNSIGNED DEFAULT 0 COMMENT 'کد کارت هدیه',
  `used_by` int(11) NOT NULL DEFAULT 0 COMMENT 'استفاده شده توسط',
  `created_by` int(11) NOT NULL DEFAULT 0 COMMENT 'ایجاد شده توسط',
  `condition_id` int(10) UNSIGNED DEFAULT 1 COMMENT 'کلید وضعیت',
  `start_date` date NOT NULL COMMENT 'تاریخ شروع اعمال تخفیف',
  `expire_date` date NOT NULL COMMENT 'تاریخ پایان اعمال تخفیف',
  `active` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `gifts`
--

INSERT INTO `gifts` (`id`, `text`, `amount`, `wallet_id`, `used_by`, `created_by`, `condition_id`, `start_date`, `expire_date`, `active`, `created_at`, `updated_at`) VALUES
(3, '81425963', 12100, 1, 1, 1, 2, '2020-09-07', '2020-09-12', 1, '2020-09-06 09:54:09', '2020-09-09 12:07:51'),
(4, '34286195', 3000, 1, 1, 1, 2, '2020-09-07', '2020-09-14', 1, '2020-09-06 16:50:55', '2020-09-09 17:28:08');

-- --------------------------------------------------------

--
-- Table structure for table `images`
--

CREATE TABLE `images` (
  `id` int(10) UNSIGNED NOT NULL,
  `src` varchar(100) NOT NULL,
  `record_id` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT 'کلید خارجی',
  `used_in` varchar(20) NOT NULL DEFAULT 'null' COMMENT 'نام جدول',
  `active` tinyint(1) NOT NULL DEFAULT 1 COMMENT 'وضعیت نمایش و یا عدم نمایش نمایش نظر',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `images`
--

INSERT INTO `images` (`id`, `src`, `record_id`, `used_in`, `active`, `created_at`, `updated_at`) VALUES
(1, 'dart_1.png', 1, 'products', 1, '2020-01-17 05:17:28', '2020-01-17 05:17:28'),
(2, 'dart_2.jpg', 2, 'products', 1, '2020-01-17 05:17:28', '2020-01-17 05:17:28'),
(3, 'dart_3.jpg', 3, 'products', 1, '2020-01-17 05:17:28', '2020-01-17 05:17:28'),
(4, 'dart_1.png', 4, 'products', 1, '2020-01-17 05:17:28', '2020-01-17 05:17:28'),
(5, 'dart_2.jpg', 5, 'products', 1, '2020-01-17 05:17:28', '2020-01-17 05:17:28'),
(6, 'dart_3.jpg', 6, 'products', 1, '2020-01-17 05:17:28', '2020-01-17 05:17:28'),
(7, 'categories_takht_dart.jpg', 1, 'categories', 1, '2020-01-17 05:17:56', '2020-01-17 05:17:56'),
(8, 'categorie_dart.jpg', 2, 'categories', 1, '2020-01-17 05:17:56', '2020-01-17 05:17:56'),
(9, 'categorie_flit.jpg', 3, 'categories', 1, '2020-01-17 05:17:56', '2020-01-17 05:17:56'),
(10, 'categories_shot.jpg', 4, 'categories', 1, '2020-01-17 05:17:56', '2020-01-17 05:17:56'),
(11, 'categories_tip.jpg', 5, 'categories', 1, '2020-01-17 05:17:56', '2020-01-17 05:17:56'),
(12, 'categories_etc.jpg', 6, 'categories', 1, '2020-01-17 05:17:56', '2020-01-17 05:17:56'),
(13, 'slider_1.jpg', 0, 'sliders', 1, '2020-01-17 05:17:56', '2020-01-17 05:17:56'),
(14, 'noPhoto.jpg', 2, 'articles', 1, '2020-06-09 14:44:39', '2020-06-09 14:44:39'),
(15, 'products_159251850134.jpg', 3, 'articles', 1, '2020-06-11 13:55:31', '2020-06-18 22:15:01'),
(16, 'products_159251848752.jpg', 4, 'articles', 1, '2020-06-14 14:48:12', '2020-06-18 22:14:47'),
(17, 'products_159251847372.jpg', 5, 'articles', 1, '2020-06-14 14:55:42', '2020-06-18 22:14:33'),
(18, 'products_159251828144.jpg', 6, 'articles', 1, '2020-06-14 14:56:49', '2020-06-18 22:11:21'),
(19, 'products_159251824434.jpg', 7, 'articles', 1, '2020-06-14 14:57:47', '2020-06-18 22:10:44'),
(20, 'noPhoto.jpg', 8, 'right.slider', 1, '2020-06-09 14:44:39', '2020-06-09 14:44:39');

-- --------------------------------------------------------

--
-- Table structure for table `meta_tages`
--

CREATE TABLE `meta_tages` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_09_04_213034_create_payments_table', 1),
(4, '2019_12_29_094125_create_failed_jobs_table', 1),
(5, '2019_12_29_094502_create_posts_table', 1),
(6, '2020_01_01_104208_create_categories_table', 1),
(7, '2020_01_01_104258_create-comments_table', 1),
(8, '2020_01_01_104453_create_images_table', 1),
(9, '2020_01_01_111720_create_customers_table', 1),
(10, '2020_01_01_112025_create_factors_table', 1),
(11, '2020_01_01_122934_create_products_table', 1),
(12, '2020_01_02_090210_create_pages_table', 1),
(13, '2020_01_02_095457_create_meta_tages_table', 1),
(14, '2020_01_02_104053_create_discounts_table', 1),
(15, '2020_01_02_113401_create_relations_table', 1),
(16, '2020_04_18_173337_create_tokens_table', 1),
(17, '2020_07_16_081158_create_wallets_table', 1),
(18, '2020_07_17_222756_create_types_table', 1),
(19, '2020_08_31_001711_create_gifts_table', 1),
(20, '2020_08_31_011723_create_conditions_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `id` int(10) UNSIGNED NOT NULL,
  `url` varchar(300) NOT NULL,
  `page_name` varchar(100) NOT NULL,
  `active` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(250) NOT NULL,
  `token` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `payments`
--

CREATE TABLE `payments` (
  `id` int(10) UNSIGNED NOT NULL,
  `payment_amount` varchar(40) NOT NULL DEFAULT '0' COMMENT 'مبلغ قابل پرداخت',
  `total_sum` varchar(40) NOT NULL DEFAULT '0' COMMENT 'مبلغ نهایی',
  `payment_message` varchar(200) NOT NULL DEFAULT 'null' COMMENT 'پیام ارسال شده از طرف درگاه',
  `payment_message_code` int(11) NOT NULL DEFAULT 0 COMMENT 'کد ارسال شده از طرف درگاه',
  `customer_id` int(10) UNSIGNED DEFAULT NULL COMMENT 'کلید ایندکس مشتری',
  `discount_id` int(10) UNSIGNED DEFAULT 0 COMMENT 'کلید کلید مربوط به تخفیفات',
  `gift_id` int(10) UNSIGNED DEFAULT 0 COMMENT 'کلید کلید مربوط به تخفیفات',
  `gift_amount` varchar(40) NOT NULL DEFAULT '0' COMMENT 'کلید کلید مربوط به تخفیفات',
  `discount_value` varchar(40) NOT NULL DEFAULT '0' COMMENT 'مقدار مبلغ تخفیف',
  `sent_address` varchar(600) NOT NULL DEFAULT 'null' COMMENT 'آدرس گیرنده کالا',
  `sent_phone` varchar(600) NOT NULL DEFAULT 'null' COMMENT 'شماره تماس گیرنده کالا',
  `receiver_fullname` varchar(150) NOT NULL DEFAULT 'null' COMMENT 'نام گیرنده کالا',
  `postal_code_sent` varchar(40) NOT NULL DEFAULT 'null' COMMENT 'کد پستی',
  `condition_id` int(10) UNSIGNED DEFAULT 1 COMMENT 'کلید وضعیت',
  `type_id` int(10) UNSIGNED DEFAULT 1,
  `Authority` varchar(100) NOT NULL DEFAULT 'null',
  `reference_code` varchar(80) NOT NULL DEFAULT 'null' COMMENT 'کد پیگیری',
  `payment_status` int(11) NOT NULL DEFAULT 0 COMMENT 'وضعیت پرداخت فاکتور ثبت شده',
  `active` tinyint(1) NOT NULL DEFAULT 1 COMMENT 'نمایش یا عدم نمایش رکورد',
  `RefID` varchar(45) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `payments`
--

INSERT INTO `payments` (`id`, `payment_amount`, `total_sum`, `payment_message`, `payment_message_code`, `customer_id`, `discount_id`, `gift_id`, `gift_amount`, `discount_value`, `sent_address`, `sent_phone`, `receiver_fullname`, `postal_code_sent`, `condition_id`, `type_id`, `Authority`, `reference_code`, `payment_status`, `active`, `RefID`, `created_at`, `updated_at`) VALUES
(1, '228000', '228000', 'null', 0, 1, 2, 0, '0', '21280.000000000004', 'null', 'null', 'null', 'null', 6, 1, 'null', 'null', 3, 1, '0', '2020-05-13 12:19:14', '2020-09-11 03:28:35'),
(7, '2000', '0', 'عمليات با موفقيت انجام گرديده است.', 100, 1, 0, 0, '0', '0', 'null', 'null', 'null', 'null', 1, 2, 'A00000000000000000000000000214740100', '37852490', 3, 1, '21474010001', '2020-09-03 22:19:23', '2020-09-03 22:22:07'),
(8, '2000', '0', 'عمليات با موفقيت انجام گرديده است.', 100, 1, 0, 0, '0', '0', 'null', 'null', 'null', 'null', 1, 2, 'A00000000000000000000000000214740431', '73029685', 3, 1, '21474043101', '2020-09-03 22:26:04', '2020-09-03 22:27:07'),
(9, '2000', '0', 'عمليات با موفقيت انجام گرديده است.', 100, 1, 0, 0, '0', '0', 'null', 'null', 'null', 'null', 1, 2, 'A00000000000000000000000000214742627', '82309764', 3, 1, '0', '2020-09-03 23:18:32', '2020-09-03 23:22:34'),
(10, '2000', '0', 'عمليات با موفقيت انجام گرديده است.', 100, 1, 0, 0, '0', '0', 'null', 'null', 'null', 'null', 1, 2, 'A00000000000000000000000000214742814', '92765183', 3, 1, '0', '2020-09-03 23:24:02', '2020-09-03 23:25:38'),
(11, '1000', '0', 'عمليات با موفقيت انجام گرديده است.', 100, 1, 0, 0, '0', '0', 'null', 'null', 'null', 'null', 1, 2, 'A00000000000000000000000000214742899', '76208543', 3, 1, '0', '2020-09-03 23:27:01', '2020-09-03 23:29:45'),
(12, '1000', '0', 'عمليات با موفقيت انجام گرديده است.', 100, 1, 0, 0, '0', '0', 'null', 'null', 'null', 'null', 1, 2, 'A00000000000000000000000000214743022', '82576130', 3, 1, '21474302201', '2020-09-03 23:30:21', '2020-09-03 23:32:26'),
(13, '76000', '0', 'عمليات با موفقيت انجام گرديده است.', 100, 1, 0, 3, '64400', '0', 'بلوار معلم، نبش معلم 16، پلاک 36', '09365647585', 'امیر مداح', '9189171111', 3, 1, 'null', '60523189', 3, 1, '0', '2020-09-07 03:20:58', '2020-09-09 12:08:07'),
(14, '1000', '0', 'عمليات با موفقيت انجام گرديده است.', 100, 1, 0, 4, '1000', '0', 'بلوار معلم، نبش معلم 16، پلاک 36', '09365647585', 'امیر مداح', '9189171111', 3, 1, 'null', '94306582', 3, 1, '0', '2020-09-09 17:21:51', '2020-09-09 17:29:02');

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) NOT NULL,
  `body` text DEFAULT NULL,
  `published` tinyint(1) NOT NULL DEFAULT 1,
  `used_in` varchar(20) NOT NULL DEFAULT 'articles',
  `visited` int(11) NOT NULL DEFAULT 1 COMMENT 'تعداد دفعات بازدید شده',
  `created_by` bigint(20) NOT NULL,
  `updated_by` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `title`, `body`, `published`, `used_in`, `visited`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(3, 'اولین مقاله دارت مارکت', '<p>متن اولین مقاله دارت مارکت</p>', 1, 'articles', 1, 0, 1, '2020-06-11 13:55:31', '2020-06-11 14:53:32'),
(4, 'دومین مقاله دارت مارکت', '<p><img style=\"display: block; margin-left: auto; margin-right: auto;\" src=\"/photos/1/bg-01.jpg\" alt=\"\" width=\"836\" height=\"409\" /></p>\n<p style=\"text-align: center;\">متن دومین مقاله دارت مارکت</p>', 1, 'articles', 1, 0, 1, '2020-06-14 14:48:12', '2020-07-02 09:38:13'),
(5, 'سومین پست دارت مارکت', '<p style=\"text-align: center;\"><img style=\"display: block; margin-left: auto; margin-right: auto;\" src=\"/photos/1/bg-01.jpg\" alt=\"\" width=\"899\" height=\"440\" /></p>\n<p style=\"text-align: center;\">متن سومین پست دارت مارکت</p>', 1, 'articles', 1, 0, 1, '2020-06-14 14:55:42', '2020-07-08 02:26:42'),
(6, 'چهارمین پست دارت مارکت', '<p>متن چهارمین پست دارت مارکت</p>', 1, 'articles', 1, 0, 1, '2020-06-14 14:56:49', '2020-06-18 22:11:17'),
(7, 'پنجمین پست دارت مارکت', '<p>متن پنجمین پست دارت مارکت</p>', 1, 'articles', 2, 0, 1, '2020-06-14 14:57:47', '2020-07-10 06:25:02'),
(8, 'هنر پرتاب دارت', '<h5 class=\"text-right\">ورزش دارت را باید یکی از شاخه های ورزش و حتی آمادگی نظامی دانست که به نوعی آن را یک هنر می دانند: هنر پرتاب دارت</h5>', 1, 'right.slider', 1, 0, 1, '2020-07-04 20:54:56', '2020-07-04 20:54:56');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(150) NOT NULL COMMENT 'عنوان کالا',
  `description` text NOT NULL COMMENT 'توضیحات مربوط به کالا',
  `sales` int(11) NOT NULL DEFAULT 0 COMMENT 'تعداد کالاهای فروخته شده',
  `amount` varchar(40) NOT NULL COMMENT 'قیمت کالا',
  `active` tinyint(1) NOT NULL DEFAULT 1,
  `number` tinyint(1) NOT NULL DEFAULT 1 COMMENT 'تعداد کالاهای موجود',
  `categorie_id` int(10) UNSIGNED DEFAULT NULL COMMENT 'کلید دسته بندی کالا',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `title`, `description`, `sales`, `amount`, `active`, `number`, `categorie_id`, `created_at`, `updated_at`) VALUES
(1, 'دارت STARTOS مدل 24G', 'در این قسمت توضیحات مربوط به کالای مورد نظر ارائه می گردد.', 2, '1000', 1, 20, 1, '2020-01-17 05:17:28', '2020-07-08 02:44:00'),
(2, 'دارت STARTOS مدل 24G', 'در این قسمت توضیحات مربوط به کالای مورد نظر ارائه می گردد.', 3, '76000', 1, 19, 2, '2020-01-17 05:17:28', '2020-01-17 05:17:28'),
(3, 'دارت STARTOS مدل 24G', 'در این قسمت توضیحات مربوط به کالای مورد نظر ارائه می گردد.', 4, '76000', 1, 18, 3, '2020-01-17 05:17:28', '2020-01-17 05:17:28'),
(4, 'دارت STARTOS مدل 24G', 'در این قسمت توضیحات مربوط به کالای مورد نظر ارائه می گردد.', 5, '76000', 1, 17, 4, '2020-01-17 05:17:28', '2020-01-17 05:17:28'),
(5, 'دارت STARTOS مدل 24G', 'در این قسمت توضیحات مربوط به کالای مورد نظر ارائه می گردد.', 6, '76000', 1, 16, 5, '2020-01-17 05:17:28', '2020-01-17 05:17:28'),
(6, 'دارت STARTOS مدل 24G', 'در این قسمت توضیحات مربوط به کالای مورد نظر ارائه می گردد.', 7, '76000', 1, 15, 6, '2020-01-17 05:17:28', '2020-01-17 05:17:28');

-- --------------------------------------------------------

--
-- Table structure for table `tokens`
--

CREATE TABLE `tokens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `types`
--

CREATE TABLE `types` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(80) NOT NULL COMMENT 'عنوان',
  `key_word` varchar(40) NOT NULL COMMENT 'کلمه کلیدی نوع',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `types`
--

INSERT INTO `types` (`id`, `title`, `key_word`, `created_at`, `updated_at`) VALUES
(1, 'خرید کالا', 'productPurchase', '2020-07-19 10:46:46', '2020-07-19 10:46:46'),
(2, 'شارژ کیف پول', 'walletCharge', '2020-07-19 10:46:46', '2020-07-19 10:46:46');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `fullname` varchar(255) NOT NULL,
  `email` varchar(250) NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `phone` varchar(16) NOT NULL DEFAULT 'null' COMMENT 'شماره تماس',
  `address` varchar(200) NOT NULL DEFAULT 'null' COMMENT 'آدرس',
  `number_cart` varchar(20) NOT NULL DEFAULT 'null' COMMENT 'آدرس',
  `number_account` varchar(20) NOT NULL DEFAULT 'null' COMMENT 'آدرس',
  `facebook_address` varchar(300) NOT NULL DEFAULT 'null' COMMENT 'آدرس فیس بوک',
  `instagram_address` varchar(300) NOT NULL DEFAULT 'null' COMMENT 'آدرس اینستاگرام',
  `telegram_address` varchar(300) NOT NULL DEFAULT 'null' COMMENT 'آدرس تلگرام',
  `active` tinyint(1) NOT NULL DEFAULT 1,
  `remember_token` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `fullname`, `email`, `email_verified_at`, `password`, `phone`, `address`, `number_cart`, `number_account`, `facebook_address`, `instagram_address`, `telegram_address`, `active`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'امیر مداح', 'omid.malek.1990@gmail.com', NULL, 'e10adc3949ba59abbe56e057f20f883e', '09365647585', 'بلوار معلم، نبش معلم 16، پلاک 36', '6104337927742401', '6104337927', 'null', 'null', 'null', 1, NULL, '2020-01-17 05:15:35', '2020-09-13 10:12:42');

-- --------------------------------------------------------

--
-- Table structure for table `wallets`
--

CREATE TABLE `wallets` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(80) NOT NULL COMMENT 'موجودی کیف پول',
  `amount` double NOT NULL DEFAULT 0 COMMENT 'موجودی کیف پول',
  `type_id` double NOT NULL DEFAULT 2 COMMENT 'کد نوع پرداخت',
  `customer_id` int(10) UNSIGNED DEFAULT NULL COMMENT 'کلید داخلی کاربر',
  `active` tinyint(1) NOT NULL DEFAULT 1 COMMENT 'متن پیام پیام فعال=1 و پیام غیر فعال=0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `wallets`
--

INSERT INTO `wallets` (`id`, `title`, `amount`, `type_id`, `customer_id`, `active`, `created_at`, `updated_at`) VALUES
(1, 'کیف پول مجازی', 20000, 2, 1, 1, '2020-09-03 01:12:03', '2020-09-06 16:50:55'),
(2, 'کیف پول مجازی', 0, 2, 2, 1, '2020-09-09 17:33:13', '2020-09-09 17:33:13');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `conditions`
--
ALTER TABLE `conditions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `customers_email_unique` (`email`);

--
-- Indexes for table `discounts`
--
ALTER TABLE `discounts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `factors`
--
ALTER TABLE `factors`
  ADD PRIMARY KEY (`id`),
  ADD KEY `factors_product_id_index` (`product_id`),
  ADD KEY `factors_type_id_index` (`type_id`),
  ADD KEY `factors_customer_id_index` (`customer_id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gifts`
--
ALTER TABLE `gifts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `gifts_wallet_id_index` (`wallet_id`),
  ADD KEY `gifts_condition_id_index` (`condition_id`);

--
-- Indexes for table `images`
--
ALTER TABLE `images`
  ADD PRIMARY KEY (`id`),
  ADD KEY `images_record_id_index` (`record_id`);

--
-- Indexes for table `meta_tages`
--
ALTER TABLE `meta_tages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `payments`
--
ALTER TABLE `payments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `payments_customer_id_index` (`customer_id`),
  ADD KEY `payments_discount_id_index` (`discount_id`),
  ADD KEY `payments_gift_id_index` (`gift_id`),
  ADD KEY `payments_condition_id_index` (`condition_id`),
  ADD KEY `payments_type_id_index` (`type_id`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `products_categorie_id_index` (`categorie_id`);

--
-- Indexes for table `tokens`
--
ALTER TABLE `tokens`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `types`
--
ALTER TABLE `types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `wallets`
--
ALTER TABLE `wallets`
  ADD PRIMARY KEY (`id`),
  ADD KEY `wallets_customer_id_index` (`customer_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `conditions`
--
ALTER TABLE `conditions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `customers`
--
ALTER TABLE `customers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `discounts`
--
ALTER TABLE `discounts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `factors`
--
ALTER TABLE `factors`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `gifts`
--
ALTER TABLE `gifts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `images`
--
ALTER TABLE `images`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `meta_tages`
--
ALTER TABLE `meta_tages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `payments`
--
ALTER TABLE `payments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `tokens`
--
ALTER TABLE `tokens`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `types`
--
ALTER TABLE `types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `wallets`
--
ALTER TABLE `wallets`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
