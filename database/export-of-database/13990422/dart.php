<?php
/**
 * Export to PHP Array plugin for PHPMyAdmin
 * @version 5.0.1
 */

/**
 * Database `dart`
 */

/* `dart`.`categories` */
$categories = array(
  array('id' => '1','title' => 'تخت دارت','active' => '1','created_at' => '2020-01-17 08:46:33','updated_at' => '2020-01-17 08:46:33'),
  array('id' => '2','title' => 'دارت','active' => '1','created_at' => '2020-01-17 08:46:33','updated_at' => '2020-01-17 08:46:33'),
  array('id' => '3','title' => 'فلایت','active' => '1','created_at' => '2020-01-17 08:46:33','updated_at' => '2020-01-17 08:46:33'),
  array('id' => '4','title' => 'شات','active' => '1','created_at' => '2020-01-17 08:46:33','updated_at' => '2020-01-17 08:46:33'),
  array('id' => '5','title' => 'تیپ','active' => '1','created_at' => '2020-01-17 08:46:33','updated_at' => '2020-01-17 08:46:33'),
  array('id' => '6','title' => 'لوازم جانبی','active' => '1','created_at' => '2020-01-17 08:46:33','updated_at' => '2020-01-17 08:46:33')
);

/* `dart`.`comments` */
$comments = array(
);

/* `dart`.`customers` */
$customers = array(
  array('id' => '1','fullname' => 'امیر مداح','email' => 'omid.malek.1990@gmail.com','password' => '25f9e794323b453885f5181f1b624d0b','address' => 'بلوار معلم، نبش معلم 16، پلاک 36','postal_code' => '9189171111','phone' => '09365647585','token' => 'vnzpquouuKb4rFYLx19Jy9RICosv7QT6TVoe2zFeByiLJuIpZLMyJAKlDVUz','active' => '1','created_at' => '2020-02-15 17:32:42','updated_at' => '2020-07-10 10:55:57')
);

/* `dart`.`discounts` */
$discounts = array(
);

/* `dart`.`factors` */
$factors = array(
  array('id' => '1','product_id' => '4','sent_number' => '1','customer_id' => '1','payment_id' => '1','active' => '1','created_at' => '2020-05-13 16:49:14','updated_at' => '2020-05-13 16:49:14'),
  array('id' => '2','product_id' => '2','sent_number' => '1','customer_id' => '1','payment_id' => '1','active' => '1','created_at' => '2020-05-13 16:50:46','updated_at' => '2020-05-13 16:50:46'),
  array('id' => '3','product_id' => '6','sent_number' => '1','customer_id' => '1','payment_id' => '2','active' => '1','created_at' => '2020-06-24 01:56:14','updated_at' => '2020-06-24 01:56:14')
);

/* `dart`.`failed_jobs` */
$failed_jobs = array(
);

/* `dart`.`images` */
$images = array(
  array('id' => '1','src' => 'dart_1.png','record_id' => '1','used_in' => 'products','active' => '1','created_at' => '2020-01-17 08:47:28','updated_at' => '2020-01-17 08:47:28'),
  array('id' => '2','src' => 'dart_2.jpg','record_id' => '2','used_in' => 'products','active' => '1','created_at' => '2020-01-17 08:47:28','updated_at' => '2020-01-17 08:47:28'),
  array('id' => '3','src' => 'dart_3.jpg','record_id' => '3','used_in' => 'products','active' => '1','created_at' => '2020-01-17 08:47:28','updated_at' => '2020-01-17 08:47:28'),
  array('id' => '4','src' => 'dart_1.png','record_id' => '4','used_in' => 'products','active' => '1','created_at' => '2020-01-17 08:47:28','updated_at' => '2020-01-17 08:47:28'),
  array('id' => '5','src' => 'dart_2.jpg','record_id' => '5','used_in' => 'products','active' => '1','created_at' => '2020-01-17 08:47:28','updated_at' => '2020-01-17 08:47:28'),
  array('id' => '6','src' => 'dart_3.jpg','record_id' => '6','used_in' => 'products','active' => '1','created_at' => '2020-01-17 08:47:28','updated_at' => '2020-01-17 08:47:28'),
  array('id' => '7','src' => 'categories_takht_dart.jpg','record_id' => '1','used_in' => 'categories','active' => '1','created_at' => '2020-01-17 08:47:56','updated_at' => '2020-01-17 08:47:56'),
  array('id' => '8','src' => 'categorie_dart.jpg','record_id' => '2','used_in' => 'categories','active' => '1','created_at' => '2020-01-17 08:47:56','updated_at' => '2020-01-17 08:47:56'),
  array('id' => '9','src' => 'categorie_flit.jpg','record_id' => '3','used_in' => 'categories','active' => '1','created_at' => '2020-01-17 08:47:56','updated_at' => '2020-01-17 08:47:56'),
  array('id' => '10','src' => 'categories_shot.jpg','record_id' => '4','used_in' => 'categories','active' => '1','created_at' => '2020-01-17 08:47:56','updated_at' => '2020-01-17 08:47:56'),
  array('id' => '11','src' => 'categories_tip.jpg','record_id' => '5','used_in' => 'categories','active' => '1','created_at' => '2020-01-17 08:47:56','updated_at' => '2020-01-17 08:47:56'),
  array('id' => '12','src' => 'categories_etc.jpg','record_id' => '6','used_in' => 'categories','active' => '1','created_at' => '2020-01-17 08:47:56','updated_at' => '2020-01-17 08:47:56'),
  array('id' => '13','src' => 'slider_1.jpg','record_id' => '0','used_in' => 'sliders','active' => '1','created_at' => '2020-01-17 08:47:56','updated_at' => '2020-01-17 08:47:56'),
  array('id' => '14','src' => 'noPhoto.jpg','record_id' => '2','used_in' => 'articles','active' => '1','created_at' => '2020-06-09 19:14:39','updated_at' => '2020-06-09 19:14:39'),
  array('id' => '15','src' => 'products_159251850134.jpg','record_id' => '3','used_in' => 'articles','active' => '1','created_at' => '2020-06-11 18:25:31','updated_at' => '2020-06-19 02:45:01'),
  array('id' => '16','src' => 'products_159251848752.jpg','record_id' => '4','used_in' => 'articles','active' => '1','created_at' => '2020-06-14 19:18:12','updated_at' => '2020-06-19 02:44:47'),
  array('id' => '17','src' => 'products_159251847372.jpg','record_id' => '5','used_in' => 'articles','active' => '1','created_at' => '2020-06-14 19:25:42','updated_at' => '2020-06-19 02:44:33'),
  array('id' => '18','src' => 'products_159251828144.jpg','record_id' => '6','used_in' => 'articles','active' => '1','created_at' => '2020-06-14 19:26:49','updated_at' => '2020-06-19 02:41:21'),
  array('id' => '19','src' => 'products_159251824434.jpg','record_id' => '7','used_in' => 'articles','active' => '1','created_at' => '2020-06-14 19:27:47','updated_at' => '2020-06-19 02:40:44'),
  array('id' => '20','src' => 'noPhoto.jpg','record_id' => '8','used_in' => 'right.slider','active' => '1','created_at' => '2020-06-09 19:14:39','updated_at' => '2020-06-09 19:14:39')
);

/* `dart`.`meta_tages` */
$meta_tages = array(
);

/* `dart`.`migrations` */
$migrations = array(
  array('id' => '1','migration' => '2014_10_12_000000_create_users_table','batch' => '1'),
  array('id' => '2','migration' => '2014_10_12_100000_create_password_resets_table','batch' => '1'),
  array('id' => '3','migration' => '2019_09_04_213034_create_payments_table','batch' => '1'),
  array('id' => '4','migration' => '2019_12_29_094125_create_failed_jobs_table','batch' => '1'),
  array('id' => '5','migration' => '2019_12_29_094502_create_posts_table','batch' => '1'),
  array('id' => '6','migration' => '2020_01_01_104208_create_categories_table','batch' => '1'),
  array('id' => '7','migration' => '2020_01_01_104258_create-comments_table','batch' => '1'),
  array('id' => '8','migration' => '2020_01_01_104453_create_images_table','batch' => '1'),
  array('id' => '9','migration' => '2020_01_01_111720_create_customers_table','batch' => '1'),
  array('id' => '10','migration' => '2020_01_01_112025_create_factors_table','batch' => '1'),
  array('id' => '11','migration' => '2020_01_01_122934_create_products_table','batch' => '1'),
  array('id' => '12','migration' => '2020_01_02_090210_create_pages_table','batch' => '1'),
  array('id' => '13','migration' => '2020_01_02_095457_create_meta_tages_table','batch' => '1'),
  array('id' => '14','migration' => '2020_01_02_104053_create_discounts_table','batch' => '1'),
  array('id' => '15','migration' => '2020_01_02_113401_create_relations_table','batch' => '1'),
  array('id' => '16','migration' => '2020_04_18_173337_create_tokens_table','batch' => '1')
);

/* `dart`.`pages` */
$pages = array(
);

/* `dart`.`password_resets` */
$password_resets = array(
);

/* `dart`.`payments` */
$payments = array(
  array('id' => '1','payment_amount' => '0','payment_message' => 'null','payment_message_code' => '0','customer_id' => '1','discount_id' => NULL,'discount_value' => '0','sent_address' => 'null','sent_phone' => 'null','receiver_fullname' => 'null','postal_code_sent' => 'null','Authority' => 'null','reference_code' => 'null','payment_status' => '1','active' => '1','RefID' => '0','created_at' => '2020-05-13 16:49:14','updated_at' => '2020-05-13 16:49:14'),
  array('id' => '2','payment_amount' => '0','payment_message' => 'null','payment_message_code' => '0','customer_id' => '1','discount_id' => NULL,'discount_value' => '0','sent_address' => 'null','sent_phone' => 'null','receiver_fullname' => 'null','postal_code_sent' => 'null','Authority' => 'null','reference_code' => 'null','payment_status' => '0','active' => '1','RefID' => '0','created_at' => '2020-06-24 01:56:14','updated_at' => '2020-06-24 01:56:14')
);

/* `dart`.`posts` */
$posts = array(
  array('id' => '3','title' => 'اولین مقاله دارت مارکت','body' => '<p>متن اولین مقاله دارت مارکت</p>','published' => '1','used_in' => 'articles','created_by' => '0','visited' => '1','updated_by' => '1','created_at' => '2020-06-11 18:25:31','updated_at' => '2020-06-11 19:23:32'),
  array('id' => '4','title' => 'دومین مقاله دارت مارکت','body' => '<p><img style="display: block; margin-left: auto; margin-right: auto;" src="/photos/1/bg-01.jpg" alt="" width="836" height="409" /></p>
<p style="text-align: center;">متن دومین مقاله دارت مارکت</p>','published' => '1','used_in' => 'articles','created_by' => '0','visited' => '1','updated_by' => '1','created_at' => '2020-06-14 19:18:12','updated_at' => '2020-07-02 14:08:13'),
  array('id' => '5','title' => 'سومین پست دارت مارکت','body' => '<p style="text-align: center;"><img style="display: block; margin-left: auto; margin-right: auto;" src="/photos/1/bg-01.jpg" alt="" width="899" height="440" /></p>
<p style="text-align: center;">متن سومین پست دارت مارکت</p>','published' => '1','used_in' => 'articles','created_by' => '0','visited' => '1','updated_by' => '1','created_at' => '2020-06-14 19:25:42','updated_at' => '2020-07-08 06:56:42'),
  array('id' => '6','title' => 'چهارمین پست دارت مارکت','body' => '<p>متن چهارمین پست دارت مارکت</p>','published' => '1','used_in' => 'articles','created_by' => '0','visited' => '1','updated_by' => '1','created_at' => '2020-06-14 19:26:49','updated_at' => '2020-06-19 02:41:17'),
  array('id' => '7','title' => 'پنجمین پست دارت مارکت','body' => '<p>متن پنجمین پست دارت مارکت</p>','published' => '1','used_in' => 'articles','created_by' => '0','visited' => '2','updated_by' => '1','created_at' => '2020-06-14 19:27:47','updated_at' => '2020-07-10 10:55:02'),
  array('id' => '8','title' => 'هنر پرتاب دارت','body' => '<h5 class="text-right">ورزش دارت را باید یکی از شاخه های ورزش و حتی آمادگی نظامی دانست که به نوعی آن را یک هنر می دانند: هنر پرتاب دارت</h5>','published' => '1','used_in' => 'right.slider','created_by' => '0','visited' => '1','updated_by' => '1','created_at' => '2020-07-05 01:24:56','updated_at' => '2020-07-05 01:24:56')
);

/* `dart`.`products` */
$products = array(
  array('id' => '1','title' => 'دارت STARTOS مدل 24G','description' => 'در این قسمت توضیحات مربوط به کالای مورد نظر ارائه می گردد.','sales' => '2','amount' => '1000','active' => '1','number' => '20','categorie_id' => '1','created_at' => '2020-01-17 08:47:28','updated_at' => '2020-07-08 07:14:00'),
  array('id' => '2','title' => 'دارت STARTOS مدل 24G','description' => 'در این قسمت توضیحات مربوط به کالای مورد نظر ارائه می گردد.','sales' => '3','amount' => '76000','active' => '1','number' => '19','categorie_id' => '2','created_at' => '2020-01-17 08:47:28','updated_at' => '2020-01-17 08:47:28'),
  array('id' => '3','title' => 'دارت STARTOS مدل 24G','description' => 'در این قسمت توضیحات مربوط به کالای مورد نظر ارائه می گردد.','sales' => '4','amount' => '76000','active' => '1','number' => '18','categorie_id' => '3','created_at' => '2020-01-17 08:47:28','updated_at' => '2020-01-17 08:47:28'),
  array('id' => '4','title' => 'دارت STARTOS مدل 24G','description' => 'در این قسمت توضیحات مربوط به کالای مورد نظر ارائه می گردد.','sales' => '5','amount' => '76000','active' => '1','number' => '17','categorie_id' => '4','created_at' => '2020-01-17 08:47:28','updated_at' => '2020-01-17 08:47:28'),
  array('id' => '5','title' => 'دارت STARTOS مدل 24G','description' => 'در این قسمت توضیحات مربوط به کالای مورد نظر ارائه می گردد.','sales' => '6','amount' => '76000','active' => '1','number' => '16','categorie_id' => '5','created_at' => '2020-01-17 08:47:28','updated_at' => '2020-01-17 08:47:28'),
  array('id' => '6','title' => 'دارت STARTOS مدل 24G','description' => 'در این قسمت توضیحات مربوط به کالای مورد نظر ارائه می گردد.','sales' => '7','amount' => '76000','active' => '1','number' => '15','categorie_id' => '6','created_at' => '2020-01-17 08:47:28','updated_at' => '2020-01-17 08:47:28')
);

/* `dart`.`tokens` */
$tokens = array(
);

/* `dart`.`users` */
$users = array(
  array('id' => '1','fullname' => 'امیر مداح','email' => 'omid.malek.1990@gmail.com','email_verified_at' => NULL,'password' => 'e10adc3949ba59abbe56e057f20f883e','phone' => '09365647585','address' => 'بلوار معلم، نبش معلم 16، پلاک 36','number_cart' => '6104337927742401','number_account' => '6104337927','active' => '1','remember_token' => NULL,'created_at' => '2020-01-17 08:45:35','updated_at' => '2020-01-17 08:45:35')
);
