-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 03, 2022 at 07:37 AM
-- Server version: 10.4.16-MariaDB
-- PHP Version: 7.4.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `drmalekp_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(40) NOT NULL DEFAULT 'بدون عنوان' COMMENT 'عنوان صفحه',
  `address` varchar(200) NOT NULL DEFAULT 'null' COMMENT 'آدرس نسبی صفحه',
  `route_name` varchar(100) NOT NULL DEFAULT 'null',
  `base_url` varchar(150) NOT NULL DEFAULT 'null' COMMENT 'آدرس سایت',
  `is_base_page` int(11) NOT NULL DEFAULT 1 COMMENT 'صفحه زیر مجموعه صفحه دیگری نیست؟',
  `allow_access` tinyint(1) NOT NULL DEFAULT 1 COMMENT 'اجازه دسترسی به کاربران',
  `permission_id` bigint(20) NOT NULL DEFAULT 0 COMMENT 'کلید سطح دسترسی',
  `permission_title` varchar(40) NOT NULL DEFAULT 'بدون عنوان' COMMENT 'عنوان سطح دسترسی',
  `active` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`id`, `title`, `address`, `route_name`, `base_url`, `is_base_page`, `allow_access`, `permission_id`, `permission_title`, `active`, `created_at`, `updated_at`) VALUES
(1, 'کسری دارو', 'drugs', 'admin.drugs.index', 'null', 1, 1, 1, 'مدیر سامانه', 1, '2022-05-10 18:07:37', '2022-05-10 18:07:46'),
(2, 'تغییر رمز', 'admin_password', 'admin.password.index', 'null', 1, 1, 1, 'مدیر سامانه', 1, '2022-05-07 19:44:33', '2022-05-07 19:44:33'),
(3, 'اطلاعات مدیریت', 'admins_show', 'admin.edit', 'null', 1, 1, 1, 'مدیر سامانه', 1, '2022-05-07 19:44:33', '2022-05-07 19:44:33'),
(4, 'محصولات', 'products_management_list', 'admin.products.index', 'null', 1, 1, 1, 'مدیر سامانه', 1, '2022-05-07 19:44:33', '2022-05-07 19:44:33'),
(5, 'افزودن محصول', 'products_create', 'admin.products.create', 'null', 0, 1, 1, 'مدیر سامانه', 1, '2022-05-07 19:44:33', '2022-05-07 19:44:33'),
(6, 'فاکتورها', 'factors_list', 'factors.management.index', 'null', 1, 1, 1, 'مدیر سامانه', 1, '2022-05-07 19:44:33', '2022-05-07 19:44:33'),
(11, 'پرسنل', 'items', 'items.index', 'null', 1, 1, 1, 'مدیر سامانه', 1, '2022-05-07 19:44:33', '2022-05-07 19:44:33'),
(13, 'مستندات', 'list_documents', 'documents.index', 'null', 0, 1, 1, 'مدیر سامانه', 1, '2022-05-07 19:44:33', '2022-05-07 19:44:33'),
(17, 'دسترسی کاربران', 'access', 'admin.access.index', 'null', 1, 0, 1, 'مدیر سامانه', 1, '2022-05-10 18:07:37', '2022-05-10 18:07:46'),
(18, 'مدیریت حضور خود', 'entries', 'entries.index', 'null', 1, 1, 2, 'پرسنل', 1, '2022-05-07 19:44:33', '2022-05-07 19:44:33'),
(19, 'لیست حضور پرسنل', 'managementEntries', 'managementEntries.index', 'null', 1, 1, 1, 'مدیر سامانه', 1, '2022-05-07 19:44:33', '2022-05-07 19:44:33');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
