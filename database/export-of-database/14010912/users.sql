-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 03, 2022 at 07:26 AM
-- Server version: 10.4.16-MariaDB
-- PHP Version: 7.4.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `drmalekp_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `fullname` varchar(255) NOT NULL,
  `username` varchar(100) NOT NULL DEFAULT 'null' COMMENT 'نام کاربری کاربر',
  `email` varchar(150) NOT NULL DEFAULT 'omid.malek.1990@gmail.com',
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `phone` varchar(16) NOT NULL DEFAULT 'null' COMMENT 'شماره تماس',
  `address` varchar(200) NOT NULL DEFAULT 'null' COMMENT 'آدرس',
  `number_cart` varchar(20) NOT NULL DEFAULT 'null' COMMENT 'آدرس',
  `number_account` varchar(20) NOT NULL DEFAULT 'null' COMMENT 'آدرس',
  `facebook_address` varchar(300) NOT NULL DEFAULT 'null' COMMENT 'آدرس فیس بوک',
  `instagram_address` varchar(300) NOT NULL DEFAULT 'null' COMMENT 'آدرس اینستاگرام',
  `telegram_address` varchar(300) NOT NULL DEFAULT 'null' COMMENT 'آدرس تلگرام',
  `permission_id` tinyint(3) UNSIGNED NOT NULL DEFAULT 0 COMMENT 'کد دسترسی',
  `active` tinyint(1) NOT NULL DEFAULT 1,
  `remember_token` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `fullname`, `username`, `email`, `email_verified_at`, `password`, `phone`, `address`, `number_cart`, `number_account`, `facebook_address`, `instagram_address`, `telegram_address`, `permission_id`, `active`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'مدیر سامانه', 'admin', 'omid.malek.1990@gmail.com', NULL, '25f9e794323b453885f5181f1b624d0b', '09365647585', 'طبس خیابان نواب صفوی', '6104337927742401', '6104337927', 'null', 'null', 'null', 1, 1, NULL, '2020-01-17 05:15:35', '2022-05-20 19:29:43'),
(7, 'فرشاد مالک', '09131554230', 'farshad.malek2011@gmail.com', NULL, 'ada867c20a36fc1a1015b38af24382b1', '09131554230', 'طبس - خیابان نواب صفوی', 'null', 'null', 'null', 'null', 'null', 2, 1, NULL, '2022-07-22 19:54:07', '2022-08-13 05:01:52'),
(19, 'امید مالک', '09365647585', 'email@sample.com', NULL, '25f9e794323b453885f5181f1b624d0b', '09365647585', 'بدون توضیح', 'null', 'null', 'null', 'null', 'null', 2, 1, NULL, '2022-11-11 13:13:28', '2022-11-30 09:21:28');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `permission_id` (`permission_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
