-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 08, 2022 at 08:31 PM
-- Server version: 10.4.16-MariaDB
-- PHP Version: 7.4.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `drmalekp_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `documents`
--

CREATE TABLE `documents` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `src` varchar(100) NOT NULL,
  `icon` varchar(100) NOT NULL DEFAULT 'folder.png',
  `title` varchar(100) NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT 'کلید خارجی',
  `description` varchar(1000) NOT NULL DEFAULT 'null' COMMENT 'نام جدول',
  `keyword` varchar(80) NOT NULL DEFAULT 'null' COMMENT 'نام جدول',
  `active` tinyint(1) NOT NULL DEFAULT 1 COMMENT 'وضعیت نمایش و یا عدم نمایش نمایش نظر',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `documents`
--

INSERT INTO `documents` (`id`, `src`, `icon`, `title`, `user_id`, `description`, `keyword`, `active`, `created_at`, `updated_at`) VALUES
(3, 'documents_14_1659123431.jpg', 'folder.png', 'کارت ملی امید', 14, 'فاقد توضیحات', 'documents', 1, '2022-07-29 19:37:11', '2022-07-29 19:37:11'),
(4, 'documents_1_1659123539.jpg', 'folder.png', 'خدمت امید', 1, 'فاقد توضیحات', 'documents', 1, '2022-07-29 19:38:59', '2022-07-29 19:38:59'),
(5, 'documents_14_1659714700.jpg', 'folder.png', 'صفحه سوم شناسنامه', 14, 'دارای توضیحات', 'documents', 1, '2022-08-05 15:51:40', '2022-08-08 06:11:29'),
(6, 'documents_14_1659714743.jpg', 'folder.png', 'صفحه اول شناسنامه', 14, 'دارای توضیح', 'documents', 1, '2022-08-05 15:52:23', '2022-08-08 07:24:21'),
(8, 'documents_1_1659905001.jpg', 'folder.png', 'تست', 1, 'تست', 'documents', 1, '2022-08-05 19:30:40', '2022-08-08 04:34:45'),
(9, 'documents_1_1659932392.jpg', 'folder.png', 'کارت', 1, 'فاقد توضیحات', 'List', 1, '2022-08-08 04:19:52', '2022-08-08 04:19:52');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `documents`
--
ALTER TABLE `documents`
  ADD PRIMARY KEY (`id`),
  ADD KEY `documents_user_id_index` (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `documents`
--
ALTER TABLE `documents`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
