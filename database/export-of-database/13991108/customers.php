<?php
/**
 * Export to PHP Array plugin for PHPMyAdmin
 * @version 5.0.4
 */

/**
 * Database `pharmacy`
 */

/* `pharmacy`.`customers` */
$customers = array(
  array('id' => '1','fullname' => 'امید مالک','email' => 'omid.malek.1990@gmail.com','password' => '25f9e794323b453885f5181f1b624d0b','address' => 'بلوار معلم، نبش معلم 16، پلاک 36','postal_code' => '9189171111','phone' => '09365647585','token' => 'EX9VvT4cpAGirjQiWGgZSjtdnfx0O2ihPV0zp6xPH8wGUMVW2Na3wP368d1A','active' => '1','created_at' => '2020-02-15 17:32:42','updated_at' => '2020-09-14 06:31:59'),
  array('id' => '2','fullname' => 'null','email' => 'test@gmail.com','password' => '25f9e794323b453885f5181f1b624d0b','address' => 'null','postal_code' => 'null','phone' => '09354612685','token' => '55JaaHmXAZc8jkZIJbIPgbFE2o3VaHkMQX9Cjr9shUBZGwFRkiDqLEvkRRIM','active' => '1','created_at' => '2020-09-09 22:02:51','updated_at' => '2020-09-09 22:03:04')
);
