-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 28, 2021 at 11:00 PM
-- Server version: 10.4.16-MariaDB
-- PHP Version: 7.4.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pharmacy`
--

-- --------------------------------------------------------

--
-- Table structure for table `images`
--

CREATE TABLE `images` (
  `id` int(10) UNSIGNED NOT NULL,
  `src` varchar(100) NOT NULL,
  `record_id` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT 'کلید خارجی',
  `used_in` varchar(20) NOT NULL DEFAULT 'null' COMMENT 'نام جدول',
  `active` tinyint(1) NOT NULL DEFAULT 1 COMMENT 'وضعیت نمایش و یا عدم نمایش نمایش نظر',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `images`
--

INSERT INTO `images` (`id`, `src`, `record_id`, `used_in`, `active`, `created_at`, `updated_at`) VALUES
(1, 'pharmacy_products_161122131882.jpg', 1, 'products', 1, '2020-01-17 05:17:28', '2021-01-21 09:28:38'),
(2, 'pharmacy_products_161122621021.jpg', 2, 'products', 1, '2020-01-17 05:17:28', '2021-01-21 10:50:10'),
(3, 'pharmacy_products_161122642566.jpg', 3, 'products', 1, '2020-01-17 05:17:28', '2021-01-21 10:53:45'),
(4, 'pharmacy_products_161122656575.jpg', 4, 'products', 1, '2020-01-17 05:17:28', '2021-01-21 10:56:05'),
(5, 'pharmacy_products_161122678249.jpg', 5, 'products', 1, '2020-01-17 05:17:28', '2021-01-21 10:59:42'),
(6, 'dart_3.jpg', 6, 'products', 1, '2020-01-17 05:17:28', '2020-01-17 05:17:28'),
(7, 'categories_takht_dart.jpg', 1, 'categories', 1, '2020-01-17 05:17:56', '2020-01-17 05:17:56'),
(8, 'categorie_dart.jpg', 2, 'categories', 1, '2020-01-17 05:17:56', '2020-01-17 05:17:56'),
(9, 'categorie_flit.jpg', 3, 'categories', 1, '2020-01-17 05:17:56', '2020-01-17 05:17:56'),
(10, 'categories_shot.jpg', 4, 'categories', 1, '2020-01-17 05:17:56', '2020-01-17 05:17:56'),
(11, 'categories_tip.jpg', 5, 'categories', 1, '2020-01-17 05:17:56', '2020-01-17 05:17:56'),
(12, 'categories_etc.jpg', 6, 'categories', 1, '2020-01-17 05:17:56', '2020-01-17 05:17:56'),
(13, 'pharmacy_slider_1.jpg', 0, 'sliders', 1, '2020-01-17 05:17:56', '2020-01-17 05:17:56'),
(14, 'noPhoto.jpg', 2, 'articles', 1, '2020-06-09 14:44:39', '2020-06-09 14:44:39'),
(15, 'products_159251850134.jpg', 3, 'articles', 1, '2020-06-11 13:55:31', '2020-06-18 22:15:01'),
(16, 'products_159251848752.jpg', 4, 'articles', 1, '2020-06-14 14:48:12', '2020-06-18 22:14:47'),
(17, 'products_159251847372.jpg', 5, 'articles', 1, '2020-06-14 14:55:42', '2020-06-18 22:14:33'),
(18, 'products_159251828144.jpg', 6, 'articles', 1, '2020-06-14 14:56:49', '2020-06-18 22:11:21'),
(19, 'products_159251824434.jpg', 7, 'articles', 1, '2020-06-14 14:57:47', '2020-06-18 22:10:44'),
(20, 'noPhoto.jpg', 8, 'right.slider', 1, '2020-06-09 14:44:39', '2020-06-09 14:44:39'),
(21, 'pharmacy_products_161122115959.jpg', 7, 'products', 1, '2020-09-24 16:33:31', '2021-01-21 09:25:59'),
(22, 'products_160336288763.jpg', 8, 'products', 1, '2020-10-22 10:34:00', '2020-10-22 10:34:47'),
(23, 'pharmacy_products_161122037280.jpg', 9, 'products', 1, '2020-10-22 12:12:29', '2021-01-21 09:12:52'),
(24, 'pharmacy_products_161122095564.jpg', 10, 'products', 1, '2020-10-22 12:26:05', '2021-01-21 09:22:35'),
(25, 'pharmacy_products_161122070863.jpg', 11, 'products', 1, '2020-10-22 12:27:33', '2021-01-21 09:18:28'),
(26, 'products_160345761695.jpg', 12, 'products', 1, '2020-10-23 12:53:26', '2020-10-23 12:53:36'),
(27, 'products_160347352920.jpg', 13, 'products', 1, '2020-10-23 17:18:31', '2020-10-23 17:18:49'),
(28, 'pharmacy_products_161121998810.jpg', 14, 'products', 1, '2020-10-23 17:27:25', '2021-01-21 09:06:28'),
(29, 'pharmacy_slider_2.jpg', 0, 'sliders', 1, '2020-01-17 05:17:56', '2020-01-17 05:17:56'),
(30, 'pharmacy_products_161122746014.jpg', 15, 'products', 1, '2021-01-21 11:10:55', '2021-01-21 11:11:00'),
(31, 'pharmacy_products_161122761892.jpg', 16, 'products', 1, '2021-01-21 11:13:33', '2021-01-21 11:13:38'),
(32, 'pharmacy_products_161122781163.jpg', 17, 'products', 1, '2021-01-21 11:16:42', '2021-01-21 11:16:51'),
(33, 'pharmacy_products_161122797092.jpg', 18, 'products', 1, '2021-01-21 11:19:22', '2021-01-21 11:19:30');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `images`
--
ALTER TABLE `images`
  ADD PRIMARY KEY (`id`),
  ADD KEY `images_record_id_index` (`record_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `images`
--
ALTER TABLE `images`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
