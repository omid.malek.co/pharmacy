-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 28, 2021 at 11:03 PM
-- Server version: 10.4.16-MariaDB
-- PHP Version: 7.4.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pharmacy`
--

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) NOT NULL,
  `body` text DEFAULT NULL,
  `published` tinyint(1) NOT NULL DEFAULT 1,
  `used_in` varchar(20) NOT NULL DEFAULT 'articles',
  `visited` int(11) NOT NULL DEFAULT 1 COMMENT 'تعداد دفعات بازدید شده',
  `created_by` bigint(20) NOT NULL,
  `updated_by` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `title`, `body`, `published`, `used_in`, `visited`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(3, 'اولین مقاله دارت مارکت', '<p>متن اولین مقاله دارت مارکت</p>', 1, 'articles', 1, 0, 1, '2020-06-11 13:55:31', '2020-06-11 14:53:32'),
(4, 'دومین مقاله دارت مارکت', '<p><img style=\"display: block; margin-left: auto; margin-right: auto;\" src=\"/photos/1/bg-01.jpg\" alt=\"\" width=\"836\" height=\"409\" /></p>\n<p style=\"text-align: center;\">متن دومین مقاله دارت مارکت</p>', 1, 'articles', 1, 0, 1, '2020-06-14 14:48:12', '2020-07-02 09:38:13'),
(5, 'سومین پست دارت مارکت', '<p style=\"text-align: center;\"><img style=\"display: block; margin-left: auto; margin-right: auto;\" src=\"/photos/1/bg-01.jpg\" alt=\"\" width=\"899\" height=\"440\" /></p>\n<p style=\"text-align: center;\">متن سومین پست دارت مارکت</p>', 1, 'articles', 1, 0, 1, '2020-06-14 14:55:42', '2020-07-08 02:26:42'),
(6, 'چهارمین پست دارت مارکت', '<p>متن چهارمین پست دارت مارکت</p>', 1, 'articles', 1, 0, 1, '2020-06-14 14:56:49', '2020-06-18 22:11:17'),
(7, 'پنجمین پست دارت مارکت', '<p>متن پنجمین پست دارت مارکت</p>', 1, 'articles', 2, 0, 1, '2020-06-14 14:57:47', '2020-07-10 06:25:02'),
(8, 'هنر پرتاب دارت', '<h5 class=\"text-right\">ورزش دارت را باید یکی از شاخه های ورزش و حتی آمادگی نظامی دانست که به نوعی آن را یک هنر می دانند: هنر پرتاب دارت</h5>', 1, 'right.slider', 2, 0, 1, '2020-07-04 20:54:56', '2020-09-30 11:43:19');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
