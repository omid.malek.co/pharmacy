<?php
/**
 * Export to PHP Array plugin for PHPMyAdmin
 * @version 5.0.4
 */

/**
 * Database `pharmacy`
 */

/* `pharmacy`.`gifts` */
$gifts = array(
  array('id' => '3','text' => '81425963','amount' => '12100','wallet_id' => '1','used_by' => '1','created_by' => '1','condition_id' => '2','start_date' => '2020-09-07','expire_date' => '2020-09-12','active' => '1','created_at' => '2020-09-06 14:24:09','updated_at' => '2020-09-09 16:37:51'),
  array('id' => '4','text' => '34286195','amount' => '3000','wallet_id' => '1','used_by' => '1','created_by' => '1','condition_id' => '2','start_date' => '2020-09-07','expire_date' => '2020-09-14','active' => '1','created_at' => '2020-09-06 21:20:55','updated_at' => '2020-09-09 21:58:08')
);
