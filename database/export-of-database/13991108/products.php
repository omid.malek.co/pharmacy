<?php
/**
 * Export to PHP Array plugin for PHPMyAdmin
 * @version 5.0.4
 */

/**
 * Database `pharmacy`
 */

/* `pharmacy`.`products` */
$products = array(
  array('id' => '1','title' => 'بیوتین جالینوس 5 میلی','description' => '<h4 style="text-align: right;">بیوتین جالینوس 5 میلی گرمی</h4>','sales' => '2','amount' => '36000','active' => '1','number' => '10','categorie_id' => '2','created_at' => '2020-01-17 08:47:28','updated_at' => '2021-01-21 12:58:00'),
  array('id' => '2','title' => 'پودر آب پنیر','description' => '<h4 style="text-align: right;">پودر آب پنیر</h4>','sales' => '3','amount' => '27000','active' => '1','number' => '15','categorie_id' => '3','created_at' => '2020-01-17 08:47:28','updated_at' => '2021-01-21 14:20:00'),
  array('id' => '3','title' => 'متاپیور زیرو کرب','description' => '<div class="title-fa">
<h3>متاپیور زیرو کرب</h3>
<span id="LblSlaveName" title="19832">( شکلات بلژیکی )</span></div>','sales' => '4','amount' => '76000','active' => '1','number' => '15','categorie_id' => '3','created_at' => '2020-01-17 08:47:28','updated_at' => '2021-01-21 14:23:00'),
  array('id' => '4','title' => 'پروتئین وی','description' => '<div class="title-fa" style="text-align: right;">
<h4>پروتئین وی 2270 گرمی</h4>
<span id="LblSlaveName" title="19476"></span></div>','sales' => '5','amount' => '918000','active' => '1','number' => '15','categorie_id' => '3','created_at' => '2020-01-17 08:47:28','updated_at' => '2021-01-21 14:25:00'),
  array('id' => '5','title' => 'آیروسول','description' => '<p style="text-align: right;">در این قسمت توضیحات مربوط به کالای مورد نظر ارائه می گردد.</p>','sales' => '6','amount' => '41500','active' => '1','number' => '16','categorie_id' => '3','created_at' => '2020-01-17 08:47:28','updated_at' => '2021-01-21 14:29:00'),
  array('id' => '7','title' => 'ویتامین ث','description' => '<h4 style="text-align: right;">ویتافلش ویتامین ث</h4>','sales' => '0','amount' => '32700','active' => '1','number' => '2','categorie_id' => '2','created_at' => '2020-09-24 20:03:31','updated_at' => '2021-01-21 12:55:00'),
  array('id' => '9','title' => 'کرم اکسیدان','description' => '<div class="title-fa">
<h4 style="text-align: right;">کرم اکسیدان 12% نمره 3 حاوی روغن بادام</h4>
<div class="each-row" style="text-align: right;">نوع محصول
<div style="text-align: right;">اکسیدان</div>
</div>
<div class="each-row" style="text-align: right;">سایز
<div style="text-align: right;">150 میلی لیتر</div>
</div>
<div class="each-row" style="text-align: right;">محل مصرف
<div>موی سر</div>
</div>
<span id="LblSlaveName" title="19902"></span></div>','sales' => '0','amount' => '9800','active' => '1','number' => '2','categorie_id' => '1','created_at' => '2020-10-22 15:42:29','updated_at' => '2021-01-21 12:41:00'),
  array('id' => '10','title' => 'ویتامین ای 400','description' => '<h4 style="text-align: right;">ویتامین ای 400 آلفا 30 عددی</h4>','sales' => '0','amount' => '29500','active' => '1','number' => '2','categorie_id' => '2','created_at' => '2020-10-22 15:56:05','updated_at' => '2021-01-21 12:52:00'),
  array('id' => '11','title' => 'برس مو آی استایل','description' => '<div class="title-layer">
<div class="title-fa">
<h4 style="text-align: right;">برس مو آی استایل لیفت اند کرل</h4>
<span id="LblSlaveName" title="19883"></span>
<h6 class="each-row" style="text-align: right;">نوع محصول</h6>
<div style="text-align: right;">شانه</div>
<h6 class="each-row" style="text-align: right;">محل مصرف</h6>
<div style="text-align: right;">موی سر</div>
</div>
</div>','sales' => '0','amount' => '46300','active' => '1','number' => '2','categorie_id' => '1','created_at' => '2020-10-22 15:57:32','updated_at' => '2021-01-21 12:48:00'),
  array('id' => '14','title' => 'بوگیر و خوشبو کننده هوای','description' => '<h4 style="text-align: right;">بوگیر و خوشبو کننده هوای توالت با رایحه شیمی سوکی</h4>
<h6 style="text-align: right;">نوع محصول: مایع</h6>
<h6 style="text-align: right;">سایز: 400 میلی لیتر</h6>','sales' => '0','amount' => '499000','active' => '1','number' => '2','categorie_id' => '1','created_at' => '2020-10-23 20:57:25','updated_at' => '2021-01-21 12:32:00'),
  array('id' => '15','title' => 'غذای کودک برنجین','description' => '<h4 style="text-align: right;">غذای کودک برنجین با شیر و مخلوط میوه</h4>
<p style="text-align: right;">300 گرم</p>','sales' => '0','amount' => '1000','active' => '1','number' => '2','categorie_id' => '4','created_at' => '2021-01-21 14:40:54','updated_at' => '2021-01-21 14:40:54'),
  array('id' => '16','title' => 'غذای کودک گندمین','description' => '<div class="title-layer">
<div class="title-fa" style="text-align: right;">
<h4>غذای کودک گندمین با شیر و مخلوط میوه</h4>
<span id="LblSlaveName" title="19053"></span></div>
</div>','sales' => '0','amount' => '12500','active' => '1','number' => '2','categorie_id' => '4','created_at' => '2021-01-21 14:43:33','updated_at' => '2021-01-21 14:44:00'),
  array('id' => '17','title' => 'فشارسنج مچی','description' => '<h4 style="text-align: right;">فشارسنج مچی مدل BP W1 Basic</h4>','sales' => '0','amount' => '994000','active' => '1','number' => '5','categorie_id' => '5','created_at' => '2021-01-21 14:46:42','updated_at' => '2021-01-21 14:46:42'),
  array('id' => '18','title' => 'ماسک بهداشتی','description' => '<div class="title-fa" style="text-align: right;">
<h4 style="text-align: right;">ماسک بهداشتی 3 بعدی</h4>
</div>
<div class="title-fa" style="text-align: right;">
<h5 style="text-align: right;">سفید</h5>
</div>','sales' => '0','amount' => '66500','active' => '1','number' => '2','categorie_id' => '5','created_at' => '2021-01-21 14:49:22','updated_at' => '2021-01-21 14:52:00')
);
