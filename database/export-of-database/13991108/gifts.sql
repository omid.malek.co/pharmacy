-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 28, 2021 at 10:44 PM
-- Server version: 10.4.16-MariaDB
-- PHP Version: 7.4.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pharmacy`
--

-- --------------------------------------------------------

--
-- Table structure for table `gifts`
--

CREATE TABLE `gifts` (
  `id` int(10) UNSIGNED NOT NULL,
  `text` varchar(30) NOT NULL DEFAULT 'null' COMMENT 'کد تخفیف',
  `amount` double NOT NULL DEFAULT 0 COMMENT 'میزان تخفیف',
  `wallet_id` int(10) UNSIGNED DEFAULT 0 COMMENT 'کد کارت هدیه',
  `used_by` int(11) NOT NULL DEFAULT 0 COMMENT 'استفاده شده توسط',
  `created_by` int(11) NOT NULL DEFAULT 0 COMMENT 'ایجاد شده توسط',
  `condition_id` int(10) UNSIGNED DEFAULT 1 COMMENT 'کلید وضعیت',
  `start_date` date NOT NULL COMMENT 'تاریخ شروع اعمال تخفیف',
  `expire_date` date NOT NULL COMMENT 'تاریخ پایان اعمال تخفیف',
  `active` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `gifts`
--

INSERT INTO `gifts` (`id`, `text`, `amount`, `wallet_id`, `used_by`, `created_by`, `condition_id`, `start_date`, `expire_date`, `active`, `created_at`, `updated_at`) VALUES
(3, '81425963', 12100, 1, 1, 1, 2, '2020-09-07', '2020-09-12', 1, '2020-09-06 09:54:09', '2020-09-09 12:07:51'),
(4, '34286195', 3000, 1, 1, 1, 2, '2020-09-07', '2020-09-14', 1, '2020-09-06 16:50:55', '2020-09-09 17:28:08');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `gifts`
--
ALTER TABLE `gifts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `gifts_wallet_id_index` (`wallet_id`),
  ADD KEY `gifts_condition_id_index` (`condition_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `gifts`
--
ALTER TABLE `gifts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
