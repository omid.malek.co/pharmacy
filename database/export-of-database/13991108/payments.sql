-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 28, 2021 at 11:02 PM
-- Server version: 10.4.16-MariaDB
-- PHP Version: 7.4.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pharmacy`
--

-- --------------------------------------------------------

--
-- Table structure for table `payments`
--

CREATE TABLE `payments` (
  `id` int(10) UNSIGNED NOT NULL,
  `payment_amount` varchar(40) NOT NULL DEFAULT '0' COMMENT 'مبلغ قابل پرداخت',
  `total_sum` varchar(40) NOT NULL DEFAULT '0' COMMENT 'مبلغ نهایی',
  `payment_message` varchar(200) NOT NULL DEFAULT 'null' COMMENT 'پیام ارسال شده از طرف درگاه',
  `payment_message_code` int(11) NOT NULL DEFAULT 0 COMMENT 'کد ارسال شده از طرف درگاه',
  `customer_id` int(10) UNSIGNED DEFAULT NULL COMMENT 'کلید ایندکس مشتری',
  `discount_id` int(10) UNSIGNED DEFAULT 0 COMMENT 'کلید کلید مربوط به تخفیفات',
  `gift_id` int(10) UNSIGNED DEFAULT 0 COMMENT 'کلید کلید مربوط به تخفیفات',
  `gift_amount` varchar(40) NOT NULL DEFAULT '0' COMMENT 'کلید کلید مربوط به تخفیفات',
  `discount_value` varchar(40) NOT NULL DEFAULT '0' COMMENT 'مقدار مبلغ تخفیف',
  `sent_address` varchar(600) NOT NULL DEFAULT 'null' COMMENT 'آدرس گیرنده کالا',
  `sent_phone` varchar(600) NOT NULL DEFAULT 'null' COMMENT 'شماره تماس گیرنده کالا',
  `receiver_fullname` varchar(150) NOT NULL DEFAULT 'null' COMMENT 'نام گیرنده کالا',
  `postal_code_sent` varchar(40) NOT NULL DEFAULT 'null' COMMENT 'کد پستی',
  `condition_id` int(10) UNSIGNED DEFAULT 1 COMMENT 'کلید وضعیت',
  `type_id` int(10) UNSIGNED DEFAULT 1,
  `Authority` varchar(100) NOT NULL DEFAULT 'null',
  `reference_code` varchar(80) NOT NULL DEFAULT 'null' COMMENT 'کد پیگیری',
  `payment_status` int(11) NOT NULL DEFAULT 0 COMMENT 'وضعیت پرداخت فاکتور ثبت شده',
  `active` tinyint(1) NOT NULL DEFAULT 1 COMMENT 'نمایش یا عدم نمایش رکورد',
  `RefID` varchar(45) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `payments`
--

INSERT INTO `payments` (`id`, `payment_amount`, `total_sum`, `payment_message`, `payment_message_code`, `customer_id`, `discount_id`, `gift_id`, `gift_amount`, `discount_value`, `sent_address`, `sent_phone`, `receiver_fullname`, `postal_code_sent`, `condition_id`, `type_id`, `Authority`, `reference_code`, `payment_status`, `active`, `RefID`, `created_at`, `updated_at`) VALUES
(1, '228000', '228000', 'null', 0, 1, 2, 0, '0', '21280.000000000004', 'null', 'null', 'null', 'null', 6, 1, 'null', 'null', 3, 1, '0', '2020-05-13 12:19:14', '2020-09-11 03:28:35'),
(7, '2000', '0', 'عمليات با موفقيت انجام گرديده است.', 100, 1, 0, 0, '0', '0', 'null', 'null', 'null', 'null', 1, 2, 'A00000000000000000000000000214740100', '37852490', 3, 1, '21474010001', '2020-09-03 22:19:23', '2020-09-03 22:22:07'),
(8, '2000', '0', 'عمليات با موفقيت انجام گرديده است.', 100, 1, 0, 0, '0', '0', 'null', 'null', 'null', 'null', 1, 2, 'A00000000000000000000000000214740431', '73029685', 3, 1, '21474043101', '2020-09-03 22:26:04', '2020-09-03 22:27:07'),
(9, '2000', '0', 'عمليات با موفقيت انجام گرديده است.', 100, 1, 0, 0, '0', '0', 'null', 'null', 'null', 'null', 1, 2, 'A00000000000000000000000000214742627', '82309764', 3, 1, '0', '2020-09-03 23:18:32', '2020-09-03 23:22:34'),
(10, '2000', '0', 'عمليات با موفقيت انجام گرديده است.', 100, 1, 0, 0, '0', '0', 'null', 'null', 'null', 'null', 1, 2, 'A00000000000000000000000000214742814', '92765183', 3, 1, '0', '2020-09-03 23:24:02', '2020-09-03 23:25:38'),
(11, '1000', '0', 'عمليات با موفقيت انجام گرديده است.', 100, 1, 0, 0, '0', '0', 'null', 'null', 'null', 'null', 1, 2, 'A00000000000000000000000000214742899', '76208543', 3, 1, '0', '2020-09-03 23:27:01', '2020-09-03 23:29:45'),
(12, '1000', '0', 'عمليات با موفقيت انجام گرديده است.', 100, 1, 0, 0, '0', '0', 'null', 'null', 'null', 'null', 1, 2, 'A00000000000000000000000000214743022', '82576130', 3, 1, '21474302201', '2020-09-03 23:30:21', '2020-09-03 23:32:26'),
(13, '76000', '0', 'عمليات با موفقيت انجام گرديده است.', 100, 1, 0, 3, '64400', '0', 'بلوار معلم، نبش معلم 16، پلاک 36', '09365647585', 'امیر مداح', '9189171111', 3, 1, 'null', '60523189', 3, 1, '0', '2020-09-07 03:20:58', '2020-09-09 12:08:07'),
(14, '1000', '0', 'عمليات با موفقيت انجام گرديده است.', 100, 1, 0, 4, '1000', '0', 'بلوار معلم، نبش معلم 16، پلاک 36', '09365647585', 'امیر مداح', '9189171111', 3, 1, 'null', '94306582', 3, 1, '0', '2020-09-09 17:21:51', '2020-09-09 17:29:02');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `payments`
--
ALTER TABLE `payments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `payments_customer_id_index` (`customer_id`),
  ADD KEY `payments_discount_id_index` (`discount_id`),
  ADD KEY `payments_gift_id_index` (`gift_id`),
  ADD KEY `payments_condition_id_index` (`condition_id`),
  ADD KEY `payments_type_id_index` (`type_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `payments`
--
ALTER TABLE `payments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
