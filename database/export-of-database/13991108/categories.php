<?php
/**
 * Export to PHP Array plugin for PHPMyAdmin
 * @version 5.0.4
 */

/**
 * Database `pharmacy`
 */

/* `pharmacy`.`categories` */
$categories = array(
  array('id' => '1','title' => 'آرایشی بهداشتی','active' => '1','created_at' => '2020-01-17 08:46:33','updated_at' => '2020-01-17 08:46:33'),
  array('id' => '2','title' => 'مکمل غذایی','active' => '1','created_at' => '2020-01-17 08:46:33','updated_at' => '2020-01-17 08:46:33'),
  array('id' => '3','title' => 'مکمل ورزشی','active' => '1','created_at' => '2020-01-17 08:46:33','updated_at' => '2020-01-17 08:46:33'),
  array('id' => '4','title' => 'مادر و کودک','active' => '1','created_at' => '2020-01-17 08:46:33','updated_at' => '2020-01-17 08:46:33'),
  array('id' => '5','title' => 'تجهیزات پزشکی','active' => '1','created_at' => '2020-01-17 08:46:33','updated_at' => '2020-01-17 08:46:33')
);
