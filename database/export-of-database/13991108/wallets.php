<?php
/**
 * Export to PHP Array plugin for PHPMyAdmin
 * @version 5.0.4
 */

/**
 * Database `pharmacy`
 */

/* `pharmacy`.`wallets` */
$wallets = array(
  array('id' => '1','title' => 'کیف پول مجازی','amount' => '20000','type_id' => '2','customer_id' => '1','active' => '1','created_at' => '2020-09-03 05:42:03','updated_at' => '2020-09-06 21:20:55'),
  array('id' => '2','title' => 'کیف پول مجازی','amount' => '0','type_id' => '2','customer_id' => '2','active' => '1','created_at' => '2020-09-09 22:03:13','updated_at' => '2020-09-09 22:03:13')
);
