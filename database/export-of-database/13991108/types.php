<?php
/**
 * Export to PHP Array plugin for PHPMyAdmin
 * @version 5.0.4
 */

/**
 * Database `pharmacy`
 */

/* `pharmacy`.`types` */
$types = array(
  array('id' => '1','title' => 'خرید کالا','key_word' => 'productPurchase','created_at' => '2020-07-19 15:16:46','updated_at' => '2020-07-19 15:16:46'),
  array('id' => '2','title' => 'شارژ کیف پول','key_word' => 'walletCharge','created_at' => '2020-07-19 15:16:46','updated_at' => '2020-07-19 15:16:46')
);
