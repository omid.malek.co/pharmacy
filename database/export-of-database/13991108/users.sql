-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 28, 2021 at 11:06 PM
-- Server version: 10.4.16-MariaDB
-- PHP Version: 7.4.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pharmacy`
--

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `fullname` varchar(255) NOT NULL,
  `email` varchar(250) NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `phone` varchar(16) NOT NULL DEFAULT 'null' COMMENT 'شماره تماس',
  `address` varchar(200) NOT NULL DEFAULT 'null' COMMENT 'آدرس',
  `number_cart` varchar(20) NOT NULL DEFAULT 'null' COMMENT 'آدرس',
  `number_account` varchar(20) NOT NULL DEFAULT 'null' COMMENT 'آدرس',
  `facebook_address` varchar(300) NOT NULL DEFAULT 'null' COMMENT 'آدرس فیس بوک',
  `instagram_address` varchar(300) NOT NULL DEFAULT 'null' COMMENT 'آدرس اینستاگرام',
  `telegram_address` varchar(300) NOT NULL DEFAULT 'null' COMMENT 'آدرس تلگرام',
  `active` tinyint(1) NOT NULL DEFAULT 1,
  `remember_token` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `fullname`, `email`, `email_verified_at`, `password`, `phone`, `address`, `number_cart`, `number_account`, `facebook_address`, `instagram_address`, `telegram_address`, `active`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'امیر مداح', 'omid.malek.1990@gmail.com', NULL, '25f9e794323b453885f5181f1b624d0b', '09365647585', 'بلوار معلم، نبش معلم 16، پلاک 36', '6104337927742401', '6104337927', 'null', 'null', 'null', 1, NULL, '2020-01-17 05:15:35', '2020-09-13 10:12:42');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
