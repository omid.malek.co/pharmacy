-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 28, 2021 at 10:36 PM
-- Server version: 10.4.16-MariaDB
-- PHP Version: 7.4.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pharmacy`
--

-- --------------------------------------------------------

--
-- Table structure for table `conditions`
--

CREATE TABLE `conditions` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(80) NOT NULL COMMENT 'عنوان',
  `key_word` varchar(40) NOT NULL COMMENT 'کلمه کلیدی نوع',
  `active` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `conditions`
--

INSERT INTO `conditions` (`id`, `title`, `key_word`, `active`, `created_at`, `updated_at`) VALUES
(1, 'دیده نشده', 'Unseen', 1, '2020-08-09 11:38:33', '2020-08-09 11:38:33'),
(2, 'استفاده نشده', 'notUsed', 1, '2020-09-06 03:45:35', '2020-09-06 03:45:35'),
(3, 'ارسال نشده', 'notSent', 1, '2020-09-06 03:50:19', '2020-09-06 03:50:19'),
(4, 'دیده شده', 'seen', 1, '2020-08-09 11:36:02', '2020-08-09 11:36:02'),
(5, 'استفاده شده', 'used', 1, '2020-09-06 03:45:35', '2020-09-06 03:45:35'),
(6, 'ارسال شده', 'Sent', 1, '2020-09-06 03:50:19', '2020-09-06 03:50:19');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `conditions`
--
ALTER TABLE `conditions`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `conditions`
--
ALTER TABLE `conditions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
