-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 28, 2021 at 10:38 PM
-- Server version: 10.4.16-MariaDB
-- PHP Version: 7.4.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pharmacy`
--

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE `customers` (
  `id` int(10) UNSIGNED NOT NULL,
  `fullname` varchar(150) NOT NULL DEFAULT 'null',
  `email` varchar(250) NOT NULL COMMENT 'ایمیل کاربر',
  `password` varchar(255) NOT NULL COMMENT 'کلمه عبور',
  `address` varchar(600) NOT NULL DEFAULT 'null' COMMENT 'آدرس',
  `postal_code` varchar(40) NOT NULL DEFAULT 'null' COMMENT 'کد پستی',
  `phone` varchar(16) NOT NULL DEFAULT '0' COMMENT 'شماره تماس',
  `token` varchar(100) NOT NULL DEFAULT 'null' COMMENT ' کد توکن',
  `active` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`id`, `fullname`, `email`, `password`, `address`, `postal_code`, `phone`, `token`, `active`, `created_at`, `updated_at`) VALUES
(1, 'امید مالک', 'omid.malek.1990@gmail.com', '25f9e794323b453885f5181f1b624d0b', 'بلوار معلم، نبش معلم 16، پلاک 36', '9189171111', '09365647585', 'EX9VvT4cpAGirjQiWGgZSjtdnfx0O2ihPV0zp6xPH8wGUMVW2Na3wP368d1A', 1, '2020-02-15 14:02:42', '2020-09-14 02:01:59'),
(2, 'null', 'test@gmail.com', '25f9e794323b453885f5181f1b624d0b', 'null', 'null', '09354612685', '55JaaHmXAZc8jkZIJbIPgbFE2o3VaHkMQX9Cjr9shUBZGwFRkiDqLEvkRRIM', 1, '2020-09-09 17:32:51', '2020-09-09 17:33:04');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `customers_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `customers`
--
ALTER TABLE `customers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
