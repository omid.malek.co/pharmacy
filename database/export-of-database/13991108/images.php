<?php
/**
 * Export to PHP Array plugin for PHPMyAdmin
 * @version 5.0.4
 */

/**
 * Database `pharmacy`
 */

/* `pharmacy`.`images` */
$images = array(
  array('id' => '1','src' => 'pharmacy_products_161122131882.jpg','record_id' => '1','used_in' => 'products','active' => '1','created_at' => '2020-01-17 08:47:28','updated_at' => '2021-01-21 12:58:38'),
  array('id' => '2','src' => 'pharmacy_products_161122621021.jpg','record_id' => '2','used_in' => 'products','active' => '1','created_at' => '2020-01-17 08:47:28','updated_at' => '2021-01-21 14:20:10'),
  array('id' => '3','src' => 'pharmacy_products_161122642566.jpg','record_id' => '3','used_in' => 'products','active' => '1','created_at' => '2020-01-17 08:47:28','updated_at' => '2021-01-21 14:23:45'),
  array('id' => '4','src' => 'pharmacy_products_161122656575.jpg','record_id' => '4','used_in' => 'products','active' => '1','created_at' => '2020-01-17 08:47:28','updated_at' => '2021-01-21 14:26:05'),
  array('id' => '5','src' => 'pharmacy_products_161122678249.jpg','record_id' => '5','used_in' => 'products','active' => '1','created_at' => '2020-01-17 08:47:28','updated_at' => '2021-01-21 14:29:42'),
  array('id' => '6','src' => 'dart_3.jpg','record_id' => '6','used_in' => 'products','active' => '1','created_at' => '2020-01-17 08:47:28','updated_at' => '2020-01-17 08:47:28'),
  array('id' => '7','src' => 'categories_takht_dart.jpg','record_id' => '1','used_in' => 'categories','active' => '1','created_at' => '2020-01-17 08:47:56','updated_at' => '2020-01-17 08:47:56'),
  array('id' => '8','src' => 'categorie_dart.jpg','record_id' => '2','used_in' => 'categories','active' => '1','created_at' => '2020-01-17 08:47:56','updated_at' => '2020-01-17 08:47:56'),
  array('id' => '9','src' => 'categorie_flit.jpg','record_id' => '3','used_in' => 'categories','active' => '1','created_at' => '2020-01-17 08:47:56','updated_at' => '2020-01-17 08:47:56'),
  array('id' => '10','src' => 'categories_shot.jpg','record_id' => '4','used_in' => 'categories','active' => '1','created_at' => '2020-01-17 08:47:56','updated_at' => '2020-01-17 08:47:56'),
  array('id' => '11','src' => 'categories_tip.jpg','record_id' => '5','used_in' => 'categories','active' => '1','created_at' => '2020-01-17 08:47:56','updated_at' => '2020-01-17 08:47:56'),
  array('id' => '12','src' => 'categories_etc.jpg','record_id' => '6','used_in' => 'categories','active' => '1','created_at' => '2020-01-17 08:47:56','updated_at' => '2020-01-17 08:47:56'),
  array('id' => '13','src' => 'pharmacy_slider_1.jpg','record_id' => '0','used_in' => 'sliders','active' => '1','created_at' => '2020-01-17 08:47:56','updated_at' => '2020-01-17 08:47:56'),
  array('id' => '14','src' => 'noPhoto.jpg','record_id' => '2','used_in' => 'articles','active' => '1','created_at' => '2020-06-09 19:14:39','updated_at' => '2020-06-09 19:14:39'),
  array('id' => '15','src' => 'products_159251850134.jpg','record_id' => '3','used_in' => 'articles','active' => '1','created_at' => '2020-06-11 18:25:31','updated_at' => '2020-06-19 02:45:01'),
  array('id' => '16','src' => 'products_159251848752.jpg','record_id' => '4','used_in' => 'articles','active' => '1','created_at' => '2020-06-14 19:18:12','updated_at' => '2020-06-19 02:44:47'),
  array('id' => '17','src' => 'products_159251847372.jpg','record_id' => '5','used_in' => 'articles','active' => '1','created_at' => '2020-06-14 19:25:42','updated_at' => '2020-06-19 02:44:33'),
  array('id' => '18','src' => 'products_159251828144.jpg','record_id' => '6','used_in' => 'articles','active' => '1','created_at' => '2020-06-14 19:26:49','updated_at' => '2020-06-19 02:41:21'),
  array('id' => '19','src' => 'products_159251824434.jpg','record_id' => '7','used_in' => 'articles','active' => '1','created_at' => '2020-06-14 19:27:47','updated_at' => '2020-06-19 02:40:44'),
  array('id' => '20','src' => 'noPhoto.jpg','record_id' => '8','used_in' => 'right.slider','active' => '1','created_at' => '2020-06-09 19:14:39','updated_at' => '2020-06-09 19:14:39'),
  array('id' => '21','src' => 'pharmacy_products_161122115959.jpg','record_id' => '7','used_in' => 'products','active' => '1','created_at' => '2020-09-24 20:03:31','updated_at' => '2021-01-21 12:55:59'),
  array('id' => '22','src' => 'products_160336288763.jpg','record_id' => '8','used_in' => 'products','active' => '1','created_at' => '2020-10-22 14:04:00','updated_at' => '2020-10-22 14:04:47'),
  array('id' => '23','src' => 'pharmacy_products_161122037280.jpg','record_id' => '9','used_in' => 'products','active' => '1','created_at' => '2020-10-22 15:42:29','updated_at' => '2021-01-21 12:42:52'),
  array('id' => '24','src' => 'pharmacy_products_161122095564.jpg','record_id' => '10','used_in' => 'products','active' => '1','created_at' => '2020-10-22 15:56:05','updated_at' => '2021-01-21 12:52:35'),
  array('id' => '25','src' => 'pharmacy_products_161122070863.jpg','record_id' => '11','used_in' => 'products','active' => '1','created_at' => '2020-10-22 15:57:33','updated_at' => '2021-01-21 12:48:28'),
  array('id' => '26','src' => 'products_160345761695.jpg','record_id' => '12','used_in' => 'products','active' => '1','created_at' => '2020-10-23 16:23:26','updated_at' => '2020-10-23 16:23:36'),
  array('id' => '27','src' => 'products_160347352920.jpg','record_id' => '13','used_in' => 'products','active' => '1','created_at' => '2020-10-23 20:48:31','updated_at' => '2020-10-23 20:48:49'),
  array('id' => '28','src' => 'pharmacy_products_161121998810.jpg','record_id' => '14','used_in' => 'products','active' => '1','created_at' => '2020-10-23 20:57:25','updated_at' => '2021-01-21 12:36:28'),
  array('id' => '29','src' => 'pharmacy_slider_2.jpg','record_id' => '0','used_in' => 'sliders','active' => '1','created_at' => '2020-01-17 08:47:56','updated_at' => '2020-01-17 08:47:56'),
  array('id' => '30','src' => 'pharmacy_products_161122746014.jpg','record_id' => '15','used_in' => 'products','active' => '1','created_at' => '2021-01-21 14:40:55','updated_at' => '2021-01-21 14:41:00'),
  array('id' => '31','src' => 'pharmacy_products_161122761892.jpg','record_id' => '16','used_in' => 'products','active' => '1','created_at' => '2021-01-21 14:43:33','updated_at' => '2021-01-21 14:43:38'),
  array('id' => '32','src' => 'pharmacy_products_161122781163.jpg','record_id' => '17','used_in' => 'products','active' => '1','created_at' => '2021-01-21 14:46:42','updated_at' => '2021-01-21 14:46:51'),
  array('id' => '33','src' => 'pharmacy_products_161122797092.jpg','record_id' => '18','used_in' => 'products','active' => '1','created_at' => '2021-01-21 14:49:22','updated_at' => '2021-01-21 14:49:30')
);
