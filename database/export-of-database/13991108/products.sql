-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 28, 2021 at 11:04 PM
-- Server version: 10.4.16-MariaDB
-- PHP Version: 7.4.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pharmacy`
--

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(150) NOT NULL COMMENT 'عنوان کالا',
  `description` text NOT NULL COMMENT 'توضیحات مربوط به کالا',
  `sales` int(11) NOT NULL DEFAULT 0 COMMENT 'تعداد کالاهای فروخته شده',
  `amount` varchar(40) NOT NULL COMMENT 'قیمت کالا',
  `active` tinyint(1) NOT NULL DEFAULT 1,
  `number` tinyint(1) NOT NULL DEFAULT 1 COMMENT 'تعداد کالاهای موجود',
  `categorie_id` int(10) UNSIGNED DEFAULT NULL COMMENT 'کلید دسته بندی کالا',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `title`, `description`, `sales`, `amount`, `active`, `number`, `categorie_id`, `created_at`, `updated_at`) VALUES
(1, 'بیوتین جالینوس 5 میلی', '<h4 style=\"text-align: right;\">بیوتین جالینوس 5 میلی گرمی</h4>', 2, '36000', 1, 10, 2, '2020-01-17 05:17:28', '2021-01-21 09:28:00'),
(2, 'پودر آب پنیر', '<h4 style=\"text-align: right;\">پودر آب پنیر</h4>', 3, '27000', 1, 15, 3, '2020-01-17 05:17:28', '2021-01-21 10:50:00'),
(3, 'متاپیور زیرو کرب', '<div class=\"title-fa\">\r\n<h3>متاپیور زیرو کرب</h3>\r\n<span id=\"LblSlaveName\" title=\"19832\">( شکلات بلژیکی )</span></div>', 4, '76000', 1, 15, 3, '2020-01-17 05:17:28', '2021-01-21 10:53:00'),
(4, 'پروتئین وی', '<div class=\"title-fa\" style=\"text-align: right;\">\r\n<h4>پروتئین وی 2270 گرمی</h4>\r\n<span id=\"LblSlaveName\" title=\"19476\"></span></div>', 5, '918000', 1, 15, 3, '2020-01-17 05:17:28', '2021-01-21 10:55:00'),
(5, 'آیروسول', '<p style=\"text-align: right;\">در این قسمت توضیحات مربوط به کالای مورد نظر ارائه می گردد.</p>', 6, '41500', 1, 16, 3, '2020-01-17 05:17:28', '2021-01-21 10:59:00'),
(7, 'ویتامین ث', '<h4 style=\"text-align: right;\">ویتافلش ویتامین ث</h4>', 0, '32700', 1, 2, 2, '2020-09-24 16:33:31', '2021-01-21 09:25:00'),
(9, 'کرم اکسیدان', '<div class=\"title-fa\">\r\n<h4 style=\"text-align: right;\">کرم اکسیدان 12% نمره 3 حاوی روغن بادام</h4>\r\n<div class=\"each-row\" style=\"text-align: right;\">نوع محصول\r\n<div style=\"text-align: right;\">اکسیدان</div>\r\n</div>\r\n<div class=\"each-row\" style=\"text-align: right;\">سایز\r\n<div style=\"text-align: right;\">150 میلی لیتر</div>\r\n</div>\r\n<div class=\"each-row\" style=\"text-align: right;\">محل مصرف\r\n<div>موی سر</div>\r\n</div>\r\n<span id=\"LblSlaveName\" title=\"19902\"></span></div>', 0, '9800', 1, 2, 1, '2020-10-22 12:12:29', '2021-01-21 09:11:00'),
(10, 'ویتامین ای 400', '<h4 style=\"text-align: right;\">ویتامین ای 400 آلفا 30 عددی</h4>', 0, '29500', 1, 2, 2, '2020-10-22 12:26:05', '2021-01-21 09:22:00'),
(11, 'برس مو آی استایل', '<div class=\"title-layer\">\r\n<div class=\"title-fa\">\r\n<h4 style=\"text-align: right;\">برس مو آی استایل لیفت اند کرل</h4>\r\n<span id=\"LblSlaveName\" title=\"19883\"></span>\r\n<h6 class=\"each-row\" style=\"text-align: right;\">نوع محصول</h6>\r\n<div style=\"text-align: right;\">شانه</div>\r\n<h6 class=\"each-row\" style=\"text-align: right;\">محل مصرف</h6>\r\n<div style=\"text-align: right;\">موی سر</div>\r\n</div>\r\n</div>', 0, '46300', 1, 2, 1, '2020-10-22 12:27:32', '2021-01-21 09:18:00'),
(14, 'بوگیر و خوشبو کننده هوای', '<h4 style=\"text-align: right;\">بوگیر و خوشبو کننده هوای توالت با رایحه شیمی سوکی</h4>\r\n<h6 style=\"text-align: right;\">نوع محصول: مایع</h6>\r\n<h6 style=\"text-align: right;\">سایز: 400 میلی لیتر</h6>', 0, '499000', 1, 2, 1, '2020-10-23 17:27:25', '2021-01-21 09:02:00'),
(15, 'غذای کودک برنجین', '<h4 style=\"text-align: right;\">غذای کودک برنجین با شیر و مخلوط میوه</h4>\r\n<p style=\"text-align: right;\">300 گرم</p>', 0, '1000', 1, 2, 4, '2021-01-21 11:10:54', '2021-01-21 11:10:54'),
(16, 'غذای کودک گندمین', '<div class=\"title-layer\">\r\n<div class=\"title-fa\" style=\"text-align: right;\">\r\n<h4>غذای کودک گندمین با شیر و مخلوط میوه</h4>\r\n<span id=\"LblSlaveName\" title=\"19053\"></span></div>\r\n</div>', 0, '12500', 1, 2, 4, '2021-01-21 11:13:33', '2021-01-21 11:14:00'),
(17, 'فشارسنج مچی', '<h4 style=\"text-align: right;\">فشارسنج مچی مدل BP W1 Basic</h4>', 0, '994000', 1, 5, 5, '2021-01-21 11:16:42', '2021-01-21 11:16:42'),
(18, 'ماسک بهداشتی', '<div class=\"title-fa\" style=\"text-align: right;\">\r\n<h4 style=\"text-align: right;\">ماسک بهداشتی 3 بعدی</h4>\r\n</div>\r\n<div class=\"title-fa\" style=\"text-align: right;\">\r\n<h5 style=\"text-align: right;\">سفید</h5>\r\n</div>', 0, '66500', 1, 2, 5, '2021-01-21 11:19:22', '2021-01-21 11:22:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `products_categorie_id_index` (`categorie_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
