-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 28, 2021 at 10:42 PM
-- Server version: 10.4.16-MariaDB
-- PHP Version: 7.4.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pharmacy`
--

-- --------------------------------------------------------

--
-- Table structure for table `factors`
--

CREATE TABLE `factors` (
  `id` int(10) UNSIGNED NOT NULL,
  `product_id` int(10) UNSIGNED DEFAULT NULL COMMENT 'کلید خارجی پست',
  `sent_number` int(11) NOT NULL COMMENT 'تعداد کالاهای ارسال شده ',
  `type_id` int(10) UNSIGNED DEFAULT 1 COMMENT 'نوع پرداخت',
  `customer_id` int(10) UNSIGNED DEFAULT NULL COMMENT 'کلید خارجی مشتری ها',
  `payment_id` varchar(40) NOT NULL DEFAULT '0' COMMENT 'مبلغ پرداخت شده',
  `active` tinyint(1) NOT NULL DEFAULT 1 COMMENT 'حذف منطقی شده=0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `factors`
--

INSERT INTO `factors` (`id`, `product_id`, `sent_number`, `type_id`, `customer_id`, `payment_id`, `active`, `created_at`, `updated_at`) VALUES
(1, 4, 1, 1, 1, '1', 1, '2020-05-13 12:19:14', '2020-05-13 12:19:14'),
(7, 6, 1, 1, 1, '1', 1, '2020-08-04 10:01:33', '2020-08-04 10:01:33'),
(10, 2, 1, 1, 1, '13', 1, '2020-09-07 05:53:21', '2020-09-07 05:53:21'),
(11, 1, 1, 1, 1, '14', 1, '2020-09-09 17:21:51', '2020-09-09 17:21:51');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `factors`
--
ALTER TABLE `factors`
  ADD PRIMARY KEY (`id`),
  ADD KEY `factors_product_id_index` (`product_id`),
  ADD KEY `factors_type_id_index` (`type_id`),
  ADD KEY `factors_customer_id_index` (`customer_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `factors`
--
ALTER TABLE `factors`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
