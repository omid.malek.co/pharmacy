<?php
/**
 * Export to PHP Array plugin for PHPMyAdmin
 * @version 5.0.1
 */

/**
 * Database `dart`
 */

/* `dart`.`categories` */
$categories = array(
  array('id' => '1','title' => 'تخت دارت','active' => '1','created_at' => '2020-01-17 08:46:33','updated_at' => '2020-01-17 08:46:33'),
  array('id' => '2','title' => 'دارت','active' => '1','created_at' => '2020-01-17 08:46:33','updated_at' => '2020-01-17 08:46:33'),
  array('id' => '3','title' => 'فلایت','active' => '1','created_at' => '2020-01-17 08:46:33','updated_at' => '2020-01-17 08:46:33'),
  array('id' => '4','title' => 'شات','active' => '1','created_at' => '2020-01-17 08:46:33','updated_at' => '2020-01-17 08:46:33'),
  array('id' => '5','title' => 'تیپ','active' => '1','created_at' => '2020-01-17 08:46:33','updated_at' => '2020-01-17 08:46:33'),
  array('id' => '6','title' => 'لوازم جانبی','active' => '1','created_at' => '2020-01-17 08:46:33','updated_at' => '2020-01-17 08:46:33')
);

/* `dart`.`comments` */
$comments = array(
);

/* `dart`.`conditions` */
$conditions = array(
  array('id' => '1','title' => 'دیده نشده','key_word' => 'Unseen','active' => '1','created_at' => '2020-08-09 16:08:33','updated_at' => '2020-08-09 16:08:33'),
  array('id' => '2','title' => 'استفاده نشده','key_word' => 'notUsed','active' => '1','created_at' => '2020-09-06 08:15:35','updated_at' => '2020-09-06 08:15:35'),
  array('id' => '3','title' => 'ارسال نشده','key_word' => 'notSent','active' => '1','created_at' => '2020-09-06 08:20:19','updated_at' => '2020-09-06 08:20:19'),
  array('id' => '4','title' => 'دیده شده','key_word' => 'seen','active' => '1','created_at' => '2020-08-09 16:06:02','updated_at' => '2020-08-09 16:06:02'),
  array('id' => '5','title' => 'استفاده شده','key_word' => 'used','active' => '1','created_at' => '2020-09-06 08:15:35','updated_at' => '2020-09-06 08:15:35'),
  array('id' => '6','title' => 'ارسال شده','key_word' => 'Sent','active' => '1','created_at' => '2020-09-06 08:20:19','updated_at' => '2020-09-06 08:20:19')
);

/* `dart`.`customers` */
$customers = array(
  array('id' => '1','fullname' => 'امیر مداح','email' => 'omid.malek.1990@gmail.com','password' => '25f9e794323b453885f5181f1b624d0b','address' => 'بلوار معلم، نبش معلم 16، پلاک 36','postal_code' => '9189171111','phone' => '09365647585','token' => 'tdWG4s6bAp6zsC1tQ6SbzTrz74J5meVeJMDrheuKxhln4wh19BP2tFNYTD9S','active' => '1','created_at' => '2020-02-15 17:32:42','updated_at' => '2020-09-10 10:21:32'),
  array('id' => '2','fullname' => 'null','email' => 'test@gmail.com','password' => '25f9e794323b453885f5181f1b624d0b','address' => 'null','postal_code' => 'null','phone' => '09354612685','token' => '55JaaHmXAZc8jkZIJbIPgbFE2o3VaHkMQX9Cjr9shUBZGwFRkiDqLEvkRRIM','active' => '1','created_at' => '2020-09-09 22:02:51','updated_at' => '2020-09-09 22:03:04')
);

/* `dart`.`discounts` */
$discounts = array(
  array('id' => '2','text' => 'takhfif','percent' => '10.00','start_date' => '2020-09-08','expire_date' => '2020-09-12','number_validation' => '1','active' => '1','created_at' => '2020-07-16 23:21:09','updated_at' => '2020-08-07 19:46:50')
);

/* `dart`.`factors` */
$factors = array(
  array('id' => '1','product_id' => '4','sent_number' => '1','type_id' => '1','customer_id' => '1','payment_id' => '1','active' => '1','created_at' => '2020-05-13 16:49:14','updated_at' => '2020-05-13 16:49:14'),
  array('id' => '7','product_id' => '6','sent_number' => '1','type_id' => '1','customer_id' => '1','payment_id' => '1','active' => '1','created_at' => '2020-08-04 14:31:33','updated_at' => '2020-08-04 14:31:33'),
  array('id' => '10','product_id' => '2','sent_number' => '1','type_id' => '1','customer_id' => '1','payment_id' => '13','active' => '1','created_at' => '2020-09-07 10:23:21','updated_at' => '2020-09-07 10:23:21'),
  array('id' => '11','product_id' => '1','sent_number' => '1','type_id' => '1','customer_id' => '1','payment_id' => '14','active' => '1','created_at' => '2020-09-09 21:51:51','updated_at' => '2020-09-09 21:51:51')
);

/* `dart`.`failed_jobs` */
$failed_jobs = array(
);

/* `dart`.`gifts` */
$gifts = array(
  array('id' => '3','text' => '81425963','amount' => '12100','wallet_id' => '1','used_by' => '1','created_by' => '1','condition_id' => '2','start_date' => '2020-09-07','expire_date' => '2020-09-12','active' => '1','created_at' => '2020-09-06 14:24:09','updated_at' => '2020-09-09 16:37:51'),
  array('id' => '4','text' => '34286195','amount' => '3000','wallet_id' => '1','used_by' => '1','created_by' => '1','condition_id' => '2','start_date' => '2020-09-07','expire_date' => '2020-09-14','active' => '1','created_at' => '2020-09-06 21:20:55','updated_at' => '2020-09-09 21:58:08')
);

/* `dart`.`images` */
$images = array(
  array('id' => '1','src' => 'dart_1.png','record_id' => '1','used_in' => 'products','active' => '1','created_at' => '2020-01-17 08:47:28','updated_at' => '2020-01-17 08:47:28'),
  array('id' => '2','src' => 'dart_2.jpg','record_id' => '2','used_in' => 'products','active' => '1','created_at' => '2020-01-17 08:47:28','updated_at' => '2020-01-17 08:47:28'),
  array('id' => '3','src' => 'dart_3.jpg','record_id' => '3','used_in' => 'products','active' => '1','created_at' => '2020-01-17 08:47:28','updated_at' => '2020-01-17 08:47:28'),
  array('id' => '4','src' => 'dart_1.png','record_id' => '4','used_in' => 'products','active' => '1','created_at' => '2020-01-17 08:47:28','updated_at' => '2020-01-17 08:47:28'),
  array('id' => '5','src' => 'dart_2.jpg','record_id' => '5','used_in' => 'products','active' => '1','created_at' => '2020-01-17 08:47:28','updated_at' => '2020-01-17 08:47:28'),
  array('id' => '6','src' => 'dart_3.jpg','record_id' => '6','used_in' => 'products','active' => '1','created_at' => '2020-01-17 08:47:28','updated_at' => '2020-01-17 08:47:28'),
  array('id' => '7','src' => 'categories_takht_dart.jpg','record_id' => '1','used_in' => 'categories','active' => '1','created_at' => '2020-01-17 08:47:56','updated_at' => '2020-01-17 08:47:56'),
  array('id' => '8','src' => 'categorie_dart.jpg','record_id' => '2','used_in' => 'categories','active' => '1','created_at' => '2020-01-17 08:47:56','updated_at' => '2020-01-17 08:47:56'),
  array('id' => '9','src' => 'categorie_flit.jpg','record_id' => '3','used_in' => 'categories','active' => '1','created_at' => '2020-01-17 08:47:56','updated_at' => '2020-01-17 08:47:56'),
  array('id' => '10','src' => 'categories_shot.jpg','record_id' => '4','used_in' => 'categories','active' => '1','created_at' => '2020-01-17 08:47:56','updated_at' => '2020-01-17 08:47:56'),
  array('id' => '11','src' => 'categories_tip.jpg','record_id' => '5','used_in' => 'categories','active' => '1','created_at' => '2020-01-17 08:47:56','updated_at' => '2020-01-17 08:47:56'),
  array('id' => '12','src' => 'categories_etc.jpg','record_id' => '6','used_in' => 'categories','active' => '1','created_at' => '2020-01-17 08:47:56','updated_at' => '2020-01-17 08:47:56'),
  array('id' => '13','src' => 'slider_1.jpg','record_id' => '0','used_in' => 'sliders','active' => '1','created_at' => '2020-01-17 08:47:56','updated_at' => '2020-01-17 08:47:56'),
  array('id' => '14','src' => 'noPhoto.jpg','record_id' => '2','used_in' => 'articles','active' => '1','created_at' => '2020-06-09 19:14:39','updated_at' => '2020-06-09 19:14:39'),
  array('id' => '15','src' => 'products_159251850134.jpg','record_id' => '3','used_in' => 'articles','active' => '1','created_at' => '2020-06-11 18:25:31','updated_at' => '2020-06-19 02:45:01'),
  array('id' => '16','src' => 'products_159251848752.jpg','record_id' => '4','used_in' => 'articles','active' => '1','created_at' => '2020-06-14 19:18:12','updated_at' => '2020-06-19 02:44:47'),
  array('id' => '17','src' => 'products_159251847372.jpg','record_id' => '5','used_in' => 'articles','active' => '1','created_at' => '2020-06-14 19:25:42','updated_at' => '2020-06-19 02:44:33'),
  array('id' => '18','src' => 'products_159251828144.jpg','record_id' => '6','used_in' => 'articles','active' => '1','created_at' => '2020-06-14 19:26:49','updated_at' => '2020-06-19 02:41:21'),
  array('id' => '19','src' => 'products_159251824434.jpg','record_id' => '7','used_in' => 'articles','active' => '1','created_at' => '2020-06-14 19:27:47','updated_at' => '2020-06-19 02:40:44'),
  array('id' => '20','src' => 'noPhoto.jpg','record_id' => '8','used_in' => 'right.slider','active' => '1','created_at' => '2020-06-09 19:14:39','updated_at' => '2020-06-09 19:14:39')
);

/* `dart`.`meta_tages` */
$meta_tages = array(
);

/* `dart`.`migrations` */
$migrations = array(
  array('id' => '1','migration' => '2014_10_12_000000_create_users_table','batch' => '1'),
  array('id' => '2','migration' => '2014_10_12_100000_create_password_resets_table','batch' => '1'),
  array('id' => '3','migration' => '2019_09_04_213034_create_payments_table','batch' => '1'),
  array('id' => '4','migration' => '2019_12_29_094125_create_failed_jobs_table','batch' => '1'),
  array('id' => '5','migration' => '2019_12_29_094502_create_posts_table','batch' => '1'),
  array('id' => '6','migration' => '2020_01_01_104208_create_categories_table','batch' => '1'),
  array('id' => '7','migration' => '2020_01_01_104258_create-comments_table','batch' => '1'),
  array('id' => '8','migration' => '2020_01_01_104453_create_images_table','batch' => '1'),
  array('id' => '9','migration' => '2020_01_01_111720_create_customers_table','batch' => '1'),
  array('id' => '10','migration' => '2020_01_01_112025_create_factors_table','batch' => '1'),
  array('id' => '11','migration' => '2020_01_01_122934_create_products_table','batch' => '1'),
  array('id' => '12','migration' => '2020_01_02_090210_create_pages_table','batch' => '1'),
  array('id' => '13','migration' => '2020_01_02_095457_create_meta_tages_table','batch' => '1'),
  array('id' => '14','migration' => '2020_01_02_104053_create_discounts_table','batch' => '1'),
  array('id' => '15','migration' => '2020_01_02_113401_create_relations_table','batch' => '1'),
  array('id' => '16','migration' => '2020_04_18_173337_create_tokens_table','batch' => '1'),
  array('id' => '17','migration' => '2020_07_16_081158_create_wallets_table','batch' => '1'),
  array('id' => '18','migration' => '2020_07_17_222756_create_types_table','batch' => '1'),
  array('id' => '19','migration' => '2020_08_31_001711_create_gifts_table','batch' => '1'),
  array('id' => '20','migration' => '2020_08_31_011723_create_conditions_table','batch' => '1')
);

/* `dart`.`pages` */
$pages = array(
);

/* `dart`.`password_resets` */
$password_resets = array(
);

/* `dart`.`payments` */
$payments = array(
  array('id' => '1','payment_amount' => '228000','total_sum' => '228000','payment_message' => 'null','payment_message_code' => '0','customer_id' => '1','discount_id' => '2','gift_id' => '0','gift_amount' => '0','discount_value' => '21280.000000000004','sent_address' => 'null','sent_phone' => 'null','receiver_fullname' => 'null','postal_code_sent' => 'null','condition_id' => '6','type_id' => '1','Authority' => 'null','reference_code' => 'null','payment_status' => '3','active' => '1','RefID' => '0','created_at' => '2020-05-13 16:49:14','updated_at' => '2020-09-11 07:58:35'),
  array('id' => '7','payment_amount' => '2000','total_sum' => '0','payment_message' => 'عمليات با موفقيت انجام گرديده است.','payment_message_code' => '100','customer_id' => '1','discount_id' => '0','gift_id' => '0','gift_amount' => '0','discount_value' => '0','sent_address' => 'null','sent_phone' => 'null','receiver_fullname' => 'null','postal_code_sent' => 'null','condition_id' => '1','type_id' => '2','Authority' => 'A00000000000000000000000000214740100','reference_code' => '37852490','payment_status' => '3','active' => '1','RefID' => '21474010001','created_at' => '2020-09-04 02:49:23','updated_at' => '2020-09-04 02:52:07'),
  array('id' => '8','payment_amount' => '2000','total_sum' => '0','payment_message' => 'عمليات با موفقيت انجام گرديده است.','payment_message_code' => '100','customer_id' => '1','discount_id' => '0','gift_id' => '0','gift_amount' => '0','discount_value' => '0','sent_address' => 'null','sent_phone' => 'null','receiver_fullname' => 'null','postal_code_sent' => 'null','condition_id' => '1','type_id' => '2','Authority' => 'A00000000000000000000000000214740431','reference_code' => '73029685','payment_status' => '3','active' => '1','RefID' => '21474043101','created_at' => '2020-09-04 02:56:04','updated_at' => '2020-09-04 02:57:07'),
  array('id' => '9','payment_amount' => '2000','total_sum' => '0','payment_message' => 'عمليات با موفقيت انجام گرديده است.','payment_message_code' => '100','customer_id' => '1','discount_id' => '0','gift_id' => '0','gift_amount' => '0','discount_value' => '0','sent_address' => 'null','sent_phone' => 'null','receiver_fullname' => 'null','postal_code_sent' => 'null','condition_id' => '1','type_id' => '2','Authority' => 'A00000000000000000000000000214742627','reference_code' => '82309764','payment_status' => '3','active' => '1','RefID' => '0','created_at' => '2020-09-04 03:48:32','updated_at' => '2020-09-04 03:52:34'),
  array('id' => '10','payment_amount' => '2000','total_sum' => '0','payment_message' => 'عمليات با موفقيت انجام گرديده است.','payment_message_code' => '100','customer_id' => '1','discount_id' => '0','gift_id' => '0','gift_amount' => '0','discount_value' => '0','sent_address' => 'null','sent_phone' => 'null','receiver_fullname' => 'null','postal_code_sent' => 'null','condition_id' => '1','type_id' => '2','Authority' => 'A00000000000000000000000000214742814','reference_code' => '92765183','payment_status' => '3','active' => '1','RefID' => '0','created_at' => '2020-09-04 03:54:02','updated_at' => '2020-09-04 03:55:38'),
  array('id' => '11','payment_amount' => '1000','total_sum' => '0','payment_message' => 'عمليات با موفقيت انجام گرديده است.','payment_message_code' => '100','customer_id' => '1','discount_id' => '0','gift_id' => '0','gift_amount' => '0','discount_value' => '0','sent_address' => 'null','sent_phone' => 'null','receiver_fullname' => 'null','postal_code_sent' => 'null','condition_id' => '1','type_id' => '2','Authority' => 'A00000000000000000000000000214742899','reference_code' => '76208543','payment_status' => '3','active' => '1','RefID' => '0','created_at' => '2020-09-04 03:57:01','updated_at' => '2020-09-04 03:59:45'),
  array('id' => '12','payment_amount' => '1000','total_sum' => '0','payment_message' => 'عمليات با موفقيت انجام گرديده است.','payment_message_code' => '100','customer_id' => '1','discount_id' => '0','gift_id' => '0','gift_amount' => '0','discount_value' => '0','sent_address' => 'null','sent_phone' => 'null','receiver_fullname' => 'null','postal_code_sent' => 'null','condition_id' => '1','type_id' => '2','Authority' => 'A00000000000000000000000000214743022','reference_code' => '82576130','payment_status' => '3','active' => '1','RefID' => '21474302201','created_at' => '2020-09-04 04:00:21','updated_at' => '2020-09-04 04:02:26'),
  array('id' => '13','payment_amount' => '76000','total_sum' => '0','payment_message' => 'عمليات با موفقيت انجام گرديده است.','payment_message_code' => '100','customer_id' => '1','discount_id' => '0','gift_id' => '3','gift_amount' => '64400','discount_value' => '0','sent_address' => 'بلوار معلم، نبش معلم 16، پلاک 36','sent_phone' => '09365647585','receiver_fullname' => 'امیر مداح','postal_code_sent' => '9189171111','condition_id' => '3','type_id' => '1','Authority' => 'null','reference_code' => '60523189','payment_status' => '3','active' => '1','RefID' => '0','created_at' => '2020-09-07 07:50:58','updated_at' => '2020-09-09 16:38:07'),
  array('id' => '14','payment_amount' => '1000','total_sum' => '0','payment_message' => 'عمليات با موفقيت انجام گرديده است.','payment_message_code' => '100','customer_id' => '1','discount_id' => '0','gift_id' => '4','gift_amount' => '1000','discount_value' => '0','sent_address' => 'بلوار معلم، نبش معلم 16، پلاک 36','sent_phone' => '09365647585','receiver_fullname' => 'امیر مداح','postal_code_sent' => '9189171111','condition_id' => '3','type_id' => '1','Authority' => 'null','reference_code' => '94306582','payment_status' => '3','active' => '1','RefID' => '0','created_at' => '2020-09-09 21:51:51','updated_at' => '2020-09-09 21:59:02')
);

/* `dart`.`posts` */
$posts = array(
  array('id' => '3','title' => 'اولین مقاله دارت مارکت','body' => '<p>متن اولین مقاله دارت مارکت</p>','published' => '1','used_in' => 'articles','visited' => '1','created_by' => '0','updated_by' => '1','created_at' => '2020-06-11 18:25:31','updated_at' => '2020-06-11 19:23:32'),
  array('id' => '4','title' => 'دومین مقاله دارت مارکت','body' => '<p><img style="display: block; margin-left: auto; margin-right: auto;" src="/photos/1/bg-01.jpg" alt="" width="836" height="409" /></p>
<p style="text-align: center;">متن دومین مقاله دارت مارکت</p>','published' => '1','used_in' => 'articles','visited' => '1','created_by' => '0','updated_by' => '1','created_at' => '2020-06-14 19:18:12','updated_at' => '2020-07-02 14:08:13'),
  array('id' => '5','title' => 'سومین پست دارت مارکت','body' => '<p style="text-align: center;"><img style="display: block; margin-left: auto; margin-right: auto;" src="/photos/1/bg-01.jpg" alt="" width="899" height="440" /></p>
<p style="text-align: center;">متن سومین پست دارت مارکت</p>','published' => '1','used_in' => 'articles','visited' => '1','created_by' => '0','updated_by' => '1','created_at' => '2020-06-14 19:25:42','updated_at' => '2020-07-08 06:56:42'),
  array('id' => '6','title' => 'چهارمین پست دارت مارکت','body' => '<p>متن چهارمین پست دارت مارکت</p>','published' => '1','used_in' => 'articles','visited' => '1','created_by' => '0','updated_by' => '1','created_at' => '2020-06-14 19:26:49','updated_at' => '2020-06-19 02:41:17'),
  array('id' => '7','title' => 'پنجمین پست دارت مارکت','body' => '<p>متن پنجمین پست دارت مارکت</p>','published' => '1','used_in' => 'articles','visited' => '2','created_by' => '0','updated_by' => '1','created_at' => '2020-06-14 19:27:47','updated_at' => '2020-07-10 10:55:02'),
  array('id' => '8','title' => 'هنر پرتاب دارت','body' => '<h5 class="text-right">ورزش دارت را باید یکی از شاخه های ورزش و حتی آمادگی نظامی دانست که به نوعی آن را یک هنر می دانند: هنر پرتاب دارت</h5>','published' => '1','used_in' => 'right.slider','visited' => '1','created_by' => '0','updated_by' => '1','created_at' => '2020-07-05 01:24:56','updated_at' => '2020-07-05 01:24:56')
);

/* `dart`.`products` */
$products = array(
  array('id' => '1','title' => 'دارت STARTOS مدل 24G','description' => 'در این قسمت توضیحات مربوط به کالای مورد نظر ارائه می گردد.','sales' => '2','amount' => '1000','active' => '1','number' => '20','categorie_id' => '1','created_at' => '2020-01-17 08:47:28','updated_at' => '2020-07-08 07:14:00'),
  array('id' => '2','title' => 'دارت STARTOS مدل 24G','description' => 'در این قسمت توضیحات مربوط به کالای مورد نظر ارائه می گردد.','sales' => '3','amount' => '76000','active' => '1','number' => '19','categorie_id' => '2','created_at' => '2020-01-17 08:47:28','updated_at' => '2020-01-17 08:47:28'),
  array('id' => '3','title' => 'دارت STARTOS مدل 24G','description' => 'در این قسمت توضیحات مربوط به کالای مورد نظر ارائه می گردد.','sales' => '4','amount' => '76000','active' => '1','number' => '18','categorie_id' => '3','created_at' => '2020-01-17 08:47:28','updated_at' => '2020-01-17 08:47:28'),
  array('id' => '4','title' => 'دارت STARTOS مدل 24G','description' => 'در این قسمت توضیحات مربوط به کالای مورد نظر ارائه می گردد.','sales' => '5','amount' => '76000','active' => '1','number' => '17','categorie_id' => '4','created_at' => '2020-01-17 08:47:28','updated_at' => '2020-01-17 08:47:28'),
  array('id' => '5','title' => 'دارت STARTOS مدل 24G','description' => 'در این قسمت توضیحات مربوط به کالای مورد نظر ارائه می گردد.','sales' => '6','amount' => '76000','active' => '1','number' => '16','categorie_id' => '5','created_at' => '2020-01-17 08:47:28','updated_at' => '2020-01-17 08:47:28'),
  array('id' => '6','title' => 'دارت STARTOS مدل 24G','description' => 'در این قسمت توضیحات مربوط به کالای مورد نظر ارائه می گردد.','sales' => '7','amount' => '76000','active' => '1','number' => '15','categorie_id' => '6','created_at' => '2020-01-17 08:47:28','updated_at' => '2020-01-17 08:47:28')
);

/* `dart`.`tokens` */
$tokens = array(
);

/* `dart`.`types` */
$types = array(
  array('id' => '1','title' => 'خرید کالا','key_word' => 'productPurchase','created_at' => '2020-07-19 15:16:46','updated_at' => '2020-07-19 15:16:46'),
  array('id' => '2','title' => 'شارژ کیف پول','key_word' => 'walletCharge','created_at' => '2020-07-19 15:16:46','updated_at' => '2020-07-19 15:16:46')
);

/* `dart`.`users` */
$users = array(
  array('id' => '1','fullname' => 'امیر مداح','email' => 'omid.malek.1990@gmail.com','email_verified_at' => NULL,'password' => 'e10adc3949ba59abbe56e057f20f883e','phone' => '09365647585','address' => 'بلوار معلم، نبش معلم 16، پلاک 36','number_cart' => '6104337927742401','number_account' => '6104337927','active' => '1','remember_token' => NULL,'created_at' => '2020-01-17 08:45:35','updated_at' => '2020-01-17 08:45:35')
);

/* `dart`.`wallets` */
$wallets = array(
  array('id' => '1','title' => 'کیف پول مجازی','amount' => '20000','type_id' => '2','customer_id' => '1','active' => '1','created_at' => '2020-09-03 05:42:03','updated_at' => '2020-09-06 21:20:55'),
  array('id' => '2','title' => 'کیف پول مجازی','amount' => '0','type_id' => '2','customer_id' => '2','active' => '1','created_at' => '2020-09-09 22:03:13','updated_at' => '2020-09-09 22:03:13')
);
