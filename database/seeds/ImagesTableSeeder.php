<?php

use Illuminate\Database\Seeder;

use App\Categorie;
use App\Image;

class ImagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
//        $categories=Categorie::where('active','=',1)->get();
//
//        for($i=0;$i<count($categories);){
//
//            $ImgCategories=[
//                    'src'=>'categorie_dart.jpg',
//                    'record_id'=>$categories[$i++]->id,
//                    'used_in'=>'categories',
//                    'active'=>1,
//            ];
//
//            Image::create($ImgCategories);
//        }
//
//        $ImgSlider=[
//            'src'=>'slider_1.jpg',
//            'record_id'=>0,
//            'used_in'=>'sliders',
//            'active'=>1,
//        ];
//        Image::create($ImgSlider);

        /* `dart`.`images` */
        $images = array(
            array('id' => '1','src' => 'dart_1.png','record_id' => '1','used_in' => 'products','active' => '1','created_at' => '2020-01-17 08:47:28','updated_at' => '2020-01-17 08:47:28'),
            array('id' => '2','src' => 'dart_2.jpg','record_id' => '2','used_in' => 'products','active' => '1','created_at' => '2020-01-17 08:47:28','updated_at' => '2020-01-17 08:47:28'),
            array('id' => '3','src' => 'dart_3.jpg','record_id' => '3','used_in' => 'products','active' => '1','created_at' => '2020-01-17 08:47:28','updated_at' => '2020-01-17 08:47:28'),
            array('id' => '4','src' => 'dart_1.png','record_id' => '4','used_in' => 'products','active' => '1','created_at' => '2020-01-17 08:47:28','updated_at' => '2020-01-17 08:47:28'),
            array('id' => '5','src' => 'dart_2.jpg','record_id' => '5','used_in' => 'products','active' => '1','created_at' => '2020-01-17 08:47:28','updated_at' => '2020-01-17 08:47:28'),
            array('id' => '6','src' => 'dart_3.jpg','record_id' => '6','used_in' => 'products','active' => '1','created_at' => '2020-01-17 08:47:28','updated_at' => '2020-01-17 08:47:28'),
            array('id' => '7','src' => 'categories_takht_dart.jpg','record_id' => '1','used_in' => 'categories','active' => '1','created_at' => '2020-01-17 08:47:56','updated_at' => '2020-01-17 08:47:56'),
            array('id' => '8','src' => 'categorie_dart.jpg','record_id' => '2','used_in' => 'categories','active' => '1','created_at' => '2020-01-17 08:47:56','updated_at' => '2020-01-17 08:47:56'),
            array('id' => '9','src' => 'categorie_flit.jpg','record_id' => '3','used_in' => 'categories','active' => '1','created_at' => '2020-01-17 08:47:56','updated_at' => '2020-01-17 08:47:56'),
            array('id' => '10','src' => 'categories_shot.jpg','record_id' => '4','used_in' => 'categories','active' => '1','created_at' => '2020-01-17 08:47:56','updated_at' => '2020-01-17 08:47:56'),
            array('id' => '11','src' => 'categories_tip.jpg','record_id' => '5','used_in' => 'categories','active' => '1','created_at' => '2020-01-17 08:47:56','updated_at' => '2020-01-17 08:47:56'),
            array('id' => '12','src' => 'categories_etc.jpg','record_id' => '6','used_in' => 'categories','active' => '1','created_at' => '2020-01-17 08:47:56','updated_at' => '2020-01-17 08:47:56'),
            array('id' => '13','src' => 'slider_1.jpg','record_id' => '0','used_in' => 'sliders','active' => '1','created_at' => '2020-01-17 08:47:56','updated_at' => '2020-01-17 08:47:56'),
            array('id' => '14','src' => 'noPhoto.jpg','record_id' => '2','used_in' => 'articles','active' => '1','created_at' => '2020-06-09 19:14:39','updated_at' => '2020-06-09 19:14:39'),
            array('id' => '15','src' => 'products_159251850134.jpg','record_id' => '3','used_in' => 'articles','active' => '1','created_at' => '2020-06-11 18:25:31','updated_at' => '2020-06-19 02:45:01'),
            array('id' => '16','src' => 'products_159251848752.jpg','record_id' => '4','used_in' => 'articles','active' => '1','created_at' => '2020-06-14 19:18:12','updated_at' => '2020-06-19 02:44:47'),
            array('id' => '17','src' => 'products_159251847372.jpg','record_id' => '5','used_in' => 'articles','active' => '1','created_at' => '2020-06-14 19:25:42','updated_at' => '2020-06-19 02:44:33'),
            array('id' => '18','src' => 'products_159251828144.jpg','record_id' => '6','used_in' => 'articles','active' => '1','created_at' => '2020-06-14 19:26:49','updated_at' => '2020-06-19 02:41:21'),
            array('id' => '19','src' => 'products_159251824434.jpg','record_id' => '7','used_in' => 'articles','active' => '1','created_at' => '2020-06-14 19:27:47','updated_at' => '2020-06-19 02:40:44'),
            array('id' => '20','src' => 'noPhoto.jpg','record_id' => '8','used_in' => 'right.slider','active' => '1','created_at' => '2020-06-09 19:14:39','updated_at' => '2020-06-09 19:14:39')
        );
        foreach ($images as $item)
            Image::create($item);


    }
}
