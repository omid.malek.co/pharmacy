<?php

use Illuminate\Database\Seeder;
use App\Type;
class TypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        /* `dart`.`types` */
        $types = array(
            array('id' => '1','title' => 'خرید کالا','key_word' => 'productPurchase','created_at' => '2020-07-19 15:16:46','updated_at' => '2020-07-19 15:16:46'),
            array('id' => '2','title' => 'شارژ کیف پول','key_word' => 'walletCharge','created_at' => '2020-07-19 15:16:46','updated_at' => '2020-07-19 15:16:46')
        );
        foreach ($types as $item)
            Type::create($item);
    }
}
