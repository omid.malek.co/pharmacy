<?php

use Illuminate\Database\Seeder;
use App\Payment;

class PaymentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /* `dart`.`payments` */
        $payments = array(
            array('id' => '1','payment_amount' => '228000','total_sum' => '228000','payment_message' => 'null','payment_message_code' => '0','customer_id' => '1','discount_id' => '2','gift_id' => '0','gift_amount' => '0','discount_value' => '21280.000000000004','sent_address' => 'null','sent_phone' => 'null','receiver_fullname' => 'null','postal_code_sent' => 'null','condition_id' => '6','type_id' => '1','Authority' => 'null','reference_code' => 'null','payment_status' => '3','active' => '1','RefID' => '0','created_at' => '2020-05-13 16:49:14','updated_at' => '2020-09-11 07:58:35'),
            array('id' => '7','payment_amount' => '2000','total_sum' => '0','payment_message' => 'عمليات با موفقيت انجام گرديده است.','payment_message_code' => '100','customer_id' => '1','discount_id' => '0','gift_id' => '0','gift_amount' => '0','discount_value' => '0','sent_address' => 'null','sent_phone' => 'null','receiver_fullname' => 'null','postal_code_sent' => 'null','condition_id' => '1','type_id' => '2','Authority' => 'A00000000000000000000000000214740100','reference_code' => '37852490','payment_status' => '3','active' => '1','RefID' => '21474010001','created_at' => '2020-09-04 02:49:23','updated_at' => '2020-09-04 02:52:07'),
            array('id' => '8','payment_amount' => '2000','total_sum' => '0','payment_message' => 'عمليات با موفقيت انجام گرديده است.','payment_message_code' => '100','customer_id' => '1','discount_id' => '0','gift_id' => '0','gift_amount' => '0','discount_value' => '0','sent_address' => 'null','sent_phone' => 'null','receiver_fullname' => 'null','postal_code_sent' => 'null','condition_id' => '1','type_id' => '2','Authority' => 'A00000000000000000000000000214740431','reference_code' => '73029685','payment_status' => '3','active' => '1','RefID' => '21474043101','created_at' => '2020-09-04 02:56:04','updated_at' => '2020-09-04 02:57:07'),
            array('id' => '9','payment_amount' => '2000','total_sum' => '0','payment_message' => 'عمليات با موفقيت انجام گرديده است.','payment_message_code' => '100','customer_id' => '1','discount_id' => '0','gift_id' => '0','gift_amount' => '0','discount_value' => '0','sent_address' => 'null','sent_phone' => 'null','receiver_fullname' => 'null','postal_code_sent' => 'null','condition_id' => '1','type_id' => '2','Authority' => 'A00000000000000000000000000214742627','reference_code' => '82309764','payment_status' => '3','active' => '1','RefID' => '0','created_at' => '2020-09-04 03:48:32','updated_at' => '2020-09-04 03:52:34'),
            array('id' => '10','payment_amount' => '2000','total_sum' => '0','payment_message' => 'عمليات با موفقيت انجام گرديده است.','payment_message_code' => '100','customer_id' => '1','discount_id' => '0','gift_id' => '0','gift_amount' => '0','discount_value' => '0','sent_address' => 'null','sent_phone' => 'null','receiver_fullname' => 'null','postal_code_sent' => 'null','condition_id' => '1','type_id' => '2','Authority' => 'A00000000000000000000000000214742814','reference_code' => '92765183','payment_status' => '3','active' => '1','RefID' => '0','created_at' => '2020-09-04 03:54:02','updated_at' => '2020-09-04 03:55:38'),
            array('id' => '11','payment_amount' => '1000','total_sum' => '0','payment_message' => 'عمليات با موفقيت انجام گرديده است.','payment_message_code' => '100','customer_id' => '1','discount_id' => '0','gift_id' => '0','gift_amount' => '0','discount_value' => '0','sent_address' => 'null','sent_phone' => 'null','receiver_fullname' => 'null','postal_code_sent' => 'null','condition_id' => '1','type_id' => '2','Authority' => 'A00000000000000000000000000214742899','reference_code' => '76208543','payment_status' => '3','active' => '1','RefID' => '0','created_at' => '2020-09-04 03:57:01','updated_at' => '2020-09-04 03:59:45'),
            array('id' => '12','payment_amount' => '1000','total_sum' => '0','payment_message' => 'عمليات با موفقيت انجام گرديده است.','payment_message_code' => '100','customer_id' => '1','discount_id' => '0','gift_id' => '0','gift_amount' => '0','discount_value' => '0','sent_address' => 'null','sent_phone' => 'null','receiver_fullname' => 'null','postal_code_sent' => 'null','condition_id' => '1','type_id' => '2','Authority' => 'A00000000000000000000000000214743022','reference_code' => '82576130','payment_status' => '3','active' => '1','RefID' => '21474302201','created_at' => '2020-09-04 04:00:21','updated_at' => '2020-09-04 04:02:26'),
            array('id' => '13','payment_amount' => '76000','total_sum' => '0','payment_message' => 'عمليات با موفقيت انجام گرديده است.','payment_message_code' => '100','customer_id' => '1','discount_id' => '0','gift_id' => '3','gift_amount' => '64400','discount_value' => '0','sent_address' => 'بلوار معلم، نبش معلم 16، پلاک 36','sent_phone' => '09365647585','receiver_fullname' => 'امیر مداح','postal_code_sent' => '9189171111','condition_id' => '3','type_id' => '1','Authority' => 'null','reference_code' => '60523189','payment_status' => '3','active' => '1','RefID' => '0','created_at' => '2020-09-07 07:50:58','updated_at' => '2020-09-09 16:38:07'),
            array('id' => '14','payment_amount' => '1000','total_sum' => '0','payment_message' => 'عمليات با موفقيت انجام گرديده است.','payment_message_code' => '100','customer_id' => '1','discount_id' => '0','gift_id' => '4','gift_amount' => '1000','discount_value' => '0','sent_address' => 'بلوار معلم، نبش معلم 16، پلاک 36','sent_phone' => '09365647585','receiver_fullname' => 'امیر مداح','postal_code_sent' => '9189171111','condition_id' => '3','type_id' => '1','Authority' => 'null','reference_code' => '94306582','payment_status' => '3','active' => '1','RefID' => '0','created_at' => '2020-09-09 21:51:51','updated_at' => '2020-09-09 21:59:02')
        );
        foreach ($payments as $item)
            Payment::create($item);

    }
}
