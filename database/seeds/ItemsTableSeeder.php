<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Item;

class ItemsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = array(
            array('id' => '1', 'personnel_id' => '0', 'user_id' => '1', 'page_id' => '1', 'page_title' => 'کسری دارو', 'route_name' => 'drugs.index', 'address' => 'drugs', 'parameters' => '', 'base_url' => 'null', 'is_base_page' => '1', 'child_id' => '0', 'parent_id' => '0', 'permission_id' => '1', 'permission_title' => 'مدیر سامانه', 'active' => '1', 'created_at' => '2022-05-09 00:47:29', 'updated_at' => '2022-05-09 00:47:29'),
            array('id' => '2', 'personnel_id' => '0', 'user_id' => '1', 'page_id' => '2', 'page_title' => 'تغییر  رمز', 'route_name' => 'admin.password.index', 'address' => 'admin_password', 'parameters' => '', 'base_url' => 'null', 'is_base_page' => '1', 'child_id' => '0', 'parent_id' => '0', 'permission_id' => '1', 'permission_title' => 'مدیر سامانه', 'active' => '1', 'created_at' => '2022-05-09 00:47:29', 'updated_at' => '2022-05-09 00:47:29'),
            array('id' => '3', 'personnel_id' => '0', 'user_id' => '1', 'page_id' => '3', 'page_title' => 'اطلاعات مدیریت', 'route_name' => 'admin.edit', 'address' => 'admins_show', 'parameters' => '', 'base_url' => 'null', 'is_base_page' => '1', 'child_id' => '0', 'parent_id' => '0', 'permission_id' => '1', 'permission_title' => 'مدیر سامانه', 'active' => '1', 'created_at' => '2022-05-09 00:47:29', 'updated_at' => '2022-05-09 00:47:29'),
            array('id' => '4', 'personnel_id' => '0', 'user_id' => '1', 'page_id' => '4', 'page_title' => 'محصولات', 'route_name' => 'admin.products.index', 'address' => 'products_management_list', 'parameters' => '', 'base_url' => 'null', 'is_base_page' => '1', 'child_id' => '0', 'parent_id' => '0', 'permission_id' => '1', 'permission_title' => 'مدیر سامانه', 'active' => '1', 'created_at' => '2022-05-09 00:47:29', 'updated_at' => '2022-05-09 00:47:29'),
            array('id' => '5', 'personnel_id' => '0', 'user_id' => '1', 'page_id' => '5', 'page_title' => 'افزودن محصول', 'route_name' => 'admin.products.create', 'address' => 'products_create', 'parameters' => '', 'base_url' => 'null', 'is_base_page' => '0', 'child_id' => '0', 'parent_id' => '0', 'permission_id' => '1', 'permission_title' => 'مدیر سامانه', 'active' => '1', 'created_at' => '2022-05-10 22:25:50', 'updated_at' => '2022-05-10 22:25:50'),
            array('id' => '6', 'personnel_id' => '0', 'user_id' => '1', 'page_id' => '6', 'page_title' => 'فاکتورها', 'route_name' => 'factors.management.index', 'address' => 'factors_list', 'parameters' => '', 'base_url' => 'null', 'is_base_page' => '1', 'child_id' => '0', 'parent_id' => '0', 'permission_id' => '1', 'permission_title' => 'مدیر سامانه', 'active' => '1', 'created_at' => '2022-05-10 22:28:18', 'updated_at' => '2022-05-10 22:28:18'),
            array('id' => '7', 'personnel_id' => '0', 'user_id' => '1', 'page_id' => '11', 'page_title' => 'پرسنل', 'route_name' => 'items.index', 'address' => 'items', 'parameters' => '', 'base_url' => 'null', 'is_base_page' => '1', 'child_id' => '0', 'parent_id' => '0', 'permission_id' => '1', 'permission_title' => 'مدیر سامانه', 'active' => '1', 'created_at' => '2022-05-09 00:47:29', 'updated_at' => '2022-05-09 00:47:29'),
            array('id' => '32', 'personnel_id' => '0', 'user_id' => '2', 'page_id' => '7', 'page_title' => 'کسری دارو', 'route_name' => 'drugs.index', 'address' => 'drugs', 'parameters' => '', 'base_url' => 'null', 'is_base_page' => '1', 'child_id' => '0', 'parent_id' => '0', 'permission_id' => '2', 'permission_title' => 'پرسنل', 'active' => '1', 'created_at' => '2022-06-24 23:54:44', 'updated_at' => '2022-06-24 23:54:44'),
            array('id' => '33', 'personnel_id' => '0', 'user_id' => '2', 'page_id' => '8', 'page_title' => 'محصولات', 'route_name' => 'admin.products.index', 'address' => 'products_management_list', 'parameters' => '', 'base_url' => 'null', 'is_base_page' => '1', 'child_id' => '0', 'parent_id' => '0', 'permission_id' => '2', 'permission_title' => 'پرسنل', 'active' => '1', 'created_at' => '2022-06-24 23:54:44', 'updated_at' => '2022-06-24 23:54:44'),
            array('id' => '34', 'personnel_id' => '0', 'user_id' => '2', 'page_id' => '10', 'page_title' => 'فاکتورها', 'route_name' => 'factors.management.index', 'address' => 'factors_list', 'parameters' => '', 'base_url' => 'null', 'is_base_page' => '1', 'child_id' => '0', 'parent_id' => '0', 'permission_id' => '2', 'permission_title' => 'پرسنل', 'active' => '1', 'created_at' => '2022-06-24 23:54:44', 'updated_at' => '2022-06-24 23:54:44'),
            array('id' => '35', 'personnel_id' => '0', 'user_id' => '2', 'page_id' => '12', 'page_title' => 'تغییر رمز', 'route_name' => 'admin.password.index', 'address' => 'admin_password', 'parameters' => '', 'base_url' => 'null', 'is_base_page' => '1', 'child_id' => '0', 'parent_id' => '0', 'permission_id' => '2', 'permission_title' => 'پرسنل', 'active' => '1', 'created_at' => '2022-06-24 23:54:44', 'updated_at' => '2022-06-24 23:54:44'),
            array('id' => '40', 'personnel_id' => '0', 'user_id' => '7', 'page_id' => '7', 'page_title' => 'کسری دارو', 'route_name' => 'drugs.index', 'address' => 'drugs', 'parameters' => '', 'base_url' => 'null', 'is_base_page' => '1', 'child_id' => '0', 'parent_id' => '0', 'permission_id' => '2', 'permission_title' => 'پرسنل', 'active' => '1', 'created_at' => '2022-07-23 00:24:07', 'updated_at' => '2022-07-23 00:24:07'),
            array('id' => '41', 'personnel_id' => '0', 'user_id' => '7', 'page_id' => '8', 'page_title' => 'محصولات', 'route_name' => 'admin.products.index', 'address' => 'products_management_list', 'parameters' => '', 'base_url' => 'null', 'is_base_page' => '1', 'child_id' => '0', 'parent_id' => '0', 'permission_id' => '2', 'permission_title' => 'پرسنل', 'active' => '1', 'created_at' => '2022-07-23 00:24:07', 'updated_at' => '2022-07-23 00:24:07'),
            array('id' => '42', 'personnel_id' => '0', 'user_id' => '7', 'page_id' => '10', 'page_title' => 'فاکتورها', 'route_name' => 'factors.management.index', 'address' => 'factors_list', 'parameters' => '', 'base_url' => 'null', 'is_base_page' => '1', 'child_id' => '0', 'parent_id' => '0', 'permission_id' => '2', 'permission_title' => 'پرسنل', 'active' => '1', 'created_at' => '2022-07-23 00:24:07', 'updated_at' => '2022-07-23 00:24:07'),
            array('id' => '43', 'personnel_id' => '0', 'user_id' => '7', 'page_id' => '12', 'page_title' => 'تغییر رمز', 'route_name' => 'admin.password.index', 'address' => 'admin_password', 'parameters' => '', 'base_url' => 'null', 'is_base_page' => '1', 'child_id' => '0', 'parent_id' => '0', 'permission_id' => '2', 'permission_title' => 'پرسنل', 'active' => '1', 'created_at' => '2022-07-23 00:24:07', 'updated_at' => '2022-07-23 00:24:07'),
            array('id' => '44', 'personnel_id' => '0', 'user_id' => '2', 'page_id' => '15', 'page_title' => 'مستندات', 'route_name' => 'documents.documents.personnels', 'address' => 'show_documents_for_personnel', 'parameters' => '', 'base_url' => 'null', 'is_base_page' => '1', 'child_id' => '0', 'parent_id' => '0', 'permission_id' => '2', 'permission_title' => 'پرسنل', 'active' => '1', 'created_at' => '2022-07-23 00:24:07', 'updated_at' => '2022-07-23 00:24:07'),
            array('id' => '45', 'personnel_id' => '0', 'user_id' => '7', 'page_id' => '15', 'page_title' => 'مستندات', 'route_name' => 'documents.documents.personnels', 'address' => 'show_documents_for_personnel', 'parameters' => '', 'base_url' => 'null', 'is_base_page' => '1', 'child_id' => '0', 'parent_id' => '0', 'permission_id' => '2', 'permission_title' => 'پرسنل', 'active' => '1', 'created_at' => '2022-07-23 00:24:07', 'updated_at' => '2022-07-23 00:24:07')
        );
        $pageChanges=array(
          array('old'=>7,'new'=>1),
          array('old'=>8,'new'=>4),
          array('old'=>10,'new'=>6),
          array('old'=>12,'new'=>2),
          array('old'=>15,'new'=>13)
        );
        //---
        $table = 'items';

        for ($i=0;count($items);$i++) {
            $pages_table = 'pages';
            (integer)$hasInpages = DB::table($pages_table)
                ->where('id', $items[$i]['page_id'])
                ->exists();
            (integer)$hasNotItem = DB::table($table)
                ->where('id', $items[$i]['id'])
                ->doesntExist();

            if ($hasNotItem && $hasInpages)
                Item::create($items[$i]);
            else if ($items[$i]['user_id'] != 2) {
                foreach($pageChanges as $changed){
                    if($items[$i]['page_id']==$changed['old']){
                        $items[$i]['page_id']=$changed['new'];
                        $newRecord=array(
                            'personnel_id' => $items[$i]['personnel_id'],
                            'user_id' => $items[$i]['user_id'],
                            'page_id' => $changed['new'],
                            'page_title' => $items[$i]['page_title'],
                            'route_name' => $items[$i]['route_name'],
                            'address' => $items[$i]['address'],
                            'parameters' => $items[$i]['parameters'],
                            'base_url' => $items[$i]['base_url'],
                            'is_base_page' => $items[$i]['is_base_page'],
                            'child_id' => $items[$i]['child_id'],
                            'parent_id' =>$items[$i]['parent_id'],
                            'permission_id' => $items[$i]['permission_id'],
                            'permission_title' => $items[$i]['permission_title'],
                            'active' => $items[$i]['active'],
                            'created_at' => $items[$i]['created_at'],
                            'updated_at' => $items[$i]['updated_at']
                        );
                        Item::create($newRecord);
                    }
                }
            }

        }
    }
}
