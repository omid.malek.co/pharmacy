<?php

use Illuminate\Database\Seeder;
use App\Categorie;



class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */



    public function run()
    {
        //
//        $data=[
//            [
//                'title'=>'تخت دارت',
//                'active'=>1
//            ],
//            [
//                'title'=>'دارت',
//                'active'=>1
//            ],
//            [
//                'title'=>'فلایت',
//                'active'=>1
//            ],
//            [
//                'title'=>'شات',
//                'active'=>1
//            ],
//            [
//                'title'=>'تیپ',
//                'active'=>1
//            ],
//            [
//                'title'=>'لوازم جانبی',
//                'active'=>1
//            ]
//        ];


        /* `dart`.`categories` */
        $categories = array(
            array('id' => '1','title' => 'تخت دارت','active' => '1','created_at' => '2020-01-17 08:46:33','updated_at' => '2020-01-17 08:46:33'),
            array('id' => '2','title' => 'دارت','active' => '1','created_at' => '2020-01-17 08:46:33','updated_at' => '2020-01-17 08:46:33'),
            array('id' => '3','title' => 'فلایت','active' => '1','created_at' => '2020-01-17 08:46:33','updated_at' => '2020-01-17 08:46:33'),
            array('id' => '4','title' => 'شات','active' => '1','created_at' => '2020-01-17 08:46:33','updated_at' => '2020-01-17 08:46:33'),
            array('id' => '5','title' => 'تیپ','active' => '1','created_at' => '2020-01-17 08:46:33','updated_at' => '2020-01-17 08:46:33'),
            array('id' => '6','title' => 'لوازم جانبی','active' => '1','created_at' => '2020-01-17 08:46:33','updated_at' => '2020-01-17 08:46:33')
        );

        foreach ($categories as $item)
            Categorie::create($item);

    }
}
