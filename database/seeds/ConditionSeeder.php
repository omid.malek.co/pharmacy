<?php

use Illuminate\Database\Seeder;
use App\Condition;
class ConditionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        /* `dart`.`conditions` */
        $conditions = array(
            array('id' => '1','title' => 'دیده نشده','key_word' => 'Unseen','active' => '1','created_at' => '2020-08-09 16:08:33','updated_at' => '2020-08-09 16:08:33'),
            array('id' => '2','title' => 'استفاده نشده','key_word' => 'notUsed','active' => '1','created_at' => '2020-09-06 08:15:35','updated_at' => '2020-09-06 08:15:35'),
            array('id' => '3','title' => 'ارسال نشده','key_word' => 'notSent','active' => '1','created_at' => '2020-09-06 08:20:19','updated_at' => '2020-09-06 08:20:19'),
            array('id' => '4','title' => 'دیده شده','key_word' => 'seen','active' => '1','created_at' => '2020-08-09 16:06:02','updated_at' => '2020-08-09 16:06:02'),
            array('id' => '5','title' => 'استفاده شده','key_word' => 'used','active' => '1','created_at' => '2020-09-06 08:15:35','updated_at' => '2020-09-06 08:15:35'),
            array('id' => '6','title' => 'ارسال شده','key_word' => 'Sent','active' => '1','created_at' => '2020-09-06 08:20:19','updated_at' => '2020-09-06 08:20:19')
        );
        foreach ($conditions as $item)
            Condition::create($item);
    }
}
