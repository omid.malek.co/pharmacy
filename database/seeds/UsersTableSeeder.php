<?php

use Illuminate\Database\Seeder;

use App\User;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //--

        /* `pharmacy`.`users` */
        $users = array(
            array('id' => '1', 'fullname' => 'مدیر سامانه', 'username' => 'admin', 'email' => 'email@sample.com', 'email_verified_at' => NULL, 'password' => '25f9e794323b453885f5181f1b624d0b', 'phone' => '09131554230', 'address' => 'بلوار معلم، نبش معلم 16، پلاک 36', 'number_cart' => '6104337927742401', 'number_account' => '6104337927', 'facebook_address' => 'null', 'instagram_address' => 'null', 'telegram_address' => 'null', 'permission_id' => '1', 'active' => '1', 'remember_token' => NULL, 'created_at' => '2020-01-17 08:45:35', 'updated_at' => '2020-09-13 14:42:42'),
            array('id' => '2', 'fullname' => 'امید مالک', 'username' => '09365647585', 'email' => 'email@sample.com', 'email_verified_at' => NULL, 'password' => '25f9e794323b453885f5181f1b624d0b', 'phone' => '09365647585', 'address' => 'بدون توضیح', 'number_cart' => 'null', 'number_account' => 'null', 'facebook_address' => 'null', 'instagram_address' => 'null', 'telegram_address' => 'null', 'permission_id' => '2', 'active' => '1', 'remember_token' => NULL, 'created_at' => '2022-06-24 23:54:44', 'updated_at' => '2022-06-24 23:54:44'),
            array('id' => '7', 'fullname' => 'فرشاد مالک', 'username' => '09131554230', 'email' => 'farshad.malek2011@gmail.com', 'email_verified_at' => NULL, 'password' => 'ada867c20a36fc1a1015b38af24382b1', 'phone' => '09131554230', 'address' => 'طبس - خیابان نواب صفوی', 'number_cart' => 'null', 'number_account' => 'null', 'facebook_address' => 'null', 'instagram_address' => 'null', 'telegram_address' => 'null', 'permission_id' => '2', 'active' => '1', 'remember_token' => NULL, 'created_at' => '2022-07-23 00:24:07', 'updated_at' => '2022-08-13 09:31:52')
        );

        $table = 'users';
        foreach ($users as $item) {
            (integer)$hasNotItem = DB::table($table)
                ->where('id', $item['id'])
                ->doesntExist();
            if ($hasNotItem)
                User::create($item);
        }
    }
}
