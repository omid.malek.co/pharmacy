<?php

use Illuminate\Database\Seeder;
use App\Discount;
class DiscountSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /* `dart`.`discounts` */
        $discounts = array(
            array('id' => '2','text' => 'takhfif','percent' => '10.00','start_date' => '2020-09-08','expire_date' => '2020-09-12','number_validation' => '1','active' => '1','created_at' => '2020-07-16 23:21:09','updated_at' => '2020-08-07 19:46:50')
        );
        foreach ($discounts as $item)
            Discount::create($item);

    }
}
