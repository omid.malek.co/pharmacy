<?php

use Illuminate\Database\Seeder;
use App\Post;
class PostSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /* `dart`.`posts` */
        $posts = array(
            array('id' => '3','title' => 'اولین مقاله دارت مارکت','body' => '<p>متن اولین مقاله دارت مارکت</p>','published' => '1','used_in' => 'articles','visited' => '1','created_by' => '0','updated_by' => '1','created_at' => '2020-06-11 18:25:31','updated_at' => '2020-06-11 19:23:32'),
            array('id' => '4','title' => 'دومین مقاله دارت مارکت','body' => '<p><img style="display: block; margin-left: auto; margin-right: auto;" src="/photos/1/bg-01.jpg" alt="" width="836" height="409" /></p>
<p style="text-align: center;">متن دومین مقاله دارت مارکت</p>','published' => '1','used_in' => 'articles','visited' => '1','created_by' => '0','updated_by' => '1','created_at' => '2020-06-14 19:18:12','updated_at' => '2020-07-02 14:08:13'),
            array('id' => '5','title' => 'سومین پست دارت مارکت','body' => '<p style="text-align: center;"><img style="display: block; margin-left: auto; margin-right: auto;" src="/photos/1/bg-01.jpg" alt="" width="899" height="440" /></p>
<p style="text-align: center;">متن سومین پست دارت مارکت</p>','published' => '1','used_in' => 'articles','visited' => '1','created_by' => '0','updated_by' => '1','created_at' => '2020-06-14 19:25:42','updated_at' => '2020-07-08 06:56:42'),
            array('id' => '6','title' => 'چهارمین پست دارت مارکت','body' => '<p>متن چهارمین پست دارت مارکت</p>','published' => '1','used_in' => 'articles','visited' => '1','created_by' => '0','updated_by' => '1','created_at' => '2020-06-14 19:26:49','updated_at' => '2020-06-19 02:41:17'),
            array('id' => '7','title' => 'پنجمین پست دارت مارکت','body' => '<p>متن پنجمین پست دارت مارکت</p>','published' => '1','used_in' => 'articles','visited' => '2','created_by' => '0','updated_by' => '1','created_at' => '2020-06-14 19:27:47','updated_at' => '2020-07-10 10:55:02'),
            array('id' => '8','title' => 'هنر پرتاب دارت','body' => '<h5 class="text-right">ورزش دارت را باید یکی از شاخه های ورزش و حتی آمادگی نظامی دانست که به نوعی آن را یک هنر می دانند: هنر پرتاب دارت</h5>','published' => '1','used_in' => 'right.slider','visited' => '1','created_by' => '0','updated_by' => '1','created_at' => '2020-07-05 01:24:56','updated_at' => '2020-07-05 01:24:56')
        );


        foreach ($posts as $item)
            Post::create($item);

    }
}
