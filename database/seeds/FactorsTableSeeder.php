<?php

use Illuminate\Database\Seeder;
use App\Factor;
class FactorsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /* `dart`.`factors` */
        $factors = array(
            array('id' => '1','product_id' => '4','sent_number' => '1','type_id' => '1','customer_id' => '1','payment_id' => '1','active' => '1','created_at' => '2020-05-13 16:49:14','updated_at' => '2020-05-13 16:49:14'),
            array('id' => '7','product_id' => '6','sent_number' => '1','type_id' => '1','customer_id' => '1','payment_id' => '1','active' => '1','created_at' => '2020-08-04 14:31:33','updated_at' => '2020-08-04 14:31:33'),
            array('id' => '10','product_id' => '2','sent_number' => '1','type_id' => '1','customer_id' => '1','payment_id' => '13','active' => '1','created_at' => '2020-09-07 10:23:21','updated_at' => '2020-09-07 10:23:21'),
            array('id' => '11','product_id' => '1','sent_number' => '1','type_id' => '1','customer_id' => '1','payment_id' => '14','active' => '1','created_at' => '2020-09-09 21:51:51','updated_at' => '2020-09-09 21:51:51')
        );
        foreach ($factors as $item)
            Factor::create($item);
    }
}
