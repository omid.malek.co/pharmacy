<?php

use Illuminate\Database\Seeder;


use App\Product;
use App\Categorie;
use App\Image;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /* `dart`.`products` */
        $products = array(
            array('id' => '1','title' => 'دارت STARTOS مدل 24G','description' => 'در این قسمت توضیحات مربوط به کالای مورد نظر ارائه می گردد.','sales' => '2','amount' => '1000','active' => '1','number' => '20','categorie_id' => '1','created_at' => '2020-01-17 08:47:28','updated_at' => '2020-07-08 07:14:00'),
            array('id' => '2','title' => 'دارت STARTOS مدل 24G','description' => 'در این قسمت توضیحات مربوط به کالای مورد نظر ارائه می گردد.','sales' => '3','amount' => '76000','active' => '1','number' => '19','categorie_id' => '2','created_at' => '2020-01-17 08:47:28','updated_at' => '2020-01-17 08:47:28'),
            array('id' => '3','title' => 'دارت STARTOS مدل 24G','description' => 'در این قسمت توضیحات مربوط به کالای مورد نظر ارائه می گردد.','sales' => '4','amount' => '76000','active' => '1','number' => '18','categorie_id' => '3','created_at' => '2020-01-17 08:47:28','updated_at' => '2020-01-17 08:47:28'),
            array('id' => '4','title' => 'دارت STARTOS مدل 24G','description' => 'در این قسمت توضیحات مربوط به کالای مورد نظر ارائه می گردد.','sales' => '5','amount' => '76000','active' => '1','number' => '17','categorie_id' => '4','created_at' => '2020-01-17 08:47:28','updated_at' => '2020-01-17 08:47:28'),
            array('id' => '5','title' => 'دارت STARTOS مدل 24G','description' => 'در این قسمت توضیحات مربوط به کالای مورد نظر ارائه می گردد.','sales' => '6','amount' => '76000','active' => '1','number' => '16','categorie_id' => '5','created_at' => '2020-01-17 08:47:28','updated_at' => '2020-01-17 08:47:28'),
            array('id' => '6','title' => 'دارت STARTOS مدل 24G','description' => 'در این قسمت توضیحات مربوط به کالای مورد نظر ارائه می گردد.','sales' => '7','amount' => '76000','active' => '1','number' => '15','categorie_id' => '6','created_at' => '2020-01-17 08:47:28','updated_at' => '2020-01-17 08:47:28')
        );
        foreach ($products as $item){
            Product::create($item);
        }

//        foreach ($categories as $item) {
//
//            $product = [
//                'title' => 'دارت STARTOS مدل 24G',
//                'description' => 'در این قسمت توضیحات مربوط به کالای مورد نظر ارائه می گردد.',
//                'sales' => $sales++,
//                'amount' => '76000',
//                'active' => 1,
//                'number' => $number--,
//                'categorie_id' => $item->id
//            ];
//
//            Product::create($product);
//
//        }


//        $Products=Product::all();
//        $images=[
//            'dart_1.png',
//            'dart_2.jpg',
//            'dart_3.jpg'
//        ];
//        for($i=0;$i<count($Products);){
//            foreach ($images as $image){
//                $record=[
//                    'src'=>$image,
//                    'record_id'=>$Products[$i]->id,
//                    'used_in'=>'products',
//                    'active'=>1,
//                ];
//                $i++;
//                Image::create($record);
//            }
//        }
    }
}
