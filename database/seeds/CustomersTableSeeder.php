<?php

use Illuminate\Database\Seeder;
use App\Customer;

class CustomersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
//        $record=[
//            'fullname'=>'امید مالک',
//            'email'=>'omid.malek.1990@gmail.com',
//            'password'=>md5('123456'),
//            'address'=>'بلوار معلم، نبش معلم 16، پلاک 36',
//            'postal_code'=>'9189171111',
//            'phone'=>'09365647585',
//            'active'=>1
//        ];

        /* `dart`.`customers` */
        $customers = array(
            array('id' => '1','fullname' => 'امیر مداح','email' => 'omid.malek.1990@gmail.com','password' => '25f9e794323b453885f5181f1b624d0b','address' => 'بلوار معلم، نبش معلم 16، پلاک 36','postal_code' => '9189171111','phone' => '09365647585','token' => 'tdWG4s6bAp6zsC1tQ6SbzTrz74J5meVeJMDrheuKxhln4wh19BP2tFNYTD9S','active' => '1','created_at' => '2020-02-15 17:32:42','updated_at' => '2020-09-10 10:21:32'),
            array('id' => '2','fullname' => 'null','email' => 'test@gmail.com','password' => '25f9e794323b453885f5181f1b624d0b','address' => 'null','postal_code' => 'null','phone' => '09354612685','token' => '55JaaHmXAZc8jkZIJbIPgbFE2o3VaHkMQX9Cjr9shUBZGwFRkiDqLEvkRRIM','active' => '1','created_at' => '2020-09-09 22:02:51','updated_at' => '2020-09-09 22:03:04')
        );
        foreach ($customers as $item) {
            Customer::create($item);
        }
    }
}
