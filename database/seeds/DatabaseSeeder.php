<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
         $this->call(CategoriesTableSeeder::class);
         $this->call(CustomersTableSeeder::class);
         $this->call(DiscountSeeder::class);
         $this->call(FactorsTableSeeder::class);
         $this->call(ImagesTableSeeder::class);
         $this->call(PaymentSeeder::class);
         $this->call(PostSeeder::class);
         $this->call(ProductsTableSeeder::class);
         $this->call(UsersTableSeeder::class);
         $this->call(TypeSeeder::class);
         $this->call(ConditionSeeder::class);
         $this->call(GiftSeeder::class);
         $this->call(WalletSeeder::class);
    }
}
