<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pages', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title',40)->comment('عنوان صفحه')->default('بدون عنوان');
            $table->string('address',200)->comment('آدرس نسبی صفحه')->default('null');
            $table->string('base_url',150)->comment('آدرس سایت')->default('null');
            $table->integer('is_base_page')->comment('صفحه زیر مجموعه صفحه دیگری نیست؟')->default(1);
            $table->bigInteger('permission_id')->comment('کلید سطح دسترسی')->default(0);
            $table->string('permission_title',40)->comment('عنوان سطح دسترسی')->default('بدون عنوان');
            $table->boolean('active')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pages');
    }
}
