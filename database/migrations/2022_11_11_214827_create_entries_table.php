<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEntriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('entries', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('user_id')->default(1)->unsigned()->index()->nullable();
            $table->string('user_fullname',200);
            $table->string('entry_date',25);
            $table->string('entry_time',25);
            $table->string('distance',40);
            $table->string('departure_date',25);
            $table->string('departure_time',25);
            $table->integer('confirm')->default(0)->unsigned();
            $table->integer('confirm_by')->default(0)->unsigned()->index()->nullable();
            $table->boolean('active')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('entries');
    }
}
