<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePersonnelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('personnels', function (Blueprint $table) {
            $table->increments('id');
            $table->string('fullname');
            $table->string('email',250)->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->string('phone',16)->comment('شماره تماس')->default('null');
            $table->string('address',200)->comment('آدرس')->default('null');
            $table->string('number_cart',20)->comment('آدرس')->default('null');
            $table->string('number_account',20)->comment('آدرس')->default('null');
            $table->string('facebook_address',300)->comment('آدرس فیس بوک')->default('null');
            $table->string('instagram_address',300)->comment('آدرس اینستاگرام')->default('null');
            $table->string('telegram_address',300)->comment('آدرس تلگرام')->default('null');
            $table->integer('permission_id')->default(1)->unsigned()->index()->nullable()->comment('کلید سطح دسترسی');
            $table->boolean('active')->default(1);
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('personnels');
    }
}
