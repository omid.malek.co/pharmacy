<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWalletsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wallets', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title',80)->comment('موجودی کیف پول');
            $table->double('amount')->comment('موجودی کیف پول')->default(0);
            $table->double('type_id')->comment('کد نوع پرداخت')->default(2);
            $table->integer('customer_id')->unsigned()->index()->nullable()->comment('کلید داخلی کاربر');
            $table->boolean('active')->comment('متن پیام پیام فعال=1 و پیام غیر فعال=0')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wallets');
    }
}
