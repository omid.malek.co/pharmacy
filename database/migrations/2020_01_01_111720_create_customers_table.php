<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('fullname',150)->default('null');
            $table->string('email',250)->comment('ایمیل کاربر')->unique();
            $table->string('password')->comment('کلمه عبور');
            $table->string('address',600)->default('null')->comment('آدرس');
            $table->string('postal_code',40)->default('null')->comment('کد پستی');
            $table->string('phone',16)->default('null')->comment('شماره تماس')->default(0);
            $table->string('token',100)->comment(' کد توکن')->default('null');
            $table->boolean('active')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customers');
    }
}
