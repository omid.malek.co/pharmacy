<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDrugsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('drugs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name',150)->comment('عنوان کالا');
            $table->string('shape',200)->comment('شکل دارو');
            $table->integer('count')->comment('تعداد');
            $table->integer('scale_id')->unsigned()->index()->nullable()->comment('کد معیار پذیرش');
            $table->text('description')->comment('توضیحات مربوط به دارو');
            $table->boolean('active')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('drugs');
    }
}
