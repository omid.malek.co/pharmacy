<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('items', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('personnel_id')->comment('کلید کاربر در جدول پرسنل')->default(0);
            $table->bigInteger('user_id')->comment('کلید کاربری')->default(0);
            $table->bigInteger('page_id')->comment('کلید صفحه')->default(0);
            $table->string('page_title',60)->comment('عنوان صفحه')->default('بدون عنوان');
            $table->string('address',200)->comment('آدرس نسبی صفحه')->default('null');
            $table->string('parameters',200)->comment('پارامترها')->default('null');
            $table->string('base_url',150)->comment('آدرس سایت')->default('null');
            $table->integer('is_base_page')->comment('صفحه زیر مجموعه صفحه دیگری نیست؟')->default(1);
            $table->bigInteger('child_id')->comment('کد آیتم زیر مجموعه این صفحه')->default(0);
            $table->bigInteger('parent_id')->comment('کد آیتم والد این صفحه')->default(0);
            $table->bigInteger('permission_id')->comment('کلید سطح دسترسی')->default(1);
            $table->string('permission_title',40)->comment('عنوان سطح دسترسی')->default(1);
            $table->boolean('active')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('items');
    }
}
