<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('options', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('categorie_id')->unsigned()->index()->nullable()->comment('کلید دسته بندی کالا');
            $table->integer('product_id')->unsigned()->index()->nullable()->comment('کلید کالا');
            $table->string('title',80)->comment('عنوان ویژگی');
            $table->string('value',100)->comment('مقدار ویژگی');
            $table->integer('active')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('options');
    }
}
