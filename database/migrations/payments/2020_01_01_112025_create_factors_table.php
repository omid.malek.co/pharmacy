<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFactorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('factors', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('product_id')->unsigned()->index()->nullable()->comment('کلید خارجی پست');
            $table->integer('sent_number')->comment('تعداد کالاهای ارسال شده ');
            $table->integer('customer_id')->unsigned()->index()->nullable()->comment('کلید خارجی مشتری ها');
            $table->string('payment_id',40)->default(0)->comment('مبلغ پرداخت شده');
            $table->boolean('active')->default(1)->comment('حذف منطقی شده=0');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('factors');
    }
}
