<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDocumentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('documents', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('src',100);
            $table->string('icon',100);
            $table->string('title',100);
            $table->integer('user_id')->default(0)->unsigned()->index()->comment('کلید خارجی');
            $table->string('description',1000)->default('null')->comment('نام جدول');
            $table->string('keyword',80)->default('null')->comment('نام جدول');
            $table->boolean('active')->comment('وضعیت نمایش و یا عدم نمایش نمایش نظر')
                ->default(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('documents');
    }
}
