<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('images', function (Blueprint $table) {
            $table->increments('id');
            $table->string('src',100);
            $table->integer('record_id')->default(0)->unsigned()->index()->comment('کلید خارجی');
            $table->string('used_in',20)->default('null')->comment('نام جدول');
            $table->boolean('active')->comment('وضعیت نمایش و یا عدم نمایش نمایش نظر')
                ->default(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('images');
    }
}
