<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGiftsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gifts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('text',30)->default('null')->comment('کد تخفیف');
            $table->double('amount')->default('0')->comment('میزان تخفیف');
            $table->integer('wallet_id')->unsigned()->index()->nullable()->default('0')->comment('کد کارت هدیه');
            $table->integer('used_by')->default('0')->comment('استفاده شده توسط');
            $table->integer('created_by')->default('0')->comment('ایجاد شده توسط');
            $table->integer('condition_id')->default(1)->unsigned()->index()->nullable()->comment('کلید وضعیت');
            $table->date('start_date')->comment('تاریخ شروع اعمال تخفیف');
            $table->date('expire_date')->comment('تاریخ پایان اعمال تخفیف');
            $table->boolean('active')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gifts');
    }
}
