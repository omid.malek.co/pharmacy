<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments', function (Blueprint $table) {
            $table->increments('id');
            $table->string('payment_amount', 40)->default(0)->comment('مبلغ قابل پرداخت');
            $table->string('total_sum', 40)->default(0)->comment('مبلغ نهایی');
            $table->string('payment_message', 200)->default('null')->comment('پیام ارسال شده از طرف درگاه');
            $table->integer('payment_message_code')->default(0)->comment('کد ارسال شده از طرف درگاه');
            $table->integer('customer_id')->unsigned()->index()->nullable()->comment('کلید ایندکس مشتری');;
            $table->integer('discount_id')->default(0)->unsigned()->index()->nullable()->comment('کلید کلید مربوط به تخفیفات');
            $table->integer('gift_id')->default(0)->unsigned()->index()->nullable()->comment('کلید کلید مربوط به تخفیفات');
            $table->string('gift_amount',40)->default('0')->comment('کلید کلید مربوط به تخفیفات');
            $table->string('discount_value', 40)->default('0')->comment('مقدار مبلغ تخفیف');
            $table->string('sent_address', 600)->default('null')->comment('آدرس گیرنده کالا');
            $table->string('sent_phone', 600)->default('null')->comment('شماره تماس گیرنده کالا');
            $table->string('receiver_fullname', 150)->default('null')->comment('نام گیرنده کالا');
            $table->string('postal_code_sent', 40)->default('null')->comment('کد پستی');
            $table->integer('condition_id')->default(1)->unsigned()->index()->nullable()->comment('کلید وضعیت');
            $table->integer('type_id')->default(1)->unsigned()->index()->nullable();
            $table->string('Authority', 100)->default('null');
            $table->string('reference_code', 80)->default('null')->comment('کد پیگیری');
            $table->integer('payment_status')->default(0)->comment('وضعیت پرداخت فاکتور ثبت شده');
            $table->boolean('active')->default(1)->comment('نمایش یا عدم نمایش رکورد');
            $table->string('RefID', 45)->default('0');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payments');
    }
}
