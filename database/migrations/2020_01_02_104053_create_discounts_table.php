<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDiscountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('discounts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('text',30)->default('null')->comment('کد تخفیف');
            $table->float('percent')->default('0')->comment('میران درصد تخفیف');
            $table->date('start_date')->comment('تاریخ شروع اعمال تخفیف');
            $table->date('expire_date')->comment('تاریخ پایان اعمال تخفیف');
            $table->integer('number_validation')->default(1);
            $table->boolean('active')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('discounts');
    }
}
