<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title',150)->comment('عنوان کالا');
            $table->text('description')->comment('توضیحات مربوط به کالا');
            $table->integer('sales')->comment('تعداد کالاهای فروخته شده')->default(0);
            $table->string('amount',40)->comment('قیمت کالا');
            $table->boolean('active')->default(1);
            $table->boolean('number')->default(1)->comment('تعداد کالاهای موجود');
            $table->integer('categorie_id')->unsigned()->index()->nullable()->comment('کلید دسته بندی کالا');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
