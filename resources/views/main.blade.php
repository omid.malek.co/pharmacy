<!DOCTYPE html>
<html lang="fa">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!---->
    @stack('meta_tags')
    <!---->

    <link  rel="stylesheet" type="text/css" href="{{ asset('css/app.css') }}" />
    @stack('styles')

    <title>@yield('title')</title>
</head>
<body dir="ltr">
    <div class="container-fluid Container">
        @yield('content')

        @includeIf('frontend.PopUp')


    </div>
    {{--@includeIf('frontend.home')--}}
    <script type="text/javascript" src="{{ asset('js/app.js') }}"></script>
    {{--<script type="text/javascript" src="{{ asset('multislider/js/multislider.min.js') }}"></script>--}}
    @stack('scripts')
    <!-- start raycall script code -->

    <!-- end raycall script code -->
</body>
</html>
