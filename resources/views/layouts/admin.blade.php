<!DOCTYPE html>
<html class="rtl" dir="rtl" lang="fa">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>@yield('title')</title>

    {{-- Styles --}}
    <link rel="stylesheet" type="text/css" href="{{ asset('css/admin.css') }}"/>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css"
          integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">

    @yield('bundles')
</head>
<body dir="ltr">
    <div class="container-fluid Container">
        <div class="FirstRow">
            <div class="SiteHeader">
                <div class="WellcomeMsg">{{ Auth::user()->name.' خوش آمدید ' }}</div>
                <div id="ItemsIcon" class="ItemsIcon">
                    <i class="fas fa-bars"></i>
                </div>
                <div class="TitleDashboard">دارت مارکت</div>
            </div>
        </div>
        <div id="NavigationRow" class="NavigationRow">
            <ul id="NavigationColumn" page="{{ $page ?? '' }}" class="NavigationColumn">
                <li id="Home">
                    <a href="#" class="">
                        <span class="fas fa-tachometer-alt"></span>
                        <span>صفحه اصلی</span>
                    </a>
                </li>
                <li id="ChangePassword">
                    <a href="#" class="">
                        <span class="fas fa-key"></span>
                        <span>تغییر رمز عبور</span>
                    </a>
                </li>
                <li id="ChangeInformation">
                    <a href="#">
                        <span class="fas fa-user-edit"></span>
                        <span>اطلاعات مدیر</span>
                    </a>
                </li>
                <li id="Collections">
                    <a href="#">
                        <span class="fas fa-clipboard-list"></span>
                        <span>پست ها</span>
                    </a>
                </li>
                <li id="Customers">
                    <a href="#">
                        <span class="fas fa-users"></span>
                        <span>کاربران</span>
                    </a>
                </li>
                <li id="Comments">
                    <a href="#">
                        <span class="fas fa-comments"></span>
                        <span>نظرات</span>
                    </a>
                </li>
                <li id="Exit">
                    <a href="{{ route('admin.logout') }}">
                        <span class="fas fa-sign-out-alt"></span>
                        <span>خروج</span>
                    </a>
                </li>
            </ul>
        </div>
        <div class="RowContent">
            <div class="ListItemsColumn">
                @yield('Items')
            </div>
            <div class="ContentColumn">
                @yield('BaseSection')
            </div>
        </div>
    </div>

    {{-- Scripts --}}
    <script type="text/javascript" src="{{ asset('js/alladmin.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/AdminApp.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/fontawesome/all.js') }}"></script>
    @yield('scripts')
</body>
</html>
