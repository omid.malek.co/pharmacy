@extends('layouts.app')
@section('title')
    صفحه ورود
@endsection
@section('content')
    <div class="FirstRow">
        <div class="LoginFormFrame">
            <h1 class="text-center">ورود به پنل مدیریت</h1>
            @includeIf('pages.error')
            {{--{{ route('admin.signin') }}--}}
            <form method="Post" action="{{ route('admin.signin') }}">
                {{ csrf_field() }}
                <div class="InputsGroup input-group input-group-lg">
                    <input type="text" name="username" class="form-control text-left" placeholder="username"/><span
                            class="w-100"></span>
                </div>
                <div class="InputsGroup input-group input-group-lg">
                    <input type="password" name="password" class="form-control text-left input-group-lg"
                           placeholder="password"/>
                </div>
                <div class="LoginFormBTN">
                    <input class="SignInBtn hvr-pulse" type="submit" value="ورود به پنل"/>
                </div>
                {{--{{ route('forgotten.password') }}--}}
                <div class="hvr-pulse ForgottenPassword">
                    <a class="" href="">بازنشانی رمز عبور</a>
                </div>
            </form>
        </div>
    </div>
    {{--https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.js--}}

@endsection