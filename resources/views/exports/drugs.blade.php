<table>
    <thead>
        <tr style="text-align: center;">
            <th style="text-align: center;">#</th>
            <th style="text-align: center;">نام دارو</th>
            <th style="text-align: center;">شکل دارو</th>
            <th style="text-align: center;">تعداد کسری</th>
            <th style="text-align: center;">واحد شمارش</th>
            <th style="text-align: center;">توضیحات</th>
        </tr>
    </thead>
    <tbody>
        @foreach($drugs as $item)
            <tr>
                <td style="text-align: center;">{{ $row++ }}</td>
                <td style="text-align: center;">{{ $item->name }}</td>
                <td style="text-align: center;">{{ $item->shape }}</td>
                <td style="text-align: center;">{{ $item->count  }}</td>
                <td style="text-align: center;">{{ $item->scale->title }}</td>
                <td style="text-align: center;">{{ $item->description }}</td>
            </tr>
        @endforeach
    </tbody>
</table>