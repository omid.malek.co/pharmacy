@extends('layouts.admin')

@section('bundles')
    <script src="{{asset('vendor/tinymce/tinymce.min.js')}}"></script>

@endsection

@section('Items')
    <ul id="ListGroup" class="list-group ListGroupItems" item="{{ $operation ?? '' }}">
        <li id="ListOperation" class="list-group-item">
            <a href="{{ route('posts.index') }}">لیست پست ها</a>
        </li>
        <li id="AddOperation" class="list-group-item">
            <a href="{{ route('posts.create') }}">انتشار پست جدید</a>
        </li>
    </ul>
@endsection

@section('BaseSection')
    <div class="CreateCollectionPage">
        <form action="{{ empty($post) ? route('posts.store') : route('posts.update', $post) }}" method="post">
            {{ csrf_field() }}
            @if(!empty($post))
                @method('PUT')
            @endif
            <div class="CollectionInfo">
            </div>
            <div class="CollectionPost">
                <div class="InputFrame">
                    <label for="title">عنوان پست</label>
                    <input id="title" type="text" name="title" class="form-control text-right"
                           value="{{ old('title', ($post->title ?? '')) }}"/>
                </div>
            </div>
            <textarea id="article" name="body" dir="rtl" cols="0"
                      rows="0">{!! old('body', ($post->body ?? '')) !!}</textarea>
            <span>
                <button class="btn btn-primary btn-md" href="#" type="submit">منتشر کن</button>
                <a class="btn btn-info btn-md" href="{{ route('posts.index') }}" role="button">بازگشت</a>
            </span>
        </form>
    </div>

@endsection

@section('scripts')
    <script>
      tinymce.init({
        selector: '#article',
        language: 'fa_IR',
        allow_unsafe_link_target: true,
        // directionality: 'rtl',
        height: document.body.scrollHeight,
        plugins: [
          'advlist autolink lists link image charmap print preview hr anchor pagebreak',
          'searchreplace wordcount visualblocks visualchars code fullscreen',
          'insertdatetime media nonbreaking save table contextmenu directionality',
          'emoticons template paste textcolor colorpicker textpattern'
        ],
        toolbar: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media',
        relative_urls: false,
        file_browser_callback: function (field_name, url, type, win) {
          var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth
          var y = window.innerHeight || document.documentElement.clientHeight || document.getElementsByTagName('body')[0].clientHeight

          var cmsURL = '/' + 'laravel-filemanager?field_name=' + field_name
          if (type == 'image') {
            cmsURL = cmsURL + '&type=Images'
          } else {
            cmsURL = cmsURL + '&type=Files'
          }

          tinymce.activeEditor.windowManager.open({
            file: cmsURL,
            title: 'گالری',
            width: x * 0.8,
            height: y * 0.8,
            resizable: 'yes',
            close_previous: 'no'
          })
        }
      })
    </script>
@endsection

