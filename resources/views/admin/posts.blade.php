@extends('layouts.admin')

@section('Items')
    <ul id="ListGroup" class="list-group ListGroupItems" item="{{ $operation ?? '' }}">
        <li id="ListOperation" class="list-group-item">
            <a href="{{ route('posts.index') }}">لیست پست ها</a>
        </li>
        <li id="AddOperation" class="list-group-item">
            <a href="{{ route('posts.create') }}">انتشار پست جدید</a>
        </li>
    </ul>
@endsection

@section('BaseSection')

    <div class="SectionMajor table-responsive">
        @if(is_null($posts) || count($posts) <= 0)
            <section class="ErrorFrame">
                <section class="alert alert-primary text-center ErrorBox BMitra">
                    <span class="IRanSans ErrorMessage">{{ 'پستی تاکنون منتشر نشده است.' }}</span>
                </section>
            </section>
        @else
            <table class="table">
                <thead class="thead-light">
                <tr>
                    <th>عنوان</th>
                    <th>ارسال کننده</th>
                    <th>وضعیت نمایش</th>
                    <th>عملیات</th>
                </tr>
                </thead>
                <tbody>
                @foreach($posts as $post)
                    <tr>
                        <td>{{ $post->title }}</td>
                        <td>{{ $post->author->name }}</td>
                        <td>
                            @component('components.status')
                                @slot('status')
                                    {{ $post->published }}
                                @endslot
                            @endcomponent
                        </td>
                        <td>
                            <div class="input-group">
                                <div class="input-group-prepend-x">
                                    <button type="button" class="btn btn-outline-secondary dropdown-toggle"
                                            data-toggle="dropdown">
                                        انتخاب کنید
                                    </button>
                                    <div class="dropdown-menu">
                                        <a class="dropdown-item DropDownItem" href="{{ route('posts.edit', $post) }}">
                                            <span class="fas fa-edit"></span>
                                            <span>ویرایش</span>
                                        </a>
                                        <a data-toggle="modal" data-target="#RemoveModal" class="dropdown-item DropDownItem"
                                           path="{{ route('posts.remove', $post) }}"
                                           href="#">
                                            <span class="fas fa-trash-alt"></span>
                                            <span>حذف</span>
                                        </a>
                                        <a class="dropdown-item DropDownItem" href="{{ route('posts.switch', $post) }}">
                                            <span class="fas fa-switch"></span>
                                            <span class="">{{$post->published ? 'عدم نمایش پست' : 'نمایش پست' }}</span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>

            @if($posts->total() > $paginate)
                <div class="FooterPagination">
                    <span class="PaginationLinks">{{ $posts->links() }}</span>
                </div>
            @endif

            @includeIf('admin.removePostModal')
        @endif


    </div>
@endsection