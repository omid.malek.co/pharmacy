@extends('pages.admin')

@section('Items')
    @includeIf('pages.admin.PaymentsItem')
@endsection

@section('BaseSection')

    <div class="ShowFactors">
        <div class="CardBox CustomerInformation card border-primary">
            <div class="card-header TitleBox">مشخصات کاربر</div>
            <div class="BodyBox card-body text-secondary">
                <div class="InnerFrame">
                    <h5 class="card-title CardTitle">نام کاربر</h5>
                    <p class="card-text CardText">{{ $payments->customer->fullname }}</p>
                </div>
                <div class="InnerFrame">
                    <h5 class="card-title CardTitle">ایمیل کاربر</h5>
                    <p class="card-text CardText">{{ $payments->customer->email }}</p>
                </div>
                <div class="InnerFrame">
                    <h5 class="card-title CardTitle">کد پستی</h5>
                    <p class="card-text CardText">{{ $payments->customer->postal_code }}</p>
                </div>
                <div class="InnerFrame">
                    <h5 class="card-title CardTitle">شماره تماس</h5>
                    <p class="card-text CardText">{{ $payments->customer->postal_code }}</p>
                </div>
                <div class="InnerFrame">
                    <h5 class="card-title CardTitle">آدرس کاربر</h5>
                    <p class="card-text CardText">{{ $payments->customer->address }}</p>
                </div>
            </div>
        </div>
        <div class="CardBox GetterInformation card border-primary">
            <div class="card-header TitleBox">مشخصات گیرنده</div>
            <div class="BodyBox card-body text-secondary">
                <div class="InnerFrame">
                    <h5 class="card-title CardTitle">نام و نام خانوادگی</h5>
                    <p class="card-text CardText">{{ $payments->receiver_fullname }}</p>
                </div>
                <div class="InnerFrame">
                    <h5 class="card-title CardTitle">کد پستی</h5>
                    <p class="card-text CardText">{{ $payments->postal_code_sent }}</p>
                </div>
                <div class="InnerFrame">
                    <h5 class="card-title CardTitle">شماره تماس</h5>
                    <p class="card-text CardText">{{ $payments->sent_phone}}</p>
                </div>
                <div class="InnerFrame">
                    <h5 class="card-title CardTitle">آدرس</h5>
                    <p class="card-text CardText">{{ $payments->sent_address }}</p>
                </div>
            </div>
        </div>
        <div class="CardBox PaymentsInformation card border-primary">
            <div class="card-header TitleBox">اطلاعات پرداخت</div>
            <div class="BodyBox card-body text-secondary">
                <div class="InnerFrame">
                    <h5 class="card-title CardTitle">مبلغ قابل پرداخت</h5>
                    <p class="card-text CardText" dir="rtl">{{ $payments->payment_amount. '  تومان  ' }}</p>
                </div>
                <div class="InnerFrame">
                    <h5 class="card-title CardTitle">مبلغ تخفیف</h5>
                    <p class="card-text CardText" dir="rtl">{{ $payments->discount_value. '  تومان  ' }}</p>
                </div>
                <div class="InnerFrame">
                    <h5 class="card-title CardTitle">مبلغ کارت هدیه</h5>
                    <p class="card-text CardText" dir="rtl">{{ $payments->gift_amount. '  تومان  ' }}</p>
                </div>
                <div class="InnerFrame">
                    <h5 class="card-title CardTitle">مبلغ پرداخت شده</h5>
                    <p class="card-text CardText" dir="rtl">{{ $payments->payment_done.'  تومان  ' }}</p>
                </div>
                <div class="InnerFrame">
                    <h5 class="card-title CardTitle">وضعیت پرداخت</h5>
                    <p class="card-text CardText">{{ $payments->payment_message }}</p>
                </div>
                <div class="InnerFrame">
                    <h5 class="card-title CardTitle">تاریخ پرداخت</h5>
                    <p class="card-text CardText">{{ $payments->persian_date }}</p>
                </div>
            </div>
        </div>
        <div class="CardBox ProductsInformation card border-primary table-responsive">
            <div class="card-header TitleBox">اطلاعات کالا</div>
            {{--<div class="BodyBox card-body text-secondary">--}}
                {{--<div class="InnerFrame">--}}
                    {{--<h5 class="card-title CardTitle">نام کالا</h5>--}}
                    {{--<p class="card-text CardText">{{ $factors->product->title }}</p>--}}
                {{--</div>--}}
                {{--<div class="InnerFrame">--}}
                    {{--<h5 class="card-title CardTitle">توضیحات</h5>--}}
                    {{--<p class="card-text CardText">{{ $factors->product->description }}</p>--}}
                {{--</div>--}}
                {{--<div class="InnerFrame">--}}
                    {{--<h5 class="card-title CardTitle">تعداد کالاهای فروخته شده</h5>--}}
                    {{--<p class="card-text CardText">{{ $factors->product->sales }}</p>--}}
                {{--</div>--}}
            {{--</div>--}}

            <table dir="rtl" class="table text-right">
                <thead class="thead-light">
                <tr>
                    <th>ردیف</th>
                    <th>عنوان کالا</th>
                    <th>{{ ' قیمت کالا'. ' (تومان) ' }}</th>
                    <th>تعداد کالا</th>
                </tr>
                </thead>
                <tbody>
                @foreach($factors as $item)
                    <tr id="{{ 'Row'.$item->id }}">
                        <td>{{ $row++ }}</td>
                        <td>{{ $item->product->title }}</td>
                        <td>{{ $item->product->amount }}</td>
                        <td>{{ $item->sent_number }}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>


        </div>
        <div class="returnBox">
            <a href="{{ route('factors.management.index') }}" class="returnBTN btn btn-outline-primary">بازگشت به صفحه قبل</a>
        </div>
    </div>
@endsection