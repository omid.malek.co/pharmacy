@extends('pages.admin.dashboard')

@section('Items')
    <ul id="ListGroup" class="list-group ListGroupItems" item="{{ $operation }}">
        <li id="ListOperation" class="list-group-item">
            <a href="{{ route('customers.index') }}">لیست مشتریان</a>
        </li>
    </ul>
@endsection

@section('BaseSection')

    <div class="SectionMajor table-responsive">

        @php
            if(empty($customers)){
        @endphp
        <section  class="ErrorFrame">
            <section class="alert alert-primary text-center ErrorBox BMitra">
                <span class="IRanSans ErrorMessage">{{ 'هیچ داده ای یافت نشد' }}</span>
            </section>
        </section>
        @php
            }else{
        @endphp

        <table class="table">
            <thead class="thead-light">
            <tr>
                <th>نام و نام خانوادگی</th>
                <th>ایمیل</th>
                <th>تلفن همراه</th>
                <th>شهر</th>
                <th>وضعیت</th>
                <th>عملیات</th>
            </tr>
            </thead>
            <tbody>
            @foreach($customers as $customer)
                <tr>
                    <td>{{ $customer->fullname }}</td>
                    <td>{{ $customer->email }}</td>
                    <td>{{ $customer->mobile }}</td>
                    <td>{{ $customer->citie->name }}</td>
                    <td>
                    </td>
                    <td>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <button type="button" class="btn btn-outline-secondary dropdown-toggle"
                                        data-toggle="dropdown">
                                    انتخاب کنید
                                </button>
                                <div class="dropdown-menu">
                                    <a data-toggle="modal" data-target="#RemoveModal" class="dropdown-item DropDownItem"
                                       path="{{ route('customers.remove',$customer->id) }}"
                                       href="#">
                                        <span class="fas fa-trash-alt"></span>
                                        <span>حذف</span>
                                    </a>
                                    <a class="dropdown-item DropDownItem" href="{{ route('customers.edit',$customer->id) }}">
                                        <span class="fas fa-edit DropdDownIcon"></span>
                                        <span class="">ویرایش</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>

        @if($customers->total()>$paginate)
            <div class="FooterPagination">
                <span class="PaginationLinks">{{ $customers->links() }}</span>
            </div>
        @endif


        @php
            }
        @endphp

        @includeIf('pages.admin.removeModalUser')

    </div>
@endsection
