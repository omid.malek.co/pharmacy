@extends('pages.admin')

@section('Items')
    @includeIf('pages.admin.drugsItems')
@endsection

@section('BaseSection')

    <div class="SectionMajor table-responsive">

        @php
            if($isArrayEmpty==0){
        @endphp
        <section class="ErrorFrame">
            <section class="alert alert-primary text-center ErrorBox BMitra">
                <span class="IRanSans ErrorMessage">{{ 'هیچ داده ای یافت نشد' }}</span>
            </section>
        </section>
        @php
            }else{
        @endphp
        <div class="SectionMajor table-responsive">
            <div class="d-flex w-100 mt-2 mb-4 justify-content-center">
                <a href="{{ route('excel.drugs.show') }}" class="btn btn-outline-secondary">دریافت فایل اکسل</a>
            </div>
            <table class="table">
                <thead class="thead-light">
                <tr>
                    <th>ردیف</th>
                    <th>عنوان کالا</th>
                    <th>تعداد</th>
                    <th>معیار تعداد</th>
                    <th>عملیات</th>
                </tr>
                </thead>
                <tbody>
                @foreach($collections as $item)
                    <tr id="{{ 'Row'.$item->id }}">
                        <td>{{ $row++ }}</td>
                        <td>{{ $item->name }}</td>
                        <td>{{ $item->count }}</td>
                        <td>{{ $item->scale->title }}</td>
                        <td>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <button type="button" class="btn btn-outline-secondary dropdown-toggle"
                                            data-toggle="dropdown">
                                        انتخاب کنید
                                    </button>
                                    <div class="dropdown-menu">
                                        {{--{{ route('posts.edit',['id'=>$collection->id]) }}--}}
                                        {{--{{ route($item->url_management_page,[ 'page_id'=>$item->id ]) }}--}}
                                        {{--{{ route('products.edit',[ 'product_id'=>$item->id ]) }}--}}
                                        <a class="dropdown-item DrDownItem"
                                           href="{{ route('drugs.edit',['id'=>$item->id]) }}">
                                            <span class="fas fa-edit DropdDownIcon"></span>
                                            <span class="">ویرایش</span>
                                        </a>
                                        <a data-toggle="modal" data-target="#ProjectsPopUpModel"
                                           class="dropdown-item DropDownItem"
                                           path="{{ route('remove.drugs',[ 'id'=>$item->id ]) }}"
                                           href="#" clicked="false">
                                            <span class="fas fa-trash-alt"></span>
                                            <span>حذف</span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            @includeIf('pages.admin.ProductPopUpModel')
        </div>
        @if($collections->total()>$paginate)
            <div class="FooterPagination">
                <span class="PaginationLinks">{{ $collections->links() }}</span>
            </div>
        @endif
        @php
            }
        @endphp
        @includeIf('pages.admin.removeModalUser')
    </div>
@endsection