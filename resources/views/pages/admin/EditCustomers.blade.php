@extends('pages.admin.dashboard')


@section('Items')
    <ul id="ListGroup" class="list-group ListGroupItems" item="{{ $operation }}">
        <li id="EditOperation" class="list-group-item">
            <a href="{{ route('customers.edit',$customer->id) }}">ویرایش کاربران</a>
        </li>
    </ul>
@endsection

@section('BaseSection')
    <div class="CustomerEditPage">
        @includeIf('pages.error')
        <form action="{{ route('customers.update', $customer->id) }}" method="post">
            @method('PUT')
            {{ csrf_field() }}
            <div class="CollectionPost">
                <div class="InputFrame">
                    <label for="fullname">نام و نام خانوادگی</label>
                    <input type="text" class="form-control text-right" name="fullname" value="{{$customer->fullname}}"/>
                </div>
                <div class="InputFrame">
                    <label for="email">آدرس ایمیل</label>
                    <input type="text" class="form-control text-left" name="email" value="{{$customer->email}}"/>
                </div>
                <div class="InputFrame">
                    <label for="mobile">تلفن همراه</label>
                    <input type="text" class="form-control text-left" name="mobile" value="{{$customer->mobile}}"/>
                </div>
                <div class="InputFrame">
                    <label for="city">نام شهر</label>
                    <select dir="rtl" class="form-control" name="city" id="city">
                        @foreach($cities as $city)
                            @if($city->id == $customer->citie_id)
                                <option value="{{ $city->id }}" selected="selected">{{ $city->name }}</option>
                            @else
                                <option value="{{ $city->id }}">{{ $city->name }}</option>
                            @endif
                        @endforeach
                    </select>
                </div>

                <button type="submit" class="CreateCollectionBTN btn btn-outline-success btn-block">ثبت اطلاعات</button>
            </div>

        </form>
    </div>
@endsection
