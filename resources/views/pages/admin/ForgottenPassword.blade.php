@extends('layouts.app')
@section('title')
    صفحه ورود
@endsection
@section('content')
    <div class="FirstRow">
        <div class="LoginFormFrame">
            <h1 class="text-center">بازیابی رمز عبور</h1>

            @includeIf('pages.error')
            {{--{{ route('reset.password') }}--}}
            <form method="POST" action="{{ route('admin.reset.password') }}" class="">
                {{ csrf_field() }}
                <div class="InputsGroup input-group input-group-lg">
                    <input type="text"
                           name="username"
                           class="form-control text-center"
                           placeholder="نام کاربری را وارد کنید"/>
                    <span class="w-100"></span>
                </div>
                <div class="LoginFormBTN">
                    <input class="SignInBtn hvr-pulse" type="submit" value="بازیابی رمز عبور"/>
                </div>
                <div class="hvr-pulse ForgottenPassword">
                    {{--{{ route('admin.login') }}--}}
                    <a class="" href="">ورود به سامانه</a>
                </div>
            </form>
        </div>
    </div>
    {{--https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.js--}}

@endsection