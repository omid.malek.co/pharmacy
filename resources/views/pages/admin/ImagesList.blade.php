@extends('pages.admin.dashboard')

@section('Items')
    <ul id="ListGroup" class="list-group ListGroupItems" item="{{ $operation }}">
        <li id="Insert" class="list-group-item">
            <a href="{{ route('images.create') }}">افزودن تصویر مجموعه ها</a>
        </li>
        <li id="InsertLogo" class="list-group-item">
            <a href="{{ route('InsertSliderIndex') }}">افزودن اسلایدر صفحه اصلی </a>
        </li>
        <li id="List" class="list-group-item">
            <a href="{{ route('imagesInsert',['used'=>'index.app']) }}">لیست تصاویر</a>
        </li>
    </ul>
@endsection

@section('BaseSection')
    <div class="ComboSection">
        <div class="input-group ImagesSelection">
            <form class="CategoryFilterForm" action="{{ route('imagesInsert',['used'=>$used]) }}">
                <select class="custom-select text-right" id="CategorieImages">
                    @foreach($ImagesCategorie as $item)
                        @if($item->used_in==$images->first()->used_in)
                            <option value="{{ $item->used_in }}" selected="selected">{{ $item->title }}</option>
                        @else
                            <option value="{{ $item->used_in }}">{{ $item->title }}</option>
                        @endif
                    @endforeach
                </select>
            </form>
        </div>
    </div>
    <div class="ListImages">

        @php
            if(empty($images)){
        @endphp
        <section class="ErrorFrame">
            <section class="alert alert-primary text-center ErrorBox BMitra">
                <span class="IRanSans ErrorMessage">{{ 'هیچ داده ای یافت نشد' }}</span>
            </section>
        </section>
        @php
            }else{
        @endphp
        <div class="OutSideImages">
            @foreach($images as $image)
                <img src="{{ asset($image->image_path) }}" data-toggle="modal" data-target="#RemoveModal"
                path="{{ route('image.remove',['id'=>$image->id]) }}"
                />
            @endforeach
        </div>
        @if($images->total()>$paginate)
            <div class="FooterPagination">
                <span class="PaginationLinks">{{ $images->links() }}</span>
                <span class="PreviousPage">
                    <a class="btn btn-outline-primary" href="{{ $images->previousPageUrl() }}">صفحه قبل</a>
                </span>
                <span class="NextPage">
                    <a class="btn btn-outline-primary" href="{{ $images->nextPageUrl() }}">صفحه بعد</a>
                </span>
            </div>
        @endif
        @php
            }
        @endphp

        @includeIf('pages.admin.removeModalUser')

    </div>
@endsection