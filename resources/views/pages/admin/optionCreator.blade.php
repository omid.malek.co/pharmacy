@extends('pages.admin')

@section('Items')
    @includeIf('pages.admin.optionItems')
@endsection


@section('BaseSection')
    <div class="optionFramePage">
        @includeIf('pages.error')
        <form class="OptionsForm" action="{{ route('admin.options.store') }}" method="post">
            <input type="hidden" name="product_id" value="{{ $product_id }}" />
            <input type="hidden" name="categorie_id" value="{{ $categorie_id }}" />
            {{ csrf_field() }}
            <div class="CollectionInfo">
                <div class="InputFrame">
                    <label for="Fullname">عنوان ویژگی</label>
                    <input dir="rtl" type="text" value="{{ old('title','') }}" placeholder="نام یا عنوان ویژگی"
                           class="form-control text-right" name="title"/>
                </div>
                <div class="InputFrame">
                    <label for="Phone">مقدار ویژگی</label>
                    <textarea dir="rtl" placeholder="مقدار ویژگی" name="TextValue" class="form-control text-right"></textarea>
                </div>
            </div>
            <div class="BTNCollectionArea">
                <span clicked="false" formsName=".OptionsForm"  class="CreateCollectionBTN FormActivator btn btn-outline-success btn-block">ثبت اطلاعات</span>
            </div>
        </form>
    </div>
@endsection




