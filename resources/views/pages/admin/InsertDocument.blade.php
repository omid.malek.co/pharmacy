@extends('pages.admin')

@push('styles')
    <link rel="stylesheet" type="text/css" href="{{ asset('documentsList/insertDocument.css') }}"/>
@endpush


@section('Items')
    <ul id="ListGroup" class="list-group ListGroupItems" item="{{ $operation }}">
        <li id="Insert" class="list-group-item">
            <a href="{{ route('documents.create') }}">افزودن مستندات</a>
        </li>
        <li id="List" class="list-group-item">
            <a href="{{ route('documents.index',['user_id'=>$info->id ]) }}">مستندات</a>
        </li>
    </ul>
@endsection

@section('BaseSection')
    <div class="CreateCollectionPage">
        @includeIf('pages.error')
        <input class type="hidden" name="path" value="{{ route('ComboCategories') }}" />
        <form class="documentInformation" action="{{ route('documents.store') }}" method="post">
            {{ csrf_field() }}

            <div class="CollectionInfo">
                {{--<div class="InputFrame">--}}
                    {{--<label for="PostTitle">دسته بندی مجموعه</label>--}}
                    {{--<select dir="rtl" class="form-control" name="categorie" id="UsersCategorie">--}}
                        {{--@foreach($categoriesUser as $Item)--}}
                            {{--<option value="{{ $Item->id }}">{{ $Item->title }}</option>--}}
                        {{--@endforeach--}}
                    {{--</select>--}}
                {{--</div>--}}
                <!--<div id="CollectionCombo" class="InputFrame">
                    <label for="PostTitle">عنوان</label>
                    <input type="text"
                           placeholder="برای سند خود یک عنوان ارائه دهید"
                           value="{{ old('title','') }}"
                           name="title"
                           class="form-control text-right" />
                </div>-->
            </div>
            <div class="CollectionPost">
                <!--
                <div class="InputFrame text-right">
                    <label for="file">بارگذاری تصویر</label>
                    <input type="file"
                           class="form-control"
                           name="file"
                           id="file" />
                </div>
                -->
                <label for="PostTitle">عنوان</label>
                <input type="text"
                       placeholder="برای سند خود یک عنوان ارائه دهید"
                       value="{{ old('title','') }}"
                       name="title"
                       class="form-control text-right" />
            </div>
            <div class="d-flex description  justify-content-center mb-4 mt-4">
                <label class="text-right" for="PostTitle">توضیحات</label>
                <textarea name="description"
                          class="d-flex form-control text-right descriptionTrim"
                            placeholder="در صورت داشتن توضیحات ارائه دهید (اختیاری)">
                    {{ old('description','') }}
                </textarea>
            </div>
            <div class="BTNCollectionArea mb-4">
                <button class="CreateCollectionBTN btn btn-outline-success btn-block">ثبت اطلاعات</button>
            </div>
        </form>
    </div>
@endsection


@section('scripts')
    <script src="{{ asset('inputs/inputsScript.js') }}" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            setTrimByClass('descriptionTrim')
        })
    </script>
@endsection
