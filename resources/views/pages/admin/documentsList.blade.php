@extends('pages.admin')

@push('styles')
    <link rel="stylesheet" type="text/css" href="{{ asset('documentsList/documentList.css') }}" />
@endpush

@section('Items')
    <ul id="ListGroup" class="list-group ListGroupItems" item="{{ $operation }}">
        <li id="Insert" class="list-group-item">
            <a href="{{ route('documents.create') }}">افزودن به مستندات</a>
        </li>
        <li id="List" class="list-group-item">
            <a href="{{ route('documents.index',['user_id'=>$user_id]) }}">مستندات</a>
        </li>
    </ul>
@endsection

@section('BaseSection')
    <div class="ComboSection">
        <div class="input-group ImagesSelection">
            {{--@php--}}
            {{--if(!empty($images)){--}}
            {{--@endphp--}}
            {{--<form class="CategoryFilterForm"--}}
            {{--action="{{ route('imagesInsert',['used'=>$used]) }}">--}}
            {{--<select class="custom-select text-right" id="CategorieImages">--}}
            {{--@foreach($ImagesCategorie as $item)--}}
            {{--<option value="{{ $item->used_in }}">--}}
            {{--{{ $item->title }}--}}
            {{--</option>--}}
            {{--@endforeach--}}
            {{--</select>--}}

            {{--</form>--}}
            {{--@php--}}
            {{--}--}}
            {{--@endphp--}}
        </div>
    </div>
    <div class="ListImages">

        @if($isEmpty==1)
            <section class="ErrorFrame">
                <section class="alert alert-primary text-center ErrorBox BMitra">
                    <span class="IRanSans ErrorMessage">{{ 'هیچ داده ای یافت نشد' }}</span>
                </section>
            </section>
        @else
            <div class="OutSideImages">
                @foreach($images as $image)
                  <div class="d-flex ImgBox">
                      <div class="ImageFrame">
                          <img src="{{ asset($image->icon_path) }}" />
                      </div>
                      <div class="d-flex Information">
                          <div class="d-flex infoItem Title mt-1 mb-1">{{ $image->title }}</div>
                          <div class="d-flex infoItem userName mt-1 mb-1">{{ $image->user->fullname }}</div>
                          <div class="d-flex operations  mt-3">
                              <div class="BtnDanger ml-2 mb-2 d-flex"
                                   data-toggle="modal" data-target="#RemoveModal"
                                   path="{{ route('documents.remove',['id'=>$image->id]) }}">حذف فایل</div>
                              <div id="downloadFile" class="downloadFileBtn d-flex ml-2 mb-2 "
                                   path="{{ route('documents.download.file',['document_id'=>$image->id]) }}">دانلود فایل</div>
                              <div content="{{ $image->description }}"
                                  data-toggle="modal" data-target="#descriptionModal"
                                  class="description d-flex ml-2 mb-2 ">توضیحات فایل</div>
                              <div class="editDocumentInfo d-flex ml-2 mb-2 ">
                                  <a href="{{ route('documents.edit',$image) }}">ویرایش اطلاعات فایل</a>
                              </div>
                          </div>
                      </div>
                  </div>
                @endforeach
            </div>
            @if($images->total()>$paginate)
                <div class="FooterPagination">
                    <span class="PaginationLinks">{{ $images->links() }}</span>
                    <span class="PreviousPage">
                    <a class="btn btn-outline-primary" href="{{ $images->previousPageUrl() }}">صفحه قبل</a>
                </span>
                    <span class="NextPage">
                    <a class="btn btn-outline-primary" href="{{ $images->nextPageUrl() }}">صفحه بعد</a>
                </span>
                </div>
            @endif
        @endif

        @includeIf('pages.admin.removeModalUser')
        @includeIf('pages.admin.descriptionModal')

    </div>
@endsection

@section('scripts')
    <script src="{{ asset('documentsList/document.js') }}" type="text/javascript"></script>
    <script src="{{ asset('inputs/inputsScript.js') }}" type="text/javascript"></script>
    <script type="text/javascript">
        document.getElementById("ConfirmRemove").addEventListener("click", removePopUpModelCtrl);
    </script>
@endsection
