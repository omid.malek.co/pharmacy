@extends('pages.admin')

@push('styles')
    <link rel="stylesheet" type="text/css" href="{{ asset('documentsList/docs.css') }}"/>
@endpush

@section('Items')
    <ul id="ListGroup" class="list-group ListGroupItems" item="{{ $operation }}">
        <li id="Insert" class="list-group-item">
            <a href="{{ route('documents.create') }}">افزودن مستندات</a>
        </li>
        <li id="List" class="list-group-item">
            <a href="{{ route('documents.index',['user_id'=>$info->id ]) }}">مستندات</a>
        </li>
    </ul>
@endsection

@section('BaseSection')
    <!-- <div class="CreateCollectionPage">  -->
    @includeIf('pages.error')
    <section class="uploadDocumentSection d-flex">
        <div class="Alerts d-flex">
            <div style="" class="alert-items alert-primary d-flex text-center" role="alert">
                حجم فایل می بایست حداکثر 50 مگابایت باشد
            </div>
            <div style="" class="alert-items alert-primary d-flex text-center" role="alert">
                پس از انتخاب دکمه بارگذاری فایل دکمه غیر فعال می گردد تا بارگذاری فایل کامل گردد
            </div>
            <div style="" class="alert-items alert-primary d-flex text-center" role="alert">
                پس از انتخاب دکمه بارگذاری فایل تا زمان تگمیل فرایند لطفا از بروزرسانی صفحه خودداری کنید
            </div>
            <div style="" class="alert-items alert-primary d-flex text-center" role="alert">
               در صورت بارگذاری دو فایل، فایل دوم جایگزین فایل اول می شود
            </div>
        </div>
        <form id="uploadDocument" action="{{ route('documents.update.file') }}"
              autocomplete="off"
              method="post"
              class="documentForm d-flex"
              enctype="multipart/form-data">
            <p>بارگذاری فایل</p>
            <input type="hidden" id="document_id" name="document_id" value="{{  $document_id }}"/>
            <div class="inputFileBox d-flex">
                <input type="file" name="file" id="upload" style=""/>
                <label for="upload">جهت انتخاب فایل اینجا کلیک کنید</label>
            </div>
            <div class="pr">
                <div class="ProgressBarSection d-flex">
                    <div class="progress">
                        <div class="progress-bar" role="progressbar" aria-label="Example with label" style="width: 1%;"
                             aria-valuenow="1" aria-valuemin="0" aria-valuemax="100">
                        </div>
                    </div>
                </div>
            </div>
            <div class="btnFormBox d-flex">
                <button id="uploadFile" class="uploadFile">
                    <span class="StartUpload">&#8682; بارگذاری فایل </span>
                </button>
            </div>
        </form>
    </section>
    <!--</div> -->
@endsection


@section('scripts')
    <script src="{{ asset('documentsList/upload.js') }}" type="text/javascript"></script>
@endsection
