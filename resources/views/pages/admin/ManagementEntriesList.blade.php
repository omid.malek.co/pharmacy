@extends('pages.admin')

@push('styles')
    <link rel="stylesheet" type="text/css" href="{{ asset('entries/entriesManagement.css') }}"/>

    <link href='{{ asset('datepicker3/css/normalize.css') }}' rel='stylesheet'/>
    <link href='{{ asset('datepicker3/css/fontawesome/css/font-awesome.min.css') }}' rel='stylesheet'/>
    <link href="{{ asset('datepicker3/css/vertical-responsive-menu.min.css') }}" rel="stylesheet"/>
    <link href="{{ asset('datepicker3/css/style.css') }}" rel="stylesheet"/>
    <link href="{{ asset('datepicker3/css/prism.css') }}" rel="stylesheet"/>
    <link rel="stylesheet" href="{{ asset('datepicker3/css/persianDatepicker-default.css') }}"/>
    <link rel="stylesheet" href="{{ asset('datepicker3/css/persianDatepicker-dark.css') }}"/>
    <link rel="stylesheet" href="{{ asset('datepicker3/css/persianDatepicker-latoja.css') }}"/>
    <link rel="stylesheet" href="{{ asset('datepicker3/css/persianDatepicker-melon.css') }}"/>
    <link rel="stylesheet" href="{{ asset('datepicker3/css/persianDatepicker-lightorang.css') }}"/>
@endpush


@push('MyScript')
    <script src="{{ asset('datepicker3/js/prism.js') }}"></script>
    <script src="{{ asset('datepicker3/js/vertical-responsive-menu.min.js') }}"></script>
    <script src="{{ asset('datepicker3/js/jquery-1.10.1.min.js') }}"></script>
    <script src="{{ asset('datepicker3/js/persianDatepicker.js') }}"></script>
    <script src="{{ asset('entries/managementEntries.js') }}" type="text/javascript"></script>
@endpush

@section('Items')
    @include('pages.admin.entriesListLink')
@endsection

@section('BaseSection')
    <div class="SectionMajor">
        @includeIf('pages.error')
        <div class="row justify-content-center">
            @isset($total_interval)
                <div dir="rtl" class="staticAlert d-flex alert-primary text-center" role="alert">
                    {!!  " جمع کل ساعت کاری در بازه انتخابی : ".$total_interval  !!}
                </div>
            @endisset
            <form class="w-100 d-flex flex-row-reverse" action="{{ route('managementEntries.search') }}"
                  method="POST">
                @csrf
                <div class="formInputs">
                    <div class="firstGroup">
                        <input class="form-control text-center formInputs"
                               name='topRange'
                               id='topRange'
                               placeholder=" نتایج تا تاریخ مذکور نمایش داده شود"
                               value="{{ $top_date }}"
                               type="text"/>
                        <input class="form-control text-center formInputs"
                               name='bottomRange'
                               id='bottomRange'
                               value="{{ $bottom_date }}"
                               placeholder=" نتایج از تاریخ مذکور نمایش داده شود"
                               type="text"/>

                    </div>
                    <div class="secondGroup">
                        <input type="submit"
                               value="بررسی کن"
                               class="btn btn-block btn-outline-info formInputs"
                        />
                        <input class="form-control text-center formInputs"
                               name='name'
                               value="{{ $name }}"
                               id='name'
                               placeholder="نام کاربر را جست و جو کنید"
                               type="text"/>
                    </div>
                </div>
            </form>
        </div>
        @if($entries['operation']==0)
            {!! $entries['pages'] !!}
        @else
            <table class="table table-responsive">
                <thead class="thead-light headeritems">
                <tr class="headeritems">
                    <th>ردیف</th>
                    <th>نام و نام خانوادگی</th>
                    <th>تاریخ ورود</th>
                    <th>زمان ورود</th>
                    <th>تاریخ خروج</th>
                    <th>زمان خروج</th>
                    <th>ساعت کاری</th>
                    <th>تایید شده توسط</th>
                    <th>عملیات</th>
                </tr>
                </thead>
                <tbody>
                @foreach($entries['pages'] as $entrie)
                    <tr>
                        <td class="Counter">{{ $entries['row']++ }}</td>
                        <td>{{ $entrie->user_fullname }}</td>
                        <td class="entryDate">{{ $entrie->persian_entry_date }}</td>
                        <td class="entringTime">{{ $entrie->entry_time }}</td>
                        <td class="leavingDate">{!! $entrie->persian_departure_date !!}</td>
                        <td class="leavingTime">{!! $entrie->label_departure_time !!}</td>
                        <td class="leavingTime">{!! $entrie->interval_time !!}</td>
                        <td>{!! $entrie->label_confirm !!}</td>
                        <td class="OperatorColumn">
                            <div class="input-group OperatorColumn">
                                <a class="btn btn-outline-success ml-3"
                                   href="{{ route('managementEntries.confirm',['id'=>$entrie->id]) }}">تایید</a>
                                <!-- $entries->hasItem --->
                                <form action="{{ route('entries.destroy',$entrie->id) }}" method="POST">
                                    @csrf()
                                    @method('DELETE')
                                    <input type="submit" value="حذف" class="btn btn-outline-danger"/>
                                </form>
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        @endif

        @if($entries['operation']!=0)
            @if($entries['pages']->total()>$paginate)
                <div class="FooterPagination">
                    <span class="PaginationLinks">{{ $entries['pages']->links() }}</span>
                </div>
            @endif
        @endif
        @includeIf('pages.admin.removeModalUser')

    </div>
@endsection
