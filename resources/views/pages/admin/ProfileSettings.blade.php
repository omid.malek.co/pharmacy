@extends('pages.admin.dashboard')

@section('Items')
    <ul id="ListGroup" class="list-group ListGroupItems" item="{{ $operation }}">
        <li id="Change" class="list-group-item">
            <a href="{{ route('profile.edit', session('id')) }}">ویرایش اطلاعات کاربری</a>
        </li>
    </ul>
@endsection



@section('BaseSection')
    <div class="ProfileSettingBox">
        @includeIf('pages.error')
        <form action="{{ route('profile.update', $info->id) }}" method="post">
            @method('PUT')
            @csrf
            <div class="CollectionInfo">
                <div class="InputFrame">
                    <label for="fullname"> نام و نام خانوادگی</label>
                    <input type="text" class="form-control text-right" name="fullname" value="{{ $info->fullname }}"/>
                </div>
                <div class="InputFrame">
                    <label for="email">آدرس الکترونیک</label>
                    <input type="text" class="form-control text-right" name="email" value="{{ $info->email }}"/>
                </div>
                <div class="InputFrame">
                    <label for="username">نام کاربری</label>
                    <input type="text" class="form-control text-right" name="username" value="{{ $info->username }}"/>

                </div>

                <button type="submit" class="CreateCollectionBTN btn btn-outline-success btn-block">ثبت اطلاعات</button>
            </div>

        </form>
    </div>
@endsection
