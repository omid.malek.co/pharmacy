@extends('pages.admin')

@section('Items')
    @includeIf('pages.admin.CollectionItems')
@endsection

@section('bundles')
    <script src="{{asset('vendor/tinymce/tinymce.min.js')}}"></script>
@endsection

@section('BaseSection')
    <div class="CreateCollectionPage">
        @includeIf('pages.error')
        <form action="{{ route('admin.products.store') }}" method="post">
            {{ csrf_field() }}
            <div class="CollectionPost">
                <div class="InputFrame">
                    <label for="Longitude">تعداد کالا</label>
                    <input type="number" min="0" value="{{ old('number',0) }}" class="form-control text-center"
                           name="number"/>
                </div>
                <div class="InputFrame">
                    <label for="Category">دسته بندی</label>
                    <select dir="rtl" class="form-control text-center" name="categorie_id" id="Category">
                        @foreach($categories as $Item)
                            <option value="{{ $Item->id }}">{{ $Item->title }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="CollectionInfo">
                <div class="InputFrame">
                    <label for="Fullname">عنوان کالا</label>
                    <input dir="rtl" type="text" value="{{ old('title','') }}" placeholder="نام یا عنوان کالا"
                           class="form-control text-right" name="title"/>
                </div>
                <div class="InputFrame">
                    <label for="Phone"> هزینه کالا (تومان)</label>
                    <input type="text" value="{{ old('amount',1000) }}" class="form-control text-right"
                           placeholder="لطفا مبلغ را به تومان وارد کنید" name="amount"/>
                </div>
            </div>
            <div class="ServicesPost">
                <div class="InputFrame">
                    <label for="Services">توضیحات کالا</label>
                    <textarea dir="rtl" name="description" rows="5"
                              id="desc"
                              class="form-control text-right"
                              placeholder="توضیحات مربوط به کالا">
                        {{ old('description','بدون توضیحات') }}
                    </textarea>
                </div>
            </div>
            <div class="BTNCollectionArea">
                <span clicked="false" id="registerProduct" class="CreateCollectionBTN btn btn-outline-success btn-block">ثبت اطلاعات</span>
            </div>
        </form>
    </div>
@endsection

@section('scripts')
    <script>
      tinymce.init({
        selector: '#desc',
        language: 'fa_IR',
        // directionality: 'rtl',
        height: document.body.scrollHeight,
        plugins: [
          "advlist autolink lists link image charmap print preview hr anchor pagebreak",
          "searchreplace wordcount visualblocks visualchars code fullscreen",
          "insertdatetime media nonbreaking save table contextmenu directionality",
          "emoticons template paste textcolor colorpicker textpattern"
        ],
        toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media",
        relative_urls: false,
        file_browser_callback : function(field_name, url, type, win) {
          var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
          var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;

          var cmsURL = '/' + 'laravel-filemanager?field_name=' + field_name;
          if (type == 'image') {
            cmsURL = cmsURL + "&type=Images";
          } else {
            cmsURL = cmsURL + "&type=Files";
          }

          tinymce.activeEditor.windowManager.open({
            file : cmsURL,
            title : 'گالری',
            width : x * 0.8,
            height : y * 0.8,
            resizable : "yes",
            close_previous : "no"
          });
        }
      })
    </script>
@endsection


