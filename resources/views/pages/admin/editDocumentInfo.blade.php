@extends('pages.admin')

@push('styles')
    <link rel="stylesheet" type="text/css" href="{{ asset('documentsList/editDocument.css') }}"/>
@endpush


@section('Items')
    <ul id="ListGroup" class="list-group ListGroupItems" item="{{ $operation }}">
        <li id="Insert" class="list-group-item">
            <a href="{{ route('documents.create') }}">افزودن مستندات</a>
        </li>
        <li id="List" class="list-group-item">
            <a href="{{ route('documents.index',['user_id'=>$info->id ]) }}">مستندات</a>
        </li>
    </ul>
@endsection

@section('BaseSection')
    <div class="CreateCollectionPage">
        @includeIf('pages.error')
        <input class type="hidden" name="path" value="{{ route('ComboCategories') }}" />
        <form class="documentInformation" action="{{ route('documents.update',$document) }}" method="post">
            {{ csrf_field() }}
            @method('PUT')
            <div class="CollectionPost">
                <label for="PostTitle">عنوان</label>
                <input type="text"
                       placeholder="برای سند خود یک عنوان ارائه دهید"
                       value="{{ old('title',$document->title) }}"
                       name="title"
                       class="form-control text-right" />
            </div>
            <div class="d-flex description  justify-content-center mb-4 mt-4">
                <label class="text-right" for="PostTitle">توضیحات</label>
                <textarea name="description"
                          class="d-flex form-control text-right descriptionTrim"
                            placeholder="در صورت داشتن توضیحات ارائه دهید (اختیاری)">
                    {{ old('description',$document->description) }}
                </textarea>
            </div>
            <div class="BTNCollectionArea mb-4">
                <button class="CreateCollectionBTN btn btn-outline-success btn-block">ثبت اطلاعات</button>
                <a class="changeFile" href="{{ route('documents.edit.file', ['document_id' => $document->id]) }}">تغییر فایل پیوست</a>
            </div>
        </form>
    </div>
@endsection


@section('scripts')
    <script src="{{ asset('inputs/inputsScript.js') }}" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            setTrimByClass('descriptionTrim')
        })
    </script>
@endsection
