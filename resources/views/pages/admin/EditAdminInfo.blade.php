@extends('pages.admin')

@section('Items')
    @includeIf('pages.admin.adminItems')
@endsection

@section('BaseSection')
    <div class="CreateCollectionPage">
        @includeIf('pages.error')
        <form action="{{ route('admin.update') }}" method="POST">
            {{ csrf_field() }}
            <div class="CollectionPost">
                <div class="InputFrame">
                    <label for="Longitude">نام و نام خانوادگی</label>
                    <input type="text" value="{{ old('fullname', $info->fullname ) }}"
                           class="form-control text-center"
                           name="fullname"/>
                </div>
                <div class="InputFrame">
                    <label for="Longitude">شماره تماس</label>
                    <input type="text" value="{{ old('phone', $info->phone ) }}"
                           class="form-control text-center"
                           name="phone"/>
                </div>
                <div class="InputFrame">
                    <label for="">ایمیل</label>
                    <input type="text" value="{{ old('email', $info->email ) }}"
                           class="form-control text-center"
                           name="email"/>
                </div>
                <div class="InputFrame">
                    <label for="Longitude">شماره کارت</label>
                    <input type="text" value="{{ old('number_cart', $info->number_cart ) }}"
                           class="form-control text-center"
                           name="number_cart"/>
                </div>
            </div>
            <div class="CollectionInfo">
                <div class="InputFrame">
                    <label for="Fullname">آدرس صفحه فیس بوک</label>
                    <input type="text" value="{{ old('facebook_address',$info->facebook_address ) }}"
                           placeholder="آدرس صفحه فیس بوک خود را وارد کنید"
                           class="form-control text-right" name="facebook_address"/>
                </div>
                <div class="InputFrame">
                    <label for="Phone">آدرس صفحه اینستاگرام</label>
                    <input type="text" value="{{ old('instagram_address',$info->instagram_address) }}" class="form-control text-right"
                           placeholder="آدرس صفحه اینستاگرام خود را وارد کنید" name="instagram_address"/>
                </div>
                <div class="InputFrame">
                    <label for="Phone">آدرس صفحه تلگرام</label>
                    <input type="text" value="{{ old('telegram_address',$info->telegram_address) }}" class="form-control text-right"
                           placeholder="آدرس صفحه تلگرام خود را وارد کنید" name="telegram_address"/>
                </div>
                <div class="InputFrame">
                    <label for="Longitude">شماره حساب</label>
                    <input type="text" value="{{ old('number_account', $info->number_account ) }}"
                           class="form-control text-center"
                           name="number_account"/>
                </div>
            </div>
            <div class="ServicesPost">
                <div class="InputFrame">
                    <label for="Services">آدرس فروشگاه</label>
                    <textarea name="address" rows="5"
                              class="form-control text-right"
                              placeholder="آدرس فروشگاه خود را وارد کنید">
                        {{ old( 'address',$info->address ) }}
                    </textarea>
                </div>
            </div>
            <div class="BTNCollectionArea">
                <span clicked="false" id="registerProduct"
                      class="CreateCollectionBTN btn btn-outline-success btn-block">ثبت اطلاعات</span>
            </div>
        </form>
    </div>
@endsection


