@extends('pages.admin')

@section('Items')
    @includeIf('pages.admin.optionItems')
@endsection

@section('BaseSection')
    <div class="SectionMajor table-responsive">
        @if($doesntExist)
           {!! $collections !!}
        @else
            <div class="SectionMajor table-responsive">
                <table class="table">
                    <thead class="thead-light">
                    <tr>
                        <th>ردیف</th>
                        <th>عنوان ویژگی</th>
                        <th>عنوان کالا</th>
                        <th>عملیات</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($collections as $item)
                        <tr id="{{ 'Row'.$item->id }}">
                            <td>{{ $row++ }}</td>
                            <td>{{ $item->title }}</td>
                            <td>{{ $item->product->title }}</td>
                            <td>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <button type="button" class="btn btn-outline-secondary dropdown-toggle"
                                                data-toggle="dropdown">
                                            انتخاب کنید
                                        </button>
                                        <div class="dropdown-menu">
                                            {{--{{ route($item->url_management_page,[ 'page_id'=>$item->id ]) }}--}}
                                            {{--{{ route('admin.products.edit',[ 'product_id'=>$item->id ]) }}--}}
                                            <a class="dropdown-item DrDownItem"
                                               href="{{ route('admin.options.edit',$item) }}">
                                                <span class="fas fa-edit DropdDownIcon"></span>
                                                <span class="">ویرایش محتوا برگه</span>
                                            </a>
                                            {{--{{ route('admin.image.remove',[ 'product_id'=>$item->id ]) }}--}}
                                            <a data-toggle="modal" data-target="#ProjectsPopUpModel"
                                               class="dropdown-item DropDownItem"
                                               path=""
                                               href="#" clicked="false">
                                                <span class="fas fa-trash-alt"></span>
                                                <span>حذف</span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                @includeIf('pages.admin.ProductPopUpModel')
            </div>
            @if($collections->total()>$paginate)
                <div class="FooterPagination">
                    <span class="PaginationLinks">{{ $collections->links() }}</span>
                </div>
            @endif
        @endif
        @includeIf('pages.admin.removeModalUser')
    </div>
@endsection