@extends('pages.admin')

@section('Items')
    @includeIf('pages.admin.personnelsItems')
@endsection

@section('bundles')
    <script src="{{asset('vendor/tinymce/tinymce.min.js')}}"></script>
@endsection

@section('BaseSection')
    <div class="CreateCollectionPage">
        @includeIf('pages.error')
        <form action="{{ route('items.store') }}" method="POST">
            {{ csrf_field() }}
            <div class="CollectionPost">
                <div class="InputFrame">
                    <label for="Longitude">نام و نام خانوادگی</label>
                    <input type="text" value="{{ old('fullname', '' ) }}"
                           class="form-control text-center"
                           name="fullname"/>
                </div>
                <div class="InputFrame">
                    <label for="Longitude">شماره تماس</label>
                    <input type="text" value="{{ old('phone', '' ) }}"
                           class="form-control text-center"
                           name="phone"/>
                </div>
                <div class="InputFrame">
                    <label for="">ایمیل</label>
                    <input type="text"
                           value="{{ old('email', 'email@sample.com' ) }}"
                           class="form-control text-center"
                           name="email"/>
                </div>
            </div>
            <div class="CollectionInfo">
                <div class="InputFrame">
                    <label for="Fullname">آدرس صفحه فیس بوک</label>
                    <input type="text"
                           value="{{ old('facebook_address',$info->facebook_address ) }}"
                           placeholder="آدرس صفحه فیس بوک خود را وارد کنید"
                           class="form-control text-right" name="facebook_address"/>
                </div>
                <div class="InputFrame">
                    <label for="Phone">آدرس صفحه اینستاگرام</label>
                    <input type="text"
                           value="{{ old('instagram_address',$info->instagram_address) }}" class="form-control text-right"
                           placeholder="آدرس صفحه اینستاگرام خود را وارد کنید" name="instagram_address"/>
                </div>
                <div class="InputFrame">
                    <label for="Phone">آدرس صفحه تلگرام</label>
                    <input type="text"
                           value="{{ old('telegram_address',$info->telegram_address) }}" class="form-control text-right"
                           placeholder="آدرس صفحه تلگرام خود را وارد کنید"
                           name="telegram_address"/>
                </div>
            </div>
            <div class="ServicesPost">
                <div class="InputFrame">
                    <label for="Services">آدرس</label>
                    <textarea name="address" rows="5"
                              class="form-control text-right"
                              placeholder="آدرس فروشگاه خود را وارد کنید">
                        {{ old( 'address','بدون توضیح' ) }}
                    </textarea>
                </div>
            </div>
            <div class="BTNCollectionArea">
                <span clicked="false" id="registerProduct"
                      class="CreateCollectionBTN btn btn-outline-success btn-block">ثبت اطلاعات</span>
            </div>
        </form>
    </div>
@endsection

@section('scripts')
    <script>
      tinymce.init({
        selector: '#desc',
        language: 'fa_IR',
        // directionality: 'rtl',
        height: document.body.scrollHeight,
        plugins: [
          "advlist autolink lists link image charmap print preview hr anchor pagebreak",
          "searchreplace wordcount visualblocks visualchars code fullscreen",
          "insertdatetime media nonbreaking save table contextmenu directionality",
          "emoticons template paste textcolor colorpicker textpattern"
        ],
        toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media",
        relative_urls: false,
        file_browser_callback : function(field_name, url, type, win) {
          var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
          var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;

          var cmsURL = '/' + 'laravel-filemanager?field_name=' + field_name;
          if (type == 'image') {
            cmsURL = cmsURL + "&type=Images";
          } else {
            cmsURL = cmsURL + "&type=Files";
          }

          tinymce.activeEditor.windowManager.open({
            file : cmsURL,
            title : 'گالری',
            width : x * 0.8,
            height : y * 0.8,
            resizable : "yes",
            close_previous : "no"
          });
        }
      })
    </script>
@endsection


