@extends('pages.admin')

@section('Items')
    @include('pages.admin.accessListLink')
@endsection

@section('BaseSection')

    <div class="SectionMajor table-responsive">
        @if($pages['operation']==0)
            {!! $pages['pages'] !!}
        @else
            <table class="table">
                <thead class="thead-light">
                <tr>
                    <th>ردیف</th>
                    <th>عنوان آیتم</th>
                    <th>عملیات</th>
                </tr>
                </thead>
                <tbody>
                @foreach($pages['pages'] as $page)
                    <tr>
                        <td>{{ $pages['row'] }}</td>
                        <td>{{ $page->title }}</td>
                        <td>
                            <div class="input-group">
                                @if($page->hasItem)
                                    <form action="{{ route('access.remover') }}" method="POST">
                                        @csrf()
                                        <input type="hidden" value="{{ $user_id  }}" name="user_id"/>
                                        <input type="hidden" value="{{ $page->id }}" name="page_id"/>
                                        <input type="submit" value="حذف آیتم" class="btn btn-outline-danger"/>
                                    </form>
                                @else
                                    <form method="POST" action="{{ route('access.store') }}">
                                        @csrf()
                                        <input type="hidden" value="{{ $user_id  }}" name="user_id"/>
                                        <input type="hidden" value="{{ $page->id }}" name="page_id"/>
                                        <input type="submit" value="افزودن آیتم" class="btn btn-outline-success"/>
                                    </form>
                                @endif
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>

            @if($pages['pages']->total()>$paginate)
                <div class="FooterPagination">
                    <span class="PaginationLinks">{{ $pages['pages']->links() }}</span>
                </div>
            @endif
        @endif
        @includeIf('pages.admin.removeModalUser')

    </div>
@endsection
