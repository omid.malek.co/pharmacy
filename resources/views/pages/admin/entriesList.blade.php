@extends('pages.admin')

@section('Items')
    @include('pages.admin.entriesListLink')
@endsection

@push('styles')
    <link rel="stylesheet" type="text/css" href="{{ asset('entries/entriesStyle.css') }}"/>
@endpush

@section('BaseSection')

    <div class="SectionMajor table-responsive">
        @if($entries['operation']==0)
            {!! $entries['pages'] !!}
        @else
            @includeIf('pages.error')
            <div class="staticAlert alert-primary text-center" role="alert">
               تنها هنگامی که ثبت خروج یک حضور را بزنید امکان ثبت حضور دیگر فعال خواهد شد
            </div>
            <table class="table">
                <thead class="thead-light">
                <tr>
                    <th>ردیف</th>
                    <th>تاریخ ورود</th>
                    <th>زمان ورود</th>
                    <th>تاریخ خروج</th>
                    <th>زمان خروج</th>
                    <th>مدت حضور</th>
                    <th>تایید شده توسط</th>
                    <th>عملیات</th>
                </tr>
                </thead>
                <tbody>
                @foreach($entries['pages'] as $entrie)
                    <tr>
                        <td>{{ $entries['row']++ }}</td>
                        <td>{{ $entrie->persian_entry_date }}</td>
                        <td>{{ $entrie->entry_time }}</td>
                        <td>{!! $entrie->persian_departure_date !!}</td>
                        <td>{!! $entrie->label_departure_time !!}</td>
                        <td>{!! $entrie->interval_time !!}</td>
                        <td>{!! $entrie->label_confirm !!}</td>
                        <td>
                            <div class="input-group">
                                <!-- $entries->hasItem --->
                                    <form action="{{ route('entries.destroy',$entrie->id) }}" method="POST">
                                        @csrf()
                                        @method('DELETE')
                                        <input type="submit" value="حذف" class="btn btn-outline-danger"/>
                                    </form>
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>

            @if($entries['pages']->total()>$paginate)
                <div class="FooterPagination">
                    <span class="PaginationLinks">{{ $entries['pages']->links() }}</span>
                </div>
            @endif
        @endif
        @includeIf('pages.admin.removeModalUser')

    </div>
@endsection
