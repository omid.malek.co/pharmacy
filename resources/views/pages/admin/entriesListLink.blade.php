<ul id="ListGroup" class="list-group ListGroupItems" item="{{ $operation }}">
    @isset($isAdmin)
        @if($isAdmin)
            <li id="ListOperation" class="list-group-item">
                <a href="{{ route('access.index') }}">لیست پرسنل</a>
            </li>
        @else
            <li id="ListOperation" class="list-group-item">
                <a href="{{ route('entries.present') }}">ثبت حضور</a>
            </li>
            <li id="ListOperation" class="list-group-item">
                <a href="{{ route('leaving.workplace') }}">ثبت خروج</a>
            </li>
        @endif
    @endisset
</ul>
