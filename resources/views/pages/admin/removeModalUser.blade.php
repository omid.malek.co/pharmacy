<div class="modal" tabindex="-1" id="RemoveModal" role="dialog">
    <div class="modal-dialog" role="document">
        <input type="hidden" value="" id="" name="pathOperation" />
        <div class="modal-content">
            <div class="modal-header">
                {{--<h5 class="modal-title">Modal title</h5>--}}
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p class="text-right">آیا این داده حذف شود ؟</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">بستن</button>
                <a href="#" id="ConfirmRemove"  class="btn btn-primary">حذف شود</a>
            </div>
        </div>
    </div>
</div>

