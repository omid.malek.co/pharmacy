@extends('pages.admin')

@section('Items')
    @includeIf('pages.admin.drugsItems')
@endsection

@section('bundles')
    {{--<script src="{{asset('vendor/tinymce/tinymce.min.js')}}"></script>--}}
@endsection

@section('BaseSection')
    <div class="CreateCollectionPage">
        @includeIf('pages.error')
        <form action="{{ route('drugs.store') }}" method="post">
            {{ csrf_field() }}
            <div class="CollectionPost">
                <div class="InputFrame">
                    <label for="Longitude">تعداد کالا</label>
                    <input type="number" min="0" value="{{ old('count',0) }}" class="form-control text-center"
                           name="count"/>
                </div>
                <div class="InputFrame">
                    <label for="Category">دسته بندی</label>
                    <select dir="rtl" class="form-control text-center" name="scale_id" id="Category">
                        @foreach($scales as $Item)
                            <option value="{{ $Item->id }}">{{ $Item->title }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="CollectionInfo">
                <div class="InputFrame">
                    <label for="Fullname">عنوان کالا</label>
                    <input dir="rtl" type="text" value="{{ old('name','') }}" placeholder="نام یا عنوان کالا"
                           class="form-control text-right" name="name"/>
                </div>
                <div class="InputFrame">
                    <label for="Phone">شکل کالا</label>
                    <input type="text" value="" class="form-control text-right"
                           placeholder="شکل کالا" name="shape"/>
                </div>
            </div>
            <div class="ServicesPost">
                <div class="InputFrame">

                    <label for="Services">توضیحات کالا</label>
                    <textarea dir="rtl" name="description" rows="5"
                              id="desc"
                              class="form-control text-right"
                              placeholder="توضیحات مربوط به کالا">
                        {{ old('description','بدون توضیحات') }}
                    </textarea>
                </div>
            </div>
            <div class="BTNCollectionArea">
                <span clicked="false" id="registerProduct" class="CreateCollectionBTN btn btn-outline-success btn-block">ثبت اطلاعات</span>
            </div>
        </form>
    </div>
@endsection

@section('scripts')
    <script>
      let text = document.getElementById("desc").value;
      let result = text.trim();
      document.getElementById("desc").value=result
    </script>
@endsection


