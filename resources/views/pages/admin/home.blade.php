@extends('pages.admin')

@section('Items')
    <ul id="ListGroup" class="list-group ListGroupItems" item="{{ $operation }}">
        <li id="dashboard" class="list-group-item">
            <a href="{{ route('home') }}">داشبورد</a>
        </li>
    </ul>
@endsection

@section('BaseSection')
    <div class="SectionMajor table-responsive">
        <ul class="ItemsBoxFrame">
            {{--<li class="customers">--}}
            {{--{{ route('customers.index') }}--}}
            {{--<a href="">--}}
            {{--{{ $customers->first()->customers_count }}--}}
            {{--<h4></h4>--}}
            {{--<h5>کاربران</h5>--}}
            {{--</a>--}}
            {{--</li>--}}
            {{--<li class="users">--}}
            {{--{{ route('posts.index') }}--}}
            {{--<a href="{{ route('posts.index') }}">--}}
            {{--{{ $users->first()->users_count }}--}}
            {{--<h4></h4>--}}
            {{--<h5>مقالات</h5>--}}
            {{--</a>--}}
            {{--</li>--}}
            @foreach($UserItems['response'] as $item)
                <li class="comments">
                    {{--{{ route('manageComments.index') }}--}}
                    <a href="{{ $item['address'] }}">
                        {{--{{ $comments->first()->comments_count }}--}}
                        <h4></h4>
                        <h5>{{ $item['page_title'] }}</h5>
                    </a>
                </li>
            @endforeach
            {{--<li class="images">--}}
            {{--{{ route('imagesInsert',['used'=>'index.app']) }}--}}
            {{--<a href="{{ route('admin.edit') }}">--}}
            {{--{{ $images->first()->images_count }}--}}
            {{--<h4></h4>--}}
            {{--<h5>اطلاعات مدیر</h5>--}}
            {{--</a>--}}
            {{--</li>--}}
            {{--<li class="images">--}}
            {{--{{ route('imagesInsert',['used'=>'index.app']) }}--}}
            {{--<a href="{{ route('factors.management.index') }}">--}}
            {{--{{ $images->first()->images_count }}--}}
            {{--<h4></h4>--}}
            {{--<h5>فاکتورها</h5>--}}
            {{--</a>--}}
            {{--</li>--}}
            {{--<li class="images">--}}
            {{--<a href="{{ route('drugs.index') }}">--}}
            {{--<h4></h4>--}}
            {{--<h5>کسری داروها</h5>--}}
            {{--</a>--}}
            {{--</li>--}}
            {{--<li class="images">--}}
            {{--<a href="{{ route('drugs.index') }}">--}}
            {{--<h4></h4>--}}
            {{--<h5>مدارک پرسنلی</h5>--}}
            {{--</a>--}}
            {{--</li>--}}
        </ul>
    </div>
@endsection
