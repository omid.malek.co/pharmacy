@extends('pages.admin')

@section('Items')
    @includeIf('pages.admin.PaymentsItem')
@endsection

@section('BaseSection')

    <div class="SectionMajor table-responsive">
        @php
            if($IsArrayEmpty){
        @endphp
        <section class="ErrorFrame">
            <section class="alert alert-primary text-center ErrorBox BMitra">
                <span class="IRanSans ErrorMessage">{{ 'هیچ داده ای یافت نشد' }}</span>
            </section>
        </section>
        @php
            }else{
        @endphp
        <table class="table">
            <thead class="thead-light">
            <tr>
                <th>ردیف</th>
                <th>نام مشتری</th>
                <th>زمان ثبت سفارش</th>
                <th>وضعیت</th>
                <th>عملیات</th>
            </tr>
            </thead>
            <tbody>
            @foreach($payments as $item)
                <tr id="{{ 'Row'.$item->id }}">
                    <td>{{ $row++ }}</td>
                    <td>{{ $item->customer->fullname }}</td>
                    <td>{{ $item->persian_date }}</td>
                    <td>{{ $item->condition->title }}</td>
                    <td>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <button type="button" class="btn btn-outline-secondary dropdown-toggle"
                                        data-toggle="dropdown">
                                    انتخاب کنید
                                </button>
                                <div class="dropdown-menu">
                                    {{--{{ route('posts.edit',['id'=>$collection->id]) }}--}}
                                    {{--{{ route($item->url_management_page,[ 'page_id'=>$item->id ]) }}--}}
                                    <a class="dropdown-item DrDownItem"
                                                                               href="{{ route('factors.management.show',[ 'payment_id'=>$item->id ]) }}">
                                        <span class="fas fa-edit DropdDownIcon"></span>
                                        <span class="">مشاهده جزئیات</span>
                                    </a>
                                    <a class="dropdown-item DrDownItem"
                                       href="{{ route('sent.product',$item) }}">
                                        <span class="fas fa-edit DropdDownIcon"></span>
                                        <span class="">ارسال شد</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        @if($payments->total()>$paginate)
            <div class="FooterPagination">
                <span class="PaginationLinks">{{ $payments->links() }}</span>
            </div>
        @endif



        @includeIf('pages.admin.ProductPopUpModel')


        @if($payments->total()>$paginate)
            <div class="FooterPagination">
                <span class="PaginationLinks">{{ $payments->links() }}</span>
            </div>
        @endif


        @php
            }
        @endphp


    </div>
@endsection