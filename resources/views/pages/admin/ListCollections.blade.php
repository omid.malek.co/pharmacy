@extends('pages.admin')

@section('Items')
    @includeIf('pages.admin.CollectionItems')
@endsection

@section('BaseSection')

    <div class="SectionMajor table-responsive">

        @php
            if(empty($collections)){
        @endphp
        <section class="ErrorFrame">
            <section class="alert alert-primary text-center ErrorBox BMitra">
                <span class="IRanSans ErrorMessage">{{ 'هیچ داده ای یافت نشد' }}</span>
            </section>
        </section>
        @php
            }else{
        @endphp
        <div class="SectionMajor table-responsive">
            @php
                if(is_null($collections)  || empty($collections) || $collections==json_encode([])){
            @endphp
            <section class="ErrorFrame">
                <section class="alert alert-primary text-center ErrorBox BMitra">
                    <span class="IRanSans ErrorMessage">{{ 'هیچ داده ای یافت نشد' }}</span>
                </section>
            </section>
            @php
                }else{
            @endphp

            <table class="table">
                <thead class="thead-light">
                <tr>
                    <th>ردیف</th>
                    <th>عنوان کالا</th>
                    <th>قیمت کالا</th>
                    <th>تعداد کالا</th>
                    <th>عملیات</th>
                </tr>
                </thead>
                <tbody>
                @foreach($collections as $item)
                    <tr id="{{ 'Row'.$item->id }}">
                        <td>{{ $row++ }}</td>
                        <td>{{ $item->title }}</td>
                        <td>{{ $item->amount }}</td>
                        <td>{{ $item->number }}</td>
                        <td>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <button type="button" class="btn btn-outline-secondary dropdown-toggle"
                                            data-toggle="dropdown">
                                        انتخاب کنید
                                    </button>
                                    <div class="dropdown-menu">
                                        {{--{{ route('posts.edit',['id'=>$collection->id]) }}--}}
                                        {{--{{ route($item->url_management_page,[ 'page_id'=>$item->id ]) }}--}}
                                        <a class="dropdown-item DrDownItem" href="{{ route('admin.products.edit',[ 'product_id'=>$item->id ]) }}">
                                            <span class="fas fa-edit DropdDownIcon"></span>
                                            <span class="">ویرایش محتوا برگه</span>
                                        </a>
                                        <a class="dropdown-item DrDownItem" href="{{ route('admin.options.index',[ 'product_id'=>$item->id ]) }}">
                                            <span class="fas fa-edit DropdDownIcon"></span>
                                            <span class="">مدیریت مشخصات</span>
                                        </a>
                                        <a data-toggle="modal" data-target="#ProjectsPopUpModel"
                                           class="dropdown-item DropDownItem"
                                           path="{{ route('admin.image.remove',[ 'product_id'=>$item->id ]) }}"
                                           href="#" clicked="false">
                                            <span class="fas fa-trash-alt"></span>
                                            <span>حذف</span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            @php
                }
            @endphp
            @includeIf('pages.admin.ProductPopUpModel')
        </div>
        @if($collections->total()>$paginate)
            <div class="FooterPagination">
                <span class="PaginationLinks">{{ $collections->links() }}</span>
            </div>
        @endif
        @php
            }
        @endphp
        @includeIf('pages.admin.removeModalUser')
    </div>
@endsection