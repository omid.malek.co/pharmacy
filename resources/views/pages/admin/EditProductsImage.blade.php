@extends('pages.admin')

@push('styles')
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('dropzone/dist/dropzone.css')}}">
@endpush


@section('Items')
    @includeIf('pages.admin.CollectionItems')
@endsection

@section('BaseSection')
    <div class="EditImageCollection">
        @includeIf('pages.error')
        {{--</form>--}}
        {{--{{ route('update.album.galleres') }}--}}
        <form action="{{ route('admin.Productsimage.update',['image_id'=>$content->id]) }}" method="POST" class='dropzone'>
            @method('PUT')
            {{--            <input type="hidden" value="{{ $content->id }}" name="image_id" />--}}
            {{ csrf_field() }}
        </form>

        <div class="UpdateImage">
            <img id="ImagesEvent" src="{{ asset( $content->image_path ) }}" alt="">
        </div>
    </div>
@endsection


@push('MyScript')
    <script src="{{ asset('dropzone/dist/dropzone.js') }}" type="text/javascript"></script>

    <script type="text/javascript">
      var CSRF_TOKEN = document.querySelector('meta[name="csrf-token"]').getAttribute('content')

      Dropzone.autoDiscover = false
      var myDropzone = new Dropzone('.dropzone', {
        maxFilesize: 1,  // 3 mb
        acceptedFiles: '.jpeg,.jpg,.png,.pdf',
        success: function (file, response) {

          console.log(response.file);

          $('#ImagesEvent').attr('src', response.file)

        }
      })
      myDropzone.on('sending', function (file, xhr, formData) {
        formData.append('_token', CSRF_TOKEN)

      })
    </script>

@endpush

