@extends('pages.admin')

@section('Items')
    @includeIf('pages.admin.discountsItems')
@endsection


@push('styles')
    <link href="{{ asset('datepicker/dist/jquery.md.bootstrap.datetimepicker.style.css') }}" rel="stylesheet"/>
@endpush

@push('MyScript')
    <script src="{{ asset('datepicker//dist/jquery.md.bootstrap.datetimepicker.js') }}"></script>
    <script type="text/javascript">
      $(document).ready(function () {
        $('#date5').MdPersianDateTimePicker({
          targetTextSelector: '#inputDate5',
          groupId: 'rangeSelector1',
          placement: 'top',
          enableTimePicker: false,
          dateFormat: 'yyyy-MM-dd',
          isGregorian: false,
          textFormat: 'yyyy-MM-dd',
        })
        $('#date6').MdPersianDateTimePicker({
          targetTextSelector: '#inputDate6',
          groupId: 'rangeSelector1',
          placement: 'top',
          enableTimePicker: false,
          isGregorian: false,
          dateFormat: 'yyyy-MM-dd',
          textFormat: 'yyyy-MM-dd',
        })
      })
    </script>
@endpush

@section('BaseSection')
    <div class="CreateCollectionPage">
        @includeIf('pages.error')
        <form action="{{ route('discounts.store') }}" method="post">
            @csrf
            <div class="CollectionPost">
                <div class="InputFrame">
                    <label for="Phone">مقدار درصد تخفیف</label>
                    <input type="number" value="{{ old('percent',1) }}" min="2" max="100"
                           class="form-control text-center" name="percent"/>
                </div>
                <div class="InputFrame">
                    <label for="Fullname">تاریخ پایان</label>
                    <pre></pre>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text cursor-pointer" id="date6">
                                <i class="far fa-calendar-alt"></i>
                            </span>
                        </div>
                        <input type="text" id="inputDate6"
                               class="form-control text-center"
                               placeholder="تاریخ پایان" aria-label="date6"
                               name="expire_date" value="{{ old('expire_date','') }}"
                               aria-describedby="date6" />
                    </div>
                </div>
            </div>
            <div class="CollectionInfo">
                <div class="InputFrame">
                    <label for="Fullname">کد تخفیف</label>
                    <input type="text" value="{{ old('text','') }}"
                           class="form-control text-center" name="text" />
                </div>
                <div class="InputFrame">
                    <label for="Fullname">تاریخ شروع</label>
                    <pre></pre>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text cursor-pointer" id="date5">
                                 <i class="far fa-calendar-alt"></i>
                            </span>
                        </div>
                        <input type="text" id="inputDate5" class="form-control text-center"
                               placeholder="تاریخ شروع" aria-label="date5"
                               value="{{ old('start_date','') }}"
                               name="start_date" aria-describedby="date5" />
                    </div>
                </div>
            </div>
            <div class="BTNCollectionArea">
                <span clicked="false" id="registerProduct"
                      class="CreateCollectionBTN btn btn-outline-success btn-block">ثبت اطلاعات</span>
            </div>
        </form>
    </div>
@endsection


