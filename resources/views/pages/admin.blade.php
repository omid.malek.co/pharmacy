<!DOCTYPE html>
<html class="rtl" dir="rtl" lang="fa">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@yield('title')</title>

    {{-- Styles --}}
    {{--<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css"--}}
          {{--integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">--}}

    <link rel="stylesheet" type="text/css" href="{{ asset('css/admin.css') }}"/>
    @yield('bundles')
    @stack('styles')
</head>
<body dir="ltr">
    <div class="container-fluid Container">
        <div class="FirstRow">
            <div class="SiteHeader">
                {{--Auth::user()->name.' خوش آمدید '--}}
                <div class="WellcomeMsg">{{ ' خوش آمدید ' }}</div>
                <div id="ItemsIcon" class="ItemsIcon">
                    <i class="fas fa-bars"></i>
                </div>
                <div class="TitleDashboard">پنل مدیریت محتوا</div>
            </div>
        </div>
        <div id="NavigationRow" class="NavigationRow">
            <ul id="NavigationColumn" page="{{ $page ?? '' }}" class="NavigationColumn">
                <li id="Home">
                    <a href="{{ route('home') }}" class="">
                        <span class="fas fa-tachometer-alt"></span>
                        <span>صفحه اصلی</span>
                    </a>
                </li>
                <li id="Exit">
                    <a href="{{ route('admin.logout') }}">
                        <span class="fas fa-sign-out-alt"></span>
                        <span>خروج</span>
                    </a>
                </li>
            </ul>
        </div>
        <div class="RowContent">
            <div class="ListItemsColumn">
                @yield('Items')
            </div>
            <div class="ContentColumn">
                @yield('BaseSection')
            </div>
        </div>
    </div>

    {{-- Scripts --}}

    <script type="text/javascript" src="{{ asset('js/alladmin.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/AdminApp.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/fontawesome/all.js') }}"></script>



    @yield('scripts')


    @stack('MyScript')


</body>
</html>
