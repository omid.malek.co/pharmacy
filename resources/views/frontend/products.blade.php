@extends('main')

@push('meta_tags')
    @includeIf('frontend.meta_tages')
@endpush

@push('styles')
    <link rel="stylesheet" type="text/css" href="{{ asset('multislider/css/custom.css') }}" />
@endpush

@section('content')
    @include('frontend.navigation')

    <div class="ProductsRow row">
        <div class="ProductsCol">

            @foreach($products as $item)

                <div class="SellerItems">
                    <a href="{{ route('products.show',$item) }}">
                        <img src="{{ asset($item->image_path) }}" class="rounded" alt="cat">
                        <h3 class="title-product">
                            {{ $item->title }}
                        </h3>

                        <p class="price">
                            {{ $item->amount.' تومان ' }}
                        </p>
                    </a>
                </div>

            @endforeach
            @if($products->total()>$paginate)
                <div class="FooterPagination">
                    <span class="PaginationLinks">{{ $products->links() }}</span>
                </div>
            @endif
        </div>
    </div>
    @includeIf('frontend.footer')
@endsection



@push('scripts')
    <script type="text/javascript" src="{{ asset('multislider/js/multislider.min.js') }}"></script>
    <script>
      $(document).ready(function () {
        $('#BestSeller').multislider({
          interval: 2000,
          slideAll: false
        });
        $('#Allproducts').multislider({
          interval: 2000,
          slideAll: false
        })
      })
    </script>
@endpush