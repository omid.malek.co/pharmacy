@extends('main')

@push('meta_tags')
    @includeIf('frontend.meta_tages')
@endpush

@section('content')
    <div class="FirstRow">
        @include('frontend.navigation')
        <div class="SliderRow">
            @include('frontend.slider')
            @include('frontend.RightSideSlider')
        </div>
    </div>
    <div class="DetailsArticle row">
        @includeIf('frontend.CategoriesItem')
        <div class="LatestContentFrame card">
            <div class="card-header TitleArticle">{{ $post->first()->title }}</div>
            <div class="card-body ArticleText">
                <blockquote class="blockquote mb-0">
                    <p>{!! $post->first()->body !!}</p>
                    <footer class="blockquote-footer badge badge-dark publishedDate">{{ $post->first()->published_at }}<cite title="Source Title"> &nbsp;&nbsp;&nbsp;&nbsp;منتشر شده در تاریخ </cite></footer>
                </blockquote>
            </div>
        </div>
        @includeIf('frontend.LastComments')
        @includeIf('frontend.AboutDart')
        @includeIf('frontend.footer')
    </div>
@endsection