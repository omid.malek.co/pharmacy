@extends('main')

@push('meta_tags')
    @includeIf('frontend.meta_tages')
@endpush

@push('styles')
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link href="{{ asset('datepicker/dist/jquery.md.bootstrap.datetimepicker.style.css') }}" rel="stylesheet"/>
    <input type="hidden" id="giftsCreator" value="{{ route('gifts.create') }}"/>
@endpush



@push('scripts')
    <script src="{{ asset('datepicker//dist/jquery.md.bootstrap.datetimepicker.js') }}"></script>
    <script type="text/javascript">
      $(document).ready(function () {
        $('#date5').MdPersianDateTimePicker({
          targetTextSelector: '#inputDate5',
          groupId: 'rangeSelector1',
          placement: 'top',
          enableTimePicker: false,
          dateFormat: 'yyyy-MM-dd',
          isGregorian: false,
          textFormat: 'yyyy-MM-dd',
        })
        $('#date6').MdPersianDateTimePicker({
          targetTextSelector: '#inputDate6',
          groupId: 'rangeSelector1',
          placement: 'top',
          enableTimePicker: false,
          isGregorian: false,
          dateFormat: 'yyyy-MM-dd',
          textFormat: 'yyyy-MM-dd',
        })
      })
    </script>
@endpush

@section('content')
    <div class="FirstRow">
        <input type="hidden" id="AddressReceiver" value=""/>
        @include('frontend.navigation')
        <div class="SliderRow">
            @include('frontend.slider')
            @include('frontend.RightSideSlider')
        </div>
    </div>
    <div class="ShowFactor row">
        @includeIf('frontend.CategoriesItem')
        @includeIf('frontend.chargeWallet')
        <input type="hidden" value="{{ csrf_token() }}" id="MyToken"/>
        <div id="Factorscol" class="Factorscol">
            <div class="WalletAccount">
                <div class="AmountAccount">
                    <div class="Label">موجودی حساب شما</div>
                    <div class="Value">{{ $wallet->amount.' تومان ' }}</div>
                </div>
                <div data-toggle="modal" data-target="#runChargeWallet" class="chargeWallet btn btn-outline-info">شارژ
                    حساب
                </div>
            </div>
            <div id="FactorsFrame" class="col-12 pr-0 pl-0 mt-4">
                <div class="giftFormFrame">
                    @includeIf('pages.error')
                    <form id="GiftCardForm" action="{{ route('gifts.store') }}" method="POST">
                        @csrf
                        <div class="CollectionPost">
                            <div class="InputFrame AmountGift">
                                <label for="">موجودی کارت (تومان)</label>
                                <input type="text" name="amount" class="form-control text-center"/>
                            </div>
                            <input type="hidden" name="wallet_id" value="{{ $wallet->id }}"/>
                            <div class="InputFrame">
                                <label for="date6">تاریخ پایان</label>
                                <pre></pre>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                            <span class="input-group-text cursor-pointer" id="date6">
                                <i class="far fa-calendar-alt"></i>
                            </span>
                                    </div>
                                    <input type="text" id="inputDate6"
                                           class="form-control text-center"
                                           placeholder="تاریخ پایان" aria-label="date6"
                                           name="expire_date" value="{{ old('expire_date','') }}"
                                           aria-describedby="date6"/>
                                </div>
                            </div>
                        </div>
                        <div class="CollectionInfo">
                            <div class="InputFrame ">
                                <label for="Fullname">کد تخفیف</label>
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="btn btn-outline-secondary" id="createCode">ایجادکد</span>
                                    </div>
                                    <input type="hidden" value="" name="text"/>
                                    <input type="text" name="textGiftCode" value="" id="giftCodeValue"
                                           disabled="disabled" class="form-control text-center" placeholder=""
                                           aria-label="Example text with button addon"
                                           aria-describedby="button-addon1"/>
                                </div>
                            </div>
                            <div class="InputFrame">
                                <label for="Fullname">تاریخ شروع</label>
                                <pre></pre>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                            <span class="input-group-text cursor-pointer" id="date5">
                                 <i class="far fa-calendar-alt"></i>
                            </span>
                                    </div>
                                    <input type="text" id="inputDate5" class="form-control text-center"
                                           placeholder="تاریخ شروع" aria-label="date5"
                                           value="{{ old('start_date','') }}"
                                           name="start_date" aria-describedby="date5"/>
                                </div>
                            </div>
                        </div>
                        <div class="BTNCollectionArea">
                <span id="CreateGiftCard" clicked="false"
                      class="CreateCollectionBTN btn btn-outline-success btn-block">ثبت کارت هدیه</span>
                        </div>
                    </form>
                </div>
                @if($isEmpty)
                    {!! $gifts !!}
                @else
                    <div dir="rtl" class="col-10 offset-1 table-responsive pr-0 pl-0 mt-4">
                        <table class="table text-right CustomTable">
                            <thead class="thead-light">
                            <tr>
                                <th>ردیف</th>
                                <th>موجودی</th>
                                <th>کد کارت هدیه</th>
                                <th>تاریخ شروع</th>
                                <th>تاریخ انقضاء</th>
                                <th>وضعیت کارت</th>
                                <th>عملیات</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($gifts as $item)
                                <tr id="{{ 'Row'.$item->id }}" class="">
                                    <td>{{ $row++ }}</td>
                                    <td>{{ $item->amount }}</td>
                                    <td>{{ $item->text }}</td>
                                    <td>{{ $item->persian_start_date }}</td>
                                    <td>{{ $item->persian_expire_date }}</td>
                                    <td>{{ $item->condition->title }}</td>
                                    <td>
                                        <form class="d-none" action="{{ route('gifts.destroy',$item) }}"
                                              id="GiftDestroy" method="POST">
                                            @csrf
                                            @method('DELETE')
                                        </form>
                                        <i id="RemoveGiftLink" clicked="false" class="fas fa-eraser text-danger cursor-pointer"></i>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        @if($gifts->total()>$paginate)
                            <div class="FooterPagination">
                                <span class="PaginationLinks">{{ $gifts->links() }}</span>
                            </div>
                        @endif
                    </div>
                @endif
            </div>
            @includeIf('frontend.MessagesModel')
        </div>
        @includeIf('frontend.LastComments')
        @includeIf('frontend.AboutDart')
        @includeIf('frontend.footer')
    </div>
@endsection