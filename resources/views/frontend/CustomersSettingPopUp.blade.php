<div class="modal" tabindex="-1" id="CustomersAccount" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div dir="rtl" class="modal-header">
                <h5 class="modal-title text-right">حساب کاربری</h5>
            </div>

            <div class="modal-body">
                <ul class="CustomerItems">
                    <li>
                        <a href="{{ route('customer.logOut') }}">
                            <span class="fas fa-sign-out-alt"></span>
                            <span>خروج</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('wallets.index') }}">
                            <span class="fas fa-sign-out-alt"></span>
                            <span>کیف پول</span>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">بستن</button>
            </div>
        </div>
    </div>
</div>