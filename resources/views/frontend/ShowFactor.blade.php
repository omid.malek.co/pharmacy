@extends('main')

@push('meta_tags')
    @includeIf('frontend.meta_tages')
@endpush

@push('styles')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@endpush

@section('content')
    @isset($payment->id)
        <input type="hidden" id="AddressReceiver"
               value="{{  route('factor.set.address',[ 'payment_id'=>$payment->id ])  }}"/>
    @endisset
    @include('frontend.navigation')
    <div class="ShowFactor row">
        <input type="hidden" value="{{ route('factors.update') }}" id="UpdateNumberProduct"/>
        <input type="hidden" value="{{ csrf_token() }}" id="MyToken"/>
        <div id="Factorscol" class="Factorscol table-responsive">
            <div id="FactorsFrame" class="col-12">
                {!! $basketTable !!}
            </div>
            @includeIf('frontend.MessagesModel')
        </div>
        <div class="col-12">
            @includeIf('frontend.BestSeller')
            @includeIf('frontend.AllProducts')
        </div>
    </div>
    @includeIf('frontend.footer')
@endsection

@push('scripts')
    <script type="text/javascript" src="{{ asset('multislider/js/multislider.min.js') }}"></script>
    <script>
      $(document).ready(function () {
        $('#BestSeller').multislider({
          interval: 2000,
          slideAll: false
        });
        $('#Allproducts').multislider({
          interval: 2000,
          slideAll: false
        })
      })
    </script>
@endpush