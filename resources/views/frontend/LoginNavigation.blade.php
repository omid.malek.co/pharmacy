<ul id="AboutNavbar" class="nav nav-tabs AboutNavbar" innerPage="{{ $innerPage }}">
    <li class="nav-item">
        <a  class="nav-link" href="{{ route('admin.panel') }}">مدیریت</a>
    </li>
    <li class="nav-item">
        <a id="Login" class="nav-link" href="{{ route('customer.login') }}">ورود</a>
    </li>
    <li class="nav-item">
        <a id="Register" class="nav-link" href="{{ route('customer.register') }}">عضویت</a>
    </li>
</ul>
