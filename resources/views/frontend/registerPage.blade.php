@extends('main')

@push('meta_tags')
    @includeIf('frontend.meta_tages')
@endpush


@section('content')
    @include('frontend.navigation')
    <div class="RegisterRow mt-2">
        <div class="RegisterCol">
            @includeIf('frontend.LoginNavigation')
            <form id="RegisterForm" action="{{ route('store.customer') }}" method="POST">
                @includeIf('pages.error')
                @csrf
                {{--<input type="text" class="form-control InputsFormSpace" name="fullname" placeholder="نام و نام خانوادگی"/>--}}
                <input type="text" class="form-control InputsFormSpace" name="email" placeholder="ایمیل"/>
                {{--<input type="text" class="form-control InputsFormSpace" name="post_code" placeholder="کد پستی"/>--}}
                <input type="text" class="form-control InputsFormSpace" name="phone" placeholder="شماره تماس"/>
                <input type="password" class="form-control  InputsFormSpace" name="password" placeholder="کلمه عبور"/>
                <input type="password" class="form-control  InputsFormSpace" name="password_confirmation" placeholder="تکرار کلمه عبور"/>
                {{--<textarea name="address" class="form-control  InputsFormSpace" placeholder="آدرس" id="" cols="30" rows="10"></textarea>--}}
                <span id="register-button" class="btn btn-outline-success CustomBTN  ClickedBTN" clicked="false">ثبت عضویت</span>
            </form>
        </div>
        @includeIf('frontend.BestSeller')
        @includeIf('frontend.AllProducts')
    </div>
    @includeIf('frontend.footer')
@endsection