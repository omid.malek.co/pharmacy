<footer class="FooterRow">
    <div class="ParentColsFooter pr-0 pl-0">
        <div class="MenuBox">
            <div class="text-right ItemsList">
                <ul>
                    @foreach($categories as $item)
                        <li>
                            <a href="{{ route('select.products',['categorie_id'=>$item->id])  }}">
                                <span class="TxtItem">{{ $item->title }}</span>
                            </a>
                        </li>
                    @endforeach
                </ul>
            </div>
        </div>
        <div class="SocialNetwork">
            <div class="text-right ItemsList">
                <ul>
                    <li class="mt-0 mb-0"><a href="#">
                            <i class="fab fa-google-plus-g"></i>
                        </a></li>
                    <li class="mt-0 mb-0"><a href="#">
                            <i class="fab fa-facebook"></i>
                        </a></li>
                    <li class="mt-0 mb-0"><a href="{{ $user->first()->telegram_address=="null" ? '' : $user->first()->telegram_address }}">
                            <i class="fab fa-telegram-plane"></i>
                        </a></li>
                    <li class="mt-0 mb-0"><a href="#">
                            <i class="fab fa-whatsapp"></i>
                        </a></li>
                    <li class="mt-0 mb-0"><a href="{{ $user->first()->instagram_address=="null" ? '' : $user->first()->instagram_address }}">
                            <i class="fab fa-instagram"></i>
                        </a></li>
                </ul>
            </div>
        </div>
        <div class="InformationMe">
            <div class="text-right ItemsList">
                <ul>
                    <li><a href="{{ route('about.us') }}">درباره ما</a></li>
                    <li><a href="{{ route('contact.us') }}">تماس با ما</a></li>
                    <li class="d-none"><a href="{{ route('articles.index') }}">دانستنی ها</a></li>
                </ul>
            </div>
        </div>
        {{--<div class="ProgrammerInformation">--}}
        {{--<a href="http://www.omid-malek.ir" class="text-white" target="_blank">طراحی و برنامه نویسی: امید مالک</a>--}}
        {{--</div>--}}
    </div>
</footer>