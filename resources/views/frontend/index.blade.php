@extends('main')

@push('meta_tags')
    @includeIf('frontend.meta_tages')
    <title>دارو | مکمل غذایی | تجهیزات | غذایی | غزاله مالک</title>
@endpush


@section('content')
    @include('frontend.navigation')
    @includeIf('frontend.slider')
    {{--<div class="FirstRow">--}}
        {{--<div class="SliderRow">--}}
            {{--@include('frontend.slider')--}}
            {{--@include('frontend.RightSideSlider')--}}
        {{--</div>--}}
    {{--</div>--}}
    <div class="ContentRow row">
        @includeIf('frontend.home')
    </div>
@endsection