@extends('main')


@push('styles')
    <link rel="stylesheet" type="text/css" href="{{ asset('multislider/css/custom.css') }}"/>
@endpush

@push('scripts')
    <script type="text/javascript" src="{{ asset('multislider/js/multislider.min.js') }}"></script>
    <script>
      $(document).ready(function () {
        $('#BestSeller').multislider({
          interval: 2000,
          slideAll: false
        })
        $('#Allproducts').multislider({
          interval: 2000,
          slideAll: false
        })
      })
    </script>
@endpush

@push('meta_tags')
    @includeIf('frontend.meta_tages')
@endpush


@section('content')
    @include('frontend.navigation')
    <div class="ContactRow">
        <div class="bannerBox">
            <img src="{{ asset('images/ContactUS_1.jpg') }}" alt="">
        </div>
        <div class="ContactInformation d-flex">
            <div class="phoneNumbers d-flex mt-4">
                <div class="TitleBox">
                    <div class="Title">راههای ارتباطی</div>
                    <div class="Icon">
                        <i class="fas fa-phone-volume"></i>
                    </div>
                </div>
                <div class="description p-4">
                    <div class="Items">
                        <div class="Label d-flex">شماره ثابت</div>
                        <div class="Text d-flex">05632834858</div>
                    </div>
                    <div class="Items">
                        <div class="Label d-flex">فکس</div>
                        <div class="Text d-flex">05632827271</div>
                    </div>
                    <div class="Items">
                        <div class="Label d-flex">واتس اپ</div>
                        <div class="Text d-flex">09022834858</div>
                    </div>
                    <div class="Items">
                        <div class="Label d-flex">تلگرام</div>
                        <div class="Text d-flex">09022834858</div>
                    </div>
                </div>
            </div>
            <div class="SocialAndAddress d-flex">
                <div class="SocialNetwork d-flex mt-4">
                    <div class="TitleBox">
                        <div class="Title">شبکه های اجتماعی</div>
                        <div class="Icon">
                            <i class="fas fa-caret-left"></i>
                        </div>
                    </div>
                    <div class="description">
                        <ul class="mt-3">
                            <li class="mt-0 mb-0"><a href="#">
                                    <i class="fab fa-google-plus-g"></i>
                                </a></li>
                            <li class="mt-0 mb-0"><a href="#">
                                    <i class="fab fa-facebook"></i>
                                </a></li>
                            <li class="mt-0 mb-0"><a href="#">
                                    <i class="fab fa-telegram-plane"></i>
                                </a></li>
                            <li class="mt-0 mb-0"><a href="#">
                                    <i class="fab fa-whatsapp"></i>
                                </a></li>
                            <li class="mt-0 mb-0"><a href="#">
                                    <i class="fab fa-instagram"></i>
                                </a></li>
                        </ul>
                    </div>
                </div>
                <div class="MyAddress mt-4">
                    <div class="TitleBox">
                        <div class="Title">آدرس</div>
                        <div class="Icon">
                            <i class="fas fa-caret-left"></i>
                        </div>
                    </div>
                    <div class="description p-4">
                        <p class="d-flex w-100">استان خراسان جنوبی</p>
                        <p class="d-flex w-100">شهرستان طبس</p>
                        <p class="d-flex w-100">
                            خیابان نواب صفوی
                        </p>
                        <p class="d-flex w-100">جنب ساختمان پزشکان حکیم</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="MyLocation d-flex mt-4">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d207.71065819826325!2d56.923705785133876!3d33.59568931972027!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3f0eb354805e7e4b%3A0x7de61a6c3e181180!2sDr%20Malek%20Pharmacy!5e0!3m2!1sen!2s!4v1614082194423!5m2!1sen!2s" width="100%" height="400" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
        </div>
        {{--@includeIf('frontend.BestSeller')--}}
        {{--@includeIf('frontend.AllProducts')--}}
    </div>
    @includeIf('frontend.footer')
@endsection

