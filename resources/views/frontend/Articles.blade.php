@extends('main')

@push('meta_tags')
    @includeIf('frontend.meta_tages')
@endpush

@section('content')
    @include('frontend.navigation')
    @include('frontend.slider')
    <div class="LatestArticle row">
        {{--@includeIf('frontend.CategoriesItem')--}}
        <div class="LatestContentFrame">
            @foreach($posts as $item)
                <div class="col-12 col-sm-6 col-md-6 ArticleBox pr-0 pl-0">
                    <a class="linkPosts" href="{{ route('articles.show',['post_id'=>$item->id]) }}">
                        <div class="col-12 pr-0 pl-0 ArticleImage">
                            <img src="{{ $item->image_path }}" class="img-fluid"/>
                            <a class="Title" class="col-12">{{ $item->title }}</a>
                        </div>
                        <div class="col-12  pr-0 pl-0 PriceTimeFrame">
                            <div dir="rtl" class="col-5 col-sm-6 col-md-5 offset-md-1 d-md-inline-block float-left mt-2 pt-2 pb-2 ArticlePrice">
                                <div class="col-6 pr-0 pl-0 NumberVisitePost">
                                    <i class="fas fa-eye"></i>
                                   {{ $item->visited }}
                                </div>
                            </div>
                            <div dir="rtl"
                                 class="col-5 col-sm-6 col-md-5 d-md-inline-block float-right mt-2 pt-2  TimeStartCourse">
                                <i class="fas fa-user"></i>
                                {{ $user->first()->fullname }}
                            </div>
                        </div>
                    </a>
                </div>
            @endforeach
        </div>
        @includeIf('frontend.BestSeller')
        @includeIf('frontend.AllProducts')
    </div>
    @includeIf('frontend.footer')
@endsection