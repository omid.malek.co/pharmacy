<div class="modal" tabindex="-1" id="runChargeWallet" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div dir="rtl" class="modal-header">
                <h5 class="modal-title text-right">شارژ کیف پول</h5>
            </div>

            <div class="modal-body">
                <form id="chargeWalletForm" method="POST" action="{{ route('wallets.store') }}" class="chargeWalletForm">
                    @csrf
                    <div class="chargeWalletInnerFrame">
                        <div class="AmountAccount">
                            <div class="Value">
                                <input type="text" class="form-control" placeholder="مبلغ مورد نظر خود را وارد کنید" name="amount" />
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">بستن</button>
                <!--
                <div id="applyChargeWallet" class="chargeWallet btn btn-outline-info">شارژ حساب</div>
                -->
            </div>
        </div>
    </div>
</div>