@extends('main')


@push('styles')
    <link rel="stylesheet" type="text/css" href="{{ asset('multislider/css/custom.css') }}"/>
@endpush


@push('meta_tags')
    @includeIf('frontend.meta_tages')
@endpush


@section('content')
    @include('frontend.navigation')
    <div class="DetailsRow row">
        <div class="Detailscol p-sm-2 p-md-2">
            <div class="Title">
                <h3 class="p-2">{{ $selectedItem->title }}</h3>
            </div>
            <div class="Image mt-4">
                <a href="">
                    <img src="{{ $selectedItem->image_path }}" alt="">
                </a>
            </div>
            <ul class="PriceBox mt-4">
                <input type="hidden" id="basketPath"
                       value="{{ route('factors.store',[ 'product_id'=>$selectedItem->id ]) }}"/>
                <li class="BasePrice">
                    <h4>{{ $selectedItem->amount }}</h4>
                    <span>{{ ' تومان ' }}</span>
                </li>
                {{--<li class="input-group-lg">--}}
                {{--<label class="input-group-append" for="">--}}
                {{--<span class="input-group-text" id="basic-addon2">{{ ' کد تخفیف ' }}</span>--}}
                {{--</label>--}}
                {{--<input type="text" class="form-control" />--}}
                {{--</li>--}}
                @if(session()->has('id'))
                    <li class="input-group-lg BTNSales">
                        <span id="AddToBasket"
                              class="btn btn-lg btn-outline-success btn-block">افزودن به سبد خرید</span>
                    </li>
                    <li class="input-group-lg BTNSales">
                        <a href="{{ route('factors.show') }}" class="btn btn-lg btn-outline-info btn-block">سبد خرید</a>
                    </li>
                @else
                    <li class="input-group-lg BTNSales">
                        <a href="#" id="AddToBasket" data-toggle="modal" data-target="#MessagePopUp"
                           class="btn btn-lg btn-outline-success btn-block">افزودن به سبد خرید</a>
                    </li>
                    {{--<li class="input-group-lg BTNSales">--}}
                    {{--<a href="#"  data-toggle="modal" data-target="#MessagePopUp" class="btn btn-lg btn-outline-info btn-block">سبد خرید</a>--}}
                    {{--</li>--}}
                @endif
            </ul>
            @includeIf('frontend.MessagesModel')
        </div>
        <div class="PropertyCol p-sm-2 p-md-2">
            <div class="Property p-md-2">
                <ul class="nav nav-tabs">
                    <li class="nav-item">
                        <a class="nav-link active" href="#">مشخصات</a>
                    </li>
                </ul>
                @if($options['doesntExist'])
                    {!! $options['options'] !!}
                @else
                    <table class="table table-bordered mt-4">
                        <tbody>
                        @foreach($options['options'] as $option)
                            <tr>
                                <td>{{ $option->title }}</td>
                                <td>{{ $option->value }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                @endif
            </div>
        </div>
        @includeIf('frontend.BestSeller')
        @includeIf('frontend.AllProducts')
    </div>
    @includeIf('frontend.footer')
@endsection


@push('scripts')
    <script type="text/javascript" src="{{ asset('multislider/js/multislider.min.js') }}"></script>
    <script>
      $(document).ready(function () {
        $('#BestSeller').multislider({
          interval: 2000,
          slideAll: false
        });
        $('#Allproducts').multislider({
          interval: 2000,
          slideAll: false
        })
      })
    </script>
@endpush

