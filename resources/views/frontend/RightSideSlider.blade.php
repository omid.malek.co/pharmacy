<div class="RightSideSlider">
    <div class="BackImage">
        <img src="{{ asset('images/slider_1_2.png') }}" alt="">
    </div>
    <ul class="SocialNetworkIcon">
        <li>
            <a href="{{ $user->first()->facebook_address=="null" ? '' : $user->first()->facebook_address }}">
                <i class="fab fa-facebook"></i>
            </a>
            <a href="{{ $user->first()->email=="null" ? '' : $user->first()->email }}">
                <i class="far fa-envelope"></i>
            </a>
            <a href="{{ $user->first()->telegram_address=="null" ? '' : $user->first()->telegram_address }}">
                <i class="fab fa-telegram-plane"></i>
            </a>
            <a href="{{ $user->first()->instagram_address=="null" ? '' : $user->first()->instagram_address }}">
                <i class="fab fa-instagram"></i>
            </a>

        </li>
    </ul>
    <div class="dartTitle">
        <h2>انواع دارت</h2>
        <h2>و لوازم جانبی</h2>
        {!! $rightSlider->body !!}
        <a href="{{ route('articles.show',['post_id'=>$rightSlider->id]) }}">جزئیات بیشتر</a>
    </div>
    <ul class="ContactNumber">
        <li>
            <i class="fas fa-map-marker-alt"></i>
            <span>{{ $user->first()->address }}</span>
        </li>
        <li>
            <i class="fas fa-phone-square-alt"></i>
            <span>{{ $user->first()->phone }}</span>
        </li>
    </ul>
</div>