@extends('main')

@push('meta_tags')
    @includeIf('frontend.meta_tages')
@endpush

@section('content')
    <input type="hidden" id="AddressReceiver" value="{{ route('factor.set.address',[ 'payment_id'=>$payment->id ]) }}"/>
    @include('frontend.navigation')
    <div class="ReceiverInformation row">
        <input type="hidden" value="{{ route('factors.update') }}" id="UpdateNumberProduct"/>
        <input type="hidden" value="{{ csrf_token() }}" id="MyToken"/>
        <form id="ReceiverInformertion" method="POST"
              action="{{ route("factor.update.address") }}"
              class="ReceiverInfoCol table-responsive">
            @csrf
            <input type="hidden" name="payment_id" value="{{ $payment->id }}"/>
            <table class="table table-bordered BorderedTable">
                <tbody>
                <tr>
                    <td>
                        <label for="">نام و نام خانوادگی گیرنده</label>
                        <input id="" type="text"
                               class="form-control"
                               name="receiver_fullname"
                               value="{{ old('receiver_fullname',$payment->receiver_fullname ) }}"/>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label for="">آدرس گیرنده</label>
                        <textarea name="sent_address"
                                  class="form-control">{{ old('sent_address',$payment->sent_address ) }}</textarea>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label for="">شماره تماس گیرنده</label>
                        <input id="" type="text" class="form-control" name="sent_phone"
                               value="{{ old('sent_phone',$payment->sent_phone ) }}"/>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label for="">کد پستی گیرنده</label>
                        <input id="" type="text" class="form-control" name="postal_code_sent"
                               value="{{ old('postal_code_sent',$payment->postal_code_sent ) }}"/>
                    </td>
                </tr>
                </tbody>
            </table>
            <!--
                        <div class="FormButton">
                            <span id="UpdateReceiverInfo" class="btn btn-outline-success CustomBTN" clicked="false">ثبت آدرس و پرداخت</span>
                        </div>
            -->
            @includeIf('frontend.MessagesModel')
        </form>
        <div class="col-12">
            @includeIf('frontend.BestSeller')
            @includeIf('frontend.AllProducts')
        </div>
    </div>
    @includeIf('frontend.footer')
@endsection
@push('scripts')
    <script type="text/javascript" src="{{ asset('multislider/js/multislider.min.js') }}"></script>
    <script>
      $(document).ready(function () {
        $('#BestSeller').multislider({
          interval: 2000,
          slideAll: false
        });
        $('#Allproducts').multislider({
          interval: 2000,
          slideAll: false
        })
      })
    </script>
@endpush