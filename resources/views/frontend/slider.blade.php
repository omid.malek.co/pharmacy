<div class="SliderFrame d-flex mt-3">
    <div id="myCarousel" class="carousel slide SliderFrame" data-ride="carousel">
        <ol class="carousel-indicators">
            @foreach($sliders as $item)
                @if($loop->first)
                    <li data-target="#myCarousel" data-slide-to="{{ $slideNumber++ }}" class="active">
                    </li>
                @else
                    <li data-target="#myCarousel" data-slide-to="{{ $slideNumber++ }}"></li>
                @endif
            @endforeach
        </ol>
        <div class="carousel-inner ImagesSlider">
            @foreach($sliders as $item)
                @if($loop->first)
                    <div class="carousel-item active">
                        <img src="{{ asset( $item->image_path ) }}" class="d-block w-100" alt="...">
                    </div>
                @else
                    <div class="carousel-item">
                        <img src="{{ asset( $item->image_path ) }}" class="d-block w-100" alt="...">
                    </div>
                @endif
            @endforeach
        </div>
        {{--<a class="carousel-control-prev" href="#myCarousel" role="button" data-slide="prev">--}}
        {{--<span class="carousel-control-prev-icon" aria-hidden="true"></span>--}}
        {{--<span class="sr-only">Previous</span>--}}
        {{--</a>--}}
        {{--<a class="carousel-control-next" href="#myCarousel" role="button" data-slide="next">--}}
        {{--<span class="carousel-control-next-icon" aria-hidden="true"></span>--}}
        {{--<span class="sr-only">Next</span>--}}
        {{--</a>--}}
    </div>
</div>