@extends('main')

@push('meta_tags')
    @includeIf('frontend.meta_tages')
@endpush


@section('content')
    @include('frontend.navigation')
    <div class="LoginRow">
        <div class="LoginPage">
            @includeIf('pages.error')
            @includeIf('frontend.LoginNavigation')
            <form action="{{ route('customer.validation') }}" method="POST">
                @csrf
                <input type="text" value="{{ old('username') }}" class="form-control InputsFormSpace"  placeholder="نام کاربری" name="username" />
                <input type="password" class="form-control InputsFormSpace" placeholder="گذر واژه" name="password" />
                <span id="register-button" class="btn btn-outline-success CustomBTN ClickedBTN" clicked="false">ورود به ناحیه کابری</span>
            </form>
        </div>
        @includeIf('frontend.BestSeller')
        @includeIf('frontend.AllProducts')
    </div>
    @includeIf('frontend.footer')
@endsection


@push('scripts')
    <script type="text/javascript" src="{{ asset('multislider/js/multislider.min.js') }}"></script>
    <script>
      $(document).ready(function () {
        $('#BestSeller').multislider({
          interval: 2000,
          slideAll: false
        });
        $('#Allproducts').multislider({
          interval: 2000,
          slideAll: false
        })
      })
    </script>
@endpush
