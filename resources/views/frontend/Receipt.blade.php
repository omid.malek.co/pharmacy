@extends('main')

@push('meta_tags')
    @includeIf('frontend.meta_tages')
@endpush

@push('styles')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@endpush

@section('content')
    <div class="FirstRow">
        @isset($factor->first()->payment_id)
            <input type="hidden" id="AddressReceiver"
                   value="{{ route('factor.set.address',[ 'payment_id'=>$factor->first()->payment_id ]) }}"/>
        @endisset
        @include('frontend.navigation')
        <div class="SliderRow">
            @include('frontend.slider')
            @include('frontend.RightSideSlider')
        </div>
    </div>
    <div class="ShowFactor row">
        @includeIf('frontend.CategoriesItem')
        <div id="Factorscol" class="Factorscol table-responsive">

            <div id="FactorsFrame" class="col-12">
                {!! $basketTable !!}
            </div>
            @includeIf('frontend.MessagesModel')
        </div>
        @includeIf('frontend.LastComments')
        @includeIf('frontend.AboutDart')
        @includeIf('frontend.footer')
    </div>
@endsection