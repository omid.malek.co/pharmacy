<div class="CategoriesRow" id="catlist">
    <div class="col-12 Cercereframe">
        <img src="{{ asset('images/cercere.png') }}" alt=""/>
    </div>
    <div class="CategoryItems">
        @foreach($categories as $item)
            {{--col-lg-2 col-md-4 col-sm-6 text-center--}}
            <div class="CategoriesFrame">
                <a href="{{ route('select.products',['categorie_id'=>$item->id])  }}">
                    <img src="{{ asset($item->image_path) }}" class="rounded" alt="cat">
                    <h3>{{ $item->title }}</h3>
                </a>
            </div>
        @endforeach
    </div>
</div>