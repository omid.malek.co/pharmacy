@extends('main')

@push('meta_tags')
    @includeIf('frontend.meta_tages')
@endpush


@section('content')
    <div class="FirstRow">
        @include('frontend.navigation')
        <div class="SliderRow">
            @include('frontend.slider')
            @include('frontend.RightSideSlider')
        </div>
    </div>
    <div class="bankAccountInfoRow">
        @includeIf('frontend.CategoriesItem')
        <div class="ContentFrame">
            {{--<a href="{{ route('customer.register') }}">عضویت در فروشگاه</a>--}}
            <div class="row">
{{--                @includeIf('frontend.CategoriesItem')--}}
                <div class="ContentFrame card">
                    <div class="card-header Title">{{ 'اطلاعات حساب' }}</div>
                    <div class="card-body Text">
                        <div class="Image">
                            <img src="{{ asset('images/card_bank.jpg') }}" class="img-fluid img-thumbnail" alt="">
                        </div>
                        <ul>
                            <li>
                                <span>شماره حساب</span>
                                <span>{{ $user->first()->number_account }}</span>
                            </li>
                            <li>
                                <span>شماره کارت</span>
                                <span>{{ $user->first()->number_cart }}</span>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        @includeIf('frontend.LastComments')
        @includeIf('frontend.AboutDart')
        @includeIf('frontend.footer')
    </div>

@endsection