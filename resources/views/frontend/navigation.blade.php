<div class="rowNav d-flex w-100">
    <div dir="ltr" class="smallScreen d-flex">
        <div class="leftCol d-flex ml-3">
            <div class="d-flex PhoneBox">
               <span>
                   <i class="fas fa-phone-alt"></i>
               </span>
                <span class="ml-2">056-32834858</span>
            </div>
        </div>
        <div class="rightCol d-flex mr-3">
           <span class="smSearch d-lg-none mr-3">
               <i class="fas fa-search"></i>
           </span>
            @if(session()->has('id'))
            <div class="smUserIcon mr-4 mr-lg-0">
                <a href="{{ route('customer.logOut') }}">
                    <i class="fas fa-user"></i>
                    <span class="Title">خروج</span>
                </a>
            </div>
            @else
                <div class="smUserIcon mr-4 mr-lg-0">
                    <a href="{{ route('customer.login') }}">
                        <i class="fas fa-user"></i>
                        <span class="Title">حساب کاربری</span>
                    </a>
                </div>
            @endif
            <span id="MenuActivator" class="smMenu">
                <i class="fas fa-align-justify"></i>
           </span>
        </div>
    </div>
</div>
<div class="rowSecond d-flex">
    <div dir="ltr" class="smallScreen d-flex">
        <div class="leftCol d-flex ml-3">
            <div class="d-flex LogoBox">
                <span class="ml-2 ImageFrame">
                    <img src="{{ asset('images/drMalekLogo.png') }}"/>
                </span>
            </div>
        </div>
        <div class="rightCol d-flex mr-3">
            <div class="searchLgBox">
                <i class="fas fa-search Icon"></i>
                <input type="text" class="form-control InputStyle"/>
            </div>
            <div class="smBasket d-flex">
                <span class="basketTXT pr-2">سبد خرید</span>
                <i class="fas fa-shopping-basket"></i>
            </div>
        </div>
    </div>
</div>
<div id="MenuSection" class="rowThird">
    <div class="closeColumn">
        <span id="closeSmMenu" class="closeFrame pl-3 pt-3">
            <i class="fas fa-times"></i>
        </span>
    </div>
    <div class="logoInMenu">
        <span class="ImageFrame mt-4">
            <img src="{{ asset('images/drMalekLogo_2.png') }}"/>
        </span>
    </div>
    <ul class="MenuItems">
        @foreach($categories as $item)
            <li>
                <a href="{{ route('select.products',['categorie_id'=>$item->id])  }}">
                    <span class="IconItem"><i class="fas fa-angle-left"></i></span>
                    <span class="TxtItem">{{ $item->title }}</span>
                </a>
            </li>
        @endforeach
    </ul>
    <ul class="OtherItems">
        <li>
            <a href="{{ route('about.us') }}">
                <span class="TxtItem">درباره ما</span>
            </a>
        </li>
        <li class="d-none">
            <a href="{{ route('articles.index') }}">
                <span class="TxtItem">دانستنی ها</span>
            </a>
        </li>
        <li>
            <a href="{{ route('contact.us') }}">
                <span class="TxtItem">تماس با ما</span>
            </a>
        </li>
    </ul>
</div>
{{--<div class="NavigationRow">--}}
{{--<div id="MenuIcon" class="NavigationItems MenuIcon">--}}
{{--<i class="fas fa-bars"></i>--}}
{{--</div>--}}
{{--<div id="MenuIcon" class="NavigationItems DartLogo">--}}
{{--<img src="{{ asset('images/dart_logo_2.png') }}" alt="">--}}
{{--</div>--}}
{{--<div id="SearchActivator" class="NavigationItems SearchIcon">--}}
{{--<i class="fas fa-search"></i>--}}
{{--</div>--}}
{{--<form id="SearchForm" class="SearchForm DisplayMd">--}}
{{--<button class="Button" type="submit">--}}
{{--<i class="fas fa-search"></i>--}}
{{--</button>--}}
{{--<input class="InputValue" type="text">--}}
{{--</form>--}}
{{--@if(session()->exists('id'))--}}
{{--<div class="NavigationItems BasketIcon DisplaySm">--}}
{{--<a class="text-white" href="{{ route('factors.show') }}"><i class="fas fa-shopping-cart"></i></a>--}}
{{--</div>--}}
{{--@endif--}}
{{--<ul id="ListMenu" class="NavigationItems ListMenu">--}}
{{--<li>--}}
{{--<a href="{{ route('home.index') }}">صفحه اصلی</a>--}}
{{--</li>--}}
{{--<li>--}}
{{--<a href="{{ route('regulations.show') }}">قوانین</a>--}}
{{--</li>--}}
{{--<li>--}}
{{--<a href="{{ route('articles.index') }}">مقالات</a>--}}
{{--</li>--}}
{{--<li>--}}
{{--<a href="{{ route('account.show') }}">اطلاعات حساب</a>--}}
{{--</li>--}}
{{--@if(session()->has('id'))--}}
{{--<li>--}}
{{--<a href="#" data-toggle="modal" data-target="#CustomersAccount">حساب کاربری</a>--}}
{{--</li>--}}
{{--@else--}}
{{--<li>--}}
{{--<a href="{{ route('customer.login') }}">ورود/عضویت</a>--}}
{{--</li>--}}
{{--@endif--}}
{{--</ul>--}}
{{--@if(session()->exists('id'))--}}
{{--<div class="NavigationItems BasketIcon DisplayMd">--}}
{{--<a class="text-white" href="{{ route('factors.show') }}">--}}
{{--<i class="text-light fas fa-shopping-cart"></i>--}}
{{--</a>--}}
{{--</div>--}}
{{--@endif--}}
{{--</div>--}}