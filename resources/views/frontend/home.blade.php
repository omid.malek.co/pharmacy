@push('styles')
    <link rel="stylesheet" type="text/css" href="{{ asset('multislider/css/custom.css') }}" />
@endpush




@includeIf('frontend.BestSeller')

@includeIf('frontend.AllProducts')

{{--@includeIf('frontend.LastComments')--}}

{{--@includeIf('frontend.AboutDart')--}}

@includeIf('frontend.footer')


@push('scripts')
    <script type="text/javascript" src="{{ asset('multislider/js/multislider.min.js') }}"></script>
    <script>
      $(document).ready(function () {
        $('#BestSeller').multislider({
          interval: 2000,
          slideAll: false
        });
        $('#Allproducts').multislider({
          interval: 2000,
          slideAll: false
        })
      })
    </script>
@endpush
