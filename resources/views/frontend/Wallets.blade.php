@extends('main')

@push('meta_tags')
    @includeIf('frontend.meta_tages')
@endpush

@push('styles')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@endpush

@section('content')
    <div class="FirstRow">
        <input type="hidden" id="AddressReceiver" value=""/>
        @include('frontend.navigation')
        <div class="SliderRow">
            @include('frontend.slider')
            @include('frontend.RightSideSlider')
        </div>
    </div>
    <div class="ShowFactor row">
        @includeIf('frontend.CategoriesItem')
        @includeIf('frontend.chargeWallet')
        <input type="hidden" value="{{ route('factors.update') }}" id="UpdateNumberProduct"/>
        <input type="hidden" value="{{ csrf_token() }}" id="MyToken"/>
        <div id="Factorscol" class="Factorscol">
            @includeIf('pages.error')
            <div class="WalletAccount">
                <div class="AmountAccount">
                    <div class="Label">موجودی حساب شما</div>
                    <div class="Value">{{ $wallet->amount.' تومان ' }}</div>
                </div>
                <div data-toggle="modal" data-target="#runChargeWallet" class="chargeWallet btn btn-outline-info">شارژ
                    حساب
                </div>
                <a href="{{ route('gifts.index') }}" class="createGiftCart btn btn-outline-info">ایجاد کارت هدیه</a>
            </div>
            <div id="FactorsFrame" class="col-12 pr-0 pl-0 mt-4">
                @if($isEmpty)
                    {!! $payments !!}
                @else
                    <div dir="rtl" class="col-6 offset-3 table-responsive pr-0 pl-0">
                        <table class="table text-right">
                            <thead class="thead-light">
                            <tr>
                                <th>ردیف</th>
                                <th>مبلغ</th>
                                <th>تاریخ پرداخت</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($payments as $item)
                                <tr id="{{ 'Row'.$item->id }}">
                                    <td>{{ $row++ }}</td>
                                    <td>{{ $item->payment_amount }}</td>
                                    <td>{{ $item->persian_date }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        @if($payments->total()>$paginate)
                            <div class="FooterPagination">
                                <span class="PaginationLinks">{{ $payments->links() }}</span>
                            </div>
                        @endif
                    </div>
                @endif
            </div>
            @includeIf('frontend.MessagesModel')
        </div>
        @includeIf('frontend.LastComments')
        @includeIf('frontend.AboutDart')
        @includeIf('frontend.footer')
    </div>
@endsection