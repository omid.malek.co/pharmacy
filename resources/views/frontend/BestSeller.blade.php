<div class="RowBestSeller">
    <div class="Title">
        <div class="ShowAll">
            <a class="Link" href="{{ route('select.products') }}">مشاهده همه</a>
        </div>
        <div class="Label">پر فروش ترین ها</div>
    </div>
    <div class="BestSellerCol">
        {{--id="BestSeller"--}}
        <div class="BestSellerFrame" id="BestSeller">      <!-- Give wrapper ID to target with jQuery & CSS -->
            <div class="MS-content">
                @foreach($products as $item)
                    <div class="item SellerItems">
                        <a href="{{ route('products.show',$item) }}">
                            <img src="{{ asset($item->image_path) }}" class="rounded" alt="cat">
                            <h3 class="title-product">
                                {{ $item->title }}
                            </h3>
                            <p class="price">
                                {{ $item->amount.' تومان ' }}
                            </p>
                        </a>
                    </div>
                @endforeach
            </div>
            <div class="MS-controls">
                <button class="MS-left"><i class="fa fa-chevron-left" aria-hidden="true"></i></button>
                <button class="MS-right"><i class="fa fa-chevron-right" aria-hidden="true"></i></button>
            </div>
        </div>
    </div>

</div>