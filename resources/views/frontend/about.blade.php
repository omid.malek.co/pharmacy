@extends('main')


@push('styles')
    <link rel="stylesheet" type="text/css" href="{{ asset('multislider/css/custom.css') }}"/>
@endpush

@push('scripts')
    <script type="text/javascript" src="{{ asset('multislider/js/multislider.min.js') }}"></script>
    <script>
      $(document).ready(function () {
        $('#BestSeller').multislider({
          interval: 2000,
          slideAll: false
        })
        $('#Allproducts').multislider({
          interval: 2000,
          slideAll: false
        })
      })
    </script>
@endpush

@push('meta_tags')
    @includeIf('frontend.meta_tages')
@endpush


@section('content')
    @include('frontend.navigation')
    <div class="AboutRow">
        <div class="bannerBox">
            <img src="{{ asset('images/pharmacy_AboutUS_1.jpg') }}" alt="">
        </div>
        <div class="aboutContent mt-4">
            <div class="TitleBox">
                <div class="Title">فعالیت و خدمات ما</div>
                <div class="Icon">
                    <i class="fas fa-users"></i>
                </div>
            </div>
            <div class="description p-4">
                <p>
                    این وبسایت رسمی دکتر غزاله مالک می باشد که جهت اطلاع هر چه بهتر هم وطنان عزیز از محصولات داروخانه راه اندازی گردیده است.
                </p>
                <p>
                    از اهداف راه اندازی وبسایت  تسهیل دسترسی مصرف کنندگان عزیز به تمامی محصولات آرایشی، بهداشتی، مکمل ها و تجهیزات پزشکی  از طریق شبکه مبتنی بر وب می باشد.
                </p>
                <p>
                    ما قصد فروش دارو از طریق این وب سایت را نداریم.
                </p>
                <p>
                    قیمت، تاریخ انقضاء و موجودی تمامی محصولات ارائه شده در سایت را توسط همکاران ما، در تمامی ایام هفته بررسی و سامانه بروزرسانی می گردد.
                </p>
                <p>
                    با ایجاد دسته بندی محصولات بر آن شدیم تا کاربران عزیز در کمترین زمان ممکن به محصول مورد نظر خود دست یابند.
                </p>
                <p>
                    اطلاعات و توضیحات مطرح شده در مورد کالاهای موجود در وب سایت با اطلاعات شرکت سازنده تطابق دارد و این وب سایت هیچ دخل و تصرفی در این توصیه ها ندارد.
                </p>
                <p>
                    تمامی توصیه ها و مشاوره های درج شده در وب سایت توسط کادر متخص در این حوزه ثبت شده است.
                </p>
            </div>
        </div>
        <div class="LawsContent mt-4">
            <div class="TitleBox">
                <div class="Title">حقوق وب سایت</div>
                <div class="Icon">
                    <i class="fas fa-caret-left"></i>
                </div>
            </div>
            <div class="description p-4">
               <p>
                   کلیه حقوق مادی و معنوی این وب سایت متعلق به داروخانه دکتر غزاله مالک بوده است.
               </p>
            </div>
        </div>
        <div class="LawsContent mt-4">
            <div class="TitleBox">
                <div class="Title">قوانین و مقررات</div>
                <div class="Icon">
                    <i class="fas fa-gavel"></i>
                </div>
            </div>
            <div class="description p-4">
                <p>
                  تمامی محصولات عرضه شده در سایت دارای تاریخ مصرف معتبر و مجوز فروش از سازمان مربوطه می باشد.
                </p>
                <p>
                   تاریخ انقضاء محصول در قسمت مشخصات مندرج در صفحه جزئیات محصول قابل مشاهده می باشد.
                </p>
                <p>
                    به دستور وزارت بهداشت و سازمان غذا و دارو امکان مرجوع شدن محصولات فروخته شده وجود ندارد.
                </p>
                <p>
                    کلیه محصولات در هنگام ارسال مورد بررسی قرار می گیرد و در صورتی که محصول توسط همکاران ما فاقد کیفیت شناخته شود، سفارش کنسل و مبلغ مورد نظر به کاربر بازگردانده می شود.
                </p>
                <p>
                    این وب سایت هیچ مسئولیتی در قبال مصرف خودسرانه و نادرست محصولات توسط مشتری ندارد.
                </p>
                <p>
                    لازم به ذکر است استفاده از نام و برند داروخانه تبعات قانونی دارد و ما حق پیگرد قانونی خاطیان را برای خود محفوظ می دانیم.
                </p>
            </div>
        </div>
        <div class="LicenseContent mt-4">
            <div class="TitleBox">
                <div class="Title">مجوزها</div>
                <div class="Icon">
                    <i class="far fa-id-badge"></i>
                </div>
            </div>
            <div class="description p-4">
               <a target="_blank" href="{{ asset('images/license_1.jpg') }}"  class="FrameLicense">
                   <img src="{{ asset('images/license_1.jpg') }}" alt="">
               </a>
                <a  target="_blank" href="{{ asset('images/license_2.jpg') }}" class="FrameLicense">
                    <img src="{{ asset('images/license_2.jpg') }}" alt="">
                </a>
            </div>
        </div>
        {{--@includeIf('frontend.BestSeller')--}}
        {{--@includeIf('frontend.AllProducts')--}}
    </div>
    @includeIf('frontend.footer')
@endsection

