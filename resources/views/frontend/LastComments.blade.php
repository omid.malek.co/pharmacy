{{--id="commentcustomer"--}}
<div class="CommentsRow">
    <div class="mt-3 CommentsCol">
        <div id="myCarousel" class="carousel slide" data-ride="carousel">

            <!-- The slideshow -->
            <div class="carousel-inner">
                <div class="carousel-item active text-center">
                    <p>ورزش دارت را باید یکی از شاخه‌های ورزش و حتی آمادگی نظامی دانست که به نوعی آن را یک هنر می‌دانند:
                        <span>هنر پرتاب دارت.</span>
                    </p>
                    <div class="Seperator">
                        <img src="{{ asset('images/redSeprator.png') }}" alt="">
                    </div>
                    <p><span>آخرین ارسال های دانستنی</span></p>
                </div>
                <div class="carousel-item text-center">
                    <p>ورزش دارت را باید یکی از شاخه‌های ورزش و حتی آمادگی نظامی دانست که به نوعی آن را یک هنر می‌دانند:
                        <span>هنر پرتاب دارت.</span></p>
                    <div class="Seperator">
                        <img src="{{ asset('images/redSeprator.png') }}" alt="">
                    </div>
                    <p><span>آخرین ارسال های دانستنی</span></p>
                </div>
                <div class="carousel-item text-center">
                    <p>ورزش دارت را باید یکی از شاخه‌های ورزش و حتی آمادگی نظامی دانست که به نوعی آن را یک هنر می‌دانند:
                        <span>هنر پرتاب دارت.</span></p>
                    <div class="Seperator">
                        <img src="{{ asset('images/redSeprator.png') }}" alt="">
                    </div>
                    <p><span>آخرین ارسال های دانستنی</span></p>
                </div>
            </div>

            <!-- Left and right controls -->
            <a class="carousel-control-prev" href="#myCarousel" data-slide="prev">
                {{--<span class="carousel-control-prev-icon"></span>--}}
                <i class="fas fa-angle-left"></i>
            </a>
            <a class="carousel-control-next" href="#myCarousel" data-slide="next">
                {{--<span class="carousel-control-next-icon"></span>--}}
                <i class="fas fa-angle-right"></i>
            </a>
        </div>

    </div>
</div>