<div class="modal" tabindex="-1" id="MessagePopUp" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div id="PopUpMessageBox" class="modal-body"></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">بستن</button>
            </div>
        </div>
    </div>
</div>