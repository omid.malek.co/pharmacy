<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Upload file</title>


    <link rel="stylesheet" type="text/css" href="{{ asset('documentsList/editDocumentFile.css') }}" />
</head>
<body>
<section>
    <form id="uploadDocument" action="{{ route('documents.update.file') }}"
          autocomplete="off"
          method="post"
          enctype="multipart/form-data">
        {{ csrf_field() }}
        <p>Upload File</p>
        <input type="hidden" name="document_id" value="{{  $document_id }}" />
        <div>
            <input type="file" name="file" id="upload" style="display:none" />
            <label for="upload">Select File</label>
        </div>
        <button>
            <span>&#8682; Upload </span>
            <span class="uploading">Uploading ...</span>
        </button>
        <button class="cancle">Cancle Upload</button>
        <div class="pr">
            <strong>
                <h4 class="ex">PDF</h4>
                <h5 class="size">2.5kb</h5>
            </strong>
            <progress min="0" max="100" value="0"></progress>
            <span class="progress-indicator"></span>
        </div>
    </form>
    <div class="">
        <img src="{{ asset('images/'.$file_name) }}" />
    </div>
</section>

{{-- Scripts --}}

<script src="{{ asset('documentsList/editDocumentFile.js') }}" type="text/javascript"></script>
</body>
</html>
