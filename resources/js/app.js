/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap')

// window.Vue = require('vue')

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i);
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default));

// Vue.component('example-component', require('./components/ExampleComponent.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

// const app = new Vue({
//     el: '#app',
// });

function ItemsActivator () {
  var page = $('#AboutNavbar').attr('innerPage')
  $(page).addClass('active')
}

$(document).ready(function () {
  //carousel slider

  // $('.carousel').carousel({
  //     interval: 2000,
  //     ride:true
  // })

  $('div.alert').delay(5000).slideUp(1000)

  $('#MenuIcon').click(function () {
    $('#ListMenu').slideToggle(1000)
  })

  $('#SearchActivator').click(function () {
    $('#SearchForm').slideToggle(200)
  })

  ItemsActivator()

  $('.ClickedBTN').click(function () {
    var clicked = $(this).attr('clicked')

    if (clicked == 'false') {
      $(this).attr('clicked', 'true')
      $(this).parent().submit()
    }

  })

  $('body').delegate('#removeFromBasket', 'click', function () {
    var path = $(this).attr('path')
    var clicked = $(this).attr('clicked')
    if (clicked == 'false') {
      $(this).attr('clicked', 'true')
      $.get(path, function (data, status) {
        selected_record = data.row
        var parent_id = data.parent_id
        var table_id = data.table_id
        var basket_content = data.basket_content
        $(table_id).remove()
        $(parent_id).append(basket_content)
      }, 'json').fail(function (data, status) {
        console.log('You have an error')
      })
    }
  })

  $('body').delegate('#productNumber', 'change', function () {

    var obj = $(this)
    var product_number = obj.val()
    var _token = $('#MyToken').val()
    var path = $('#UpdateNumberProduct').val()
    var product_id = $('#Product').val()
    var id = obj.attr('code')

    $.post(path, {
      '_token': _token,
      'count': product_number,
      'id': id,
      'product_id': product_id
    }, function (data, status) {

      if (data.operation == 0) {

        $('#PopUpMessageBox').html(data.MessagesModel)
        $('#MessagePopUp').modal('show')
        $(obj).val(data.numberExistProduct)
      }
    }, 'json').fail(function (data, status) {
      console.log('You have an error')
      console.log(status)
    })

  })

  $('body').delegate('[data-target="#MessagePopUp"]', 'click', function () {
    $('#MessagePopUp').on('shown.bs.modal', function () {
      $('#myInput').trigger('focus')
    })
  })

  $('body').delegate('[data-target="#CustomersAccount"]', 'click', function () {
    $('#CustomersAccount').on('shown.bs.modal', function () {
      $('#myInput').trigger('focus')
    })

  })

  $('#AddToBasket').click(function () {
    var path = $('#basketPath').val()
    $.get(path, function (data, status) {
      console.log(data.MessagesModel)
      $('#PopUpMessageBox').html(data.MessagesModel)
      $('#MessagePopUp').modal('show')
    }, 'json').fail(function () {
      console.log('You have an error')
    })

  })

  $('#SetAddressReceiver').click(function () {
    var clicked = $(this).attr('clicked')
    if (clicked == 'false') {
      $(this).attr('clicked', 'true')
      window.location.href = $('#AddressReceiver').val()
    }
  })

  $('#discountApply').click(function () {
    var obj = $(this)
    var CSRF_TOKEN = document.querySelector('meta[name="csrf-token"]').getAttribute('content')
    var path = $('#path').val()
    var discount_value = $('#discountTxt').val()
    var payment_id = $('#payment_id').val()
    $.post(path, {
      '_token': CSRF_TOKEN,
      'discount_value': discount_value,
      'payment_id': payment_id
    }, function (data, status) {

      if (data.operation == 0) {
        $('#PopUpMessageBox').html(data.MessagesModel)
        $('#MessagePopUp').modal('show')
      } else {
        $('#PopUpMessageBox').html(data.MessagesModel)
        $('#MessagePopUp').modal('show')
        $('#totalSum').text(data.totalSum)
      }
    }, 'json').fail(function (data, status) {
      console.log('You have an error')
      // console.log(status)
    })
  })

  $('#giftsApply').click(function () {
    var obj = $(this)
    var CSRF_TOKEN = document.querySelector('meta[name="csrf-token"]').getAttribute('content')
    var path = $('#SetGiftpath').val()
    var gift_text = $('#giftText').val()
    var payment_id = $('#payment_id').val()
    $.post(path, {
      '_token': CSRF_TOKEN,
      'gift_text': gift_text,
      'payment_id': payment_id
    }, function (data, status) {

      if (data.operation == 0) {
        $('#PopUpMessageBox').html(data.MessagesModel)
        $('#MessagePopUp').modal('show')
      } else {
        $('#PopUpMessageBox').html(data.MessagesModel)
        $('#MessagePopUp').modal('show')
        $('#totalSum').text(data.totalSum)
      }
    }, 'json').fail(function (data, status) {
      console.log('You have an error')
      // console.log(status)
    })
  })

  $('#UpdateReceiverInfo').click(function () {
    var This = $(this)
    var clicked = This.attr('clicked')
    if (clicked == 'false') {
      This.attr('clicked', 'true')
      $('#ReceiverInformertion').submit()
    }
  })
  $('#applyChargeWallet').click(function () {
    $('#chargeWalletForm').submit()
  })
  $('#createCode').click(function () {
    var path = $('#giftsCreator').val()
    $.get(path, function (data, status) {
      var code = data.code
      $('#giftCodeValue').val(code)
      $('input[name="text"]').val(code)
    }, 'json').fail(function () {
      console.log('You have an error')
    })
  })
  $('#CreateGiftCard').click(function () {
    var object = $(this)
    var clicked = object.attr('clicked')
    if (clicked == 'false') {
      object.attr('clicked', 'true')
      $('#GiftCardForm').submit()
    }
  })
  $('#RemoveGiftLink').click(function () {
    var object = $(this)
    var clicked = object.attr('clicked')
    if (clicked == 'false') {
      object.attr('clicked', 'true')
      $('#GiftDestroy').submit()
    }
  })
})

/**********************************************/

$(document).ready(function () {
  $('#MenuActivator').click(function () {
    $('#MenuSection').css({
      'display': 'block',
      'right': '0'
    })
  })
  $('#closeSmMenu').click(function(){
    $('#MenuSection').css({
      'display': 'none',
      'right': '0'
    })
  })
})

