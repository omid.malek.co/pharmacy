/* eslint-disable no-undef */
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */
require('./bootstrap')

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

function LinkActivator () {
  var page = $('#NavigationColumn').attr('page')
  $(page).addClass('ActivePage')
}

function ItemsActivator () {
  var page = $('#ListGroup').attr('item')
  $(page).addClass('active')
}

function CollectionCombo (path, categorie_id, _token) {
  $.ajax({
    url: path,
    type: 'POST',
    data: {_token: _token, categorie_id: categorie_id},
    success: function (data) {
      console.log(data.data)
      if (data.error == 0) {
        $('#CollectionCombo select').remove('')
        $('#CollectionCombo').append(data.data)
      } else {
        // window.alert(data.error);
      }
    },
    error: function () {
      console.log('IsErorr')
    }
  })
}

$(document).ready(function () {
  $('div.alert').delay(3000).slideUp(1000)
  LinkActivator()
  ItemsActivator()

  var path = $('[name="path"]').val()
  var categorie_id = $('#UsersCategorie').val()
  var _token = $('input[name="_token"]').val()

  CollectionCombo(path, categorie_id, _token)
  $('#ItemsIcon').click(function () {
    $('#NavigationRow').slideToggle()
  })

  $('#CategoryImages').change(function () {
    var used_in = $(this).val()
    if (used_in == 'categories') {
      $('#CollectionCombo select').slideUp()
    } else {
      $('#CollectionCombo select').slideDown()
    }
  })
})

$(document).ready(function () {
  $('body').delegate('#UsersCategorie', 'change', function () {
    var path = $('[name="path"]').val()
    var categorie_id = $(this).val()
    var _token = $('input[name="_token"]').val()
    CollectionCombo(path, categorie_id, _token)
  })
  $('body').delegate('#CategorieImages', 'change', function () {
    // var path=$('#Path').val();
    var categorie_id = $(this).val()
    var action = $(this).parent('form').attr('action')
    var arr = action.split('/')
    // arr[4] is used variable value from address url
    var urlArr = Array(arr[2], arr[3], arr[4], categorie_id)
    var url = 'http://' + urlArr.join('/')
    /* window.alert(url); */
    window.location = url
  })

})

/** *****************RemoveUserModal********************/

$(document).ready(function () {
  $('body').delegate('[data-target="#RemoveModal"]', 'click', function () {
    var href = $(this).attr('path')
    $('#ConfirmRemove').attr('href', href)
    $('#RemoveModal').on('shown.bs.modal', function () {
      $('#myInput').trigger('focus')
    })
  })

  $('body').delegate('[data-target="#ProjectsPopUpModel"]', 'click', function () {
    var href = $(this).attr('path')

    $('#ConfirmRemove').attr('href', href)
    $('#RemoveModal').on('shown.bs.modal', function () {
      $('#myInput').trigger('focus')
    })
  })

  $('#ConfirmRemove').click(function () {

    var obj = $(this)

    var path = obj.attr('path')

    var clicked = obj.attr('clicked')

    if (clicked == 'false') {

      obj.attr('clicked', 'true')
      $('#ConfirmRemove').on('shown.bs.modal', function () {
        $('#myInput').trigger('hide')
      })
      // $.get(path, function (data, status) {
      //
      //   // selected_record = data.row
      //   // $(selected_record).remove()
      //
      // }, 'json').fail(function (data, status) {
      //
      //   console.log('You have an error')
      // })

    }

  })

})

/***************register product operation**************/

$(document).ready(function () {
  $('#registerProduct').click(function () {
    var obj = $(this)
    var form = obj.parent().parent()
    var clicked = $(this).attr('clicked')
    if (clicked == 'false') {
      $(this).attr('clicked', 'true')
      form.submit()
    }
  })
  $('.FormActivator').click(function () {
    var obj = $(this)
    var formsName = obj.attr('formsName')
    var clicked = $(this).attr('clicked')
    if (clicked == 'false') {
      $(this).attr('clicked', 'true')
      $(formsName).submit()
    }
  })
})