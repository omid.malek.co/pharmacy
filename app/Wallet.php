<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Wallet extends Model
{
    protected $fillable = [
        'title',
        'amount',
        'type_id',
        'customer_id',
        'active',
        'created_at',
        'updated-at',
    ];

    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }
    public function gifts(){
        return $this->hasMany(Gift::class);
    }
    public function type(){
        return $this->hasOne(Type::class);
    }

}
