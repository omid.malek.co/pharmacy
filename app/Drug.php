<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Drug extends Model
{
    //--
    protected $fillable = [
        'name',
        'shape',
        'count',
        'scale_id',
        'description',
        'active',
        'updated_at',
        'created_at'
    ];
    //--
    public function scale(){
        return $this->belongsTo('App\Scale');
    }
}
