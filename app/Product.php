<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable=[
        'title','description','sales','amount','active','number','categorie_id'
    ];

    public function categorie(){
        return $this->belongsTo('App\Categorie');
    }
    public function images(){
        return $this->hasMany(Image::class);
    }

    public function factors(){
        return $this->hasMany('App\Factor');
    }
    public function options(){
        return $this->hasMany('App\Option');
    }
}
