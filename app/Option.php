<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Option extends Model
{
    protected $fillable = [
        'categorie_id',
        'product_id',
        'title',
        'value',
        'active'
    ];

    public function product()
    {
        return $this->belongsTo(Product::class);
    }
    public function categorie()
    {
        return $this->belongsTo(Categorie::class);
    }
}
