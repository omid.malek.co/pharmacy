<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    protected $fillable=[
        'src','record_id','used_in','active'
    ];

    public function product(){
        return $this->belongsTo('App\Product');
    }
}
