<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{
    //--
    protected $fillable = [
        'title',
        'keyword',
        'active',
        'created_at',
        'updated_at'
    ];


    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
