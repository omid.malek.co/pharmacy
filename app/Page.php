<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    //--
    protected $fillable = [
        'title',
        'address',
        'base_url',
        'is_base_page',
        'permission_id',
        'permission_title',
        'active',
        'created_at',
        'updated_at'
    ];

    public function items(){
        return $this->hasMany(Item::class);
    }
}
