<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    //--
    protected $fillable = [
        'personnel_id',
        'user_id',
        'page_id',
        'page_title',
        'route_name',
        'address',
        'base_url',
        'is_base_page',
        'child_id',
        'parent_id',
        'permission_id',
        'permission_title',
        'active',
        'created_at',
        'updated_at'
    ];

    public function page()
    {
        return $this->belongsTo(Page::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
