<?php

namespace App\Providers\Services;

use App\Image;
use App\Services\Collections\File;
use Illuminate\Support\ServiceProvider;

class FileProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(File::class,function ($app){
            return new File();
        });
    }
}
