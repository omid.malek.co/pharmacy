<?php

namespace App\Providers\Services;

use App\Services\Collections\GlobalServices;
use Illuminate\Support\ServiceProvider;

class GlobalFunctionsProviders extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(GlobalServices::class, function ($app) {
            return new GlobalServices();
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
