<?php

namespace App\Providers\Services;

use Illuminate\Support\ServiceProvider;
use App\Services\EntriesServices;

class EntriesServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //--
        $this->app->bind(EntriesServices::class,function($app){
            return new EntriesServices();
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
