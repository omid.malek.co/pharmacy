<?php

namespace App\Providers\Services;

use Illuminate\Support\ServiceProvider;

use App\Services\Collections\Token;

class TokenProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(Token::class,function($app){
            return new Token();
        });
    }
}
