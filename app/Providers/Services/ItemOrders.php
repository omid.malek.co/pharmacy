<?php

namespace App\Providers\Services;

use Illuminate\Support\ServiceProvider;
use App\Services\ItemsServices;

class ItemOrders extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //--
        $this->app->bind(ItemsServices::class,function($app){
            return new ItemsServices();
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
