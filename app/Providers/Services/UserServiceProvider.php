<?php

namespace App\Providers\Services;

use App\Services\UserServices;
use Illuminate\Support\ServiceProvider;
use function foo\func;

class UserServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //--
        $this->app->bind(UserServices::class,function ($app){
            return new UserServices();
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
