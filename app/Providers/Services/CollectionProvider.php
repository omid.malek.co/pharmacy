<?php

namespace App\Providers\Services;

use Illuminate\Support\ServiceProvider;
use App\Services\Collections\Collection;


class CollectionProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(Collection::class,function($app){
            return new Collection();
        });
    }}
