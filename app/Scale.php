<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Scale extends Model
{
    //--
    protected $fillable = [
        'title','active','updated_at','created_at'
    ];
    //--
    public function drugs(){
        return $this->hasMany('App\Drug');
    }
}
