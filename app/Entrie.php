<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Entrie extends Model
{
    //--
    protected $fillable = [
        'user_id',
        'user_fullname',
        'entry_date',
        'entry_time',
        'departure_date',
        'departure_time',
        'distance',
        'confirm',
        'confirm_by',
        'created_at',
        'updated_at',
        'active'
    ];
    public function user(){
        return $this->belongsTo(User::class);
    }
}
