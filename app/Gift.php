<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gift extends Model
{
    protected $fillable = [
        'text',
        'amount',
        'created_by',
        'used_by',
        'wallet_id',
        'condition_id',
        'start_date',
        'expire_date',
        'active'
    ];

    public function wallet(){
        return $this->belongsTo(Wallet::class);
    }
    public function condition(){
        return $this->belongsTo(Condition::class);
    }
}
