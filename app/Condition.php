<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Condition extends Model
{
    protected $fillable = [
        'title',
        'key_word',
        'active',
        'created_at',
        'updated_at'
    ];

    public function payments(){
        return $this->hasMany(Payment::class);
    }
    public function gift(){
        return $this->hasOne(Gift::class);
    }
}
