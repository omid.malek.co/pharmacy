<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Services\Collections\Collection;
use Validator;
use Lang;
use App\Image;
use App\Categorie;
use App\Product;
use App\User;
use App\Customer;
use App\Post;

class CustomersController extends Controller
{

    private $collection;


    public function __construct(Collection $collection)
    {
        $this->collection = $collection;
    }

    public function loginPage()
    {
        $images = Image::all();
        $sliders = $images->where('used_in', '=', 'sliders');
//        $user = User::first()->get(['fullname', 'phone', 'email', 'address', 'number_cart', 'number_account']);
        $user=$this->collection->ManagerInformation();
        $categories = Categorie::select(['id', 'title', 'active'])->where('active', 1)->get();
        $categories = $this->collection->AddImage($categories, 'categories', 'images');
        $products = Product::select(['title', 'amount', 'id'])->where('active', 1)->orderBy('sales', 'DESC')->get();
        $products = $this->AddImage($products, 'products', 'images');
        $AllProducts = Product::select(['title', 'amount', 'id'])->where('active', 1)->get();
        $rightSlider=Post::where('published',1)->where('used_in','right.slider')->first();
        $slideNumber = 0;
        return view('frontend.loginPage', [
            'sliders' => $sliders,
            'user' => $user,
            'products' => $products,
            'AllProducts' => $AllProducts,
            'categories' => $categories,
            'innerPage' => '#Login',
            'rightSlider'=>$rightSlider
        ]);
    }

    public function registerPage()
    {
        $images = Image::all();
        $sliders = $images->where('used_in', '=', 'sliders');
//        $user = User::first()->get(['fullname', 'phone', 'email', 'address', 'number_cart', 'number_account']);
        $user=$this->collection->ManagerInformation();
        $categories = Categorie::select(['id', 'title', 'active'])->where('active', 1)->get();
        $categories = $this->collection->AddImage($categories, 'categories', 'images');
        $products = Product::select(['title', 'amount', 'id'])->where('active', 1)->orderBy('sales', 'DESC')->get();
        $products = $this->AddImage($products, 'products', 'images');
        $AllProducts = Product::select(['title', 'amount', 'id'])->where('active', 1)->get();
        $rightSlider=Post::where('published',1)->where('used_in','right.slider')->first();
        return view('frontend.registerPage', [
            'sliders' => $sliders,
            'products' => $products,
            'AllProducts' => $AllProducts,
            'user' => $user,
            'categories' => $categories,
            'innerPage' => '#Register',
            'rightSlider'=>$rightSlider
        ]);
    }


    public function AddImage($arr, $used_in, $directory)
    {
        for ($i = 0; $i < count($arr); $i++) {
            $image = Image::select(['src', 'record_id', 'used_in'])->where('used_in', 'LIKE', '%' . $used_in . '%')
                ->where('record_id', $arr[$i]->id)
                ->where('active', 1)->first();
            $path = $directory . '/' . $image->src;
            array_add($arr[$i], 'image_path', $path);
        }

        return $arr;

    }


    public function storeCustomer(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|unique:customers,email',
            'phone' => 'required|numeric|unique:customers,phone',
            'password' => 'required',
            'password_confirmation' => 'required|same:password',
        ]);

        if ($validator->fails()) {
            session()->flash('class', 'alert-danger');
            return redirect()->back()
                ->withErrors($validator->errors())->withInput(['username']);
        } else {
            $password = md5($request->password);
            $data = [
                'password' => $password,
                'email' => $request->email,
                'phone' => $request->phone
            ];

            Customer::create($data);

            return redirect()->route('customer.login');

        }
    }


    public function validation(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'username' => 'required|exists:customers,email',
            'password' => 'required',
        ]);

        if ($validator->fails()) {
            session()->flash('class', 'alert-danger');
            return redirect()->route('customer.login')
                ->withErrors($validator->errors())->withInput(['username']);
        } else {
            $password = md5($request->password);

            $customer = Customer::where('email', '=', $request->username)
                ->where('password', '=', $password)->where('active', '=', 1);

            if (count($customer->get()) > 0) {
                 $token = $this->collection->createToken(60);
                    $customer->update(['token' => $token]);
                    session(['id' => $token]);
                    return redirect()->route('home.index');
            } else {

                session()->flash('class', 'alert-danger');
                return redirect()->route('customer.login')
                    ->withErrors($validator->errors())->withInput(['username']);
//                return response()->json([], 204);
            }
        }
    }

    public function logOut()
    {
        $customer=Customer::where('token',session()->forget('id'));
        $this->collection->ExitUser($customer);
        return redirect()->route('home.index');
    }

}
