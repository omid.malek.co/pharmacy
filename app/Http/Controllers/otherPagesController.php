<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\Collections\Collection;
use App\Services\Collections\File;
use Illuminate\Support\Facades\DB;
use App\Image;
use App\Categorie;
class otherPagesController extends Controller
{

    private $paginate=6;
    private $collection;
    private $file;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Collection $collection, File $file)
    {
        $this->collection = $collection;
        $this->file = $file;
    }

    public function about(){
        $user = $this->collection->ManagerInformation();
        $categories = Categorie::select(['id', 'title', 'active'])->where('active', 1)->get();
        return view('frontend.about', [
            'user' => $user,
            'categories' => $categories,
        ]);


    }

    public function contact(){
//        ContactUS_1
        $user = $this->collection->ManagerInformation();
        $categories = Categorie::select(['id', 'title', 'active'])->where('active', 1)->get();
        return view('frontend.contact', [
            'user' => $user,
            'categories' => $categories,
        ]);
    }
}
