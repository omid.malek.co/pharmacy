<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Contracts\View\View;
use App\Exports\ApplicantsExport;
use Maatwebsite\Excel\Excel;
use App\Services\Collections\Collection;
use App\Drug;
use Validator;
use Lang;

class ExcelController extends Controller
{

    private $MyServices;
    private $excel;

    public function __construct(Collection $MyServices,Excel $excel)
    {
        $this->MyServices = $MyServices;
        $this->excel=$excel;
    }

    //
    public function drugs()
    {
        $filename='drugs_'.time().'.xlsx';
        return $this->excel->download(new ApplicantsExport,$filename);
    }
}
