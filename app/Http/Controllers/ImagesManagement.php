<?php

namespace App\Http\Controllers;

use App\Services\Collections\File;
use App\Services\Collections\Collection;
use App\Image;
use App\Categorie;
use App\Product;
use Illuminate\Http\Request;
use Validator;
use Lang;

class ImagesManagement extends Controller
{

    private $collection;
    private $file;

    private $paginate = 10;


    public function __construct(Collection $collection, File $file)
    {
        $this->collection = $collection;
        $this->file = $file;
    }


    public function edit(Image $image)
    {

        $user = $this->collection->ManagerInformation();
        $sliders = $this->collection->GetSliderPictures();
        $sliders = $this->file->SetImagesPath($sliders->get(), 'sliders');
        $collection = Product::select(['id', 'title', 'categorie_id', 'number', 'description', 'sales', 'amount', 'active'])
            ->where('active', '=', 1)
            ->with(['categorie:title,id'])->orderBy('updated_at', 'DESC')->paginate($this->paginate);

        $categories = Categorie::select(['id', 'title', 'active'])->where('active', 1)->get();
        $categories = $this->collection->AddImage($categories, 'categories', 'images');


        $image = $this->file->SetImagesPath([0 => $image], 'products');


        return view('pages.admin.EditImage', [
            'sliders' => $sliders,
            'info' => $user->first(),
            'categories' => $categories,
            'collections' => $collection,
            'paginate' => $this->paginate,
            'operation' => "#InsertOperation",
            'content' => $image[0]
        ]);

    }

    public function update(Image $image, Request $request)
    {
        $validator = Validator::make([
            'image_id' => $image->id,
            'file' => $request->file
        ], [
            'image_id' => 'required|numeric|exists:images,id',
            'file' => 'required|image',
        ]);
        session()->flash('class', 'alert-danger');
        if ($validator->fails())
            return response()->json(['file' => 'not validate'], 200);


        $image_id = $image->id;
        $image = Image::where('id', $image_id)->where('active', 1);

        $destinationPath = $this->file->Getdirectory();

        $extension = $request->file('file')->getClientOriginalExtension();

        $fileName = 'articles_' . time() . rand(10, 100) . '.' . $extension;

        $request->file('file')->move($destinationPath, $fileName);

        $record = [ 'src' => $fileName ];
        $image->update($record);

        $file_path = $destinationPath . '/' . $fileName;

        $file_path = asset($file_path);

        return response()->json(['file' => $file_path], 200);

    }


}
