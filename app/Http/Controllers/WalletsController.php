<?php

namespace App\Http\Controllers;

use App\Factor;
use App\Image;
use App\Payment;
use App\Product;
use App\Services\Collections\File;
use App\User;
use App\Wallet;
use Illuminate\Http\Request;
use App\Services\Collections\Collection;
use App\Categorie;
use Validator;
use Lang;
use App\Type;
use App\Post;

class WalletsController extends Controller
{
    private $collection;
    private $file;
    private $type;
    private $used_in = 'products';
    private $paginate = 10;

    public function __construct(Collection $collection, File $file)
    {
        $this->collection = $collection;
        $this->file = $file;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function CreateOrFindPayment($customer_id, $payment_amount, $active_column = 1, $payment_status = 1)
    {
        $type = $this->collection->WalletType();
        $paymentObj = Payment::where('active', $active_column)
            ->where('customer_id', $customer_id)
            ->where('type_id', $type->id)
            ->where('payment_status', $payment_status);

        $data = $paymentObj->get();
        $reference_code = $this->collection->trackerCodeCreator(8, 'payments', 'reference_code');

        $record = [
            'customer_id' => $customer_id,
            'type_id' => $type->id,
            'payment_status' => $payment_status,
            'active' => $active_column,
            'payment_amount' => $payment_amount,
            'total_sum' => $payment_amount,
            'reference_code' => $reference_code
        ];
        if (count($data) > 0)
            return $data->first();

        return $payment = Payment::create($record);
    }

    public function index()
    {
        $customer_id = $this->collection->getCustomerId();
        $walletQuery = Wallet::where('customer_id', $customer_id)
            ->where('active', 1);
        $isWalletEmpty = $this->collection->IsArrayEmpty($walletQuery->take(1)->get());
        if ($isWalletEmpty)
            $wallet = $this->collection->CreateAccountInWallet();
        else
            $wallet = $walletQuery->first();

        $paymentQuery = Payment::where('customer_id', $customer_id)
            ->where('type_id', $wallet->type_id)->where('active', 1);

        $isPaymentEmpty = $this->collection->IsArrayEmpty($paymentQuery->take(1)->get());
        $row=1;
        if ($isPaymentEmpty)
            $payments = $this->collection->NoDataFound();
        else {
            $payments = $paymentQuery->paginate($this->paginate);
            $row = $payments->firstItem();
            $paymentCount = count($payments);
            for ($i = 0; $i < $paymentCount; $i++) {
                $persian_date = $this->collection->getJalaliDate($payments[$i]->updated_at);
                $payments[$i]->persian_date = $persian_date;
            }
        }
        $sliders = $this->collection->GetSliderPictures();
        $sliders = $this->file->SetImagesPath($sliders->get(), 'sliders');
        $user = $this->collection->ManagerInformation();


        $categories = Categorie::select(['id', 'title', 'active'])->where('active', 1)->get();
        $categories = $this->collection->AddImage($categories, 'categories', 'images');
        $rightSlider = Post::where('published', 1)->where('used_in', 'right.slider')->first();

        return view('frontend.Wallets', [
            'sliders' => $sliders,
            'user' => $user,
            'payments' => $payments,
            'wallet' => $wallet,
            'categories' => $categories,
            'row' => $row,
            'rightSlider' => $rightSlider,
            'isEmpty' => $isPaymentEmpty,
            'paginate' => $this->paginate
        ]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make([
            'amount' => $request->amount,
        ], [
            'amount' => array('required', 'numeric'),
        ]);
        session()->flash('class', 'alert-danger');
        if ($validator->fails())
            return redirect()->back()
                ->withErrors($validator->errors());

        $customer_id = $this->collection->getCustomerId();
        if (!$customer_id)
            return redirect()->route('customer.logOut');

        $payment_amount = $request->amount;
        $payment = $this->CreateOrFindPayment($customer_id, $payment_amount);
        return redirect()->route('payment.zarinpal', ['payment_id' => $payment->id]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
