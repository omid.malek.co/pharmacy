<?php

namespace App\Http\Controllers;

use App\Permission;
use Illuminate\Http\Request;
use App\Services\Collections\Collection;
use App\Services\Collections\File;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Http\Middleware\isValideCustomer;
use App\Document;
use App\Categorie;
use App\Image;
use App\User;
use Carbon\Carbon;
use Validator;
use Lang;

class documentsController extends Controller
{
    private $paginate = 16;
    protected $collection;
    protected $file;
    private $table = 'documents';

//--  /usr/local/bin/ea-php72 /home/coinbite/source/artisan schedule:run >> /dev/null 2>&1
    public function __construct(Collection $collection, File $file)
    {
        $this->middleware([IsValideCustomer::class]);
        $this->collection = $collection;
        $this->file = $file;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\View\View|void
     */
    public function viewPage($data, $clause_permission_id)
    {
        if (1) {
            return view('pages.admin.documentsList', $data);
        } else {

        }
    }

    public function document_list()
    {
        $user_id = session()->get('user_id');

    }

    public function index($user_id)
    {
        //--
        $validator = Validator::make([
            'user_id' => $user_id
        ], [
            'user_id' => 'required|exists:users,id',
        ]);
        if ($validator->fails())
            return abort(404);
        //--
        $admin_permission_id = $this->collection->AdminPermissionId();
        $admin = User::find(session()->get('id'));
        $active_value = 1;
        $used = 'documents';
        $testrecord = Document::where('active', '=', $active_value)
            ->where('user_id', '=', $user_id)
            ->take(1)->get(['id']);
        $images = Document::where('active', '=', $active_value)
            ->where('user_id', '=', $user_id)
            ->orderBy('created_at', 'DESC')
            ->with(['user:id,fullname'])
            ->paginate($this->paginate);
        $ImagesCategorie = $this->file->getDirectory();
        //--
        for ($i = 0; $i < count($images); $i++) {
            $path = $this->file->getDirectory();
            array_add($images[$i], 'image_path', $path . '/' . $images[$i]->src);
            array_add($images[$i], 'icon_path', $path . '/' . $images[$i]->icon);
        }

        $isEmpty = $this->collection->IsArrayEmpty($testrecord);
        $myData = [
            'info' => $admin,
            'page' => '#Images',
            'operation' => '#List',
            'images' => $images,
            'paginate' => $this->paginate,
            'isEmpty' => $isEmpty,
            'user_id'=>$user_id,
            'ImagesCategorie' => $ImagesCategorie,
            'used' => $used
        ];
        return view('pages.admin.documentsList', $myData);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function showDocs()
    {
        //--
        $user_id = Auth::id();
        $validator = Validator::make([
            'user_id' => $user_id
        ], [
            'user_id' => 'required|exists:users,id',
        ]);
        if ($validator->fails())
            return abort(404);
        //--
        $admin_permission_id = $this->collection->AdminPermissionId();
        $admin = User::find(session()->get('id'));
        $active_value = 1;
        $used = 'documents';
        if ($admin_permission_id == $admin->permission_id) {
            $testrecord = Document::where('active', '=', $active_value)
                ->take(1)->get(['id']);
            $images = Document::where('active', '=', $active_value)
                ->paginate($this->paginate);
        } else {
            $testrecord = Document::where('active', '=', $active_value)
                ->where('user_id', '=', $user_id)
                ->take(1)->get(['id']);
            $images = Document::where('active', '=', $active_value)
                ->where('user_id', '=', $user_id)
                ->paginate($this->paginate);
        }

        $ImagesCategorie = $this->file->getDirectory();;

        //--
        for ($i = 0; $i < count($images); $i++) {
            $path = $this->file->getDirectory();
            array_add($images[$i], 'image_path', $path . '/' . $images[$i]->src);
            array_add($images[$i], 'icon_path', $path . '/' . $images[$i]->icon);
        }

        $isEmpty = $this->collection->IsArrayEmpty($testrecord);
        $myData = [
            'info' => $admin,
            'page' => '#Images',
            'operation' => '#List',
            'images' => $images,
            'user_id'=>$user_id,
            'paginate' => $this->paginate,
            'isEmpty' => $isEmpty,
            'ImagesCategorie' => $ImagesCategorie,
            'used' => $used
        ];
        return view('pages.admin.documentsList', $myData);
    }

    public function create()
    {
        $admin = User::find(session()->get('id'));

        $ImagesCategorie = $this->file->getDirectory();
        $categories = Categorie::where('active', '=', 1)->get();

        return view('pages.admin.InsertDocument', [
            'info' => $admin,
            'page' => '#Images',
            'operation' => '#Insert',
            'categoriesUser' => $categories,
            'paginate' => $this->paginate,
            'categoriesImages' => $ImagesCategorie,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function InsertToDocumentsTable($information)
    {
    }

    //--
    public function store(Request $request)
    {
        //--
        $validator = Validator::make($request->all(), [
            'title' => 'required',
        ]);
        if ($validator->fails()) {
            session()->flash('class', 'alert-danger');
            return back()->withErrors($validator->errors());
        } else {
            $user_id = Auth::id();
//            $image = $request->file('file');
            $file_name = 'documents_' . $user_id . '_' . time() . '.' . 'jpg';
            $destinationPath = 'images';
            $from_path = $destinationPath . '/' . 'noPhoto.jpg';
            $to_path = $destinationPath . '/' . $file_name;
            copy($from_path, $to_path);
//            $image->move($destinationPath, $input['imagename']);
            $timezon = date('Y-m-d H:i:s');
            $description = (empty($request->description) ? 'فاقد توضیحات' : $request->description);
            $data = [
                'title' => $request->title,
                'src' => $file_name,
                'user_id' => $user_id,
                'description' => $description,
                'keyword' => 'List',
                'active' => 1,
                'created_at' => $timezon,
                'updated_at' => $timezon
            ];
            session()->flash('class', 'alert-success');
            $document = Document::firstOrCreate($data);
            //-- return back()->withErrors(Lang::get('validation.successupdate'));
            return redirect()->route('documents.edit.file', ['document_id' => $document->id]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function edit($id)
    {
        $hasRecord = DB::table($this->table)->where('id', $id)->exists();
        if (!$hasRecord)
            abort(404);
        //--
        $admin = User::find(session()->get('id'));
        $ImagesCategorie = $this->file->getDirectory();
        $categories = Categorie::where('active', '=', 1)->get();
        $document = Document::where('id', $id)->first();
        return view('pages.admin.editDocumentInfo', [
            'info' => $admin,
            'document' => $document,
            'page' => '#Images',
            'operation' => '#Insert',
            'categoriesUser' => $categories,
            'paginate' => $this->paginate,
            'categoriesImages' => $ImagesCategorie,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make([
            'title' => $request->title,
            'id' => $id
        ], [
            'title' => 'required',
            'id' => 'required|numeric|exists:documents,id',
        ]);
        if ($validator->fails()) {
            session()->flash('class', 'alert-danger');
            return back()->withErrors($validator->errors());
        } else {
            $user_id = Auth::id();
            $document_id = $id;
            $timezon = date('Y-m-d H:i:s');
            $description = (empty($request->description) ? 'فاقد توضیحات' : $request->description);
            $data = [
                'title' => $request->title,
                'description' => $description,
                'updated_at' => $timezon
            ];

            $document = Document::where('id', $document_id)->update($data);
            session()->flash('class', 'alert-success');
            return back()->withErrors(Lang::get('validation.successupdate'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //--
        $timezon = date('Y-m-d H:i:s');
        $image = Document::where('id', '=', $id)->update([
            'active' => 0,
            'updated_at' => $timezon
        ]);
        return back();
    }

    public function downloadFile($document_id)
    {
        $table = 'documents';
        $doesNtExist = DB::table($table)
            ->where('active', '=', 1)
            ->where('id', '=', $document_id)->doesntExist();
        if ($doesNtExist)
            return abort(404);
        //-- download file
        $document = Document::where('id', '=', $document_id)->first();
        $documents_path = 'images/' . $document->src;
        header('Content-Type: application/octet-stream');
        header("Content-Transfer-Encoding: Binary");
        header("Content-disposition: attachment; filename=\"" . basename($documents_path) . "\"");
        readfile($documents_path);
    }

    public function editDocumentFile($document_id)
    {
        $validator = Validator::make([
            'document_id' => $document_id,
        ], [
            'document_id' => 'required|numeric|exists:documents,id',
        ]);
        if ($validator->fails())
            return abort(404);
        //--
        $admin = User::find(session()->get('id'));

        $ImagesCategorie = $this->file->getDirectory();
        $categories = Categorie::where('active', '=', 1)->get();
        $document = Document::where('active', 1)->where('id', '=', $document_id)->first();
        //-- pages.admin.editDocumentFile
        //-- temp
        return view('pages.admin.editDocumentFile', [
            'info' => $admin,
            'document_id' => $document_id,
            'file_name' => $document->src,
            'page' => '#Images',
            'operation' => '#Insert',
            'categoriesUser' => $categories,
            'paginate' => $this->paginate,
            'categoriesImages' => $ImagesCategorie,
        ]);
    }

    public function docsUploader(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'document_id' => 'required|numeric',
            'file_size' => 'required|numeric',
        ]);
        if ($validator->fails())
            return response()->json([
                'msg' => 'لطفا فیلد ها را به درستی پر نمایید',
                'operation' => 0
            ], 200);


        $user_id = Auth::id();
        $document_id = $request->document_id;
        $file_size = $request->file_size;
        $max_file_size = 50000000;
        if ($file_size > $max_file_size)
            return response()->json([
                'msg' => 'file must <50 mg',
                'operation' => 0
            ], 200);

        $image = Document::where('id', $document_id)->where('active', 1);
        //--
        $destinationPath = $this->file->Getdirectory();
        $docsInfo = $image->first();
        $old_file_name = $docsInfo->src;
        $this->file->RemoveImage($old_file_name);
        //--
        $extension = $request->file('file')->getClientOriginalExtension();

        $fileName = 'documents_' . $user_id . '_' . time() . '.' . $extension;
        move_uploaded_file($fileName, "images/" . $fileName);
        $request->file('file')->move($destinationPath, $fileName);

        $record = ['src' => $fileName];
        $image->update($record);
        $file_path = $destinationPath . '/' . $fileName;

        $file_path = asset($file_path);

        return response()->json([
            'msg' => 'upload has been success',
            'operation' => 1
        ], 200);
    }

}
