<?php

namespace App\Http\Controllers;

use App\Factor;
use App\Wallet;
use Illuminate\Http\Request;
use App\Payment;
use App\Customer;
use App\Product;
use SoapClient;
use nusoap_client;
//use App\Services\Collections\GlobalServices;
use App\Services\Collections\Collection;
use Illuminate\Session;

class ZarinPalController extends Controller
{
    // merchantId is from omid malek
//    private $MerchantID = '332b7539-b29c-4d64-89f2-2987ff1bfb87';
    // merchantId is from farhad yousefi
    private $MerchantID = 'e6e910bc-e0f9-467f-9503-ddc7d93dd146';
    private $CallbackURL = [];
    private $AmountApplicants = 20000;
    private $collection;

    public function __construct(Collection $collection)
    {
        $this->collection = $collection;
    }


    protected function CallbackURL($type_id)
    {
        $WalletsType = $this->collection->WalletType();
        $PurchaseType = $this->collection->purchaseType();

    }

    public function ProductRemaining($payment_id)
    {
        $factors = Factor::where('payment_id', $payment_id)->get();
        $isEmpty = $this->collection->IsArrayEmpty();
        if (!$isEmpty)
            foreach ($factors as $item) {
                $productObj = Product::where('id', $item->product_id);
                $product = $productObj->first();
                $number = $product->number - $item->sent_number;
                $productObj->update(['number' => $number]);
            }
    }

    public function ZarinPalPayment($payment_id)
    {
        /**************************************************/

        date_default_timezone_set("Iran");
        $MerchantID = $this->MerchantID;
        $Description = "پرداخت هزینه محصول دارت مارکت";
        $paymentObj = Payment::where('id', $payment_id);
        $payment = $paymentObj->first();
        $customer_id = $payment->customer_id;
        $customer = Customer::find($customer_id);
        // calculate cost for payment
        $payment_amount = $payment->payment_amount;
//        $discount_value = $payment->discount_value;
//        $arr_discount_value = explode('.', $discount_value);
//        $discount_value = $arr_discount_value[0];
//        $amountForpayment = (double)$payment_amount - (double)$discount_value;
        $amountForpayment = (double)$payment->total_sum;
        $reference = $payment->reference_code;
        $Authority = 0;
        $CallbackURL = route('verify.zarinpal', ['reference' => $reference]);
//        $CallbackURL = 'https://www.coinbit-exchange.com/api/verify_payment/' . $reference;

        $payment_request = [
            'MerchantID' => $MerchantID,
            'Amount' => $amountForpayment,
            'Description' => $Description,
            'Email' => $customer->email,
            'Mobile' => $customer->phone,
            'CallbackURL' => $CallbackURL,
        ];
        $client = new SoapClient('https://www.zarinpal.com/pg/services/WebGate/wsdl', ['encoding' => 'UTF-8']);
        $result = $client->PaymentRequest($payment_request);

//Redirect to URL You can do it also by creating a form
        if ($result->Status == 100) {
            Header('Location: https://www.zarinpal.com/pg/StartPay/' . $result->Authority);
//برای استفاده از زرین گیت باید ادرس به صورت زیر تغییر کند:
//Header('Location: https://www.zarinpal.com/pg/StartPay/'.$result->Authority.'/ZarinGate');
        } else {
            echo 'ERR: ' . $result->Status;
        }
    }


    public function ZarinPalVerify($reference)
    {
        // calculate cost for payment

        $reference_code = $reference;
        $paymentObj = Payment::where('reference_code', $reference_code);

        $payment = $paymentObj->first();

        $payment_amount = $payment->payment_amount;
//        $discount_value = $payment->discount_value;
//        $arr_discount_value = explode('.', $discount_value);
//        $discount_value = $arr_discount_value[0];
//        $amountForpayment = (double)$payment_amount - (double)$discount_value;
        $amountForpayment = $payment->total_sum;
//        $amountForpayment *= 10;
        /******************** payment verification code **************/
        $Authority = $_GET['Authority'];
        $data = array(
            'MerchantID' => $this->MerchantID,
            'Authority' => $Authority,
            'Amount' => $amountForpayment
        );
        $jsonData = json_encode($data);
        $ch = curl_init('https://www.zarinpal.com/pg/rest/WebGate/PaymentVerification.json');
        curl_setopt($ch, CURLOPT_USERAGENT, 'ZarinPal Rest Api v1');
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonData);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($jsonData)
        ));
        $result = curl_exec($ch);
        $err = curl_error($ch);
        curl_close($ch);
        $result = json_decode($result, true);
        /*******************/
        $description = 'پرداخت هزینه محصول دارت مارکت';
        $authority = $Authority;
        $refID = $result['RefID'];
        $status = $result['Status'];
        $WalletsType = $this->collection->WalletType();
        $PurchaseType = $this->collection->purchaseType();
        //
//        if ($this->collection->IsArrayEmpty($payment)) {
//            $dataPayment = [
//                'payment_message' => 'کاربر از سمت درگاه وارد نشده است',
//                'Authority' => $authority,
//                'payment_message_code' => $status,
//                'payment_status' => 2,
//                'RefID' => $refID
//            ];
//            $paymentObj->update($dataPayment);
//            return redirect()->route('receipt.show', ['reference' => $reference]);
//        }

        if ($err) {
//            echo "cURL Error #:" . $err;

            $paymentData = [
                'Authority' => $authority,
                'payment_message_code' => (isset($status) ? $status : -22),
                'RefID' => 0,
                'payment_message' => $this->ZarinPalStatusMessage($status),
                'payment_status' => 2,
                'active' => 1,
            ];

            $paymentObj->update($paymentData);
            if ($payment->type_id == $WalletsType->id)
                return redirect()->route('wallets.index');
            else if ($payment->type_id == $PurchaseType->id)
                return redirect()->route('receipt.show', ['reference' => $reference]);
            else
                return 'Callback not valide';
//        $record['status']=$this->PaymentStatusMessages('NOK');

        } else {
            if ($status == 100) {
//                echo 'Transation success. RefID:' . $result['RefID'];
                $paymentData = [
                    'Authority' => $authority,
                    'payment_message_code' => $status,
                    'RefID' => $refID,
                    'payment_message' => $this->ZarinPalStatusMessage($status),
                    'payment_status' => 3,
                    'active' => 1,
                ];
                $paymentObj->update($paymentData);
                if ($payment->type_id == $WalletsType->id) {
                    $customer_id = $payment->customer_id;
                    $walletObj = Wallet::where('customer_id', $customer_id)->where('active', 1);
                    $wallet = $walletObj->first();
                    $amount = (double)$wallet->amount + (double)$payment_amount;
                    $walletObj->update(['amount' => (double)$amount]);
                    return redirect()->route('wallets.index');
                } else if ($payment->type_id == $PurchaseType->id) {
                    $this->ProductRemaining($payment->id);
                    return redirect()->route('receipt.show', ['reference' => $reference]);
                } else
                    return 'Callback not valide';
//                $record['status']=$this->PaymentStatusMessages('OK');
            } else {
//                echo 'Transation failed. Status:' . $result['Status'];

                $paymentData = [
                    'Authority' => $authority,
                    'payment_message_code' => $status,
                    'RefID' => 0,
                    'payment_status' => 2,
                    'payment_message' => $this->ZarinPalStatusMessage($status),
                    'active' => 1,
                ];
                $paymentObj->update($paymentData);
                if ($payment->type_id == $WalletsType->id)
                    return redirect()->route('wallets.index');
                else if ($payment->type_id == $PurchaseType->id)
                    return redirect()->route('receipt.show', ['reference' => $reference]);
                else
                    return 'Callback not valide';
//                $record['status']=$this->PaymentStatusMessages('NOK');
            }
        }
        if ($payment->type_id == $WalletsType->id)
            return redirect()->route('wallets.index');
        else if ($payment->type_id == $PurchaseType->id)
            return redirect()->route('receipt.show', ['reference' => $reference]);
        else
            return 'Callback not valide';
    }


    public function ZarinPalStatusMessage($status)
    {

        $comment = '';

        switch ($status) {
            case '-1':
                $comment = 'اطلاعات ارسال شده ناقص است.';
                break;
            case '-2':
                $comment = 'و يا مرچنت كد پذيرنده صحيح نيست. IP';
                break;
            case '-3':
                $comment = 'با توجه به محدوديت هاي شاپرك امكان پرداخت با رقم درخواست شده ميسر نمي باشد.';
                break;
            case '-4':
                $comment = 'سطح تاييد پذيرنده پايين تر از سطح نقره اي است.';
                break;
            case '-11':
                $comment = 'درخواست مورد نظر يافت نشد.';
                break;
            case '-12':
                $comment = 'امكان ويرايش درخواست ميسر نمي باشد.';
                break;
            case '-21':
                $comment = 'هيچ نوع عمليات مالي براي اين تراكنش يافت نشد.';
                break;
            case '-22':
                $comment = 'تراكنش نا موفق ميباشد';
                break;
            case '-33':
                $comment = 'رقم تراكنش با رقم پرداخت شده مطابقت ندارد.';
                break;
            case '-34':
                $comment = 'سقف تقسيم تراكنش از لحاظ تعداد يا رقم عبور نموده است';
                break;
            case '-42':
                $comment = 'مدت زمان معتبر طول عمر شناسه پرداخت بايد بين 30 دقيه تا 45 روز مي باشد.';
                break;
            case '-54':
                $comment = 'درخواست مورد نظر آرشيو شده است.';
                break;
            case 100:
                $comment = 'عمليات با موفقيت انجام گرديده است.';
                break;
            case 101:
                $comment = 'عملیات پرداخت موفق بوده و قبلا PaymentVerification تراکنش انجام شده است.';
                break;
            default:
                $comment = 'کد ارسالی نامعتبر است.';
                break;
        }
        return $comment;
    }

}
