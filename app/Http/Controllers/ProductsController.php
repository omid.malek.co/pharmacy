<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\Collections\Collection;
use App\Services\Collections\File;
use Illuminate\Support\Facades\DB;
use App\Image;
use App\Categorie;
use App\Product;
use App\Option;
use App\User;
use App\Post;

class ProductsController extends Controller
{

    private $paginate=20;
    private $collection;
    private $file;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Collection $collection, File $file)
    {
        $this->collection = $collection;
        $this->file = $file;
    }
    public function AddImage($arr, $used_in, $directory)
    {
        for ($i = 0; $i < count($arr); $i++) {
            $image = Image::select(['src', 'record_id', 'used_in'])->where('used_in', $used_in)
                ->where('record_id', $arr[$i]->id)
                ->where('active', 1)->first();
            $path = $directory . '/' . $image->src;
            $path = asset($path);
            array_add($arr[$i], 'image_path', $path);
        }

        return $arr;

    }

    public function setOption($product){

        $NoDataFound=$this->collection->NoDataFound();
        $doesntExist=DB::table('options')
            ->where('product_id', $product->id)
            ->where('active', 1)
            ->doesntExist();
        if($doesntExist)
           return [
              'options'=>$NoDataFound,
              'doesntExist'=>$doesntExist
            ];

        $options=Option::where('product_id', $product->id)
            ->where('active', 1)->get();

        return [
            'options'=>$options,
            'doesntExist'=>$doesntExist
        ];
    }

    public function AddCategory($arr)
    {
        for ($i = 0; $i < count($arr); $i++) {
            $categorie = Categorie::select(['title'])
                ->where('id', $arr[$i]->categorie_id)->first();
            array_add($arr[$i], 'categorie_title', $categorie->title);
        }

        return $arr;
    }

    public function show(Product $product)
    {
        /**************public codes******************/
        $images = Image::all();
        $slidersQuey = $this->collection->GetSliderPictures();
        $sliders = $slidersQuey->get();
        $sliders = $this->file->SetImagesPath($sliders);
        $user = $this->collection->ManagerInformation();
        $categories = Categorie::select(['id', 'title', 'active'])->where('active', 1)->get();
        $products = Product::select(['title', 'amount', 'id'])->where('active', 1)->orderBy('sales', 'DESC')->get();

        $products = $this->AddImage($products, 'products', 'images');
        $AllProducts = Product::select(['title', 'amount', 'id'])->where('active', 1)->get();
        $rightSlider = Post::where('published', 1)->where('used_in', 'right.slider')->first();
        $slideNumber = 0;
        $options=$this->setOption($product);
        /*******custom code for details product*****/
        $selectedproduct = [
            0 => $product
        ];
        $selectedItem = $this->AddImage($selectedproduct, 'products', 'images');
        $selectedItem = $this->AddCategory($selectedItem);
        return view('frontend.DetailsProducts', [
            'sliders' => $sliders,
            'user' => $user,
            'options' =>$options,
            'categories' => $categories,
            'products' => $products,
            'AllProducts' => $AllProducts,
            'rightSlider' => $rightSlider,
            'slideNumber' => $slideNumber,
            'selectedItem' =>$selectedItem[0]
        ]);
    }
    public function getProducts($categorie_id=null,Collection $collection,File $file)
    {
        $products=$collection->selectProducts(null,$categorie_id);
        $records=$this->AddImage($products->paginate($this->paginate),'products','images');
        $sliders=$collection->GetSliderPictures();
        $sliders=$file->SetImagesPath($sliders->get(),'sliders');
        $user=$collection->ManagerInformation();

        $categories=Categorie::select(['id','title','active'])->where('active',1)->get();
        $categories=$collection->AddImage($categories,'categories','images');
        $rightSlider=Post::where('published',1)->where('used_in','right.slider')->first();
/*****************************************/
        $images = Image::all();
//        $sliders = $images->where('used_in', '=', 'sliders')->where('active',1);
        $slidersQuey = $this->collection->GetSliderPictures();
        $sliders = $slidersQuey->get();
        $sliders = $this->file->SetImagesPath($sliders);
        $user = $this->collection->ManagerInformation();
        $categories = Categorie::select(['id', 'title', 'active'])->where('active', 1)->get();
//        $categories = $this->AddImage($categories, 'categories', 'images');
        $products = Product::select(['title', 'amount', 'id'])->where('active', 1)->orderBy('sales', 'DESC')->get();
        $products = $this->AddImage($products, 'products', 'images');
        $AllProducts = Product::select(['title', 'amount', 'id'])->where('active', 1)->get();
        $rightSlider = Post::where('published', 1)->where('used_in', 'right.slider')->first();
        $slideNumber = 0;
        return view('frontend.products',[
            'sliders'=>$sliders,
            'user'=>$user,
            'categories'=>$categories,
            'products'=>$records,
            'paginate'=>$this->paginate,
            'rightSlider'=>$rightSlider,
            'slideNumber' => $slideNumber
        ]);
    }

}
