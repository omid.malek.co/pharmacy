<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Post;
use App\Image;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class PostController extends Controller
{
    private $paginate = 10;

    public function __construct()
    {
//        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Post::select(['id', 'title', 'published', 'created_by', 'updated_by'])
            ->orderBy('updated_at', 'desc')
            ->paginate($this->paginate);

        return view('pages.admin.posts')
            ->with('posts', $posts)
            ->with('operation', '#ListOperation')
            ->with('paginate', $this->paginate);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.admin.publish')->with('operation', '#AddOperation');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user_id = session()->get('id');
//        $title = $request->title;
        $title = ($request->title == '' ? 'بدون توضیحات' : $request->title);
//        $body = $request->body;
        $body = ($request->body == '' ? 'بدون توضیحات' : $request->body);
        $timezon = date('Y-m-d H:i:S');
        $data = [
            'title' => $title,
            'body' => $body,
        ];
        $post = Post::create($data);

        $image_record = [
            'src' => 'noPhoto.jpg',
            'record_id' => $post->id,
            'used_in' => 'articles',
            'created_at' => $timezon,
            'updated_at' => $timezon
        ];

        $image = Image::create($image_record);

        return redirect()->route('edit.articles.icon', ['image_id' => $image->id]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Post $post
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Post $post
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $post)
    {
        return view('pages.admin.publish')->with('post', $post);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Post $post
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Post $post)
    {
        $post->title = $request->title;
        $post->body = $request->body;
        $post->updated_by = Auth::id();
        $post->save();

        $imageObj = Image::where('record_id', $post->id)
            ->where('used_in', 'LIKE', '%articles%')
            ->where('active', 1);

        $image = $imageObj->first();


        return redirect()->route('admin.image.edit', ['image_id' => $image->id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Post $post
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post)
    {
        $post->delete();

        return redirect()->route('posts.index');
    }

    /**
     * Change the visibility of specified resource.
     *
     * @param  \App\Post $post
     * @return \Illuminate\Http\Response
     */
    public function switch(Post $post)
    {
        $post->published = !$post->published;
        $post->save();

        return redirect()->route('posts.index');
    }
}
