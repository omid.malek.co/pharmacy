<?php

namespace App\Http\Controllers\Admin;

use App\Entrie;
use App\Http\Middleware\isValideCustomer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Services\Collections\Collection;
use Illuminate\Support\Facades\DB;
use App\Services\EntriesServices;
use Carbon\Carbon;
use Validator;
use Lang;

class AdminEntriesController extends Controller
{
    private $paginate = 10;
    private $tableName = 'entries';
    private $collection;
    private $entriesServices;

    public function __construct(Collection $collection, EntriesServices $entriesServices)
    {
        $this->middleware([IsValideCustomer::class]);
        $this->collection = $collection;
        $this->entriesServices = $entriesServices;
    }

    public function searchEntries(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'bottomRange' => 'required|regex:/^\d\d\d\d[-]\d\d[-]\d\d$/',
            'topRange' => 'required|regex:/^\d\d\d\d[-]\d\d[-]\d\d$/',
        ]);

        session()->flash('class', 'alert-danger');
        if ($validator->fails())
            return back()
                ->withErrors(Lang::get('validation.ValueIsIncorrenct'))
                ->withInput($request->all());
        //--
        $arrParameter = [];
        $variables = $request->all();
        //--
        if (isset($variables['name'])
            && !$this->collection->IsArrayEmpty($variables['name'])
        ) {
            $name = trim($variables['name']);
            array_push($arrParameter, [
                'column' => 'name',
                'value' => $name
            ]);
        }
        //--
        if (isset($variables['bottomRange'])
            && !$this->collection->IsArrayEmpty($variables['bottomRange'])
        ) {
            $bottomRange = trim($variables['bottomRange']);
            $GregoriantimeStamp = $this->setGregoriantimeStamp($bottomRange);
            array_push($arrParameter, [
                'column' => 'bottom_date',
                'value' => $GregoriantimeStamp
            ]);
        }
        //--
        if (isset($variables['topRange'])
            && !$this->collection->IsArrayEmpty($variables['topRange'])
        ) {
            $topRange = trim($variables['topRange']);
            $GregoriantimeStamp = $this->setGregoriantimeStamp($topRange);
            array_push($arrParameter, [
                'column' => 'top_date',
                'value' => $GregoriantimeStamp
            ]);
        }
        $query = $this->entriesServices->queryCreator($arrParameter);
        $query = $this->collection->based64url_encode($query);
//        if (empty($searchInput))
//            return redirect()->route('managementEntries.index');
        return redirect()->route('result.search.managementEntries', ['keyword' => $query]);
    }

    public function setGregoriantimeStamp($Jalalidate)
    {
        $arr_data = explode(" ", $Jalalidate);
        $arr_date = explode("-", $arr_data[0]);
        return $this->collection->convertJalaliDateToGregorian($arr_date);
    }

    public function SearchResult($keyword)
    {
        $inputClause = $this->entriesServices->getInputClause($keyword);
        $parameters = [];
        if (isset($inputClause['bottom_date'])) {
            $arr_bottom_date = $this->entriesServices->getArrTimestampFromStrTimeStamp($inputClause['bottom_date']);
            //-- check parameter
            $bottom_date = $this->entriesServices->DateTimeArrayToTimeZone($arr_bottom_date);
            $parameters['bottom_date'] = $bottom_date;
        }
        if (isset($inputClause['top_date'])) {
            $arr_top_date = $this->entriesServices->getArrTimestampFromStrTimeStamp($inputClause['top_date']);
            $top_date = $this->entriesServices->DateTimeArrayToTimeZone($arr_top_date);
            $parameters['top_date'] = $top_date;
        }
        if (isset($inputClause['name'])) {
            $parameters['name'] = $inputClause['name'];
        }
        //--
        $result = $this->entriesServices->searchWithName($parameters);
        $result['top_date'] = (isset($top_date) ? $top_date : '');
        $result['bottom_date'] = (isset($bottom_date) ? $bottom_date : '');
        $result['name'] = (isset($parameters['name']) ? $parameters['name'] : '');
        return $this->filesView($result);
    }

    public function setTopDate($checkouts)
    {
        $top_date = '';
        if (isset($checkouts['top_date']) && !$this->collection->IsArrayEmpty($checkouts['top_date'])) {
            $date = (string)$checkouts['top_date'];
            $top_date = $this->collection->getJalaliDate($date);
        }
        return $top_date;
    }

    public function setBottomDate($checkouts)
    {
        $temp = '';
        if (isset($checkouts['bottom_date']) && !$this->collection->IsArrayEmpty($checkouts['bottom_date'])) {
            $date = (string)$checkouts['bottom_date'];
            $temp = $this->collection->getJalaliDate($date);
        }
        return $temp;
    }

    public function filesView($checkouts)
    {
        $products = $checkouts['checkouts'];
        $IsArrayEmpty = (integer)$this->collection->IsArrayEmpty($products->take(1)->get());
        $operation=0;
        if ($IsArrayEmpty) {
            $records = $this->collection->NoDataFound();
            $row = 0;
            $operation = 0;
        } else {
            $records = $products->orderBy('confirm', 'ASC')
                ->select([
                    'id',
                    'user_fullname','entry_date',
                    'entry_time','departure_date',
                    'departure_time','distance',
                    'confirm_by','created_at','updated_at'
                ])->orderBy('entry_date', 'DESC')
                ->paginate($this->paginate);

            $checkouts['total_interval']=$products->sum('distance');
            //-- set customer information to record
            $records = $this->entriesServices->setPersianDate($records, 'entry_date');
            $records = $this->entriesServices->setPersianDate($records, 'departure_date');
            $records = $this->entriesServices->setStatusTitle($records);
            $records = $this->entriesServices->SetInterval($records);
            //--
            $row = $records->firstItem();
            $operation = 1;
        }
        $top_date = $this->setTopDate($checkouts);
        $bottom_date = $this->setBottomDate($checkouts);
        $name = (isset($checkouts['name']) ? $checkouts['name'] : '');
        $total_interval = (isset($checkouts['total_interval']) ? $checkouts['total_interval'] : '');
        $total_interval= $this->entriesServices->getIntervalTag($total_interval);
        $user = Auth::user();
        $user_id = $user->id;
        $isAdmin = 1;

        $entries = array(
            'pages' => $records,
            'operation' => $operation,
            'row' => $row
        );
        $information = [
            'user_id' => $user_id,
            'isAdmin' => $isAdmin,
            'top_date' => $top_date,
            'bottom_date' => $bottom_date,
            'name' => $name,
            'total_interval'=>$total_interval,
            'info' => $user->first(),
            'IsArrayEmpty'=>$IsArrayEmpty,
            'entries' => $entries,
            'paginate' => $this->paginate,
            'page' => '#Home',
            'operation' => "#ListOperation"
        ];
        return view('pages.admin.ManagementEntriesList', $information);
    }

    public function confirm($id)
    {
        //--
        $user_id = Auth::id();
        $validator = Validator::make([
            'id' => $id
        ], [
            'id' => 'required|numeric|exists:entries,id',
        ]);
        session()->flash('class', 'alert-danger');
        if ($validator->fails())
            return abort(404);
        //-------------------
        $variables = array(
            'entrie_id' => $id,
            'confirm' => 1,
            'confirm_by' => $user_id
        );
        $this->entriesServices->confirmStatus($variables);
        return back();
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        //--
        $now_date = Carbon::today();
        $top_date = Carbon::today();
        $bottom_date = $now_date->subDay(5);
        $result = $this->entriesServices->searchWithName(array(
            'active' => 1,
            'name' => '',
            'top_date' => $top_date,
            'bottom_date' => $bottom_date
        ));
        $result['top_date'] = $top_date;
        $result['bottom_date'] = $bottom_date;
        //-----------------------------------------------------
        return $this->filesView($result);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
