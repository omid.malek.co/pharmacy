<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\Collections\Collection;
use App\Services\Collections\File;
use Illuminate\Support\Facades\DB;
use App\Http\Middleware\isValideCustomer;
use App\Drug;
use App\Scale;
use Validator;
use Lang;

class drugsController extends Controller
{
    private $collection;
    private $file;

    private $used_in = '10';

    private $paginate = 10;

    private $table='drugs';


    public function __construct(Collection $collection, File $file)
    {
        $this->middleware([IsValideCustomer::class]);
        $this->collection = $collection;
        $this->file = $file;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = $this->collection->ManagerInformation();
//        $sliders = $this->collection->GetSliderPictures();
//        $sliders = $this->file->SetImagesPath($sliders->get(), 'sliders');


        $collections = Drug::where('active', '=', 1)
            ->with(['scale'])->orderBy('updated_at', 'DESC')->paginate($this->paginate);

//        $categories = Categorie::select(['id', 'title', 'active'])->where('active', 1)->get();
//        $categories = $this->collection->AddImage($categories, 'categories', 'images');
//

        $isArrayEmpty= (integer)$hasRecord=DB::table($this->table)->where('active', 1)->exists();
        $row = 1;

        return view('pages.admin.drugsList', [
            'isArrayEmpty'=>$isArrayEmpty,
            'info' => $user->first(),
            'collections' => $collections,
            'paginate' => $this->paginate,
            'row' => $row,
            'operation' => "#ListOperation"
        ]);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = $this->collection->ManagerInformation();

        $scales = Scale::select(['id', 'title', 'active'])->where('active', 1)->get();

        return view('pages.admin.drugsCreator', [
            'info' => $user->first(),
            'scales' => $scales,
            'operation' => "#InsertOperation"
        ]);


    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'count' => 'required|numeric',
            'scale_id' => 'required|numeric|exists:scales,id',
            'name' => 'required',
            'shape' => 'required',
            'description' => 'required',
        ]);
        $description=$request->description;
        $description = $this->collection->InputIsEmpty($description,'بدون توضیحات');
        if ($validator->fails()) {
            session()->flash('class', 'alert-danger');
            return back()->withErrors(Lang::get('validation.ValueIsIncorrenct'))->withInput($request->all());
        } else {
            $timezon = date('Y-m-d H:i:s');
            $record = [
                'count' => (integer)$request->count,
                'scale_id' => (integer)$request->scale_id,
                'name' => $request->name,
                'shape'=>$request->shape,
                'description' => $description,
                'active'=>1,
                'created_at' => $timezon,
                'updated_at' => $timezon
            ];
            $product = Drug::create($record);

            return back()->withErrors(Lang::get('validation.successupdate'))
                ->withInput($request->all());

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //--

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $drug=Drug::find($id);
        $user = $this->collection->ManagerInformation();

        $scales = Scale::select(['id', 'title', 'active'])->where('active', 1)->get();

        return view('pages.admin.drugsEditor', [
            'info' => $user->first(),
            'scales' => $scales,
            'drug'=>$drug,
            'operation' => "#InsertOperation"
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $validator = Validator::make($request->all(), [
            'count' => 'required|numeric',
            'scale_id' => 'required|numeric|exists:scales,id',
            'name' => 'required',
            'shape' => 'required',
            'description' => 'required',
        ]);
        $description=$request->description;
        $description = $this->collection->InputIsEmpty($description,'بدون توضیحات');
        if ($validator->fails()) {
            session()->flash('class', 'alert-danger');
            return back()->withErrors(Lang::get('validation.ValueIsIncorrenct'))->withInput($request->all());
        } else {
            $timezon = date('Y-m-d H:i:s');
            $record = [
                'count' => (integer)$request->count,
                'scale_id' => (integer)$request->scale_id,
                'name' => $request->name,
                'shape'=>$request->shape,
                'description' => $description,
                'active'=>1,
                'created_at' => $timezon,
                'updated_at' => $timezon
            ];
            $product = Drug::where('id',$id)->update($record);
            return back()->withErrors(Lang::get('validation.successupdate'))
                ->withInput($request->all());

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //--
        $hasRecord=DB::table($this->table)->where('id', $id)->exists();
        if ($hasRecord)
            $result=Drug::where('id',$id)->delete();
        //--
        return back();

    }
}
