<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\Collections\Collection;
use Illuminate\Support\Facades\DB;
use App\Http\Middleware\isValideCustomer;
use App\Services\ItemsServices;
use App\User;
use App\Permission;
use App\Categorie;
use App\Page;
use App\Item;
use Validator;
use Lang;

class itemsController extends Controller
{
    private $paginate = 10;
    private $collection;
    private $table = 'items';

    //--
    public function __construct(Collection $collection)
    {
        $this->middleware([IsValideCustomer::class]);
        $this->collection = $collection;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //--
        $user = $this->collection->ManagerInformation();
        //--
        $permission_id = $this->collection->AdminPermissionId();
        $collection = User::where('active', 1)
            ->where('permission_id', '!=', $permission_id)
            ->paginate($this->paginate);

//        $categories = Categorie::select(['id', 'title', 'active'])->where('active', 1)->get();
//        $categories = $this->collection->AddImage($categories, 'categories', 'images');

        $row = 1;
        //--     'sliders' => $sliders,
        //--    'categories' => $categories,
        return view('pages.admin.personnels', [
            'info' => $user->first(),
            'collections' => $collection,
            'paginate' => $this->paginate,
            'row' => $row,
            'operation' => "#ListOperation"
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //--
        $user = $this->collection->ManagerInformation();
        $categories = Categorie::select(['id', 'title', 'active'])
            ->where('active', 1)->get();
        $categories = $this->collection->AddImage($categories, 'categories', 'images');

        return view('pages.admin.InsertPersonnel', [
            'info' => $user->first(),
            'categories' => $categories,
            'paginate' => $this->paginate,
            'operation' => "#InsertOperation"
        ]);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     */
    public function store(Request $request,ItemsServices $items)
    {
        //--
        $validator = Validator::make($request->all(), [
            'fullname' => 'required',
            'phone' => 'required|numeric|unique:users,username',
        ]);
        session()->flash('class', 'alert-danger');
        if ($validator->fails())
            return redirect()->back()
                ->withErrors(Lang::get('validation.ValueIsIncorrenct'))
                ->withInput($request->all());
//        //
        $table = 'users';
        $facebook_address = empty($request->facebook_address) ? 'null' : $request->facebook_address;
        $instagram_address = empty($request->instagram_address) ? 'null' : $request->instagram_address;
        $telegram_address = empty($request->telegram_address) ? 'null' : $request->telegram_address;
        $number_account = empty($request->number_account) ? 'null' : $request->number_account;
        $number_cart = empty($request->number_cart) ? 'null' : $request->number_cart;
        $username = empty($request->phone) ? 'null' : $request->phone;
        $address = empty($request->adress) ? 'null' : $request->adress;
        $email = empty($request->email) ? 'null' : $request->email;
        //--
        $code = $this->collection->CreateRandomCode(4);
        $password = 'pharmacy-' . $code;
        $Hashpassword = md5($password);
        //--
        $personnelPermissionId = $this->collection->personnelPermissionId();
        $record = [
            'fullname' => $request->fullname,
            'phone' => $request->phone,
            'email' => $email,
            'permission_id'=>$personnelPermissionId,
            'password' => $Hashpassword,
            'username' => $username,
            'number_cart' => $number_cart,
            'facebook_address' => $facebook_address,
            'instagram_address' => $instagram_address,
            'telegram_address' => $telegram_address,
            'number_account' => $number_account,
            'address' => $request->address,
        ];
        $hasNotItem = DB::table($table)
            ->where('active', 1)
            ->where('username', '=', $record['username'])
            ->doesntExist();
        if ($hasNotItem) {
            $user = User::create($record);
        } else {
        }
         //-- to do
        $user = User::where('active', 1)
            ->where('username', '=', $record['username'])
            ->first();
        //--
       $items->addItemsforUser($user,$record['permission_id']);

        //--
        $receptor = $record['phone'];
        $tokens = [
            $username,
            $password,
            $record['fullname']
        ];
        //--
        User::where('id', '=', $user->id)->update(['password' => $Hashpassword]);
        $this->collection->SendAuthorizeSms($tokens, $receptor, 'PharmacyAuthrize');
        //----
            session()->flash('class', 'alert-success');
            return redirect()->back()
                ->withErrors(Lang::get('validation.successupdate'))
                ->withInput($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //--
        //--
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
