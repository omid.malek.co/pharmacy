<?php

namespace App\Http\Controllers\Admin;

use App\Http\Middleware\isValideCustomer;
use App\Item;
use App\Page;
use App\Services\Collections\Collection;
use App\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Services\ItemsServices;
use Validator;
use Lang;


class accessController extends Controller
{
    private $paginate = 10;
    private $collection;
    private $table = 'items';
    private $itemsService;

    //--
    public function __construct(Collection $collection, ItemsServices $itemsServices)
    {
        $this->middleware([IsValideCustomer::class]);
        $this->itemsServices = $itemsServices;
        $this->collection = $collection;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function hasItem($pagesRecord, $user_id)
    {
        $hasRecord = DB::table($this->table)
            ->where('active', 1)
            ->where('page_id', $pagesRecord->id)
            ->where('user_id', $user_id)
            ->exists();
        if ($hasRecord)
            return 1;
        return 0;
    }

    public function getPages($user_id)
    {
        $table = 'pages';
        $hasRecord = DB::table($table)
            ->where('active', 1)
            ->where('allow_access',1)
            ->where('is_base_page', '=', 1)
            ->exists();
        if ($hasRecord) {
            $pages = Page::where('active', 1)
                ->where('is_base_page', '=', 1)
                ->where('allow_access',1)
                ->paginate($this->paginate);
            for ($i = 0; $i < count($pages); $i++) {
                $pages[$i]->hasItem = $this->hasItem($pages[$i], $user_id);
            }
            $row = $pages->firstItem();
            $operation = 1;
        } else {
            $pages = $this->collection->NoDataFound();
            $row = 0;
            $operation = 0;
        }
        return [
            'pages' => $pages,
            'row' => $row,
            'operation' => $operation
        ];
    }


    public function index()
    {
        //--
        $user = $this->collection->ManagerInformation();
        //--
        $permission_id = $this->collection->AdminPermissionId();
        $collection = User::where('active', 1)
            ->where('permission_id', '!=', $permission_id)
            ->paginate($this->paginate);

        $row = 1;
        return view('pages.admin.userListForItems', [
            'info' => $user->first(),
            'collections' => $collection,
            'paginate' => $this->paginate,
            'row' => $row,
            'operation' => "#ListOperation"
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, ItemsServices $itemsServices)
    {
        //--
        $validator = Validator::make($request->all(), [
            'page_id' => 'required|exists:pages,id',
            'user_id' => 'required|exists:users,id',
        ]);
        if ($validator->fails())
            return abort(404);
        //--
        $page_id = $request->page_id;
        $user_id = $request->user_id;
        $user = User::find($user_id);
        $page = Page::find($page_id);
        $this->itemsServices->addItemToEmployer($page, $user, $user->permission_id);
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //---
        //--
        $user = $this->collection->ManagerInformation();
        //--
        $pages = $this->getPages($id);
        $permission_id = $this->collection->AdminPermissionId();
        $information = [
            'user_id'=>$id,
            'info' => $user->first(),
            'pages' => $pages,
            'paginate' => $this->paginate,
            'page' => '#Home',
            'operation' => "#ListOperation"
        ];
        return view('pages.admin.itemsList', $information);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'page_id' => 'required|numeric|exists:pages,id',
            'user_id' => 'required|exists:users,id',
        ]);
        if ($validator->fails())
            return abort(404);
        $page_id=$request->page_id;
        $user_id=$request->user_id;
        $resualt=Item::where('page_id',$page_id)
            ->where('user_id',$user_id)->delete();
        return back();
    }
}
