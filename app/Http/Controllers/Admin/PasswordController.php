<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\Collections\Collection;
use App\Services\Collections\File;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\Http\Middleware\isValideCustomer;
use Illuminate\Support\Facades\Auth;
use App\User;
use Validator;
use Lang;

class PasswordController extends Controller
{
    private $collection;

    private $paginate = 10;


    public function __construct(Collection $collection, File $file)
    {
        $this->middleware([IsValideCustomer::class]);
        $this->collection = $collection;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $MyInformation = $this->collection->ManagerInformation();
        return view('pages.admin.ChangePassword', [
            'isEmpty' => false,
            'operation' => '#ChangePassword'
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //
        $validator = Validator::make([
            'old_password' => md5($request->old_password),
            'password' => md5($request->password),
            'password_confirmation' => md5($request->password_confirmation),
        ], [
            'old_password' => 'required|exists:users,password',
            'password' => 'required',
            'password_confirmation' => 'required|same:password',
        ]);
        $erorr = "پسورد فعلی خود را صحیح وارد کنید";
        session()->flash('class', 'alert-primary');
        if ($validator->fails())
            return back()->withErrors($validator->errors());

        $password = md5($request->password);
        $oldPassword = md5($request->old_password);
        $user = Auth::user();
        $user_id = $user->id;
        $MyInformationObj = User::where('id', $user_id)
            ->where('active', '!=', '0')
            ->where('password', $oldPassword);

        $MyInformation = $MyInformationObj->get();
        if ($this->collection->IsArrayEmpty($MyInformation))
            return back()->withErrors($erorr);

        $MyInformationObj->update(['password' => $password]);
        session()->flash('class', 'alert-success');
        return back()->withErrors(Lang::get('validation.successupdate'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
