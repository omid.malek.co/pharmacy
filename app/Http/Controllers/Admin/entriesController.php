<?php

namespace App\Http\Controllers\Admin;

use App\Http\Middleware\isValideCustomer;
use App\Page;
use App\Permission;
use App\Services\Collections\Collection;
use http\Client\Curl\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Services\EntriesServices;
use Carbon\Carbon;

class entriesController extends Controller
{
    private $paginate = 10;
    private $collection;
    private $entriesServices;

    public function __construct(Collection $collection, EntriesServices $entriesServices)
    {
        $this->middleware([IsValideCustomer::class]);
        $this->collection = $collection;
        $this->entriesServices = $entriesServices;
    }


    public function departure()
    {

    }


    public function present()
    {

        $user = Auth::user();
        $permissionId = $this->collection->personnelPermissionId();
        return $this->entriesServices->createOrUpdateItem($user, $permissionId);
    }

    public function leaving()
    {
        $user = Auth::user();
        $permissionId = $this->collection->personnelPermissionId();
        $variables = array(
            'user' => $user,
            'permissionId' => $permissionId
        );
        return $this->entriesServices->leavingTheWorkPlace($variables);
    }

    /**
     * Display a listing of the resource.
     *
     * @return array|\Illuminate\Http\Response
     */
    public function index()
    {
        //--
        $user = Auth::user();
        $user_id = $user->id;
        $entries = $this->entriesServices->getEntries($user);
        $isAdmin = 0;
        //--
        $permission_id = $this->collection->AdminPermissionId();
        $information = [
            'user_id' => $user_id,
            'info' => $user->first(),
            'entries' => $entries,
            'paginate' => $this->paginate,
            'isAdmin' => $isAdmin,
            'page' => '#Home',
            'operation' => "#ListOperation"
        ];
        return view('pages.admin.entriesList', $information);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }


    public function destroyRecord(Request $request)
    {
        return $request->all();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $this->entriesServices->destroyRecord($id);
        return back();
    }
}
