<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\Collections\Collection;
use Illuminate\Support\Facades\DB;
use App\Services\UserServices;
use App\Document;
use App\User;
use App\Permission;
use App\Categorie;
use App\Page;
use App\Item;
use Validator;
use Lang;

class personnelsController extends Controller
{
    private $paginate = 10;
    private $collection;
    private $table = 'users';
    private $userServices;

    //--
    public function __construct(Collection $collection,UserServices $userServices)
    {
        $this->collection = $collection;
        $this->userServices= $userServices;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    protected function destroyUserItems($user_id){
        $table_name="items";
        $hasRecord=DB::table($table_name)
            ->where('user_id', $user_id)
            ->exists();
        if ($hasRecord)
        Item::where('user_id',$user_id)->delete();
        return 1;
    }
    protected function destroyUserRecord($user_id){
        $hasRecord=DB::table($this->table)
            ->where('id', $user_id)
            ->exists();
        if ($hasRecord)
            User::where('id',$user_id)->delete();
        return 1;
    }

    public function index()
    {
        //--------------------

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //---

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //--
        //--
        $validator = Validator::make([
            'id' => $id
        ], [
            'id' => 'required|numeric|exists:users,id',
        ]);
        if ($validator->fails())
            return abort(404);
        //--
        //--
//        $user = $this->collection->ManagerInformation();
        $user_id = $id;
        //-- doesn't item
        $table_name = 'users';
        //--
        $user = User::where('id', $id)->first();
        $categories = Categorie::select(['id', 'title', 'active'])
            ->where('active', 1)->get();
        $categories = $this->collection->AddImage($categories, 'categories', 'images');

        return view('pages.admin.editorPersonnel', [
            'info' => $user,
            'categories' => $categories,
            'paginate' => $this->paginate,
            'operation' => "#InsertOperation"
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //--
        $validator = Validator::make([
            'fullname' => $request->fullname,
            'phone' => $request->phone,
            'id' => $id
        ], [
            'fullname' => 'required',
            'phone' => 'required|numeric',
            'id' => 'required|numeric|exists:users,id'
        ]);
        session()->flash('class', 'alert-danger');
        if ($validator->fails())
            return redirect()->back()
                ->withErrors(Lang::get('validation.ValueIsIncorrenct'))
                ->withInput($request->all());
//        //
        $table = 'users';
        $facebook_address = empty($request->facebook_address) ? 'null' : $request->facebook_address;
        $instagram_address = empty($request->instagram_address) ? 'null' : $request->instagram_address;
        $telegram_address = empty($request->telegram_address) ? 'null' : $request->telegram_address;
        $number_account = empty($request->number_account) ? 'null' : $request->number_account;
        $number_cart = empty($request->number_cart) ? 'null' : $request->number_cart;
        $username = empty($request->phone) ? 'null' : $request->phone;
        $address = empty($request->adress) ? 'null' : $request->adress;
        $email = empty($request->email) ? 'null' : $request->email;
        //--
        $password = "";
        //--
        $personnelPermissionId = $this->collection->personnelPermissionId();
        $record = [
            'fullname' => $request->fullname,
            'phone' => $request->phone,
            'email' => $email,
            'permission_id' => $personnelPermissionId,
            'username' => $username,
            'number_cart' => $number_cart,
            'facebook_address' => $facebook_address,
            'instagram_address' => $instagram_address,
            'telegram_address' => $telegram_address,
            'number_account' => $number_account,
            'address' => $request->address,
        ];
        //--
        $user_id=$id;
        //--
        $receptor = $record['phone'];
        $tokens = [
            $username,
            $password,
            $record['fullname']
        ];
        //--

//        User::where('id', '=', $user_id)->update($record);

        $this->userServices->updateUser($record,$user_id);
        //----
        session()->flash('class', 'alert-success');
        return redirect()->back()
            ->withErrors(Lang::get('validation.successupdate'))
            ->withInput($request->all());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $validator = Validator::make([
            'id' => $id
        ], [
            'id' => 'required|numeric|exists:users,id'
        ]);
        session()->flash('class', 'alert-danger');
        if ($validator->fails())
            return redirect()->back()
                ->withErrors(Lang::get('validation.ValueIsIncorrenct'))
                ->withInput($request->all());
        //--
        $user_id=$id;
        $this->destroyUserItems($user_id);

        $this->destroyUserRecord($user_id);
        $this->unistallDocument($user_id);
        //--
        //----
        session()->flash('class', 'alert-success');
        return redirect()->back()
            ->withErrors(Lang::get('validation.successupdate'));
    }
    public function unistallDocument($user_id){
        $document=Document::where('user_id','=',$user_id);
        $timezon = date('Y-m-d H:i:s');
        if($this->collection->IsArrayEmpty($document->get()))
          $document->update([
              'active'=>0,
              'updated_at'=>$timezon
          ]);

        return 1;
    }

}
