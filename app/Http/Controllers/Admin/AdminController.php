<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Http\Middleware\isValideCustomer;
use App\User;
use App\Item;
use App\Services\Collections\Collection;
use Validator;
use Lang;

class AdminController extends Controller
{
    private $collection;

    public function __construct(Collection $collection)
    {
        $this->middleware([IsValideCustomer::class]);
        $this->collection = $collection;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function TableCount($table_name)
    {
        $table = DB::table($table_name)->select(DB::raw('count(*) as ' . $table_name . '_count'))->get();
        return $table;
    }

    public function index()
    {
        $info_admin = User::find(session()->get('id'));
        $is_base_url = 1;
        $user_id = $info_admin->id;
        $UserItems = $this->collection->UserItems($user_id, $is_base_url);
        $comments = $this->TableCount('comments');
        $users = $this->TableCount('users');
        $customers = $this->TableCount('customers');
        $images = $this->TableCount('images');
        return view('pages.admin.home', [
            'info' => $info_admin,
            'page' => '#Home',
            'comments' => $comments,
            'users' => $users,
            'customers' => $customers,
            'UserItems' => $UserItems,
            'images' => $images,
            'operation' => '#dashboard',
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        //
        $info_admin = $this->collection->ManagerInformation();
        $info_admin->first()->facebook_address = $info_admin->first()->facebook_address == 'null' ? '' : $info_admin->first()->facebook_address;
        $info_admin->first()->instagram_address = $info_admin->first()->instagram_address == 'null' ? '' : $info_admin->first()->instagram_address;
        $info_admin->first()->telegram_address = $info_admin->first()->telegram_address == 'null' ? '' : $info_admin->first()->telegram_address;
        return view('pages.admin.EditAdminInfo', [
            'info' => $info_admin->first(),
            'page' => '#Home',
            'operation' => '#EditInformation',
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'fullname' => 'required',
            'phone' => 'required|numeric',
            'number_cart' => 'required',
            'number_account' => 'required|numeric',
            'address' => 'required',
        ]);
        session()->flash('class', 'alert-danger');
        if ($validator->fails())
            return redirect()->back()
                ->withErrors(Lang::get('validation.ValueIsIncorrenct'))
                ->withInput($request->all());
        //
        $user_id = Auth::id();
        $facebook_address = empty($request->facebook_address) ? 'null' : $request->facebook_address;
        $instagram_address = empty($request->instagram_address) ? 'null' : $request->instagram_address;
        $telegram_address = empty($request->telegram_address) ? 'null' : $request->telegram_address;
        $username = empty($request->phone) ? 'null' : $request->phone;
        $email = empty($request->email) ? 'null' : $request->email;
        $record = [
            'fullname' => $request->fullname,
            'phone' => $request->phone,
            'email' => $email,
            'username' => $username,
            'number_cart' => $request->number_cart,
            'facebook_address' => $facebook_address,
            'instagram_address' => $instagram_address,
            'telegram_address' => $telegram_address,
            'number_account' => $request->number_account,
            'address' => $request->address,
        ];
        $user = User::where('id', $user_id)->where('active', 1);
        $user->update($record);
        session()->flash('class', 'alert-success');
        return redirect()->back()
            ->withErrors(Lang::get('validation.successupdate'))
            ->withInput($request->all());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function logout()
    {
        Auth::logout();
        session()->forget('id');
        return redirect('/');
    }
}
