<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\Collections\Collection;
use App\Services\Collections\File;
use App\Categorie;
use Validator;
use App\Image;
use App\Post;
use App\Product;
use App\User;
use Lang;

class ArticlesController extends Controller
{
	
	
    private $collection;
    private $file;

    private $used_in = 'products';

    private $paginate = 10;

    public function __construct(Collection $collection, File $file)
    {
        $this->collection = $collection;
        $this->file = $file;
    }

    public function index()
    {

        $images = Image::all();
        $sliders = $images->where('used_in', '=', 'sliders');

        $slidersQuey = $this->collection->GetSliderPictures();
        $sliders = $slidersQuey->get();
        $sliders = $this->file->SetImagesPath($sliders);

        $user=$this->collection->ManagerInformation();
        $categories = Categorie::select(['id', 'title', 'active'])->where('active', 1)->get();
        $categories = $this->collection->AddImage($categories, 'categories', 'images');

        $products = Product::select(['title', 'amount', 'id'])->where('active', 1)->orderBy('sales', 'DESC')->get();

        $products = $this->collection->AddImage($products, 'products', 'images');
        $AllProducts = Product::select(['title', 'amount', 'id'])->where('active', 1)->get();

        $posts = Post::where('published', 1)->where('used_in', 'articles')->paginate($this->paginate);

        $postsWithImages = $this->collection->AddImage($posts, 'articles', 'images');
        $rightSlider = Post::where('published', 1)->where('used_in', 'right.slider')->first();
        $slideNumber = 0;

        return view('frontend.Articles', [
            'sliders' => $sliders,
            'posts' => $postsWithImages,
            'products' => $products,
            'AllProducts' => $AllProducts,
            'user' => $user,
            'categories' => $categories,
            'innerPage' => '#Articles',
            'rightSlider' => $rightSlider,
            'slideNumber' => $slideNumber
        ]);
    }

    public function show($post_id)
    {
        $images = Image::all();
        $sliders = $images->where('used_in', '=', 'sliders');
//        $user = User::first()->get(['fullname', 'phone', 'email', 'address', 'number_cart', 'number_account']);
        $user=$this->collection->ManagerInformation();
        $categories = Categorie::select(['id', 'title', 'active'])->where('active', 1)->get();
        $categories = $this->collection->AddImage($categories, 'categories', 'images');


        $posts = Post::where('published', 1)->where('used_in', 'articles')->paginate($this->paginate);

        $postsWithImages = $this->collection->AddImage($posts, 'articles', 'images');


        $postObj = Post::where('id', $post_id);
        $post = $postObj->get();

        $used_in = $post->first()->used_in;
        $visited = $post->first()->visited + 1;
        $postObj->update(['visited' => $visited]);

        $postWithImage = $this->collection->AddImage($post, $used_in, 'images');

        $published_at = $this->collection->getJalaliDate($postWithImage->first()->updated_at);

        array_add($postWithImage->first(), 'published_at', $published_at);

        $rightSlider = Post::where('published', 1)->where('used_in', 'right.slider')->first();
        return view('frontend.ArticleShow', [
            'posts' => $postsWithImages,
            'post' => $postWithImage,
            'user' => $user,
            'categories' => $categories,
            'innerPage' => '#Articles',
            'rightSlider' => $rightSlider,
        ]);

    }

}
