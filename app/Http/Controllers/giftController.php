<?php

namespace App\Http\Controllers;

use App\Factor;
use App\Image;
use App\Payment;
use App\Services\Collections\File;
use App\User;
use App\Wallet;
use Illuminate\Http\Request;
use App\Services\Collections\Collection;
use App\Categorie;
use App\Gift;
use Validator;
use Lang;
use App\Type;
use App\Post;

class giftController extends Controller
{
    private $collection;
    private $file;
    private $type;
    private $used_in = 'products';
    private $paginate = 10;

    public function __construct(Collection $collection, File $file)
    {
        $this->collection = $collection;
        $this->file = $file;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $customer_id = $this->collection->getCustomerId();
        $walletQuery = Wallet::where('customer_id', $customer_id)
            ->where('active', 1);
        $isWalletEmpty = $this->collection->IsArrayEmpty($walletQuery->take(1)->get());
        if ($isWalletEmpty)
            $wallet = $this->collection->CreateAccountInWallet();
        else
            $wallet = $walletQuery->first();

        $giftsQuery = Gift::select([
            'id', 'text', 'amount', 'used_by', 'created_by', 'condition_id',
            'start_date', 'expire_date'
        ])->where('created_by', $customer_id)
            ->where('active', 1)
            ->with(['condition:id,title']);

        $isGiftEmpty = $this->collection->IsArrayEmpty($giftsQuery->take(1)->get());
        $row = 1;
        if ($isGiftEmpty)
            $gifts = $this->collection->NoDataFound();
        else {
            $gifts = $giftsQuery->paginate($this->paginate);
            $row = $gifts->firstItem();
            $giftsCount = count($gifts);
            for ($i = 0; $i < $giftsCount; $i++) {
                $persian_date = $this->collection->getJalaliDate($gifts[$i]->start_date);
                $gifts[$i]->persian_start_date = $persian_date;
                $persian_date = $this->collection->getJalaliDate($gifts[$i]->expire_date);
                $gifts[$i]->persian_expire_date = $persian_date;
            }
        }
        $sliders = $this->collection->GetSliderPictures();
        $sliders = $this->file->SetImagesPath($sliders->get(), 'sliders');
        $user = $this->collection->ManagerInformation();
        $categories = Categorie::select(['id', 'title', 'active'])->where('active', 1)->get();
        $categories = $this->collection->AddImage($categories, 'categories', 'images');
        $rightSlider = Post::where('published', 1)->where('used_in', 'right.slider')->first();
        return view('frontend.gifts', [
            'sliders' => $sliders,
            'user' => $user,
            'gifts' => $gifts,
            'wallet' => $wallet,
            'categories' => $categories,
            'row' => $row,
            'rightSlider' => $rightSlider,
            'isEmpty' => $isGiftEmpty,
            'paginate' => $this->paginate
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $code = $this->collection->trackerCodeCreator(8, 'gifts', 'text');
        return response()->json(['code' => $code], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $validator = Validator::make($request->all(), [
            'amount' => 'required|numeric',
            'expire_date' => 'required',
            'start_date' => 'required',
            'text' => 'required|unique:gifts,text',
            'wallet_id' => 'required|numeric|exists:wallets,id',
        ]);

        session()->flash('class', 'alert-danger');
        if ($validator->fails())
            return back()->withErrors(Lang::get('validation.ValueIsIncorrenct'))->withInput($request->all());

        $gregorian_today = date('Y-m-d');
        $created_by = $this->collection->getCustomerId();
        $conditionNotUsed = $this->collection->conditionNotUsed();
        $start_date = $request->start_date;
        $wallet_id = (int)$request->wallet_id;
        $amount = (double)$request->amount;
        $expire_date = $request->expire_date;
        $gregorian_start_date = $this->collection->ConvertJalaliToGregorian('Y-m-d', $start_date);
        $gregorian_expire_date = $this->collection->ConvertJalaliToGregorian('Y-m-d', $expire_date);

        if ($gregorian_start_date > $gregorian_expire_date || $gregorian_today >= $gregorian_expire_date)
            return back()->withErrors(Lang::get('validation.ValueIsIncorrenct'))->withInput($request->all());

        $with_drawal_collect = $this->collection->WithDrawal($amount, $created_by);
        if (!$with_drawal_collect['status'])
            return back()->withErrors($with_drawal_collect['message']);

        Wallet::where('id', $with_drawal_collect['wallet_id'])
            ->update(['amount' => $with_drawal_collect['amount_should_be']]);

        $timezon = date('Y-m-d H:i:s');
        $record = [
            'text' => $request->text,
            'amount' => $amount,
            'wallet_id' => $wallet_id,
            'created_by' => $created_by,
            'condition_id' => $conditionNotUsed->id,
            'start_date' => $gregorian_start_date,
            'expire_date' => $gregorian_expire_date,
            'created_at' => $timezon,
            'updated_at' => $timezon
        ];
        $gift = Gift::create($record);
        return redirect()->route('gifts.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }


    public function InsertToPayment(Request $request)
    {
        $validator = Validator::make([
            'gift_text' => $request->gift_text,
            'payment_id' => $request->payment_id
        ], [
            'gift_text' => 'required|exists:gifts,text',
            'payment_id' => 'required|exists:payments,id'
        ]);
        $MessagesModel = '<h3 class="text-center">کارت هدیه معتبر نیست</h3>';
        if ($validator->fails())
            return response()->json([
                'operation' => 0,
                'MessagesModel' => $MessagesModel,
                'totalSum' => 0
            ], 200);

        $conditionNotUsed=$this->collection->conditionNotUsed();
        $payment_id = $request->payment_id;
        $gift_text = $request->gift_text;
        $today = date('Y-m-d');
        $giftQuery = Gift::where('text', $gift_text)
            ->where('active', 1)
            ->where('condition_id', $conditionNotUsed->id)
            ->where('start_date', '<=', $today)
            ->where('expire_date', '>', $today)
            ->take(1);

        $gift = $giftQuery->get();
        if ($this->collection->IsArrayEmpty($gift))
            return response()->json([
                'operation' => 0,
                'MessagesModel' => $MessagesModel,
                'totalSum' => 0
            ], 200);

        $paymentObj = Payment::where('id', $payment_id);
        $payment = $paymentObj->first();
        // calculate discount amount
        $giftValue = (string)$gift->first()->amount;
        // calculate payment amount
        $totalSum = 0;
//        $totalSum = $this->collection->CalculatePaymentValue($payment->payment_amount, $giftValue, 1);
        $totalSum = $this->collection->checkGiftCard($paymentObj, $payment, $giftQuery, $gift, $totalSum);
        $MessagesModel = '<h3 class="text-center">کارت هدیه اعمال شد</h3>';
//        $payment_record = [
//            'gift_amount' => $giftValue,
//            'gift_id' => $gift->first()->id
//        ];
//        $paymentObj->update($payment_record);
        $totalSum = $totalSum . ' ' . 'تومان';
        return response()->json([
            'operation' => 1,
            'MessagesModel' => $MessagesModel,
            'totalSum' => $totalSum
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Gift::where('id', $id)->update(['active' => 0]);
        return back();
    }
}
