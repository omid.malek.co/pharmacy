<?php

namespace App\Http\Controllers;

use App\Services\Collections\File;
use Illuminate\Http\Request;
use App\Services\Collections\Collection;
use Illuminate\Support\Facades\DB;
use App\Image;
use App\Product;
use App\Option;
use App\User;
use App\Categorie;
use Validator;
use Lang;

class optionManagement extends Controller
{
    private $collection;
    private $file;
    private $table;
    private $paginate = 10;

    public function __construct(Collection $collection, File $file)
    {
        $this->collection = $collection;
        $this->file = $file;
        $this->setTable('options');
    }

    public function setTable($table)
    {
        $this->table = $table;
    }

    public function index($product_id)
    {
        $user = $this->collection->ManagerInformation();
        $doesntExist = DB::table($this->table)->where('active', 1)->where('product_id', $product_id)->doesntExist();
        if ($doesntExist)
            $collection = $this->collection->NoDataFound();
        else
            $collection = Option::where('active', 1)
                ->where('product_id', $product_id)
                ->with(['categorie:title,id', 'product'])
                ->orderBy('updated_at', 'DESC')
                ->paginate($this->paginate);


        $row = 1;
        return view('pages.admin.options', [
            'info' => $user->first(),
            'collections' => $collection,
            'paginate' => $this->paginate,
            'row' => $row,
            'operation' => "#ListOperation",
            'product_id' => $product_id,
            'doesntExist' => $doesntExist
        ]);
    }

    public function create($product_id)
    {
        $validator = Validator::make([
            'product_id' => $product_id
        ], [
            'product_id' => 'required|numeric|exists:products,id'
        ]);
        if ($validator->fails())
            return abort(500);
        $user = $this->collection->ManagerInformation();
        $product = Product::find($product_id);
        $categorie_id = $product->categorie_id;
        return view('pages.admin.optionCreator', [
            'info' => $user->first(),
            'product_id' => $product_id,
            'categorie_id' => $categorie_id,
            'paginate' => $this->paginate,
            'operation' => "#InsertOperation"
        ]);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'product_id' => 'required|numeric|exists:products,id',
            'categorie_id' => 'required|numeric|exists:categories,id',
            'title' => 'required',
            'TextValue' => 'required',
        ]);
        session()->flash('class', 'alert-danger');
        if ($validator->fails())
            return back()->withErrors($validator->errors())->withInput($request->all());

        $timezon = date('Y-m-d H:i:S');
        $record = [
            'categorie_id' => $request->categorie_id,
            'product_id' => $request->product_id,
            'title' => $request->title,
            'value' => $request->TextValue,
            'active' => 1,
            'created_at' => $timezon,
            'updated_at' => $timezon
        ];
        $product = Option::create($record);
        session()->flash('class', 'alert-success');
        return back()->withErrors(Lang::get('validation.successupdate'));
    }

    public function edit($option_id){
        $validator = Validator::make([
            'option_id' => $option_id
        ], [
            'option_id' => 'required|numeric|exists:options,id'
        ]);
        if ($validator->fails())
            return abort(500);
        $user = $this->collection->ManagerInformation();
        $option=Option::find($option_id);
        return view('pages.admin.optionEditor', [
            'info' => $user->first(),
            'product_id' => $option->product_id,
            'option' => $option,
            'operation' => "#InsertOperation"
        ]);
    }

    public function update(Request $request,Option $option){
        $validator = Validator::make([
            'option_id' => $option->id,
            'title' => $request->title,
            'TextValue' => $request->TextValue,
        ], [
            'option_id' => 'required|numeric|exists:options,id',
            'title' => 'required',
            'TextValue' => 'required',
        ]);
        if ($validator->fails())
            return back()->withErrors(Lang::get('validation.ValueIsIncorrenct'))->withInput($request->all());

        $timezon = date('Y-m-d H:i:S');
        $record = [
            'title' => $request->title,
            'value' => $request->TextValue,
            'updated_at' => $timezon
        ];
        $product = Option::where('id',$option->id)->update($record);
        session()->flash('class', 'alert-success');
        return back()->withErrors(Lang::get('validation.successupdate'));
    }
}
