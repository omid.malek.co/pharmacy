<?php

namespace App\Http\Controllers;

use function GuzzleHttp\Psr7\str;
use Illuminate\Http\Request;
use App\Services\Collections\Collection;
use App\Services\Collections\File;
use App\Discount;
use App\Payment;
use App\Categorie;
use Validator;
use Lang;

class DiscountController extends Controller
{
    private $collection;

    private $paginate = 10;


    public function __construct(Collection $collection, File $file)
    {
        $this->collection = $collection;
    }



    public function edit(Request $request)
    {
        $validator = Validator::make([
            'discount_text' => $request->discount_value,
            'payment_id' => $request->payment_id
        ], [
            'discount_text' => 'required|exists:discounts,text',
            'payment_id' => 'required|exists:payments,id'
        ]);
        $MessagesModel = '<h3 class="text-center">کد تخفیف معتبر نیست</h3>';
        if ($validator->fails())
            return response()->json([
                'operation' => 0,
                'MessagesModel' => $MessagesModel,
                'totalSum' => 0
            ], 200);

        $payment_id = $request->payment_id;
        $discount_text = $request->discount_value;
        $today = date('Y-m-d');
        $discount = Discount::where('text', $discount_text)
            ->where('active', 1)
            ->where('start_date', '<=', $today)
            ->where('expire_date', '>', $today)
            ->take(1)->get();

        if ($this->collection->IsArrayEmpty($discount))
            return response()->json([
                'operation' => 0,
                'MessagesModel' => $MessagesModel,
                'totalSum' => 0
            ], 200);

        $paymentObj = Payment::where('id', $payment_id);
        $payment = $paymentObj->first();
        // calculate discount amount
        $totalSum = $this->collection->SetDiscountInPayment($discount, $paymentObj, $payment);
        $totalSum = $totalSum . ' ' . 'تومان';
        $MessagesModel = '<h3 class="text-center">کد تخفیف اعمال شد</h3>';
        return response()->json([
            'operation' => 1,
            'MessagesModel' => $MessagesModel,
            'totalSum' => $totalSum
        ], 200);
    }

    public function destroy($discount_id)
    {
        Discount::where('id', $discount_id)->update('active', 0);
        return back();
    }
}
