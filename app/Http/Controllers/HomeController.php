<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\Collections\Collection;
use App\Services\Collections\File;
use App\Image;
use App\Categorie;
use App\Product;
use App\User;
use App\Post;

class HomeController extends Controller
{
    private $collection;
    private $file;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Collection $collection, File $file)
    {
        $this->collection = $collection;
        $this->file = $file;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    /*
    public function index()
    {
        return view('home');
    }
    */


    public function AddImage($arr, $used_in, $directory)
    {
        for ($i = 0; $i < count($arr); $i++) {
            $image = Image::select(['src', 'record_id', 'used_in'])->where('used_in', 'LIKE', '%' . $used_in . '%')
                ->where('record_id', $arr[$i]->id)
                ->where('active', 1)->first();
            $path = $directory . '/' . $image->src;
            array_add($arr[$i], 'image_path', $path);
        }

        return $arr;

    }


    public function index()
    {

        $images = Image::all();
//        $sliders = $images->where('used_in', '=', 'sliders')->where('active',1);
        $slidersQuey = $this->collection->GetSliderPictures();
        $sliders = $slidersQuey->get();
        $sliders = $this->file->SetImagesPath($sliders);
        $user = $this->collection->ManagerInformation();
        $categories = Categorie::select(['id', 'title', 'active'])->where('active', 1)->get();
//        $categories = $this->AddImage($categories, 'categories', 'images');
        $products = Product::select(['title', 'amount', 'id'])->where('active', 1)->orderBy('sales', 'DESC')->get();
        $products = $this->AddImage($products, 'products', 'images');
        $AllProducts = Product::select(['title', 'amount', 'id'])->where('active', 1)->get();
        $rightSlider = Post::where('published', 1)->where('used_in', 'right.slider')->first();
        $slideNumber = 0;
        return view('frontend.index', [
            'sliders' => $sliders,
            'user' => $user,
            'categories' => $categories,
            'products' => $products,
            'AllProducts' => $AllProducts,
            'rightSlider' => $rightSlider,
            'slideNumber' => $slideNumber
        ]);
    }

    public function regulations()
    {
        return 'regulation';
    }

    public function AcountInformation()
    {
        $images = Image::all();
        $sliders = $images->where('used_in', '=', 'sliders');
//        $user = User::first()->get(['fullname', 'phone', 'email', 'address', 'number_cart', 'number_account']);

        $user = $this->collection->ManagerInformation();
        $categories = Categorie::select(['id', 'title', 'active'])->where('active', 1)->get();
        $categories = $this->AddImage($categories, 'categories', 'images');

        $products = Product::select(['title', 'amount', 'id'])->where('active', 1)->orderBy('sales', 'DESC')->get();
        $products = $this->AddImage($products, 'products', 'images');

        $AllProducts = Product::select(['title', 'amount', 'id'])->where('active', 1)->get();
        $rightSlider = Post::where('published', 1)->where('used_in', 'right.slider')->first();

        $slideNumber = 0;
        return view('frontend.AccountBankShow', [
            'sliders' => $sliders,
            'user' => $user,
            'categories' => $categories,
            'products' => $products,
            'AllProducts' => $AllProducts,
            'rightSlider' => $rightSlider,
            'slideNumber' =>$slideNumber
        ]);
    }
}
