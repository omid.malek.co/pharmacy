<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\Collections\Collection;
use App\Services\Collections\File;
use Validator;
use Lang;
use App\Image;
use App\Categorie;
use App\Product;
use App\User;
use App\Customer;
use App\Factor;
use App\Payment;
use App\Post;

class PaymentsController extends Controller
{
    private $collection;

    private $file;

    public function __construct(Collection $collection, File $file)
    {
        $this->collection = $collection;
        $this->file = $file;
    }

    public function paymentRecord($request, $payment)
    {
        $reference_code = $this->collection->trackerCodeCreator(8, 'payments', 'reference_code');
        $total_sum = (double)$payment->total_sum;
        $paymentStatus=$this->collection->PaymentStatusAfterPurchase();
        $conditionNotSent=$this->collection->conditionNotSent();
        $SuccessStatus=100;
        if ($total_sum == 0)
            $record = [
                'receiver_fullname' => $request['receiver_fullname'],
                'sent_address' => $request['sent_address'],
                'sent_phone' => $request['sent_phone'],
                'postal_code_sent' => $request['postal_code_sent'],
                'reference_code' => $reference_code,
                'payment_status'=>$paymentStatus,
                'payment_message_code'=>$SuccessStatus,
                'payment_message'=>$this->collection->ZarinPalStatusMessage($SuccessStatus),
                'condition_id'=>$conditionNotSent->id,
            ];
        else
            $record = [
                'receiver_fullname' => $request['receiver_fullname'],
                'sent_address' => $request['sent_address'],
                'sent_phone' => $request['sent_phone'],
                'postal_code_sent' => $request['postal_code_sent'],
                'reference_code' => $reference_code,
                'condition_id'=>$conditionNotSent->id
            ];

       return $record;
    }

    public function show($payment_id)
    {
        $customer_id = $this->collection->getCustomerId();
//        ->where('payment_status', 1)
//        ->where('active', 1)
        $paymentObj = Payment::select(['id', 'receiver_fullname',
            'discount_value', 'sent_address',
            'sent_phone', 'postal_code_sent', 'customer_id', 'payment_amount', 'discount_value'
        ])->where('id', $payment_id)
            ->with(['customer:id,fullname,address,postal_code,phone']);

        $payment = $paymentObj->get();
//        $count = count($factorRecords);

        $fullname = $payment->first()->customer->fullname;
        $postal_code = $payment->first()->customer->postal_code;
        $address = $payment->first()->customer->address;
        $phone = $payment->first()->customer->phone;

        if ($fullname != "null")
            $payment->first()->receiver_fullname = $fullname;
        else
            $payment->first()->receiver_fullname = '';

        if ($postal_code != "null")
            $payment->first()->postal_code_sent = $postal_code;
        else
            $payment->first()->postal_code_sent = '';

        if ($address != "null")
            $payment->first()->sent_address = $address;
        else
            $payment->first()->sent_address = '';

        if ($phone != "null")
            $payment->first()->sent_phone = $phone;
        else
            $payment->first()->sent_phone = '';

        $sliders = $this->collection->GetSliderPictures();
        $sliders = $this->file->SetImagesPath($sliders->get(), 'sliders');
        $user = $this->collection->ManagerInformation();
        $categories = Categorie::select(['id', 'title', 'active'])->where('active', 1)->get();
        $categories = $this->collection->AddImage($categories, 'categories', 'images');
        $products = Product::select(['title', 'amount', 'id'])->where('active', 1)->orderBy('sales', 'DESC')->get();
        $products = $this->collection->AddImage($products, 'products', 'images');
        $AllProducts = Product::select(['title', 'amount', 'id'])->where('active', 1)->get();
        $row = 1;

//        return $factorRecords;
        return view('frontend.SetReceiverInformation', [
            'sliders' => $sliders,
            'products' => $products,
            'AllProducts' => $AllProducts,
            'user' => $user,
            'categories' => $categories,
            'payment' => $payment->first(),
        ]);
    }

    public function update(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'payment_id' => 'required|numeric|exists:payments,id',
            'receiver_fullname' => 'required',
            'sent_address' => 'required',
            'sent_phone' => 'required|numeric',
            'postal_code_sent' => 'required|numeric'
        ]);

        session()->flash('class', 'alert-danger');
        if ($validator->fails())
            return back()->withErrors($validator->errors())->withInput($request->all());

        $payment_id = $request->payment_id;
        $paymentQuery = Payment::where('id', $payment_id);
        $payment = $paymentQuery->first();
        $total_sum = (double)$payment->total_sum;
        $record=$this->paymentRecord($request->all(),$payment);
        $paymentQuery->update($record);
        if ($total_sum == 0)
            return redirect()->route('receipt.show',['reference'=>$record['reference_code']]);

        return redirect()->route('payment.zarinpal', ['payment_id' => $payment_id]);
    }
}
