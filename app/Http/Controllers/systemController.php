<?php

namespace App\Http\Controllers;

use App\Permission;
use Illuminate\Http\Request;
use App\Services\Collections\Collection;
use App\Services\Collections\File;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Http\Middleware\isValideCustomer;
use App\Document;
use App\Categorie;
use App\Image;
use App\User;
use Carbon\Carbon;
class systemController extends Controller
{
    //--
    private $file;
    private $collection;
    public function __construct(Collection $collection, File $file)
    {
        $this->collection = $collection;
        $this->file = $file;
    }
    //--  /usr/local/bin/ea-php72 /home/coinbite/source/artisan schedule:run >> /dev/null 2>&1
    //--
    public function removeFile(){

        $currentTime=Carbon::now();
        $document=Document::where('active',0);
        $documentRecord=$document->get();
        if(!$this->collection->IsArrayEmpty($documentRecord)){
            for ($i=0;$i<count($documentRecord);$i++){
                $date=$documentRecord[$i]->updated_at;
                $dateObj=$this->collection->SetCarbonObjectWithTime($date);
                $ExpireDaysHavePassed=$this->collection->ExpireDaysHavePassed($dateObj,1);
                if($ExpireDaysHavePassed){
                    $this->file->RemoveImage($documentRecord[$i]->src);
                    $document->delete();
                }
            }

        }
        return 1;
    }
}
