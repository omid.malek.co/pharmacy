<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Storage;
use App\Services\Collections\File;
use Illuminate\Support\Arr;
use App\Categorie;
use Illuminate\Http\Request;
use App\Image;
use App\User;
use App\Services\Collections\Collection;
use Validator;
use Lang;

class ImagesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private $paginate = 16;
    protected $collection;
    protected $file;

    public function __construct(Collection $collection, File $file)
    {
        $this->collection = $collection;
        $this->file = $file;
    }


    public function TitleForIMGCategory($used_in)
    {
        $title = '';
        switch ($used_in) {
            case 'sliders':
                $title = 'اسلایدر صفحه اصلی';
                break;
            case 'articles':
                $title = 'تصاویر مقالات';
                break;
            case 'categories':
                $title = 'آیکن دسته بندی ها';
                break;
            case 'products':
                $title = 'لوگوی مجموعه ها';
                break;
            case 'products':
                $title = 'اسلایدر مجموعه ها';
                break;
            default:
                $title = 'تعریف نشده';
                break;
        }
        return $title;
    }

    public function IndexSliderCategorie()
    {

        $used_in = Image::where('record_id', '=', 0)
            ->groupBy('used_in')->distinct()->get(['used_in']);
        return $used_in;

    }

    public function CheckImagesCategorie()
    {

        $used_in = Image::where('record_id', '!=', 0)->groupBy('used_in')->distinct()->get(['used_in']);
        return $used_in;

    }

    public function AllImagesCategorie()
    {
        $used_in = Image::groupBy('used_in')->distinct()->get(['used_in']);
        return $used_in;
    }

    public function GetImagesCategorie($page)
    {
        if ($page == 'list') {
            $categorie = $this->AllImagesCategorie();
        } else if ($page == 'sliders') {
            $categorie = $this->IndexSliderCategorie();
        } else {
            $categorie = $this->CheckImagesCategorie();
        }
        for ($i = 0; $i < count($categorie); $i++) {
            array_add($categorie[$i], 'title', $this->TitleForIMGCategory($categorie[$i]->used_in));
        }
        return $categorie;
    }

    public function Getdirectory($used_in)
    {
        $path = '';
        switch ($used_in) {
            case 'sliders':
                $path = 'images';
                break;
            case 'categories':
                $path = 'images';
                break;
            case 'articles':
                $path = 'images';
                break;
            case 'products':
                $path = 'images';
                break;
            case 'products/products':
                $path = 'images';
                break;
        }
        return $path;
    }

    public function CheckEmptyArr($record, $arr)
    {
        if ($record == json_encode([]) || empty($record)) {
            return '';
        } else {
            return $arr;
        }

    }

    public function index($used)
    {
        $admin = User::find(session()->get('id'));

        $testrecord = Image::where('used_in', '=', $used)->take(1)->get(['id']);

        $ImagesCategorie = $this->GetImagesCategorie('list');

        $images = Image::where('used_in', '=', $used)->paginate($this->paginate);

        for ($i = 0; $i < count($images); $i++) {
            $path = $this->Getdirectory($images[$i]->used_in);
            array_add($images[$i], 'image_path', $path . '/' . $images[$i]->src);
        }

        $images = $this->CheckEmptyArr($testrecord, $images);
        return view('pages.admin.ImagesList', [
            'info' => $admin,
            'page' => '#Images',
            'operation' => '#List',
            'images' => $images,
            'paginate' => $this->paginate,
            'ImagesCategorie' => $ImagesCategorie,
            'used' => $used
        ]);
    }

    public function imageIndexSlider()
    {
        $admin = Admin::find(session()->get('id'));


        $ImagesCategorie = $this->GetImagesCategorie('sliders');

        $categories = Categorie::where('visible', '=', 1)->get(['id', 'title']);

        return view('pages.admin.InsertGeneralImage', [
            'info' => $admin,
            'page' => '#Images',
            'operation' => '#InsertSlider',
            'categoriesUser' => $categories,
            'categoriesImages' => $ImagesCategorie,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $admin = User::find(session()->get('id'));


        $ImagesCategorie = $this->GetImagesCategorie('combo');

        $categories = Categorie::where('active', '=', 1)->get(['id', 'title']);

        return view('pages.admin.InsertImage', [
            'info' => $admin,
            'page' => '#Images',
            'operation' => '#Insert',
            'categoriesUser' => $categories,
            'paginate' => $this->paginate,
            'categoriesImages' => $ImagesCategorie,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'categorie' => 'required|numeric|exists:categories,id',
            'file' => 'required|image',
        ]);
        if ($validator->fails()) {
            session()->flash('class', 'alert-danger');
            return back()->withErrors($validator->errors());
        } else {

            $imageDB = Image::where('used_in', '=', $request->CategoryImages)
                ->where('record_id', '=', $request->title)->get();
            if (Arr::has($imageDB->first(), 'id') && $request->CategoryImages != 'slider.post') {
                $path = $this->Getdirectory($request->CategoryImages);
//                unlink($path.'/'.$imageDB->first()->src);
                $imageDB->first()->delete();
            }

            $image = $request->file('file');
            $input['imagename'] = time() . rand(10, 1000) . '.' . $image->getClientOriginalExtension();
            $destinationPath = 'images';
            $image->move($destinationPath, $input['imagename']);
            $data = [
                'src' => $input['imagename'],
                'record_id' => $request->title,
                'used_in' => $request->CategoryImages,
            ];
            if ($request->CategoryImages == 'slider.post') {
                Image::create($data);
            } else {
                Image::firstOrCreate($data);
            }
            return back()->withErrors(Lang::get('validation.successupdate'));
        }

    }

    public function storeSliderIndex(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'CategoryImages' => 'required',
            'file' => 'required|image',
        ]);
        if ($validator->fails()) {
            session()->flash('class', 'alert-danger');
            return back()->withErrors($validator->errors());
        } else {

            $image = $request->file('file');
            $input['imagename'] = time() . rand(10, 1000) . '.' . $image->getClientOriginalExtension();
            $destinationPath = 'images';
            $image->move($destinationPath, $input['imagename']);
            $data = [
                'src' => $input['imagename'],
                'record_id' => 0,
                'used_in' => $request->CategoryImages,
            ];
            Image::firstOrCreate($data);
            return back()->withErrors(Lang::get('validation.successupdate'));
        }
    }

    public function editArticlesIcon(Image $image){
        $user = $this->collection->ManagerInformation();
        $sliders = $this->collection->GetSliderPictures();
        $sliders = $this->file->SetImagesPath($sliders->get(), 'sliders');
//        $collection = Product::select(['id', 'title', 'categorie_id', 'number', 'description', 'sales', 'amount', 'active'])
//            ->where('active', '=', 1)
//            ->with(['categorie:title,id'])->orderBy('updated_at', 'DESC')->paginate($this->paginate);

        $categories = Categorie::select(['id', 'title', 'active'])->where('active', 1)->get();
        $categories = $this->collection->AddImage($categories, 'categories', 'images');


        $image = $this->file->SetImagesPath([0 => $image], 'products');


        return view('pages.admin.IconArticles', [
            'sliders' => $sliders,
            'info' => $user->first(),
            'categories' => $categories,
            'paginate' => $this->paginate,
            'operation' => "#InsertOperation",
            'content' => $image[0]
        ]);


    }

    public function updateArticlesIcon(Image $image, Request $request){
        $validator = Validator::make([
            'image_id' => $image->id,
            'file' => $request->file
        ], [
            'image_id' => 'required|numeric|exists:images,id',
            'file' => 'required|image',
        ]);
        session()->flash('class', 'alert-danger');
        if ($validator->fails())
            return response()->json(['file' => 'not validate'], 200);


        $image_id = $image->id;
        $image = Image::where('id', $image_id)->where('active', 1);

        $destinationPath = $this->file->Getdirectory();

        $extension = $request->file('file')->getClientOriginalExtension();

        $fileName = 'articles_' . time() . rand(10, 100) . '.' . $extension;

        $request->file('file')->move($destinationPath, $fileName);

        $record = [ 'src' => $fileName ];
        $image->update($record);

        $file_path = $destinationPath . '/' . $fileName;

        $file_path = asset($file_path);

        return response()->json(['file' => $file_path], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $image = Image::where('id', '=', $id)->take(1)->get();
        $this->file->RemoveImage($image->first()->src);
        $image->first()->delete();
        return back();
    }
}
