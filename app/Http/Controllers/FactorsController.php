<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\Collections\Collection;
use App\Services\Collections\File;
use Validator;
use Lang;
use App\Image;
use App\Categorie;
use App\Product;
use App\User;
use App\Customer;
use App\Factor;
use App\Payment;
use App\Post;

class FactorsController extends Controller
{
    private $collection;


    public function __construct(Collection $collection, File $file)
    {
        $this->collection = $collection;
        $this->file = $file;
    }

    public function CreateOrFindPayment($customer_id, $active_column, $payment_status)
    {

        $paymentObj = Payment::where('active', $active_column)
            ->where('customer_id', $customer_id)
            ->where('payment_status', $payment_status);

        $data = $paymentObj->get();
        $type = $this->collection->purchaseType();
        $record = [
            'customer_id' => $customer_id,
            'type_id' => $type->id,
            'payment_status' => $payment_status,
            'active' => $active_column
        ];
        if (count($data) > 0)
            return $data->first();

        return $payment = Payment::create($record);
    }

    public function store($product_id)
    {
        $validator = Validator::make(['product_id' => $product_id], [
            'product_id' => 'required|numeric|exists:products,id',
        ]);
        if ($validator->fails()) {
            $MessagesModel = '<h3 class="text-right">خطا در اعتبارسنجی</h3>';
            return response()->json(['MessagesModel' => $MessagesModel], 200);

        } else {
            $MessagesModel = '<h3 class="text-right">لطفا ابتدا وارد سامانه شوید</h3>';
            $customer_id = $this->collection->getCustomerId();
            if (!$customer_id)
                return response()->json(['MessagesModel' => $MessagesModel], 200);
            else {

                $factor = Factor::select(['id'])
                    ->where('product_id', $product_id)
                    ->where('customer_id', $customer_id)
                    ->where('active', 1);
                if (count($factor->get())) {
                    $payment = Payment::select(['payment_status'])
                        ->where('id', $factor->first()->payment_id)
                        ->where('payment_status', 1)->get();
                    if (!$this->collection->IsArrayEmpty($payment))
                        $MessagesModel = '<h3 class="text-right">کالا در سبد خرید شما موجود است</h3>';
                    else {
                        $product = Product::select(['number'])->where('id', $product_id)->where('number', '>', 0);
                        if (count($product->get()) > 0) {
                            $payment = $this->CreateOrFindPayment($customer_id, 1, 1);
                            $record = [
                                'product_id' => $product_id,
                                'customer_id' => $customer_id,
                                'payment_id' => $payment->id,
                                'sent_number' => 1,
                            ];
                            $factor = Factor::create($record);
                            $MessagesModel = '<h3 class="text-right">کالا مورد نظر به سبد خرید اضافه گردید.</h3>';
                        } else
                            $MessagesModel = '<h3 class="text-right">کالا مورد نظر شما موجود نیست</h3>';
                    }
                } else {
                    $product = Product::select(['number'])->where('id', $product_id)->where('number', '>', 0);
                    if (count($product->get()) > 0) {
                        $payment = $this->CreateOrFindPayment($customer_id, 1, 1);
                        $record = [
                            'product_id' => $product_id,
                            'customer_id' => $customer_id,
                            'payment_id' => $payment->id,
                            'sent_number' => 1,
                        ];
                        $factor = Factor::create($record);
                        $MessagesModel = '<h3 class="text-right">کالا مورد نظر به سبد خرید اضافه گردید.</h3>';
                    } else
                        $MessagesModel = '<h3 class="text-right">کالا مورد نظر شما موجود نیست</h3>';
                }
                return response()->json(['MessagesModel' => $MessagesModel], 200);
            }
        }
    }


    public function SubtractionFactorProduct($product_count, $sent_factor)
    {
        $new_count_product = ((integer)$product_count - (integer)$sent_factor);
        if ($new_count_product < 0)
            return -1;
        else
            return $new_count_product;
    }


    public function show()
    {
        $PaymentStatus = $this->collection->PaymentStatusBeforePurchase();
        $customer_id = $this->collection->getCustomerId();
        $paymentQuery = Payment::select(['id'])->where('customer_id', $customer_id)
            ->where('active', 1)
            ->where('payment_status', $PaymentStatus);
        $payment = $paymentQuery->first();
        $sliders = $this->collection->GetSliderPictures();
        $sliders = $this->file->SetImagesPath($sliders->get(), 'sliders');
        $user = $this->collection->ManagerInformation();
        $categories = Categorie::select(['id', 'title', 'active'])->where('active', 1)->get();
        $categories = $this->collection->AddImage($categories, 'categories', 'images');
        $row = 1;
        $rightSlider = Post::where('published', 1)->where('used_in', 'right.slider')->first();

        $products = Product::select(['title', 'amount', 'id'])->where('active', 1)->orderBy('sales', 'DESC')->get();
        $products = $this->collection->AddImage($products, 'products', 'images');
        $AllProducts = Product::select(['title', 'amount', 'id'])->where('active', 1)->get();
        // 'totalSum' => $totalSum,
        // 'factor' => $factorRecords,
        $basketTable = $this->collection->basketTable($customer_id);
        return view('frontend.ShowFactor', [
            'sliders' => $sliders,
            'products' => $products,
            'AllProducts' => $AllProducts,
            'user' => $user,
            'payment' => $payment,
            'basketTable' => $basketTable,
            'categories' => $categories,
            'row' => $row,
            'rightSlider' => $rightSlider
        ]);

    }


    public function destroy($factor_id)
    {
        $validator = Validator::make(['factor_id' => $factor_id], [
            'factor_id' => 'required|numeric|exists:factors,id',
        ]);
        if ($validator->fails()) {
            $MessagesModel = '<h3 class="text-right">خطا در اعتبارسنجی</h3>';
            return response()->json(['MessagesModel' => $MessagesModel], 404);

        } else {
            $MessagesModel = '<h3 class="text-right">لطفا ابتدا وارد سامانه شوید</h3>';
            $customer_id = $this->collection->getCustomerId();
            if (!$customer_id)
                return response()->json(['MessagesModel' => $MessagesModel], 400);
            else {
                $factor = Factor::select(['id'])->where('id', $factor_id);
                if (count($factor->get())) {
                    $factor->delete();
                    $row = "#Row" . $factor_id;
                    $basket_content = $this->collection->basketTable($customer_id);
                    $table_id = "#basketTable";
                    $parent_id = "#FactorsFrame";
                    return response()->json([
                        'row' => $row,
                        'basket_content' => $basket_content,
                        'table_id' => $table_id,
                        'parent_id' => $parent_id
                    ], 200);
                } else {
                    return response()->json(['MessagesModel' => $MessagesModel], 404);
                }

            }
        }
    }


    public function update(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'id' => 'required|numeric|exists:factors,id',
            'count' => 'required|numeric|min:1',
            'product_id' => 'required|numeric|exists:products,id',
        ]);
        $productNotExist = '<h3 class="text-right">این تعداد کالا در سامانه موجود نیست</h3>';
        if ($validator->fails()) {
            $MessagesModel = '<h3 class="text-right">خخطا در اعتبارسنجی</h3>';
            return response()->json(['MessagesModel' => $MessagesModel], 404);

        } else {
            $MessagesModel = '<h3 class="text-right">لطفا ابتدا وارد سامانه شوید</h3>';
            $customer_id = $this->collection->getCustomerId();
            if (!$customer_id)
                return response()->json(['MessagesModel' => $MessagesModel], 400);
            else {
                $factor_id = $request->id;
                $sent_number = $request->count;
                $product_id = $request->product_id;
                $factor = Factor::where('id', $factor_id);
                $product = Product::where('id', $product_id);
                if (count($factor->get())) {
                    $productRecords = $product->get();
                    $product_number = $productRecords->first()->number;
                    $product_exist = $this->SubtractionFactorProduct($product_number, $sent_number);
                    if ($product_exist >= 0) {
                        $factorRecord = [
                            'sent_number' => $sent_number
                        ];
                        $productUpdate = [
                            'number' => $product_exist
                        ];
//                        $product->update($productUpdate);
//                        $factor->update($factorRecord);
                        $row = $factor_id;
                        return response()->json(['MessagesModel' => $row, 'operation' => 1], 200);

                    } else {

                        $factorRecord = [
                            'sent_number' => $product_number
                        ];
                        $productUpdate = [
                            'number' => 0
                        ];
//                        $product->update($productUpdate);
//                        $factor->update($factorRecord);
                        return response()->json(['MessagesModel' => $productNotExist, 'operation' => 0, 'numberExistProduct' => $product_number], 200);
                    }
                } else return response()->json(['MessagesModel' => 'factor_not_exist'], 404);
            }
            return response()->json(['MessagesModel' => $productNotExist], 404);
        }
    }


    public function index()
    {

    }

}
