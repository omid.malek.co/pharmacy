<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\Collections\Collection;
use App\Services\Collections\File;
use App\Discount;
use App\Categorie;
use Validator;
use Lang;

class DiscountManagement extends Controller
{
    private $collection;
    private $file;


    private $paginate = 10;


    public function __construct(Collection $collection, File $file)
    {
        $this->collection = $collection;
        $this->file = $file;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = $this->collection->ManagerInformation();
        $sliders = $this->collection->GetSliderPictures();
        $sliders = $this->file->SetImagesPath($sliders->get(), 'sliders');
        $collectionObj = Discount::select([
            'id', 'text', 'percent',
            'start_date', 'expire_date', 'active'
        ])->where('active', '=', 1)
            ->orderBy('updated_at', 'DESC');

        $collection = $collectionObj->paginate($this->paginate);
        $isEmpty = $this->collection->IsArrayEmpty($collectionObj->get());
        $categories = Categorie::select(['id', 'title', 'active'])->where('active', 1)->get();
        $categories = $this->collection->AddImage($categories, 'categories', 'images');

        $row = $collection->firstItem();

        return view('pages.admin.discountsList', [
            'sliders' => $sliders,
            'info' => $user->first(),
            'categories' => $categories,
            'collections' => $collection,
            'paginate' => $this->paginate,
            'row' => $row,
            'operation' => "#ListOperation",
            'isEmpty' => $isEmpty
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = $this->collection->ManagerInformation();
        $sliders = $this->collection->GetSliderPictures();
        $sliders = $this->file->SetImagesPath($sliders->get(), 'sliders');
//        $collection = Product::select(['id', 'title', 'categorie_id', 'number', 'description', 'sales', 'amount', 'active'])
//            ->where('active', '=', 1)
//            ->with(['categorie:title,id'])
//            ->orderBy('updated_at', 'DESC')->paginate($this->paginate);

        $categories = Categorie::select(['id', 'title', 'active'])->where('active', 1)->get();
        $categories = $this->collection->AddImage($categories, 'categories', 'images');

        return view('pages.admin.discountCreatore', [
            'sliders' => $sliders,
            'info' => $user->first(),
            'categories' => $categories,
            'paginate' => $this->paginate,
            'operation' => "#InsertOperation"
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'percent' => 'required|numeric',
            'expire_date' => 'required',
            'start_date' => 'required',
            'text' => 'required|unique:discounts,text',
        ]);

        session()->flash('class', 'alert-danger');
        if ($validator->fails())
            return back()->withErrors($validator->errors())->withInput($request->all());

        $gregorian_today = date('Y-m-d');
        $start_date = $request->start_date;
        $expire_date = $request->expire_date;
        $gregorian_start_date = $this->collection->ConvertJalaliToGregorian('Y-m-d', $start_date);
        $gregorian_expire_date = $this->collection->ConvertJalaliToGregorian('Y-m-d', $expire_date);

        if ($gregorian_start_date > $gregorian_expire_date || $gregorian_today >= $gregorian_expire_date)
            return back()->withErrors(Lang::get('validation.ValueIsIncorrenct'))->withInput($request->all());

        $timezon = date('Y-m-d H:i:S');
        $record = [
            'text' => $request->text,
            'percent' => $request->percent,
            'start_date' => $gregorian_start_date,
            'expire_date' => $gregorian_expire_date,
            'created_at' => $timezon,
            'updated_at' => $timezon
        ];
        $discount = Discount::create($record);
        return redirect()->route('discounts.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($discount_id)
    {
        Discount::where('id', $discount_id)->update(['active' => 0]);
        return back();
    }
}
