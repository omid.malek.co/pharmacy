<?php

namespace App\Http\Controllers;

use App\Image;
use App\Product;
use App\Services\Collections\File;
use App\User;
use Illuminate\Http\Request;
use App\Services\Collections\Collection;
use App\Http\Middleware\isValideCustomer;
use App\Categorie;
use Validator;
use Lang;

class ProductsManagement extends Controller
{

    private $collection;
    private $file;

    private $used_in = 'products';

    private $paginate = 10;


    public function __construct(Collection $collection, File $file)
    {
        $this->middleware([IsValideCustomer::class]);
        $this->collection = $collection;
        $this->file = $file;
    }

    public function index()
    {
        $user = $this->collection->ManagerInformation();
        $sliders = $this->collection->GetSliderPictures();
        $sliders = $this->file->SetImagesPath($sliders->get(), 'sliders');
        $collection = Product::select(['id', 'title', 'categorie_id', 'number', 'description', 'sales', 'amount', 'active'])
            ->where('active', '=', 1)
            ->with(['categorie:title,id'])->orderBy('updated_at', 'DESC')->paginate($this->paginate);

        $categories = Categorie::select(['id', 'title', 'active'])->where('active', 1)->get();
        $categories = $this->collection->AddImage($categories, 'categories', 'images');

        $row = 1;

        return view('pages.admin.ListCollections', [
            'sliders' => $sliders,
            'info' => $user->first(),
            'categories' => $categories,
            'collections' => $collection,
            'paginate' => $this->paginate,
            'row' => $row,
            'operation' => "#ListOperation"
        ]);
    }


    public function create()
    {
        $user = $this->collection->ManagerInformation();
        $sliders = $this->collection->GetSliderPictures();
        $sliders = $this->file->SetImagesPath($sliders->get(), 'sliders');
        $collection = Product::select(['id', 'title', 'categorie_id', 'number', 'description', 'sales', 'amount', 'active'])
            ->where('active', '=', 1)
            ->with(['categorie:title,id'])
            ->orderBy('updated_at', 'DESC')->paginate($this->paginate);

        $categories = Categorie::select(['id', 'title', 'active'])->where('active', 1)->get();
        $categories = $this->collection->AddImage($categories, 'categories', 'images');

        return view('pages.admin.CreateCollections', [
            'sliders' => $sliders,
            'info' => $user->first(),
            'categories' => $categories,
            'collections' => $collection,
            'paginate' => $this->paginate,
            'operation' => "#InsertOperation"
        ]);


    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'number' => 'required|numeric',
            'categorie_id' => 'required|numeric|exists:categories,id',
            'title' => 'required',
            'amount' => 'required|numeric',
        ]);
        $description = $request->input('description', 'بدون توضیحات');
        if ($validator->fails()) {
            session()->flash('class', 'alert-danger');
            return back()->withErrors($validator->errors())->withInput($request->all());
        } else {
            $timezon = date('Y-m-d H:i:S');
            $record = [
                'number' => $request->number,
                'categorie_id' => $request->categorie_id,
                'title' => $request->title,
                'amount' => $request->amount,
                'description' => $description,
                'created_at' => $timezon,
                'updated_at' => $timezon
            ];
            $product = Product::create($record);

            $image_record = [
                'src' => 'noPhoto.jpg',
                'record_id' => $product->id,
                'used_in' => 'products',
                'created_at' => $timezon,
                'updated_at' => $timezon
            ];

            $image = Image::create($image_record);

            return redirect()->route('admin.products.image', ['image_id' => $image->id]);

        }


    }

    public function destroy($product_id)
    {
        $product = Product::where('id', $product_id);

        $imageObj = Image::where('used_in', $this->used_in)
            ->where('record_id', $product_id);

        $image = $imageObj->get();

        if (count($image) > 0)
            $imageObj->update(['active' => 0]);
//
        $product->update(['active' => 0]);

        $row = "#Row" . $product_id;
        return redirect()->route('admin.products.index');
    }

    public function edit($product_id)
    {
        $validator = Validator::make([
            'product_id' => $product_id
        ], [
            'product_id' => 'required|numeric|exists:products,id',
        ]);
        session()->flash('class', 'alert-danger');
        if ($validator->fails())
            return redirect()->route('admin.logout');

        $user = $this->collection->ManagerInformation();
        $sliders = $this->collection->GetSliderPictures();
        $sliders = $this->file->SetImagesPath($sliders->get(), 'sliders');

        $collection = Product::select(['id', 'title', 'categorie_id', 'number', 'description', 'sales', 'amount', 'active'])
            ->where('active', '=', 1)
            ->with(['categorie:title,id'])
            ->orderBy('updated_at', 'DESC')->paginate($this->paginate);

        $categories = Categorie::select(['id', 'title', 'active'])->where('active', 1)->get();
        $categories = $this->collection->AddImage($categories, 'categories', 'images');

        $product = Product::select([
            'title', 'number', 'amount',
            'id','categorie_id', 'description'
        ])->where('id', $product_id);
        return view('pages.admin.EditCollections', [
            'sliders' => $sliders,
            'info' => $user->first(),
            'categories' => $categories,
            'collections' => $collection,
            'paginate' => $this->paginate,
            'product' => $product->first(),
            'operation' => "#InsertOperation"
        ]);

    }

    public function update(Request $request,$product_id){
        $validator = Validator::make($request->all(), [
            'number' => 'required|numeric',
            'categorie_id' => 'required|numeric|exists:categories,id',
            'title' => 'required',
            'amount' => 'required|numeric',
        ]);
        $description = $request->input('description', 'بدون توضیحات');
        session()->flash('class', 'alert-danger');
        if ($validator->fails())
            return back()->withErrors($validator->errors())->withInput($request->all());

        $timezon = date('Y-m-d H:i:S');
        $record = [
            'number' => $request->number,
            'categorie_id' => $request->categorie_id,
            'title' => $request->title,
            'amount' => $request->amount,
            'description' => $description,
            'updated_at' => $timezon
        ];
        $product = Product::where('id',$product_id);
        $product->update($record);

        $imageObj=Image::where('record_id',$product_id)
            ->where('used_in','LIKE','%products%')
            ->where('active',1);

        $image=$imageObj->first();

        return redirect()->route('admin.products.image', ['image_id' => $image->id]);

    }


    public function editImage(Image $image)
    {

        $user = $this->collection->ManagerInformation();
        $sliders = $this->collection->GetSliderPictures();
        $sliders = $this->file->SetImagesPath($sliders->get(), 'sliders');
        $collection = Product::select(['id', 'title', 'categorie_id', 'number', 'description', 'sales', 'amount', 'active'])
            ->where('active', '=', 1)
            ->with(['categorie:title,id'])->orderBy('updated_at', 'DESC')->paginate($this->paginate);

        $categories = Categorie::select(['id', 'title', 'active'])->where('active', 1)->get();
        $categories = $this->collection->AddImage($categories, 'categories', 'images');


        $image = $this->file->SetImagesPath([0 => $image], 'products');


        return view('pages.admin.EditProductsImage', [
            'sliders' => $sliders,
            'info' => $user->first(),
            'categories' => $categories,
            'collections' => $collection,
            'paginate' => $this->paginate,
            'operation' => "#InsertOperation",
            'content' => $image[0]
        ]);

    }


    public function updateImage(Image $image, Request $request)
    {
        $validator = Validator::make([
            'image_id' => $image->id,
            'file' => $request->file
        ], [
            'image_id' => 'required|numeric|exists:images,id',
            'file' => 'required|image',
        ]);
        session()->flash('class', 'alert-danger');
        if ($validator->fails())
            return response()->json(['file' => 'not validate'], 200);


        $image_id = $image->id;
        $image = Image::where('id', $image_id)->where('active', 1);

        $destinationPath = $this->file->Getdirectory();

        $extension = $request->file('file')->getClientOriginalExtension();

        $fileName = 'pharmacy_products_' . time() . rand(10, 100) . '.' . $extension;

        $request->file('file')->move($destinationPath, $fileName);

        $record = [ 'src' => $fileName ];
        $image->update($record);

        $file_path = $destinationPath . '/' . $fileName;

        $file_path = asset($file_path);

        return response()->json(['file' => $file_path], 200);

    }

}
