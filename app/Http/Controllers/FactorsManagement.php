<?php

namespace App\Http\Controllers;

use App\Factor;
use App\Image;
use App\Payment;
use App\Product;
use App\Services\Collections\File;
use App\Type;
use App\User;
use Illuminate\Http\Request;
use App\Services\Collections\Collection;
use App\Http\Middleware\isValideCustomer;
use App\Categorie;
use Validator;
use Lang;

class FactorsManagement extends Controller
{
    private $collection;
    private $file;

    private $used_in = 'products';

    private $paginate = 10;

    public function __construct(Collection $collection, File $file)
    {
        $this->middleware([IsValideCustomer::class]);
        $this->collection = $collection;
        $this->file = $file;
    }

    public function index()
    {
        $user = $this->collection->ManagerInformation();
        $sliders = $this->collection->GetSliderPictures();
        $sliders = $this->file->SetImagesPath($sliders->get(), 'sliders');
        $PaymentStatus = $this->collection->PaymentStatusAfterPurchase();
        $type = $this->collection->purchaseType();
        $payments = Payment::select([
            'id', 'customer_id', 'condition_id', 'reference_code', 'created_at', 'active'
        ])->where('active', '!=', 0)
            ->where('payment_status', '=', $PaymentStatus)
            ->where('type_id', $type->id)
            ->orderBy('condition_id', 'ASC')
            ->orderBy('created_at', 'DESC')
            ->with(['customer:id,fullname,phone', 'condition:id,title']);

        $recordsPayment = $payments->paginate($this->paginate);
        for ($i = 0; $i < count($recordsPayment); $i++) {
            $created_at = $recordsPayment[$i]->created_at;
            $recordsPayment[$i]->persian_date = $this->collection->getJalaliDate($created_at);
        }

        $IsArrayEmpty = $this->collection->IsArrayEmpty($payments->get());
        $categories = Categorie::select(['id', 'title', 'active'])->where('active', 1)->get();
        $categories = $this->collection->AddImage($categories, 'categories', 'images');
        $row = 1;

        return view('pages.admin.PaymentsList', [
            'sliders' => $sliders,
            'info' => $user->first(),
            'categories' => $categories,
            'payments' => $recordsPayment,
            'IsArrayEmpty' => $IsArrayEmpty,
            'paginate' => $this->paginate,
            'row' => $row,
            'operation' => "#ListOperation"
        ]);
    }

    public function show($payment_id)
    {
        $user = $this->collection->ManagerInformation();
        $sliders = $this->collection->GetSliderPictures();
        $sliders = $this->file->SetImagesPath($sliders->get(), 'sliders');
        $categories = Categorie::select(['id', 'title', 'active'])->where('active', 1)->get();
        $categories = $this->collection->AddImage($categories, 'categories', 'images');

        $paymentsObj = Payment::where('id', $payment_id)
            ->with(['customer:id,fullname,email,address,postal_code,phone']);
        $payments = $paymentsObj->get();

        $factors = Factor::where('payment_id', $payment_id)->with('product:id,title,amount');
//        $paymentDone = $payments->first()->payment_amount - $payments->first()->discount_value;
        $paymentDone = $payments->first()->total_sum;
        $payments->first()->payment_done = $paymentDone;
//        $factors=$this->collection->GetFactorsPayment($payments,'payment_amount');
        $IsArrayEmpty = $this->collection->IsArrayEmpty($payments);
        $persian_date = $this->collection->getJalaliDate($payments->first()->updated_at);
        $payments->first()->persian_date = $persian_date;
        $row = 1;

        return view('pages.admin.PaymentsShow', [
            'sliders' => $sliders,
            'info' => $user->first(),
            'categories' => $categories,
            'payments' => $payments->first(),
            'factors' => $factors->get(),
            'IsArrayEmpty' => $IsArrayEmpty,
            'row' => $row,
            'operation' => "#ListOperation"
        ]);
    }

    public function sentProduct($payment_id)
    {
        $SentCondition = $this->collection->conditionSent();
        $paymentQuery = Payment::where('id', $payment_id);
        $paymentQuery->update(['condition_id' => $SentCondition->id]);
        return back();
    }
}
