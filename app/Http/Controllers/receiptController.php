<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\Collections\Collection;
use App\Services\Collections\File;
use Validator;
use Lang;
use App\Image;
use App\Categorie;
use App\Product;
use App\User;
use App\Customer;
use App\Factor;
use App\Payment;
use App\Post;

class receiptController extends Controller
{


    private $collection;

    private $file;

    public function __construct(Collection $collection, File $file)
    {
        $this->collection = $collection;
        $this->file = $file;
    }

    public function show($reference)
    {
        $validator = Validator::make([
            'reference_code' => $reference
        ], [
            'reference_code' => 'required|numeric|exists:payments,reference_code',
        ]);
        if ($validator->fails())
            return redirect()->route('customer.logOut');

        $sliders = $this->collection->GetSliderPictures();
        $sliders = $this->file->SetImagesPath($sliders->get(), 'sliders');
        $user = $this->collection->ManagerInformation();

        $categories = Categorie::select(['id', 'title', 'active'])->where('active', 1)->get();
        $categories = $this->collection->AddImage($categories, 'categories', 'images');

        $row = 1;
        $rightSlider = Post::where('published', 1)->where('used_in', 'right.slider')->first();

        $paymentObj = Payment::where('reference_code', $reference);
        $payment = $paymentObj->first();
        $products = Product::select(['title', 'amount', 'id'])->where('active', 1)->orderBy('sales', 'DESC')->get();

        $products = $this->collection->AddImage($products, 'products', 'images');
        $AllProducts = Product::select(['title', 'amount', 'id'])->where('active', 1)->get();
        $this->collection->ProductRemaining($payment->id);

        $factor = Factor::select(['id',
            'product_id', 'payment_id', 'customer_id', 'sent_number'
        ])->where('payment_id', $payment->id)
            ->with(['product:id,title,amount,number']);
//        'payment:id,receiver_fullname,postal_code_sent,payment_amount,sent_phone,sent_address'
        $factorRecords = $factor->get();
        $count = count($factorRecords);
        if ($this->collection->IsArrayEmpty($factorRecords)) {
            $paymentAmount = 0;
            for ($i = 0; $i < $count; $i++) {
                $paymentAmount = $this->collection->CalculatePaymentValue(
                    $factorRecords[$i]->product->amount,
                    $factorRecords[$i]->product->discount_value,
                    $factorRecords[$i]->sent_number
                );
                $factorRecords[$i]->payment_amount = $paymentAmount;
            }
            $totalSum = $this->collection->GetFactorsPayment($factorRecords, 'payment_amount');
        }
        // 'totalSum' => $totalSum,
        // 'factor' => $factorRecords,
        $slideNumber = 0;
        $basketTable = $this->collection->receiptPurchase($payment->id);
        return view('frontend.Receipt', [
            'sliders' => $sliders,
            'user' => $user,
            'factor' => $factorRecords,
            'basketTable' => $basketTable,
            'categories' => $categories,
            'row' => $row,
            'rightSlider' => $rightSlider,
            'products' => $products,
            'AllProducts' => $AllProducts,
            'slideNumber' => $slideNumber,
        ]);
//        return $payment;
    }
}
