<?php

namespace App\Http\Controllers;
//namespace App\Http\Controllers\Auth;
//use App/Http/Controllers/Auth/AuthController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Contracts\Auth\Authenticatable;
use App\Services\Collections\Collection;
use App\User;
use Validator;
use Lang;


class SignInController extends Controller
{
    use AuthenticatesUsers;
    public function __construct(Collection $collection)
    {
        $this->collection = $collection;
    }


    public function LoginPage()
    {
        return view('pages.login');
    }

    public function TableCount($table_name)
    {
        $table = DB::table($table_name)->select(DB::raw('count(*) as ' . $table_name . '_count'))->get();
        return $table;
    }

    public function Signin(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|exists:users,username',
            'password' => 'required'
        ]);
        $password = md5($request->password);
        if ($validator->fails()) {
//            return '1';
            session()->flash('class', 'alert-danger');
            return back()->withErrors($validator->errors())->withInput(['username']);
        } else {

            $this->validateLogin($request);

//            if ($this->hasTooManyLoginAttempts($request)) {
//                $this->fireLockoutEvent($request);
//
//               $this->sendLockoutResponse($request);
// --           }
// --           return '2';
// --
            $password = md5($request->password);

           $admin = User::where('username', '=', $request->email)
                ->where('password', '=', $password)
                ->take(1)->get(['id', 'fullname']);
            if (empty($admin) || json_encode([]) == $admin) {
//                return '3';
                session()->flash('class', 'alert-danger');
                return back()->withErrors(Lang::get('validation.PasswordNotValidate'));
            } else {
//                return '4';
                Auth::login($admin->first());
                $AdminID = $admin->first()->id;
                session(['id' => $AdminID]);
                return redirect()->route('home');
            }
        }
    }
    //--
    public function forgottenPassword(){
        return view('pages.admin.ForgottenPassword');
    }
    public function resetPassword(Request $request){
        $validator = Validator::make($request->all(), [
            'username' => 'required|numeric|exists:users,username',
        ]);
        session()->flash('class', 'alert-danger');
        if ($validator->fails())
            return redirect()->back()
                ->withErrors(Lang::get('validation.ValueIsIncorrenct'))
                ->withInput($request->all());
        //--
        $table = 'users';
        $code = $this->collection->CreateRandomCode(4);
        $password = 'pharmacy-' . $code;
        $Hashpassword = md5($password);
        //--
        $username=$request->username;
        $user=User::where('username', 'LIKE', '%'.$username.'%')->where('active',1)->first();
        $receptor = $user->phone;
        $tokens = [
            $username,
            $password,
            'کاربر'
        ];
        //--
        User::where('id', '=', $user->id)->where('active',1)->update(['password' => $Hashpassword]);
        $this->collection->SendAuthorizeSms($tokens, $receptor, 'PharmacyAuthrize');
        //----
        session()->flash('class', 'alert-success');
        return redirect()->back()
            ->withErrors(Lang::get('validation.PasswordSent'))
            ->withInput($request->all());
    }
}
