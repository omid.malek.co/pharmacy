<?php

namespace App\Http\Middleware;

use Closure;
use App\Permission;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class AccessAdmin
{
    public function isAdmin($role)
    {
        $hasPermission = DB::table('permissions')
            ->where('keyword', 'LIKE', $role)
            ->where('active', 1)->exists();
        if ($hasPermission) {
            $permission = DB::table('permissions')
                ->where('keyword', 'LIKE', $role)
                ->where('active', 1)->first();
            $user = Auth::user();
            if ($user->permission_id == $permission->id)
                return 1;
            return 0;
        }
        return 0;

    }

    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $role = 'Admin';
        if ($this->isAdmin($role))
            return $next($request);
        else
            return redirect('/admin/panel');

    }
}
