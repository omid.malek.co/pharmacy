<?php

namespace App\Http\Middleware;

use Closure;
use Validator;
class admin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $validator = Validator::make([
            'id'=>session()->get('id')
        ], [
            'id' => 'required|exists:users,id',
        ]);

        if ($validator->fails())
            return redirect()->route('admin.logout');

        return $next($request);
    }
}
