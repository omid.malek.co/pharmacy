<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Permission;
use Illuminate\Http\Request;
use App\Services\Collections\Collection;
use App\Services\Collections\File;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Http\Middleware\isValideCustomer;
use App\Document;
use App\Categorie;
use App\Image;
use App\User;
use Carbon\Carbon;

class SystemCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    private $file;
    private $collection;
    protected $signature = 'system:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(Collection $collection, File $file)
    {
        parent::__construct();
        $this->collection=$collection;
        $this->file=$file;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //--
        $currentTime=Carbon::now();
        $document=Document::where('active',0);
        $documentRecord=$document->get();
        if(!$this->collection->IsArrayEmpty($documentRecord)){
            for ($i=0;$i<count($documentRecord);$i++){
                $date=$documentRecord[$i]->updated_at;
                $dateObj=$this->collection->SetCarbonObjectWithTime($date);
                $ExpireDaysHavePassed=$this->collection->ExpireDaysHavePassed($dateObj,7);
                if($ExpireDaysHavePassed){
                    $this->file->RemoveImage($documentRecord[$i]->src);
                    $document->delete();
                }
            }

        }
        return response(['response'=>1],200);
    }
}
