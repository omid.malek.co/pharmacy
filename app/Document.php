<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Document extends Model
{
    //--
    protected $table = 'documents';
    protected $fillable = [
        'title', 'src','icon', 'user_id',
        'description','keyword', 'active',
        'created_at','updated_at'
    ];
    //--
    public function user(){
        return $this->belongsTo('App\User');
    }
}
