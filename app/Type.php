<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Type extends Model
{

    protected $fillable = [
        'title',
        'key_word',
        'created_at',
        'updated_at'
    ];

    public function payment(){
        return $this->belongsTo(Payment::class);
    }

    public function factor(){
        return $this->belongsTo(Factor::class);
    }
    public function wallet(){
        return $this->belongsTo(Wallet::class);
    }
}
