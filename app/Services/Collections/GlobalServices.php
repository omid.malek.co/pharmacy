<?php
namespace App\Library;

class GlobalServices
{

    private $TitleSite = ['دکتر غزاله مالک', 'دکتر غزاله مالک'];

    public function GetTitleSite()
    {
        return $this->TitleSite;
    }



    public function CheckEmptyArr($arr)
    {
        if ($arr == json_encode([]) || empty($arr)) {
            return '';
        } else {
            return $arr;
        }

    }

    public function IsEmptyObj($arr){
        if ($arr == json_encode([]) || empty($arr)) {
            return true;
        } else {
            return false;
        }
    }


    public function CreateRandomCode($count){
        $nu = range(0, 9);
        shuffle($nu);
        $random = '';
        for ($i = 0; $i < $count; $i++)
            $random .= $nu[$i];
        return $random;
    }

    public function PaymentStatusMessages($status){


        $comment = '';

        switch ($status) {
            case 'OK':
                $comment = 'پرداخت مبلغ صورت گرفته است.';
                break;
            case 'NOK':
                $comment = 'پرداخت مبلغ صورت نگرفته است.';
                break;
            default:
                $comment = 'کد ارسالی نامعتبر است.';
                break;
        }
        return $comment;
    }


}
