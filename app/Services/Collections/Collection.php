<?php

namespace App\Services\Collections;

use App\Gift;
use App\Type;
use App\Wallet;
use Illuminate\Support\Str;
use App\Post;
use App\Product;
use App\User;
use App\Image;
use App\Customer;
use App\Factor;
use Carbon\Carbon;
use App\Discount;
use App\Payment;
use App\Permission;
use Morilog\Jalali\Jalalian;
use App\Condition;
use Illuminate\Support\Facades\DB;
use SebastianBergmann\Comparator\Exception;
use App\Item;
use Illuminate\Support\Facades\Auth;

class  Collection
{
    public function ExitUser($Model)
    {
        $Model->update(['token' => 'null']);
        session()->forget('id');
        return 1;
    }

    public function IsArrayEmpty($arr)
    {
        if ($arr == json_encode([]) || $arr == 'null' || empty($arr) || is_null($arr) || (!isset($arr))) {
            return 1;
        } else {
            return 0;
        }
    }

    public function InputIsEmpty($input, $default)
    {
        return ((!isset($input)) || empty($input)) ? $default : $input;
    }

    public function getCustomerId()
    {
        if (session()->has('id')) {
            $token = session()->get('id');
            $customer = Customer::select(['id'])->where('token', $token)->get();

            if (!$this->IsArrayEmpty($customer))
                return $customer->first()->id;
            else
                return false;
//                return 'is not customer';

        } else {
//            return 'has not token';
            return false;
        }
    }

    public function getCustomerWithId($id, $arr_column = null)
    {
        if (empty($arr_column)) {
            $arr_column = [
                'fullname', 'email', 'password', 'address', 'postal_code',
                'phone', 'active', 'token'
            ];
        }
        $customer = Customer::select($arr_column)->where('id', $id)->get();

        if ($this->IsArrayEmpty($customer))
            return false;
        else
            return $customer->first();
    }

    public function getCustomer($token, $arr_column = null)
    {
        if (empty($arr_column)) {
            $arr_column = [
                'fullname', 'email', 'password', 'address', 'postal_code',
                'phone', 'active', 'token'
            ];
        }
        $customer = Customer::select($arr_column)->where('token', $token)->get();

        if ($this->IsArrayEmpty($customer))
            return false;
        else
            return $customer->first();

    }

    public function getCollections()
    {
        return User::all();
    }

    public function createToken($count)
    {
        $token = Str::random($count);

        $customer = Customer::select(['id'])->where('token', $token)->take(1)->get();

        if (count($customer) > 0) {
            return $this->createToken($count);
        } else {
            return $token;
        }

    }

    public function AddImage($arr, $used_in, $directory)
    {
        for ($i = 0; $i < count($arr); $i++) {
            $image = Image::select(['src', 'record_id', 'used_in'])->where('used_in', $used_in)
                ->where('record_id', $arr[$i]->id)
                ->where('active', 1)->first();
            $path = $directory . '/' . $image->src;
            array_add($arr[$i], 'image_path', $path);
        }

        return $arr;

    }

    public function ManagerInformation($arr_field = null)
    {

        if (empty($arr_field)) {
            $arr_field = [
                'fullname', 'phone', 'username',
                'email', 'address', 'number_cart',
                'number_account', 'facebook_address', 'instagram_address',
                'telegram_address'
            ];
        }
        $user = User::first()->get($arr_field);
        return $user;
    }

    public function ShowCollection($id)
    {
        return $user = User::find($id);
    }

    public function numberFormat($number, $decimal_number = 2, $decimal_pointer = '.', $thousands_pointer = ',')
    {
        $number_string = (string)$number;
        $arr_number = explode($decimal_pointer, $number_string);
        $arr_number[1] = number_format($arr_number[1], $decimal_number, $decimal_pointer, $thousands_pointer);
        $result_number = implode('.', $arr_number);
        return (double)$result_number;
    }

    public function numberWithoutDecimal($number, $thousands_pointer = '/')
    {
        $number_string = (string)$number;
        $decimal_number = 0;
        $decimal_pointer = '.';
        $number_string = number_format($number_string, $decimal_number, $decimal_pointer, $thousands_pointer);
        return $number_string;
    }

    public function GetSliderPictures()
    {
        return $sliders = Image::select(['used_in', 'src', 'id', 'record_id'])
            ->where('used_in', '=', 'sliders')->where('active', 1);
    }

    public function selectProducts($title = null, $categorie_id = null)
    {

        if (empty($title) && empty($categorie_id)) {
            $product = Product::select(['title', 'amount', 'id'])->where('active', 1);
        } else if (empty($title) && !empty($categorie_id)) {
            $product = Product::select(['title', 'amount', 'id'])
                ->where('active', 1)->where('categorie_id', $categorie_id);
        } else {
            $product = 0;
        }

        return $product;
    }

    public function GetPostsCategorieCombo($categorie_id)
    {
        $posts = Post::where('categorie_id', '=', $categorie_id)->get();
        $combo = '';
        $combo .= '<select dir="rtl" class="form-control" name="title" id="InsertUsersCategorie">';
        foreach ($posts as $Item) {
            $combo .= '<option value="' . $Item->id . '">' . $Item->title . '</option>';
        }
        $combo .= '</select>';
        return $combo;
    }

    public function GetFactorsPayment($factors_record, $column)
    {
        $sum = 0;
        foreach ($factors_record as $item)
            $sum += intval($item->$column);

        return $sum;
    }

    public function PaymentStatusBeforePurchase()
    {
        return 1;
    }

    public function PaymentStatusAfterPurchase()
    {
        return 3;
    }

    public function NoDataFound()
    {
        $message = "";
        $message .= '<section class="ErrorFrame">';
        $message .= '<section class="alert alert-primary text-center ErrorBox BMitra">';
        $message .= '<span class="IRanSans ErrorMessage">' . 'هیچ داده ای یافت نشد' . '</span>';
        $message .= '</section>';
        $message .= '</section>';
        return $message;
    }


    public function donePurchase($payment_id)
    {
        $payment = Payment::select(['total_sum'])
            ->where('id', $payment_id)
            ->where('payment_status', 3)->take(1)->get();
        return ($this->IsArrayEmpty($payment) ? 0 : 1);
    }

    public function ProductRemaining($payment_id)
    {
        if ($this->donePurchase($payment_id)) {
            $factors = Factor::where('payment_id', $payment_id)->get();
            $isEmpty = $this->IsArrayEmpty($factors);
            if (!$isEmpty)
                foreach ($factors as $item) {
                    $productObj = Product::where('id', $item->product_id);
                    $product = $productObj->first();
                    $number = $product->number - $item->sent_number;
                    $productObj->update(['number' => $number]);
                }
        }
    }

    public function checkGiftCard($paymentQuery, $payment, $giftQuery, $gift, $totalSum = 0)
    {

        $gift_amount = $gift->first()->amount;
        $customer_id = $this->getCustomerId();
        $gift_id = $gift->first()->id;
        $total_sum = $payment->total_sum;
        $price = (double)$total_sum;
        $conditionUsed = $this->conditionUsed();
        if ($price >= $gift_amount) {
            $remaining = $price - $gift_amount;
            if ($remaining < (double)1200) {
                $totalSum = 0;
                $payment_record = [
                    'gift_amount' => (string)$gift_amount,
                    'gift_id' => $gift_id,
                    'total_sum' => (string)$totalSum
                ];
                $paymentQuery->update($payment_record);
                $gift_record = [
                    'condition_id' => $conditionUsed->id,
                    'used_by' => $customer_id
                ];
                $giftQuery->update($gift_record);
                return $totalSum;
            }
            $payment_record = [
                'gift_amount' => (string)$gift_amount,
                'gift_id' => $gift_id,
                'total_sum' => (string)$remaining
            ];
            $paymentQuery->update($payment_record);
            $gift_record = [
                'condition_id' => $conditionUsed->id,
                'used_by' => $customer_id
            ];
            $giftQuery->update($gift_record);
            return $remaining;

        } elseif ($price < $gift_amount) {
            $remaining = $gift_amount - $price;
            $totalSum = 0;
            $payment_record = [
                'gift_amount' => (string)$price,
                'gift_id' => $gift_id,
                'total_sum' => (string)$totalSum
            ];
            $paymentQuery->update($payment_record);
            $gift_record = [
                'amount' => $remaining,
                'used_by' => $customer_id
            ];
            $giftQuery->update($gift_record);
            return $totalSum = 0;
        }
    }

    public function ApplyGiftInTotalSum($paymentQuery, $payment, $giftQuery, $gift, $total_sum = 0)
    {
        $gift_amount = (double)$gift->first()->amount;
        $customer_id = $this->getCustomerId();
        $gift_id = $gift->first()->id;
        $total_sum = $total_sum;
        $price = (double)$total_sum;
        $conditionUsed = $this->conditionUsed();
        if ($price >= $gift_amount) {
            $remaining = $price - $gift_amount;
            if ($remaining < (double)1200) {
                $totalSum = 0;
                $payment_record = [
                    'gift_amount' => (string)$gift_amount,
                    'gift_id' => $gift_id,
                    'total_sum' => (string)$totalSum
                ];
                $paymentQuery->update($payment_record);
                $gift_record = [
                    'condition_id' => $conditionUsed->id,
                    'used_by' => $customer_id
                ];
                $giftQuery->update($gift_record);
                return $totalSum;
            }
            $payment_record = [
                'gift_amount' => (string)$gift_amount,
                'gift_id' => $gift_id,
                'total_sum' => (string)$remaining
            ];
            $paymentQuery->update($payment_record);
            $gift_record = [
                'condition_id' => $conditionUsed->id,
                'used_by' => $customer_id
            ];
            $giftQuery->update($gift_record);
            return $remaining;

        } elseif ($price < $gift_amount) {
            $remaining = $gift_amount - $price;
            $totalSum = 0;
            $payment_record = [
                'gift_amount' => (string)$price,
                'gift_id' => $gift_id,
                'total_sum' => (string)$totalSum
            ];
            $paymentQuery->update($payment_record);
            $gift_record = [
                'amount' => $remaining,
                'used_by' => $customer_id
            ];
            $giftQuery->update($gift_record);
            return $totalSum = 0;
        }
    }

    public function basketTable($customer_id)
    {
        $PaymentStatus = $this->PaymentStatusBeforePurchase();
        $noDataFound = $this->NoDataFound();
        $paymentObj = Payment::where('payment_status', $PaymentStatus)
            ->where('active', 1)
            ->where('customer_id', $customer_id);
        $payment = $paymentObj->first();
        if ($this->IsArrayEmpty($paymentObj->take(1)->get()))
            return $noDataFound;

        $totalSum = 0;
        $factor = Factor::select(['id',
            'product_id', 'payment_id', 'customer_id', 'sent_number'
        ])->where('customer_id', $customer_id)
            ->where('payment_id', $payment->id)
            ->where('active', 1)
            ->with(['product:id,title,amount,number']);
//        'payment:id,receiver_fullname,postal_code_sent,payment_amount,sent_phone,sent_address'
        $factorRecords = $factor->get();
        $count = count($factorRecords);
        if ($count > 0) {
            $paymentAmount = 0;
            for ($i = 0; $i < count($factorRecords); $i++) {
                $paymentAmount = $this->CalculatePaymentValue(
                    $factorRecords[$i]->product->amount,
                    0,
                    $factorRecords[$i]->sent_number
                );
                $factorRecords[$i]->payment_amount = $paymentAmount;
            }
            $totalSum = $this->GetFactorsPayment($factorRecords, 'payment_amount');
            $paymentObj = Payment::where('id', $factorRecords->first()->payment_id);
            if ($payment->payment_amount == '0') {
                $paymentObj->update(['payment_amount' => $totalSum]);
            }
            if ($payment->total_sum == '0') {
                $paymentObj->update(['total_sum' => $totalSum]);
            }
            /********************calculate total payment amount with discount***********************/
            // calculate discount amount
            if ($payment->discount_id == Null || $payment->discount_id == '' || $payment->discount_id == 0) {
                $discountValue = 0;
            } else {
                $discountQuery = Discount::where('id', $payment->discount_id)->take(1);
                $discount = $discountQuery->get();
                $totalSum = $this->SetDiscountInTotalSum($discount, $paymentObj, $payment, $totalSum);
            }
            if ($payment->gift_id == Null || $payment->gift_id == '' || $payment->gift_id == 0) {
                $gift_amount = 0;
            } else {
                $totalSum;
                $giftQuery = Gift::where('id', $payment->gift_id)
                    ->take(1);
                $gift = $giftQuery->get();
                $totalSum = $this->ApplyGiftInTotalSum($paymentObj, $payment, $giftQuery, $gift, $totalSum);
            }
//            return $totalSum;
            // calculate payment amount
//            $totalSum = $this->CalculatePaymentValue($payment->payment_amount, $discountValue, 1);
        }

//        $totalSum = $this->checkGiftCard($paymentObj, $payment, $totalSum);

        $row = 1;
        $basket_Sales = "";
        if ($this->IsArrayEmpty($factorRecords))
            return $this->NoDataFound();
        else {
            $basket_Sales .= '<table id="basketTable" class="table table-bordered BorderedTable">';
            $basket_Sales .= '<thead>';
            $basket_Sales .= '<tr>';
            $basket_Sales .= '<th>ردیف</th>';
            $basket_Sales .= '<th>عنوان کالا</th>';
            $basket_Sales .= '<th>قیمت کالا (تومان)</th>';
            $basket_Sales .= '<th>تعداد کالا</th>';
            $basket_Sales .= '<th>عملیات</th>';
            $basket_Sales .= '</tr>';
            $basket_Sales .= '</thead>';
            $basket_Sales .= '<tbody>';
            foreach ($factorRecords as $item) {
                $basket_Sales .= '<tr id="Row"' . $item->id . '>';
                $basket_Sales .= '<input type="hidden" value="' . $item->product->id . '" id="Product" />';
                $basket_Sales .= '<td>' . $row++ . '</td>';
                $basket_Sales .= '<td>' . $item->product->title . '</td>';
                $basket_Sales .= '<td>' . $item->product->amount . '</td>';
                $basket_Sales .= '<td>';
                $basket_Sales .= '<input id="productNumber" type="number"  code="' . $item->id;
                $basket_Sales .= '" class="form-control text-center" min="1"';
                $basket_Sales .= 'value="' . $item->sent_number . '"/>';
                $basket_Sales .= '</td>';
                $basket_Sales .= '<td>';
                $basket_Sales .= '<span id="removeFromBasket" class="btn btn-outline-danger" clicked="false"';
                $basket_Sales .= 'path="' . route('factors.destroy', ['factor_id' => $item->id]) . '"';
                $basket_Sales .= '>';
                $basket_Sales .= 'حذف';
                $basket_Sales .= '</span>';
                $basket_Sales .= "</td>";
                $basket_Sales .= "</tr>";
            }
            /**********discount and gifts card form***********/
//            $basket_Sales .= '<tr id="SumPrices">';
//            $basket_Sales .= '<td >#</td>';
//            $basket_Sales .= '<td>';
//            $basket_Sales .= '<div dir = "ltr" class="input-group mb-0" >';
//            $basket_Sales .= '<div class="input-group-prepend">';
//            $basket_Sales .= '<button class="btn btn-outline-success" type="button" id="discountApply"> اعمال تخفیف </button>';
//            $basket_Sales .= '</div>';
//            $basket_Sales .= '<input type="hidden" id = "payment_id" value= "' . $factor->first()->payment_id . '" />';
//            $basket_Sales .= '<input type="hidden" id = "path" value = "' . route('discount.play') . '" />';
//            $basket_Sales .= '<input type="text" id="discountTxt" class="form-control text-center" aria-label="Example text with button addon" aria-describedby="button-addon1" />';
//            $basket_Sales .= '</div>';
//            $basket_Sales .= '</td>';
//            $basket_Sales .= '<td colspan="3">';
//            $basket_Sales .= '<div dir = "ltr" class="input-group mb-0" >';
//            $basket_Sales .= '<div class="input-group-prepend">';
//            $basket_Sales .= '<button class="btn btn-outline-success" type="button" id="giftsApply"> اعمال کارت هدیه </button>';
//            $basket_Sales .= '</div>';
//            $basket_Sales .= '<input type="hidden" id="payment_id" value= "' . $factor->first()->payment_id . '" />';
//            $basket_Sales .= '<input type="hidden" id="SetGiftpath" value="' . route('gift.to.payments') . '" />';
//            $basket_Sales .= '<input type="text" id="giftText" class="form-control text-center" aria-label="Example text with button addon" aria-describedby="button-addon1" />';
//            $basket_Sales .= '</div>';
//            $basket_Sales .= '</td>';
//            $basket_Sales .= '</tr>';
            /***************total sum area*****************/
            $basket_Sales .= '<tr id="SumPrices">';
            $basket_Sales .= '<td >#</td>';
            $basket_Sales .= '<td>';
            $basket_Sales .= '<span > مبلغ قابل پرداخت </span >';
            $basket_Sales .= '</td>';
            $basket_Sales .= '<td colspan="3">';
            $basket_Sales .= '<span class="text-secondary" id = "totalSum" >';
            $basket_Sales .= $totalSum . '  ' . 'تومان';
            $basket_Sales .= '</span>';
            $basket_Sales .= '</td>';
            $basket_Sales .= '</tr>';
            $basket_Sales .= '</tbody>';
            $basket_Sales .= '</table>';
            $basket_Sales .= '<div class="FormButton">';
            $basket_Sales .= '<span id="SetAddressReceiver" class="text-center btn btn-outline-success CustomBTN" clicked="false">ثبت آدرس و پرداخت</span>';
            $basket_Sales .= '</div>';
        }
        return $basket_Sales;
    }

    public function SetDiscountInPayment($discount, $paymentQuery, $payment)
    {
        // calculate payment amount
        if ($payment->discount_id == Null || $payment->discount_id == '' || $payment->discount_id == 0) {
            $discount_percent = (float)($discount->first()->percent) / (100);
            $discountValue = (float)$discount_percent * (float)$payment->total_sum;
            $totalSum = $this->CalculatePaymentValue($payment->total_sum, $discountValue, 1);
            if ($totalSum < (double)1200)
                $payment_record = [
                    'discount_value' => $discountValue,
                    'discount_id' => $discount->first()->id,
                    'total_sum' => 0
                ];
            else
                $payment_record = [
                    'discount_value' => $discountValue,
                    'discount_id' => $discount->first()->id,
                    'total_sum' => $totalSum
                ];

            $paymentQuery->update($payment_record);
            return $totalSum;
        } else
            return $payment->total_sum;
    }

    public function SetDiscountInTotalSum($discount, $paymentQuery, $payment, $total_sum)
    {
        // calculate payment amount
        if ($payment->discount_id == Null || $payment->discount_id == '' || $payment->discount_id == 0) {
            return $total_sum;
        } else {
            $discount_percent = (float)($discount->first()->percent) / (100);
            $discountValue = (double)$discount_percent * (double)$total_sum;
            $totalSum = $this->CalculatePaymentValue($total_sum, $discountValue, 1);
            $totalSum = (double)$totalSum;
            if ($totalSum < (double)1200)
                $payment_record = [
                    'discount_value' => $discountValue,
                    'discount_id' => $discount->first()->id,
                    'total_sum' => 0
                ];
            else
                $payment_record = [
                    'discount_value' => $discountValue,
                    'discount_id' => $discount->first()->id,
                    'total_sum' => $totalSum
                ];

            $paymentQuery->update($payment_record);
            return $totalSum;
        }
    }

    public function receiptPurchase($payment_id)
    {
        $noDataFound = $this->NoDataFound();
        $paymentObj = Payment::where('id', $payment_id);
        $payment = $paymentObj->first();
        if ($this->IsArrayEmpty($paymentObj->take(1)->get()))
            return $noDataFound;

        $factor = Factor::select(['id',
            'product_id', 'payment_id', 'customer_id', 'sent_number'
        ])->where('payment_id', $payment->id)
            ->where('active', 1)
            ->with(['product:id,title,amount,number']);
//        'payment:id,receiver_fullname,postal_code_sent,payment_amount,sent_phone,sent_address'
        $factorRecords = $factor->get();
        $count = count($factorRecords);
        if ($count > 0) {
            $paymentAmount = 0;
            for ($i = 0; $i < count($factorRecords); $i++) {
                $paymentAmount = $this->CalculatePaymentValue(
                    $factorRecords[$i]->product->amount,
                    $factorRecords[$i]->product->discount_value,
                    $factorRecords[$i]->sent_number
                );
                $factorRecords[$i]->payment_amount = $paymentAmount;
            }
            $totalSum = $this->GetFactorsPayment($factorRecords, 'payment_amount');
            /********************calculate total payment amount with discount***********************/
            // calculate discount amount
            if ($payment->discount_id == Null || $payment->discount_id == '' || $payment->discount_id == 0) {
                $discountValue = 0;
            } else {
                $discount = Discount::where('id', $payment->discount_id)
                    ->take(1)->get();
                $discount_percent = (float)($discount->first()->percent) / (100);
                $discountValue = (float)$discount_percent * (float)$payment->payment_amount;
            }
            // calculate payment amount
            $totalSum = $this->CalculatePaymentValue($payment->payment_amount, $discountValue, 1);
        }
        $row = 1;
        $basket_Sales = "";
        $basket_Sales .= '<section class="ErrorFrame">';
        $basket_Sales .= '<section class="alert alert-primary text-center ErrorBox BMitra">';
        $basket_Sales .= '<span class="IRanSans ErrorMessage " dir="rtl">' . $payment->payment_message . '</span>';
        $basket_Sales .= '</section>';
        $basket_Sales .= '</section>';
        $basket_Sales .= '<table id="basketTable" class="table table-bordered BorderedTable">';
        $basket_Sales .= '<thead>';
        $basket_Sales .= '<tr>';
        $basket_Sales .= '<th>ردیف</th>';
        $basket_Sales .= '<th>عنوان کالا</th>';
        $basket_Sales .= '<th dir="rtl">  قیمت کالا(تومان)</th>';
        $basket_Sales .= '<th>تعداد کالا</th>';
        $basket_Sales .= '</tr>';
        $basket_Sales .= '</thead>';
        $basket_Sales .= '<tbody>';
        foreach ($factorRecords as $item) {
            $basket_Sales .= '<tr id="Row"' . $item->id . '>';
            $basket_Sales .= '<input type="hidden" value="' . $item->product->id . '" id="Product" />';
            $basket_Sales .= '<td>' . $row++ . '</td>';
            $basket_Sales .= '<td>' . $item->product->title . '</td>';
            $basket_Sales .= '<td>' . $item->product->amount . '</td>';
            $basket_Sales .= '<td>';
            $basket_Sales .= $item->sent_number;
            $basket_Sales .= '</td>';
            $basket_Sales .= "</tr>";
        }

        // cost of payment
        $basket_Sales .= '<tr id="SumPrices">';
        $basket_Sales .= '<td >#</td>';
        $basket_Sales .= '<td>';
        $basket_Sales .= '<div dir = "ltr" class="mb-0">';
        $basket_Sales .= '<span> مبلغ قابل پرداخت </span >';
        $basket_Sales .= '</div>';
        $basket_Sales .= '</td>';
        $basket_Sales .= '<td colspan="3">';
        $basket_Sales .= '<span class="text-secondary" id = "totalSum" >';
        $totalSumForShow = $this->numberWithoutDecimal($totalSum);
        $basket_Sales .= $totalSumForShow . '  ' . 'تومان';
        $basket_Sales .= '</span>';
        $basket_Sales .= '</td>';
        $basket_Sales .= '</tr>';
        // discount value
        $basket_Sales .= '<tr id="SumPrices">';
        $basket_Sales .= '<td >#</td>';
        $basket_Sales .= '<td>';
        $basket_Sales .= '<div dir = "ltr" class="mb-0">';
        $basket_Sales .= '<span > کد پیگیری </span >';
        $basket_Sales .= '</div>';
        $basket_Sales .= '</td>';
        $basket_Sales .= '<td colspan="3">';
        $basket_Sales .= '<span class="text-secondary" id = "totalSum" >';
        $basket_Sales .= $payment->reference_code;
        $basket_Sales .= '</span>';
        $basket_Sales .= '</td>';
        $basket_Sales .= '</tr>';
        // end of body table
        $basket_Sales .= '</tbody>';
        $basket_Sales .= '</table>';
        $basket_Sales .= '<div class="FormButton">';
        $basket_Sales .= '<a href="' . route('home.index') . '" class="text-center btn btn-outline-success CustomBTN">بازگشت به صفحه اصلی</a>';
        $basket_Sales .= '</div>';

        return $basket_Sales;
    }

    public function CalculatePaymentValue($price, $discountValue, $number_product)
    {
        $payment_price = ($price * $number_product) - $discountValue;

        return $payment_price;
    }

    public function checkExpireDate($date, $start_date = null, $expire_date = null)
    {
        if ($start_date != null)
            if ($start_date > $date)
                return 0;
            else return 1;
        else
            if ($expire_date < $date)
                return 0;
        return 1;
    }

    public function getImagesArticle($record_id, $used_in, $active)
    {
        $images = Image::where('active', $active)->where('record_id', $record_id)
            ->where('used_in', $used_in);

        return $images;
    }

    public function getJalaliDate($date)
    {
        $JalaliDateTime = Jalalian::fromDateTime($date);
        $arrDateAndTime = explode(' ', $JalaliDateTime);
        $JalaliDate = $arrDateAndTime[0];
        return $JalaliDate;
    }

    public function latinNumber($str_number)
    {
        $array_number = str_split($str_number[0], 2);
        $latin_number_string = "";
        foreach ($array_number as $item) {
            switch ($item) {
                case "0":
                    $latin_number_string .= "1";
                    break;
                case "2":
                    $latin_number_string .= "2";
                    break;
                case "3":
                    $latin_number_string .= "3";
                    break;
                case "4":
                    $latin_number_string .= "4";
                    break;
                case "5":
                    $latin_number_string .= "5";
                    break;
                case "6":
                    $latin_number_string .= "6";
                    break;
                case "7":
                    $latin_number_string .= "7";
                    break;
                case "8":
                    $latin_number_string .= "8";
                    break;
                case "9":
                    $latin_number_string .= "9";
                    break;
                case "0":
                    $latin_number_string .= "0";
                    break;
            }
        }
        return $latin_number_string;
    }

    public function convertPersianNumbersToEnglish($input)
    {
        $persian = ['۰', '۱', '۲', '۳', '۴', '٤', '۵', '٥', '٦', '۶', '۷', '۸', '۹'];
        $english = [0, 1, 2, 3, 4, 4, 5, 5, 6, 6, 7, 8, 9];
        return str_replace($persian, $english, $input);
    }

    public function ConvertJalaliToGregorian($format, $value)
    {
        $arrJalali = explode('-', $value);
        $year = intval($this->convertPersianNumbersToEnglish($arrJalali[0]));
        $month = intval($this->convertPersianNumbersToEnglish($arrJalali[1]));
        $day = intval($this->convertPersianNumbersToEnglish($arrJalali[2]));
        $date = (new Jalalian($year, $month, $day))->toCarbon()->format($format);
        return $date;
    }

    public function CreateRandomCode($count)
    {
        $nu = range(0, 9);
        shuffle($nu);
        $random = '';
        for ($i = 0; $i < $count; $i++)
            $random .= $nu[$i];
        return $random;
    }

    public function trackerCodeCreator($count, $table_name, $column_name)
    {
        $code = $this->CreateRandomCode($count);
        $factor = $user = DB::table($table_name)->where($column_name, $code)->take(1)->get();

        if ($this->IsArrayEmpty($factor))
            return $code;
        else
            return $this->trackerCodeCreator($count);
    }

    public function WalletType()
    {
        $key_word = 'walletCharge';
        $type = Type::where('key_word', '=', $key_word)->first();
        return $type;
    }

    public function purchaseType()
    {
        $key_word = 'productPurchase';
        $type = Type::where('key_word', '=', $key_word)->first();
        return $type;
    }

    public function CreateAccountInWallet($record = null)
    {
        $wallet = '';
        if (!empty($record))
            return $wallet = Wallet::create($record);

        $customer_id = $this->getCustomerId();
        $type = $this->WalletType();
        $wallet_record = [
            'title' => 'کیف پول مجازی',
            'amount' => 0,
            'type_id' => $type->id,
            'customer_id' => $customer_id,
            'active' => 1,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ];
        return Wallet::create($wallet_record);
    }

    public function WithDrawal($price, $value_clause)
    {
        $table_clause = 'wallets';
        $column_clause = 'customer_id';
        $wallet = Wallet::where($column_clause, '=', $value_clause)
            ->where('active', '=', 1)->first();

        if ($this->IsArrayEmpty($wallet))
            $wallet = $this->CreateAccountInWallet();

        $user_amount = (double)$wallet->amount;
        if (($user_amount - (double)$price) < 0)
            return collect([
                'status' => 0,
                'message' => 'این مبلغ در اکانت شما موجود نیست'
            ]);
        $result = ($user_amount - (double)$price);
        return collect([
            'status' => 1,
            'wallet_id' => $wallet->id,
            'amount_should_be' => $result,
            'message' => 'مبلغ مورد نظر قابل برداشت است'
        ]);
    }

    public function conditionNotUsed()
    {
        $condition = Condition::where('key_word', 'notUsed')->first();
        return $condition;
    }

    public function conditionUsed()
    {
        $condition = Condition::where('key_word', 'used')->first();
        return $condition;
    }

    public function conditionNotSent()
    {
        $condition = Condition::where('key_word', 'notSent')->first();
        return $condition;
    }

    public function conditionSent()
    {
        $condition = Condition::where('key_word', 'Sent')->first();
        return $condition;
    }

    public function ZarinPalStatusMessage($status)
    {

        $comment = '';

        switch ($status) {
            case '-1':
                $comment = 'اطلاعات ارسال شده ناقص است.';
                break;
            case '-2':
                $comment = 'و يا مرچنت كد پذيرنده صحيح نيست. IP';
                break;
            case '-3':
                $comment = 'با توجه به محدوديت هاي شاپرك امكان پرداخت با رقم درخواست شده ميسر نمي باشد.';
                break;
            case '-4':
                $comment = 'سطح تاييد پذيرنده پايين تر از سطح نقره اي است.';
                break;
            case '-11':
                $comment = 'درخواست مورد نظر يافت نشد.';
                break;
            case '-12':
                $comment = 'امكان ويرايش درخواست ميسر نمي باشد.';
                break;
            case '-21':
                $comment = 'هيچ نوع عمليات مالي براي اين تراكنش يافت نشد.';
                break;
            case '-22':
                $comment = 'تراكنش نا موفق ميباشد';
                break;
            case '-33':
                $comment = 'رقم تراكنش با رقم پرداخت شده مطابقت ندارد.';
                break;
            case '-34':
                $comment = 'سقف تقسيم تراكنش از لحاظ تعداد يا رقم عبور نموده است';
                break;
            case '-42':
                $comment = 'مدت زمان معتبر طول عمر شناسه پرداخت بايد بين 30 دقيه تا 45 روز مي باشد.';
                break;
            case '-54':
                $comment = 'درخواست مورد نظر آرشيو شده است.';
                break;
            case 100:
                $comment = 'عمليات با موفقيت انجام گرديده است.';
                break;
            case 101:
                $comment = 'عملیات پرداخت موفق بوده و قبلا PaymentVerification تراکنش انجام شده است.';
                break;
            default:
                $comment = 'کد ارسالی نامعتبر است.';
                break;
        }
        return $comment;
    }

    //--
    public function UserItems($user_id, $is_base_url)
    {
        //--
        $table = 'items';
        (integer)$hasNotItem = DB::table($table)
            ->where('active', 1)
            ->where('user_id', '=', $user_id)
            ->where('is_base_page', '=', $is_base_url)
            ->doesntExist();
        if ($hasNotItem)
            return [
                'operation' => 0,
                'response' => []
            ];
//        //--
        $items = Item::where('active', 1)
            ->where('user_id', '=', $user_id)
            ->where('is_base_page', '=', $is_base_url)
            ->get();
        return [
            'response' => $items,
            'operation' => 1
        ];

    }

    public function AdminPermissionId()
    {
        $permission = Permission::where('keyword', 'Admin')->first();
        return $permission->id;
    }

    public function personnelPermissionId()
    {
        $permission = Permission::where('keyword', 'Personnel')->first();
        return $permission->id;
    }

    public function SendAuthorizeSms($tokens, $receptor, $template)
    {
        //-- PharmacyAuthrize
        $receptor = $receptor;
        $token = $tokens[0];
        $token2 = $tokens[1];
//        $token3=$tokens[2];
        $token3 = 'کاربر';
        $template = $template;
        $statusShouldbe = 200;
        $apiKey = "444A6B313852723275337456414B3368654F587561547A755277772F36666F4F75545230512B4B315641413D";
        $URL = 'https://api.kavenegar.com/v1/' . $apiKey . '/verify/lookup.json?receptor=' . $receptor . '&token=' . $token . '&token2=' . $token2 . '&template=' . $template;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $URL);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
        ));
        $response = curl_exec($ch);
        $errors = curl_error($ch);
        $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        $StatusRequest = json_decode($httpcode);
        return 200;
        if ($StatusRequest == $statusShouldbe) {
            return $StatusRequest['status'];
        } else
            return $StatusRequest['status'];
    }

    //--
    public function ExpireDaysHavePassed($dateObj, $number_expire_days)
    {
        $current = Carbon::now();
        $trialExpires = $dateObj->addDays($number_expire_days);
        if ($trialExpires < $current)
            return 1;
        return 0;
    }

    public function SetCarbonObjectWithTime($dateTimeZone)
    {
        $dateString = (string)$dateTimeZone;
        $dattime_array = explode(' ', $dateString);
        $dateArray = explode('-', $dattime_array[0]);
        $timeArray = explode(':', $dattime_array[1]);
        $carbonObj = Carbon::create($dateArray[0], $dateArray[1], $dateArray[2], $timeArray[0], $timeArray[1], $timeArray[2], 0);
        return $carbonObj;
    }

    /*
     * input = 2022-11-16 23:16:18
     * output = [2022,11,16,23,16,18]
    */
    public function getTimeStampArr($date_time_value)
    {
        $timeStampArr = explode(' ', $date_time_value);
        $dateArr = explode('-', $timeStampArr[0]);
        $timeArr = explode(':', $timeStampArr[1]);
        return [
            'year' => (integer)$dateArr[0],
            'month' => (integer)$dateArr[1],
            'day' => (integer)$dateArr[2],
            'hour' => (integer)$timeArr[0],
            'minute' => (integer)$timeArr[1],
            'second' => (integer)$timeArr[2],
        ];
    }

    public function messageTxt($keyValues)
    {
        $result = "";
        switch ($keyValues) {
            case "undefined":
                $result = "مشخص نشده";
                break;
            case "not_confirmed":
                $result = "تایید نشده";
                break;
            case "confirmed":
                $result = "تایید شده";
                break;
            default:
                $result = " keyValues not defined ";
                break;
        }
        return $result;
    }

    public function labelMessageCreator($text, $classes)
    {
        return '<label class="' . $classes . '">' . $text . '</label>';
    }

    public function convertJalaliDateToGregorian($jalaliDatearray = ['1400', '10', '12'])
    {
        $year = (integer)$jalaliDatearray[0];
        $month = (integer)$jalaliDatearray[1];
        $day = (integer)$jalaliDatearray[2];
        return $date = (new Jalalian($year, $month, $day, 00, 00, 00))
            ->toCarbon()->toDateTimeString();
    }
    public function based64url_encode($data){
        return rtrim(strtr(base64_encode($data),'+/','-_'),'=');
    }
    public function based64url_decode($data){
        return base64_decode(str_pad(strtr($data,'-_','+/'),strlen($data)%4,'=',STR_PAD_RIGHT));
    }
}


?>
