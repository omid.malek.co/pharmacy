<?php

namespace App\Services\Collections;

use App\User;
use App\Customer;
use Laravel\Passport\Bridge\Client;
use OAuth;
use GuzzleHttp;
use Illuminate\Support\Facades\DB;



class  Token
{


    private $linkRetrieveAcces = 'localhost:8000/oauth/token';

    private $ClientId=1;

    private $ClientSecret='TwujozQgLJfEZCZA9nGNojSSKxSZ5fbaK6LKJoR9';

    public function TokenCollectionCreator($Id)
    {
        $Users = User::find($Id);
        return $token = $Users->createToken('My Token', ['collection-orders'])->accessToken;
    }

    public function TokenCustomerCreator($id)
    {
        $customer = Customer::find($id);
        $token = $customer->createToken('My Token', ['customer-orders'])->accessToken;
        return $token;
    }

    public function getToken($id, $ClientId, $ClientSecret, $scope)
    {
        $token = DB::table('oauth_access_tokens')->join('oauth_clients', 'oauth_clients.id', '=', 'oauth_access_tokens.client_id')
            ->where('oauth_access_tokens.user_id', '=', $id)
            ->where('oauth_clients.id', '=', $ClientId)
            ->where('oauth_clients.secret', '=', $ClientSecret)
            ->where('oauth_access_tokens.scopes', '=', $scope)->select('oauth_access_tokens.id')->get();

        return $token;
    }

    public function getClientId(){
        return $this->ClientId;
    }

    public function getClientSecret(){
        return $this->ClientSecret;
    }

    public function getUserId($Token, $Scope)
    {
        $UserId = DB::table('oauth_access_tokens')
            ->where('id', '=', $Token)
            ->where('scopes', '=', $Scope)->select('user_id')->take(1)->get();
        return $UserId;
    }

}


?>