<?php
namespace App\Services;

use App\Item;
use App\Page;
use Illuminate\Support\Facades\DB;

class ItemsServices
{


    public function pagesKeyForCreateUser()
    {
        return [1, 4, 6, 2, 15];
    }

    public function createOrUpdateItem($page, $user, $personnelPermissionId)
    {
        $timezon = date('Y-m-d H:i:s');
        $item = [
            'personnel_id' => 0,
            'user_id' => $user->id,
            'page_id' => $page->id,
            'page_title' => $page->title,
            'route_name' => $page->route_name,
            'address' => $page->address,
            'base_url' => $page->base_url,
            'is_base_page' => $page->is_base_page,
            'child_id' => 0,
            'parent_id' => 0,
            'permission_id' => $personnelPermissionId,
            'permission_title' => $page->permission_title,
            'active' => 1,
            'created_at' => $timezon,
            'updated_at' => $timezon
        ];
        $hasNotItem = DB::table('items')
            ->where('active', '=', 1)
            ->where('user_id', '=', $user->id)
            ->where('page_id', '=', $page->id)
            ->doesntExist();
//        //--
        if ($hasNotItem)
            $result = Item::create($item);
        else
            $result = Item::where('active', '=', 1)
                ->where('user_id', '=', $user->id)
                ->where('page_id', '=', $page->id)->update($item);
//        //--
        return 1;
    }

    public function addItemsforUser($user, $personnelPermissionId)
    {
        //--
        $pagesKeyForCreateUser = $this->pagesKeyForCreateUser();
        $pages=Page::whereIn('id',$pagesKeyForCreateUser)->get();
        //--
        foreach ($pages as $item)
            $this->createOrUpdateItem($item, $user, $personnelPermissionId);
        //--
        return 1;
    }

    public function addItemToEmployer($page, $user, $personnelPermissionId)
    {
        $timezon = date('Y-m-d H:i:s');
        $item = [
            'personnel_id' => 0,
            'user_id' => $user->id,
            'page_id' => $page->id,
            'page_title' => $page->title,
            'route_name' => $page->route_name,
            'address' => $page->address,
            'base_url' => $page->base_url,
            'is_base_page' => $page->is_base_page,
            'child_id' => 0,
            'parent_id' => 0,
            'permission_id' => $personnelPermissionId,
            'permission_title' => $page->permission_title,
            'active' => 1,
            'created_at' => $timezon,
            'updated_at' => $timezon
        ];
        $hasNotItem = DB::table('items')
            ->where('active', '=', 1)
            ->where('user_id', '=', $user->id)
            ->where('page_id', '=', $page->id)
            ->doesntExist();
//        //--
        if ($hasNotItem)
            $result = Item::create($item);
        else
            $result = Item::where('active', '=', 1)
                ->where('user_id', '=', $user->id)
                ->where('page_id', '=', $page->id)->update($item);
//        //--
        return 1;
    }
}

?>
