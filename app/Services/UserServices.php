<?php
namespace App\Services;

use App\User;
use App\Services\EntriesServices;

class UserServices
{

    public function updateUser($record, $user_id)
    {
        User::where('id', '=', $user_id)->update($record);
        $entriesServices=new EntriesServices();
        $entriesServices->updateUserInformationInEntries(
            array('user_fullname' => $record['fullname']),
            $user_id
        );
        return 1;
    }
    //-------------
}

?>
