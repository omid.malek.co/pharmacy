<?php
namespace App\Services;

use App\Item;
use App\Page;
use App\Entrie;
use Illuminate\Support\Facades\Auth;
use App\User;
use Illuminate\Support\Facades\DB;
use App\Services\Collections\Collection;
use Carbon\Carbon;
use Illuminate\Support\Facades\Lang;

class EntriesServices
{
    private $collection;
    private $paginate = 10;
    private $tableName = 'entries';

    public function __construct()
    {
        $collection = new Collection();
        $this->collection = $collection;
    }

    public function updateUserInformationInEntries($record, $user_id)
    {
        $hasItem = (integer)$hasRecord = DB::table($this->tableName)
            ->where('user_id', $user_id)
            ->exists();
        if ($hasItem)
            return Entrie::where('user_id', '=', $user_id)->update($record);
        return 0;
    }

    public function setPersianDate($entries, $columnKey)
    {
        $entriesCount = count($entries);
        for ($i = 0; $i < $entriesCount; $i++) {
            if ($entries[$i][$columnKey] != "0") {
                $value = $entries[$i][$columnKey];
                $key_value = 'persian_' . $columnKey;
                $entries[$i][$key_value] = $this->collection->getJalaliDate($value);
            } else {
                $key_value = 'persian_' . $columnKey;
                $entries[$i][$key_value] = $this->columnLableText([
                    'keysColumn' => 'departure_date',
                    'keyValue' => $entries[$i][$key_value]
                ]);
            }
        }
        return $entries;
    }


    public function columnLableText($inputs)
    {
        $keysColumn = $inputs['keysColumn'];
        $keyValue = $inputs['keyValue'];
        $content_ = '';
        $textColumn = '';
        switch ($keysColumn) {
            case "distance":
                if ($keyValue == 0) {
                    $textColumn = $this->collection->messageTxt('undefined');
                    $textColumn = $this->collection->labelMessageCreator($textColumn, 'badge badge-info');
                } else {
                    $textColumn = $keyValue;
                }
                break;
            case "departure_date":
                if ($keyValue == 0) {
                    $textColumn = $this->collection->messageTxt('undefined');
                    $textColumn = $this->collection->labelMessageCreator($textColumn, 'badge badge-info');
                } else {
                    $textColumn = $keyValue;
                }
                break;
            case "departure_time":
                if ($keyValue == "0") {
                    $textColumn = $this->collection->messageTxt('undefined');
                    $textColumn = $this->collection->labelMessageCreator($textColumn, 'badge badge-info');
                } else {
                    $textColumn = $keyValue;
                }
                break;
            case "confirm":
                if ($keyValue == 0) {
                    $textColumn = $this->collection->messageTxt('not_confirmed');
                    $textColumn = $this->collection->labelMessageCreator($textColumn, 'badge badge-danger');
                } else {
                    $textColumn = $this->collection->messageTxt('confirmed');
                    $textColumn = $this->collection->labelMessageCreator($textColumn, 'badge badge-success');
                }
                break;
            case "not_confirm":
                if ($keyValue == 0) {
                    $textColumn = $this->collection->messageTxt('not_confirmed');
                    $textColumn = $this->collection->labelMessageCreator($textColumn, 'badge badge-danger');
                } else {
                    $textColumn = $keyValue;
                }
                break;
            case "confirm_by":
                if ($keyValue == 0) {
                    $textColumn = $this->collection->messageTxt('not_confirmed');
                    $textColumn = $this->collection->labelMessageCreator($textColumn, 'badge badge-danger');
                } else {
                    $user = User::where('active', 1)->where('id', $keyValue)->first();
                    $textColumn = $user->fullname;
                    $textColumn = $this->collection->labelMessageCreator($textColumn, 'badge badge-success');
                }
                break;
            default:
                $textColumn = "keysColumn variable not defined";
                break;
        }
        return $textColumn;
    }

    public function setStatusTitle($records)
    {
        $recordCount = count($records);
        for ($i = 0; $i < $recordCount; $i++) {
            $records[$i]->label_departure_time = $this->columnLableText([
                'keysColumn' => 'departure_time',
                'keyValue' => $records[$i]->departure_time
            ]);
            $records[$i]->label_confirm = $this->columnLableText([
                'keysColumn' => 'confirm_by',
                'keyValue' => $records[$i]->confirm_by
            ]);

        }
        return $records;
    }

    /**
     * @param $start_date
     * @param $end_date
     * @return calculate distance between start_dateTime and end_dateTime as second
     */


    public function periodSecondBase($start_date, $end_date)
    {
        $diff = $end_date - $start_date;
        return $diff;
    }

    /****
     * @param $interval
     * @return interval between entry date time and departure date time
     */
    public function calculateInterval($interval)
    {
        $SecondInMinute = 60;
        $secondInHour = 60 * $SecondInMinute;
        $SecondInDay = 24 * $secondInHour;
        $resultHour = round((integer)$interval / $secondInHour);
        $resultMinutes = ((integer)$interval % $secondInHour);
        $resultMinutes = round($resultMinutes / $SecondInMinute);
        return array(
            'hour' => $resultHour,
            'minute' => $resultMinutes
        );
    }

    public function getIntervalTag($interval_time)
    {
        $result = '';
        if ($interval_time == '0') {
            $textColumn = $this->collection->messageTxt('undefined');
            $result = $this->collection->labelMessageCreator($textColumn, 'badge badge-info');
        } else {
            $interval = $this->calculateInterval($interval_time);
            $result = '<div dir="rtl">' . '&nbsp;' . $interval['hour'] . '&nbsp;' . 'ساعت' . '&nbsp;' . $interval['minute'] . '&nbsp;' . 'دقیقه' . '&nbsp;' . '</div>';
        }
        return $result;
    }

    public function SetInterval($entries)
    {
        for ($i = 0; $i < count($entries); $i++)
            $entries[$i]->interval_time = $this->getIntervalTag($entries[$i]->distance);
        return $entries;
    }

    public function getEntries($user)
    {
        $table = 'entries';
        $hasRecord = DB::table($table)
            ->where('active', 1)
            ->where('user_id', '=', $user->id)
            ->exists();
        if ($hasRecord) {
            $result = Entrie::where('active', 1)
                ->where('user_id', '=', $user->id)
                ->paginate($this->paginate);
            //---------- test area
            $result = $this->setPersianDate($result, 'entry_date');
            $result = $this->setPersianDate($result, 'departure_date');
            $result = $this->setStatusTitle($result);
            $result = $this->SetInterval($result);
            //---
            $row = $result->firstItem();
            $operation = 1;
        } else {
            $result = $this->collection->NoDataFound();
            $row = 0;
            $operation = 0;
        }
        return [
            'pages' => $result,
            'row' => $row,
            'operation' => $operation
        ];
    }

    public function getAllusersEntrie()
    {
        $table = 'entries';
        $hasRecord = DB::table($table)
            ->where('active', 1)
            ->exists();
        if ($hasRecord) {
            $result = Entrie::where('active', 1)
                ->with(['user:id,fullname'])
                ->paginate($this->paginate);
            $this->setPersianDate($result, 'entry_date');
            $this->setPersianDate($result, 'departure_date');
            $this->setStatusTitle($result);
            $row = $result->firstItem();
            $operation = 1;
        } else {
            $result = $this->collection->NoDataFound();
            $row = 0;
            $operation = 0;
        }
        return [
            'pages' => $result,
            'row' => $row,
            'operation' => $operation
        ];
    }

    //---
    public function subMinutesToTime($carbonObj, $minute)
    {
        $result = $carbonObj->subMinutes($minute);
        return $result;
    }

    public function addMinutesToTime($carbonObj, $minute)
    {
        $result = $carbonObj->addMinutes($minute);
        return $result;
    }

    /*
     * return array that index=0 has gregorian date and index=1 has time
     *
     * input is date as timestamp
     */
    public function getEntryTime($timeStamp)
    {
        $timeStamp = (string)$timeStamp;
        $timeStamp = trim($timeStamp);
        $arrEntryTime = explode(' ', $timeStamp);
        return $arrEntryTime;
    }

    public function createOrUpdateItem($user, $personnelPermissionId)
    {
        $timeStamp = date('Y-m-d H:i:s');/**************/;
        $dt = Carbon::now();
        $LowRange = (string)$this->subMinutesToTime($dt, 2);
        $HighRange = (string)$this->addMinutesToTime($dt, 2);
        $tenMinutesAgo = (string)$this->subMinutesToTime($dt, 10);
        $entryInformation = $this->getEntryTime($tenMinutesAgo);
        /**************/
        $entries = array(
            'user_id' => $user->id,
            'user_fullname' => $user->fullname,
            'entry_date' => $entryInformation[0],
            'entry_time' => $entryInformation[1],
            'departure_date' => 0,
            'departure_time' => 0,
            'confirm' => 0,
            'confirm_by' => 0,
            'created_at' => $timeStamp,
            'updated_at' => $timeStamp,
            'active' => 1,
        );
        $hasNotItem = DB::table('entries')
            ->where('active', '=', 1)
            ->where('user_id', '=', $user->id)
            ->where('departure_date', '=', 0)
            ->doesntExist();
//        //--
        if ($hasNotItem) {
            $result = Entrie::create($entries);
        } else {
        }
        //--
        return back();
    }

    public function destroyRecord($id)
    {
        $table = 'entries';
        $entries_id = $id;
        $hasRecord = DB::table($table)
            ->where('id', '=', $entries_id)
            ->exists();
        if ($hasRecord)
            Entrie::where('id', '=', $entries_id)->delete();
        return 1;
    }

    /**
     * @param $entries_date as Georgian
     * @param $entries_time
     * @return $timestamp as time
     */

    public function entriesTimeStamp($entries_date, $entries_time)
    {
        $entries_date = trim((string)$entries_date);
        $entries_time = trim((string)$entries_time);
        $date_time = $entries_date . " " . $entries_time;
        $entries_timeStamp = strtotime($date_time);
        return $entries_timeStamp;
    }

    public function leavingTheWorkPlace($variables)
    {
        $user = $variables['user'];
        $permissionId = $variables['permissionId'];
        $timeStamp = date('Y-m-d H:i:s');
        //---

        $dt = Carbon::now();
        $departureInformation = $this->getEntryTime($dt);
        /**************/
        $hasNotItem = DB::table('entries')
            ->where('active', '=', 1)
            ->where('user_id', '=', $user->id)
            ->where('departure_date', '=', '0')
            ->doesntExist();
        //--
        if ($hasNotItem) {
            session()->flash('class', 'alert-danger');
            $msg = 'ابتدا باید ورود خود را ثبت کنید';
            return back()->withErrors($msg);
        } else {
            $entries = array(
                'departure_date' => (string)$departureInformation[0],
                'departure_time' => (string)$departureInformation[1],
                'distance' => '0',
                'updated_at' => (string)$timeStamp,
            );
            $result = Entrie::where('active', '=', 1)
                ->where('user_id', '=', $user->id)
                ->where('departure_date', '=', '0')->first();

            $entrie_timeStamp = $this->entriesTimeStamp($result->entry_date, $result->entry_time);
            $end_time = strtotime((string)$timeStamp);
            $distanceSecond = $this->periodSecondBase($entrie_timeStamp, $end_time);
            $entries['distance'] = (string)$distanceSecond;

            $result = Entrie::where('active', '=', 1)
                ->where('user_id', '=', $user->id)
                ->where('departure_date', '=', '0')->update($entries);
        }
        //--
        return back();
    }

    public function confirmStatus($variables)
    {
        $entrie_id = $variables['entrie_id'];
        $confirm_by = $variables['confirm_by'];
        $confirm = $variables['confirm'];
        $timeStamp = date('Y-m-d H:i:s');
        $entrie_record = [
            'updated_at' => $timeStamp,
            'confirm_by' => $confirm_by,
            'confirm' => $confirm
        ];
        $result = Entrie::where('id', $entrie_id)->where('active', 1)->update($entrie_record);
        return 1;
    }


    public function DateTimeArrayToTimeZone($arr_date_time)
    {
        $result = Carbon::create($arr_date_time[0],
            $arr_date_time[1],
            $arr_date_time[2],
            $arr_date_time[3],
            $arr_date_time[4],
            $arr_date_time[5],
            $arr_date_time[6]
        );
        return $result;
    }


    public function queryCreator($arrInput)
    {
        $query = "";
        $func = new Collection();
        $first_condition = 0;
        if ($func->IsArrayEmpty($arrInput))
            return "1";
        for ($i = 0; $i < count($arrInput); $i++) {
            if ($first_condition == 1) {
                $query .= "+";
                $query .= $arrInput[$i]['column'];
                $query .= "=";
                $query .= $arrInput[$i]['value'];
            } else {
                $first_condition = 1;
                $query .= $arrInput[$i]['column'];
                $query .= "=";
                $query .= $arrInput[$i]['value'];
                continue;
            }
        }
        return $query;
    }


    public function getInputClause($keyword)
    {
        $parameters = $this->collection->based64url_decode($keyword);
        $arr_parameter = explode("+", $parameters);
        $clause = [];
        if ($parameters == "1")
            return "";
        foreach ($arr_parameter as $item) {
            $arr = explode("=", $item);
            $arr[0] = trim($arr[0]);
            $arr[1] = trim($arr[1]);
            $clause[$arr[0]] = $arr[1];

//            if ($arr[0] == "name")
//                array_push($clause, [
//                    "name" => $arr[1]
//                ]);
//            else if ($arr[0] == "bottom_date")
//                array_push($clause, [
//                    "bottom_date" => $arr[1]
//                ]);
//            else if($arr[0] == "top_date")
//                array_push($clause,
//                    "top_date" => $arr[1]
//                );
            //--
//            if (isset($variables['bottomRange'])
//                && !$this->collection->IsArrayEmpty($variables['bottomRange'])
//            ) {
//                $bottomRange = trim($variables['bottomRange']);
//                $GregoriantimeStamp = $this->setGregoriantimeStamp($bottomRange);
//                array_push($arrParameter, [
//                    'column' => 'bottom_date',
//                    'value' => $GregoriantimeStamp
//                ]);
//            }
//            //--
//            if (isset($variables['topRange'])
//                && !$this->collection->IsArrayEmpty($variables['topRange'])
//            ) {
//                $topRange = trim($variables['topRange']);
//                array_push($arrParameter, [
//                    'column' => 'top_date',
//                    'value' => $GregoriantimeStamp
//                ]);
//            }
//            array_push($clause,[
//                'column'=>$arr[0],
//                'value'=>$arr[1],
//            ]);
        }
        return $clause;
        $arrBottomRange = $this->getArrTimestampFromStrTimeStamp($arr_parameter[0]);
    }

    public function getArrTimestampFromStrTimeStamp($dataString)
    {
        $arrDateAndTime = explode(" ", $dataString);
        $parameters = [];
        $date = explode("-", $arrDateAndTime[0]);
        $time = explode(":", $arrDateAndTime[1]);
        if (!$this->collection->IsArrayEmpty($date))
            foreach ($date as $item)
                array_push($parameters, $item);
        //--
        if (!$this->collection->IsArrayEmpty($time))
            foreach ($time as $item)
                array_push($parameters, $item);
        //--
        array_push($parameters, 'Asia/Tehran');
        return $parameters;
    }

    public function searchWithName($variables)
    {
        $func = new Collection();
        $active = 1;
        $button_date = '';
        $top_date = '';
        $clause = [];

        //--
        if (isset($variables['active']) && !$func->IsArrayEmpty($variables['active']))
            $active = $variables['active'];
        //--
        $entries = Entrie::where('active', '=', $active);
        //--
        if (isset($variables['name']) && !$func->IsArrayEmpty($variables['name'])) {
            $name = '%' . $variables['name'] . '%';
            $entries->where('user_fullname', 'LIKE', $name);
        }
        //--
        if (isset($variables['bottom_date']) && !$func->IsArrayEmpty($variables['bottom_date'])) {
            $bottom_date = trim($variables['bottom_date']);
            $arr_bottom_date = $this->getEntryTime($bottom_date);
            $bottom_date = $arr_bottom_date[0];
            $entries->where('entry_date', '>=', $bottom_date);
        }
        //--
        if (isset($variables['top_date']) && !$func->IsArrayEmpty($variables['top_date'])) {
            $top_date = trim($variables['top_date']);
            $arr_top_date = $this->getEntryTime($top_date);
            $top_date = $arr_top_date[0];
            $entries->where('entry_date', '<', $top_date);
        }
        //--
        //--
        return [
            'checkouts' => $entries
        ];


    }
}

?>
