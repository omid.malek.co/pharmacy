<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Factor extends Model
{

    /*
     if active=1 --> record is active and visible else if active =0 record is deactive and invisible



     payment_status=0 --> This product has not been transferred to the payment gateway
     * */

    protected $fillable=[
        'product_id','sent_number','customer_id',
        'payment_id','created_at','updated_at','active','type_id'

    ];

    public function customer(){
        return $this->belongsTo('App\Customer');
    }

    public function product(){
        return $this->belongsTo('App\Product');
    }

    public function payment(){
        return $this->belongsTo(Payment::class);
    }

    public function type(){
        return $this->hasOne(Type::class);
    }
}
