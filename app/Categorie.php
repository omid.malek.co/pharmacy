<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Categorie extends Model
{

    protected $fillable = [
        'title', 'active','created_at','updated_at'
    ];

    public function products(){
        return $this->hasMany('App\Product');
    }

    public function categories(){
        return $this->hasMany('App\Categorie');
    }
}
