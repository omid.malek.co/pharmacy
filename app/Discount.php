<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Discount extends Model
{
    protected $fillable = [
        'text', 'percent', 'start_date',
        'expire_date', 'number_validation', 'active'
    ];

    public function payments()
    {
        return $this->hasMany(Payment::class);
    }
}
