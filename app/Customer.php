<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $fillable=[
        'fullname','email','password','address','postal_code',
        'phone','active','token'
    ];

    public function payments(){
        return $this->hasMany(Payment::class);
    }
    public function wallets(){
        return $this->hasMany(Wallet::class);
    }
}
