<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $fillable = [
        'title', 'body', 'published', 'created_by', 'updated_by'
    ];

    public function author(){
        return $this->belongsTo(User::class, 'created_by');
    }
    
}

