<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    /*

     payment_status=1 فاکتور ثبت شده ولی پرداخت نشده
     payment_status=2 فاکتور ثبت شده و پرداخت با موفقیت انجام نشده یعنی عملیات درگاه پرداخت موفق نبوده
     payment_status=3 فاکتور ثبت شده و پرداخت شده
     payment_status=4 فاکتور ثبت شده و پرداخت شده و ارسال شده

     discount_id =0 کد تخفیف اعمال نشده
     discount_id !=0 کد تخفیف اعمال شده
     */
    protected $fillable=[
        'payment_amount','total_sum','payment_message','payment_message_code',
        'customer_id','discount_id','discount_value',
        'sent_address','sent_phone','receiver_fullname',
        'postal_code_sent','Authority','reference_code',
        'payment_status','active','RefID',
        'created_at','updated_at','condition_id','type_id','gift_id','gift_amount'
    ];

    public function factors(){
        return $this->hasMany(Factor::class);
    }

    public function discount(){
        return $this->belongsTo(Discount::class);
    }

    public function customer(){
        return $this->belongsTo(Customer::class);
    }
    public function type(){
        return $this->hasOne(Type::class);
    }
    public function condition(){
        return $this->belongsTo(Condition::class);
    }
}
