<?php

namespace App\Exports;

use App\Http\Controllers\applicantsController;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Contracts\View\View;
use Illuminate\Contracts\Support\Responsable;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithCustomStartCell;
use Maatwebsite\Excel\Events\BeforeExport;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Facades\Excel;
use App\Drug;


class ApplicantsExport implements
    ShouldAutoSize,
    FromView,
    WithMapping,
    WithEvents,
    WithCustomStartCell
{
    use Exportable;
    private $filname='applicants.xlsx';
    /**
    * @return \Illuminate\Support\Collection
    */
//    public function collection()
//    {
//        return Applicant::where('active',1)->with(['state'])->get();
//    }

    /**
     * Create an HTTP response that represents the object.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function toResponse($request)
    {
        // TODO: Implement toResponse() method.
    }

    /**
     * @return array
     */
    public function array(): array
    {
        // TODO: Implement array() method.
    }

    /**
     * @return View
     */
    public function view(): View
    {
        // TODO: Implement view() method.

        $drugs=Drug::where('active',1)->with('scale')->orderBy('id','DESC')->get();
        $row=1;
        return view('exports.drugs',[
            'row'=>1,
            'drugs'=>$drugs
        ]);
    }

    /**
     * @param mixed $row
     *
     * @return array
     */
    public function map($row): array
    {
        // TODO: Implement map() method.
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        // TODO: Implement collection() method.
    }

    /**
     * @return string
     */
    public function startCell(): string
    {
        // TODO: Implement startCell() method.
//        return 'R1';
    }

    /**
     * @return array
     */
    public function registerEvents(): array
    {
        // TODO: Implement registerEvents() method.
        return [
            AfterSheet::class    => function(AfterSheet $event) {
                $event->sheet->getDelegate()->setRightToLeft(true);
            },
        ];
    }
}
